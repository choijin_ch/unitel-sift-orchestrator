package com.knowesis.sift.orchestrator.utils;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author SO Development Team
 *
 */

public class ByteUtil {
	private static Logger logger = LoggerFactory.getLogger(ByteUtil.class);

	/**
	 * Allow derivatives.
	 */
	protected ByteUtil() {
	}

	/**
	 * Converts a byte array to short value. Equivalent to
	 * byteArrayToShort(paRawBytes, 0, pbBigEndian);
	 * 
	 * @param paRawBytes
	 *            the byte array
	 * @param pbBigEndian
	 *            true if the bytes are in Big-endian order; false otherwise
	 * 
	 * @return short representation of the bytes
	 */
	public static short byteArrayToShort(byte[] paRawBytes, boolean pbBigEndian) {
		return byteArrayToShort(paRawBytes, 0, pbBigEndian);
	}

	/**
	 * Converts a portion of a byte array with given offset to short value.
	 * 
	 * @param paRawBytes
	 *            the byte array
	 * @param piOffset
	 *            offset in the original array to start reading bytes from
	 * @param pbBigEndian
	 *            true if the bytes are in Big-endian order; false otherwise
	 * 
	 * @return short representation of the bytes
	 */
	public static short byteArrayToShort(byte[] paRawBytes, int piOffset,
			boolean pbBigEndian) {
		int iRetVal = -1;


		// we ingnore excessing bytes?
		if (paRawBytes.length < piOffset + 2)
			return -1;

		int iLow;
		int iHigh;

		if (pbBigEndian) {
			iLow = paRawBytes[piOffset + 1];
			iHigh = paRawBytes[piOffset + 0];
		} else {
			iLow = paRawBytes[piOffset + 0];
			iHigh = paRawBytes[piOffset + 1];
		}

		// Merge high-order and low-order byte to form a 16-bit double value.
		iRetVal = (iHigh << 8) | (0xFF & iLow);

		return (short) iRetVal;
	}

	/**
	 * Converts a byte array to int value. Equivalent to
	 * intArrayToShort(paRawBytes, 0, pbBigEndian);
	 * 
	 * @param paRawBytes
	 *            the byte array
	 * @param pbBigEndian
	 *            true if the bytes are in Big-endian order; false otherwise
	 * 
	 * @return int representation of the bytes
	 */
	public static int byteArrayToInt(byte[] paRawBytes, boolean pbBigEndian) {
		return byteArrayToInt(paRawBytes, 0, pbBigEndian);
	}

	/**
	 * Converts a portion of a byte array with given offset to int value.
	 * 
	 * @param paRawBytes
	 *            the byte array
	 * @param piOffset
	 *            offset in the original array to start reading bytes from
	 * @param pbBigEndian
	 *            true if the bytes are in Big-endian order; false otherwise
	 * 
	 * @return int representation of the bytes
	 */
	public static int byteArrayToInt(byte[] paRawBytes, int piOffset,
			boolean pbBigEndian) {
		int iRetVal = -1;

		if (paRawBytes.length < piOffset + 4)
			return iRetVal;

		int iLowest;
		int iLow;
		int iMid;
		int iHigh;

		if (pbBigEndian) {
			iLowest = paRawBytes[piOffset + 3];
			iLow = paRawBytes[piOffset + 2];
			iMid = paRawBytes[piOffset + 1];
			iHigh = paRawBytes[piOffset + 0];
		} else {
			iLowest = paRawBytes[piOffset + 0];
			iLow = paRawBytes[piOffset + 1];
			iMid = paRawBytes[piOffset + 2];
			iHigh = paRawBytes[piOffset + 3];
		}

		// Merge four bytes to form a 32-bit int value.
		iRetVal = ((iHigh & 0xFF) << 24) | ((iMid & 0xFF) << 16) | ((iLow & 0xFF) << 8) | (0xFF & iLowest);

		return iRetVal;
	}

	/**
	 * Converts an int value to a byte array.
	 * 
	 * @param piValueToConvert
	 *            the original integer
	 * @param pbBigEndian
	 *            true if the bytes are in Big-endian order; false otherwise
	 * 
	 * @return byte[] representation of the int
	 */
	public static byte[] intToByteArray(int piValueToConvert,
			boolean pbBigEndian) {
		byte[] aRetVal = new byte[4];

		byte iLowest;
		byte iLow;
		byte iMid;
		byte iHigh;

		iLowest = (byte) (piValueToConvert & 0xFF);
		iLow = (byte) ((piValueToConvert >> 8) & 0xFF);
		iMid = (byte) ((piValueToConvert >> 16) & 0xFF);
		iHigh = (byte) ((piValueToConvert >> 24) & 0xFF);

		if (pbBigEndian) {
			aRetVal[3] = iLowest;
			aRetVal[2] = iLow;
			aRetVal[1] = iMid;
			aRetVal[0] = iHigh;
		} else {
			aRetVal[0] = iLowest;
			aRetVal[1] = iLow;
			aRetVal[2] = iMid;
			aRetVal[3] = iHigh;
		}

		return aRetVal;
	}

	/**
	 * Converts a byte array to String value. Cleans up non-word characters
	 * along the way.
	 * 
	 * Equivalent to byteArrayToString(paRawBytes, piOffset, paRawBytes.length -
	 * piOffset);
	 * 
	 * @param paRawBytes
	 *            the byte array, non-UNICODE
	 * @param piOffset
	 *            offset in the original array to start reading bytes from
	 * 
	 * @return UNICODE String representation of the bytes
	 */
	public static String byteArrayToString(byte[] paRawBytes, int piOffset) {
		return byteArrayToString(paRawBytes, piOffset, paRawBytes.length
				- piOffset);
	}

	/**
	 * Converts a byte array to String value. Cleans up non-word characters
	 * along the way.
	 * 
	 * Equivalent to byteArrayToString(paRawBytes, 0, paRawBytes.length);
	 * 
	 * @param paRawBytes
	 *            the byte array, non-UNICODE
	 * 
	 * @return UNICODE String representation of the bytes
	 */
	public static String byteArrayToString(byte[] paRawBytes) {
		return byteArrayToString(paRawBytes, 0, paRawBytes.length);
	}

	/**
	 * Converts a portion of a byte array to String value. Cleans up non-word
	 * characters along the way.
	 * 
	 * @param paRawBytes
	 *            the byte array, non-UNICODE
	 * @param piOffset
	 *            offset in the original array to start reading bytes from
	 * @param piLength
	 *            how many bytes of the array paramter to interpret as String
	 * 
	 * @return UNICODE String representation of the bytes with trailing garbage
	 *         stripped; "" if array length is less than piOffset + piLength; ""
	 *         if the generatied string begins with garbage
	 */
	public static String byteArrayToString(byte[] paRawBytes, int piOffset,
			int piLength) {
		if (piLength < 0 || paRawBytes.length < piOffset + piLength)
			return "";

		String oBeautifulString = new String(paRawBytes, piOffset, piLength);
		int i = 0;

		if (oBeautifulString.matches("^\\W") == true)
			oBeautifulString = "";
		else {
			for (i = piOffset; i < piOffset + piLength; i++) {
				if (paRawBytes[i] < 32 || paRawBytes[i] > 128)
					break;
			}

			oBeautifulString = oBeautifulString.substring(0, i - piOffset);
		}

		return oBeautifulString;
	}

	/**
	 * Converts a String value to a byte array in US-ASCII charset.
	 * 
	 * Equivalent to stringToByteArray(pstrStringToConvert, "US-ASCII");
	 * 
	 * @param pstrStringToConvert
	 *            the original string
	 * 
	 * @return null-terminated byte[] representation of the String
	 */
	public static byte[] stringToByteArray(String pstrStringToConvert) {
		return stringToByteArray(pstrStringToConvert, "US-ASCII");
	}

	/**
	 * Attempts to convert a String value to a byte array in specified charset.
	 * If the charset is invalid, returns plain byte-representation of the host
	 * environment.
	 * 
	 * @param pstrStringToConvert
	 *            the original string
	 * @param pstrCharSet
	 *            characted set to assume for the original string
	 * 
	 * @return null-terminated byte[] representation of the String
	 */
	public static byte[] stringToByteArray(String pstrStringToConvert, String pstrCharSet) {
		byte[] aRecordData = null;

		try {
			aRecordData = pstrStringToConvert.getBytes(pstrCharSet);
		} catch (UnsupportedEncodingException e) {
			logger.warn(e.getMessage());
			aRecordData = pstrStringToConvert.getBytes();
		}

		return aRecordData;
	}

	/**
	 * Converts a String value to a byte array in US-ASCII charset.
	 *
	 * Equivalent to stringToByteArray(pstrStringToConvert, "US-ASCII");
	 *
	 * @param pstrStringToConvert
	 *            the original string
	 * @param piLength
	 *            the length
	 *
	 * @return null-terminated byte[] representation of the String
	 */
	public static byte[] stringToByteArray(String pstrStringToConvert, int piLength) {
		byte[] aRecordData = null;

		try {
			aRecordData = pstrStringToConvert.getBytes("US-ASCII");
		} catch (UnsupportedEncodingException e) {
			logger.warn(e.getMessage());
			aRecordData = pstrStringToConvert.getBytes();
		}
		
		byte[] retBytes = new byte[piLength];
		Arrays.fill(retBytes, (byte)0);
		System.arraycopy(aRecordData, 0, retBytes, 0, aRecordData.length>piLength?piLength:aRecordData.length);

		return retBytes;
	}

	/**
	 * Converts a String value to a byte array in US-ASCII charset.
	 *
	 * Equivalent to stringToByteArray(pstrStringToConvert, "US-ASCII");
	 *
	 * @param pstrStringToConvert
	 *            the original string
	 * @param piLength
	 *            the length
	 * @param pstrCharSet
	 *            the pstrCharSet
	 *
	 * @return null-terminated byte[] representation of the String
	 */

	public static byte[] stringToByteArray(String pstrStringToConvert, int piLength, String pstrCharSet) {
		byte[] aRecordData = null;

		try {
			aRecordData = pstrStringToConvert.getBytes(pstrCharSet);
		} catch (UnsupportedEncodingException e) {
			logger.warn(e.getMessage());
			aRecordData = pstrStringToConvert.getBytes();
		}
		
		byte[] retBytes = new byte[piLength];
		Arrays.fill(retBytes, (byte)0);
		System.arraycopy(aRecordData, 0, retBytes, 0, piLength);

		return retBytes;
	}


	/**
	 * Converts a Byte Array to String value in US-ASCII charset.
	 *
	 *
	 *
	 * @param bytes
	 *            the Array as bytes
	 *
	 * @return String representation of the String
	 */

	public static String byteArrayToHexString(byte[] bytes) {
		String strRet = "";
		for(int i=0; i < bytes.length; i++) {
			if (i % 16 == 0) {
				strRet += "\n";
			}
			strRet += String.format(" %02X", bytes[i]);
			
		}
		return strRet;
	}

	/**
	 * Converts a String value to Byte Array  US-ASCII charset.
	 *
	 *
	 *
	 * @param str
	 *            the String
	 *
	 * @return null-terminated byte[] representation of the String
	 *
	 * @throws Exception
	 */

	public static byte[] HexStringToByteArray(String str) throws Exception {
		//System.out.println(str);
		if (str.length() % 2 != 0) {
			throw new Exception();
		}
		ByteArrayOutputStream tmp = new ByteArrayOutputStream();
		for (int i = 0; i < str.length(); i+=2) {
			//System.out.print(":" + str.substring(i, i+2) + "-" + Byte.parseByte(str.substring(i, i+2), 16));
			tmp.write(Byte.parseByte(str.substring(i, i+2), 16));
		}
		return tmp.toByteArray();
	}
}