/*
 * Copyright (c) 2015, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.exceptions;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author SO Development Team
 *
 */
public class JmsExceptionHandler implements ExceptionListener {

	
	private Logger log = LoggerFactory.getLogger(JmsExceptionHandler.class);
	/**
	 *  @param e JMSException
	 * 
	 */

	public void onException(JMSException e) {

		log.info("JMS Reconnect failed.  Shutting down the connection ...");

		/**
		 * Set this flag to false so that the run() method will exit.
		 */

		log.error("JMS Reconnect failed.  Shutting down the connection ...", e);
	}

}
