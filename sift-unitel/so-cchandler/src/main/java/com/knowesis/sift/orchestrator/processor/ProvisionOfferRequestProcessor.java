
/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.time.Instant;
import java.util.Calendar;
import java.util.TimeZone;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * To Process CC Request
 * 
 * @author SO Development Team
 */

@Component("ProvisionOfferRequestProcessor")
public class ProvisionOfferRequestProcessor implements Processor {

	private final static Logger LOG = LoggerFactory.getLogger(ProvisionOfferRequestProcessor.class);

	@Autowired
	ObjectMapper jsonObjectMapper;

	@Autowired
	RedisConnectionPool redisConnectionPool;

	@Value("${so.notification.template}")
	private String soMessageTemplate;

	@Value("${cc.channel.name}")
	private String channelName;

	@Value("${so.triggerType.optinNotify}")
	private String optinNotify;

	@Value("${so.actionType.notify}")
	private String notify;

	@Value("${cc.notify.optin.invalidKeyword}")
	private String optinInvalidMessage;

	@Value("${so.actionStatus.success}")
	private String success;

	@Value("${so.actionStatus.failure}")
	private String failure;

	@Value("${cc.optin.actionType}")
	private String actionTypeOptin;

	@Value("${so.triggerType.optinResponse}")
	private String optinResponse;

	@Value("${so.monitoring.ttl.additionalBufferValue}")
	private int ttlBuffer;

	@Value("${so.optin.status.key}")
	private String optinStatus;

	@Value("${cc.notify.optin.ssp.inprogress}")
	private String alreadyOptinSspInProgress;

	@Value("${cc.notify.optin.monitoring.expired}")
	private String notInMonitoringPeriodMessage;

	@Value("${so.actionType.optin}")
	private String optin;

	@Value("${cc.notify.optin.offer.alreadyProvisioned}")
	private String optinOfferTakenMessage;

	@Value("${cc.notify.optin.cc.requestErrorMsg}")
	private String errorRequestMessage;

	@Override
	public void process(Exchange exchange) throws Exception {
		String request = exchange.getIn().getBody(String.class);
		LOG.debug("Request from customer care {}", request);
		JsonNode jsonNode = jsonObjectMapper.readTree(request);
		String msisdn = getNodeValueFromJsonString(jsonNode, "msisdn");
		String offerId = getNodeValueFromJsonString(jsonNode, "offerId");
		String programId = getNodeValueFromJsonString(jsonNode, "programId");
		String flowId = getNodeValueFromJsonString(jsonNode, "flowId");
		String userOptedKeyword = getNodeValueFromJsonString(jsonNode, "optinKeyword");
		String shortCode = getNodeValueFromJsonString(jsonNode, "optinShortCode");
		String transactionDate = getNodeValueFromJsonString(jsonNode, "transactionDate");
		String requesterId = getNodeValueFromJsonString(jsonNode, "requesterId");
		String requesterChannel = getNodeValueFromJsonString(jsonNode, "requesterChannel");
		if (checkForProcessingCondition(msisdn, offerId, programId, flowId, userOptedKeyword, transactionDate,
				requesterId, requesterChannel, shortCode)) {
			LOG.info("User Opted Keyword: {}", userOptedKeyword);
			String optinShortCode = jsonNode.get("optinShortCode").asText();
			LOG.info("User Opted ShortCode: {}", optinShortCode);
			String cacheKey = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn, userOptedKeyword);
			String soMessageAsJSON = null;
			if (StringUtils.isNotBlank(cacheKey)) {
				soMessageAsJSON = redisConnectionPool.getObjectFromCache(cacheKey);
			}
			routeForProcessingWithRespectToCacheValue(exchange, soMessageAsJSON, msisdn, cacheKey, optinShortCode, jsonNode, userOptedKeyword);
		} else {
			String erorrResponse = setErrorResponse(msisdn, errorRequestMessage);
			exchange.getIn().setBody(erorrResponse);
			exchange.getIn().setHeader("inValidRequest", true);
		}

	}

	/**
	 * routeForProcessingWithRespectToCacheValue
	 * 
	 * @param exchange
	 *            camel exchange
	 * @param soMessageAsJSON
	 *            soMessageAsJSON
	 * @param msisdn
	 *            msisdn
	 * @param cacheKey
	 *            cacheKey
	 * @param optinShortCode
	 *            optinShortCode
	 * @param jsonNode
	 *            jsonNode
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @throws Exception
	 *             Exception
	 */
	private void routeForProcessingWithRespectToCacheValue(Exchange exchange, String soMessageAsJSON, String msisdn,
			String cacheKey, String optinShortCode, JsonNode jsonNode, String userOptedKeyword) throws Exception {
		LOG.debug("SOMessage Fetched from Cache Key: {}, value: {}", cacheKey, soMessageAsJSON);
		if (soMessageAsJSON == null) {
			whenOptinNotFound(exchange, msisdn, userOptedKeyword);
		} else if (soMessageAsJSON.equals("OFFER-TAKEN")) {
			whenOptinOfferTaken(exchange, msisdn);
		} else {
			whenOptinFound(exchange, soMessageAsJSON, msisdn, cacheKey, optinShortCode, jsonNode, userOptedKeyword);
		}
	}

	/**
	 * 
	 * checkForProcessingCondition
	 * 
	 * @param msisdn
	 *            msisdn
	 * @param offerId
	 *            offerId
	 * @param programId
	 *            programId
	 * @param flowId
	 *            flowId
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @param transactionDate
	 *            transactionDate
	 * @param requesterId
	 *            requesterId
	 * @param requesterChannel
	 *            requesterChannel
	 * @param shortCode
	 *            shortCode
	 * @return true/false
	 */
	private boolean checkForProcessingCondition(String msisdn, String offerId, String programId, String flowId,
			String userOptedKeyword, String transactionDate, String requesterId, String requesterChannel,
			String shortCode) {
		String[] jsonNodeList = {msisdn, offerId, programId, flowId, userOptedKeyword, transactionDate,
				requesterId, requesterChannel, shortCode};
		for (String jsonNode : jsonNodeList) {
			if (StringUtils.isBlank(jsonNode)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * getNodeValueFromJsonString after null check
	 * 
	 * @param jsonNode
	 *            jsonNode
	 * @param inputNode
	 *            inputNode
	 * @return nodeValue/null
	 */
	private String getNodeValueFromJsonString(JsonNode jsonNode, String inputNode) {
		String returnValue = jsonNode.get(inputNode) == null ? "" : jsonNode.get(inputNode).asText();
		return returnValue;
	}

	/**
	 * Executes when optin for already taken offer
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @throws Exception
	 *             Exception
	 */
	private void whenOptinOfferTaken(Exchange exchange, String msisdn)
			throws Exception {
		String messageToSend = optinOfferTakenMessage;
		if (StringUtils.isNotBlank(messageToSend)) {
			String erorrResponse = setErrorResponse(msisdn, messageToSend);
			LOG.debug("Offer is already provisioned {}",erorrResponse );
			exchange.getIn().setBody(erorrResponse);
		}	
		exchange.getIn().setHeader("isAlreadyProvisioned", true);
		exchange.getIn().setHeader("isInvalidKeyword", false);
		exchange.getIn().setHeader("isMOSuccess", false);

	}

	/**
	 * 
	 * @param jsonNode
	 *            jsonNode
	 * @return isSuccessProvitionResponse
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	private String isSuccessProvition(JsonNode jsonNode) throws JsonProcessingException {
		ObjectNode node = jsonObjectMapper.createObjectNode();
		node.put("responseCode", "200");
		node.put("responseMessage", "Success");
		node.put("msisdn", jsonNode.get("msisdn").textValue());
		node.put("transactionDate", jsonNode.get("transactionDate").textValue());
		node.put("requesterId", jsonNode.get("requesterId").textValue());
		node.put("offerId", jsonNode.get("offerId").textValue());
		node.put("flowId", jsonNode.get("flowId").textValue());
		node.put("programId", jsonNode.get("programId").textValue());
		LOG.debug("Sucess Response to customer care {}", jsonObjectMapper.writeValueAsString(node));
		return jsonObjectMapper.writeValueAsString(node);
	}

	/**
	 * 
	 * Executes when optin not found or invalid keyword
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @throws Exception
	 *             Exception to be thrown
	 */
	private void whenOptinNotFound(Exchange exchange, String msisdn, String userOptedKeyword) throws Exception {
		String messageToSend = optinInvalidMessage;
		LOG.info("No redis entry for msidn :{}: with keyword :{} ", msisdn, userOptedKeyword);
		if (StringUtils.isNotBlank(messageToSend)) {
			String erorrResponse = setErrorResponse(msisdn, messageToSend);
			LOG.debug("Invalid CC received with Invalid Keyword {}", erorrResponse);
			exchange.getIn().setBody(erorrResponse);
		} else {
			exchange.getIn().setBody(null);
		}
		exchange.getIn().setHeader("isInvalidKeyword", true);
		exchange.getIn().setHeader("isMOSuccess", false);
		// exchange.getIn().setHeader("isOptinNotFound", true);
	}

	/**
	 * When optin message is not expired
	 * 
	 * @param exchange
	 *            camel exchange
	 * @param soMessageAsJSON
	 *            SOMessage in JSON
	 * @param msisdn
	 *            Mobile Number
	 * @param cacheKeyToRemove
	 *            key to remove
	 * @param optinShortCode
	 *            short code received as part of MO
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @throws Exception
	 *             if anySuccess
	 * @param jsonNode
	 *            jsonNode
	 */
	private void whenOptinFound(Exchange exchange, String soMessageAsJSON, String msisdn, String cacheKeyToRemove,
			String optinShortCode, JsonNode jsonNode, String userOptedKeyword) throws Exception {
		userOptedKeyword = userOptedKeyword != null ? userOptedKeyword.trim().toUpperCase() : null;
		SOMessage soMessage = RawMessageUtils.processRawMessage(soMessageAsJSON, jsonObjectMapper);
		LOG.info("Redis Entry found for msisdn: {} ,userResponse {}", msisdn, userOptedKeyword);
		if (validateUserResponse(soMessage, userOptedKeyword, optinShortCode, "CC")) {
			onReceivingValidOptinInMO(exchange, msisdn, soMessageAsJSON, cacheKeyToRemove, soMessage, userOptedKeyword,
					optinShortCode, jsonNode);
		} else {
			onReceivingInValidOptinInMO(exchange, msisdn, soMessageAsJSON, userOptedKeyword, soMessage);
		}
	}

	/**
	 * 
	 * Validate whether optin in success or not
	 * 
	 * @param soMessage
	 *            message from cache
	 * @param optinKeyword
	 *            optinKeyword received from SMS
	 * @param optinShortCode
	 *            destination address of SMS
	 * @param optinChannel
	 *            SMS
	 * @return true/false
	 */
	private static boolean validateUserResponse(SOMessage soMessage, String optinKeyword, String optinShortCode,
			String optinChannel) {
		LOG.debug("Cache Message: {} optinKeyword: {}, optinShortCode: {}, optinChannel: {}", soMessage, optinKeyword,
				optinShortCode, optinChannel);
		String optinKeywords = SOMessageUtils.getOptinKeyword(soMessage);
		String[] keywords = optinKeywords.split(",");
		for (int i = 0; i < keywords.length; i++) {
			if (checkUserResponseForCCOptin(soMessage, optinChannel, optinShortCode)
					&& keywords[i].toString().equalsIgnoreCase(optinKeyword)) {
				LOG.debug("CC OPTIN Success");
				return true;
			}
		}
		LOG.debug("CC OPTIN Failed");
		return false;
	}

	/**
	 * checkUserResponseForCCOptin
	 * 
	 * @param soMessage
	 *            soMessage
	 * @param optinChannel
	 *            optinChannel
	 * @param optinShortCode
	 *            optinShortCode
	 * @return true/false
	 */
	private static boolean checkUserResponseForCCOptin(SOMessage soMessage, String optinChannel,
			String optinShortCode) {
		if ((SOMessageUtils.getOptinChannel(soMessage).equals(optinChannel)
				|| (SOMessageUtils.getOptinChannel(soMessage).equals("SMS")))
				&& SOMessageUtils.getOptinShortCode(soMessage).equals(optinShortCode))
			return true;
		return false;
	}

	/**
	 * 
	 * Checks whether the offer is being processed
	 * 
	 * @param exchange
	 *            Camel Exchange
	 * @param msisdn
	 *            Mobile
	 * @param soMessageAsJSON
	 *            soMessageAsJSON
	 * @param cacheKeyToRemove
	 *            key to remove
	 * @param soMessage
	 *            SONotificationMessage
	 * @param userOptedKeyword
	 *            SMS text that user send for OPT-IN
	 * @param optinShortCode
	 *            short code received as part of MO
	 * @throws IOException
	 *             ioException
	 * @throws JsonProcessingException
	 *             parse exception
	 * @throws Exception
	 *             Exception
	 * @param jsonNode
	 *            jsonNode
	 */
	private void onReceivingValidOptinInMO(Exchange exchange, String msisdn, String soMessageAsJSON,
			String cacheKeyToRemove, SOMessage soMessage, String userOptedKeyword, String optinShortCode,
			JsonNode jsonNode) throws JsonProcessingException, IOException, Exception {
		if (isMOInProcessing(soMessage)) {
			SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
					soMessageTemplate, msisdn, soMessageAsJSON, alreadyOptinSspInProgress, channelName,
					jsonObjectMapper);
			optinNotificationMessage.setTriggerType(optinNotify);
			optinNotificationMessage.setActionType(notify);
			optinNotificationMessage.setActionStatus(success);
			optinNotificationMessage.getMetaInfo().setChannelName(channelName);
			optinNotificationMessage.getMessage().setRequesterChannel(channelName);
			String finalMessageAsJson = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
			createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure);
			String provisionOfferErorrResponse = setErrorResponse(msisdn, alreadyOptinSspInProgress);
			exchange.getIn().setHeader("provisionOfferInProcessing", provisionOfferErorrResponse);
			exchange.getIn().setHeader("isMOSuccess", false);
			exchange.getIn().setHeader("isInvalidKeyword", false);
			exchange.getIn().setBody(createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure));
		} else {
			checkValidMonitoringPeriod(exchange, msisdn, soMessageAsJSON, cacheKeyToRemove, soMessage, userOptedKeyword,
					optinShortCode, jsonNode);
		}
	}

	/**
	 * Checks whether the offer is being processed
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return true/false
	 */
	public boolean isMOInProcessing(SOMessage soMessage) {
		String optinStat = SOMessageUtils.getHeaderValue(soMessage, "optinStatus");// set optin status
		if (optinStat != null && optinStat.equals("PROCESSING")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks whether the optin is in valid monitoring period
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @param soMessageAsJSON
	 *            soMessageAsJSON
	 * @param cacheKeyToRemove
	 *            cacheKeyToRemove
	 * @param soMessage
	 *            soMessage
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @param optinShortCode
	 *            optinShortCode
	 * @throws JsonProcessingException
	 *             Exception
	 * @throws IOException
	 *             Exception
	 * @throws Exception
	 *             Exception
	 * @param jsonNode
	 *            jsonNode
	 */
	public void checkValidMonitoringPeriod(Exchange exchange, String msisdn, String soMessageAsJSON,
			String cacheKeyToRemove, SOMessage soMessage, String userOptedKeyword, String optinShortCode,
			JsonNode jsonNode) throws JsonProcessingException, IOException, Exception {
		if (isMOInValidTime(soMessage)) {
			onReceivingValidMonitoringPeriodInMO(exchange, msisdn, cacheKeyToRemove, soMessage, userOptedKeyword,
					optinShortCode, jsonNode);
		} else { // Not in Monitoring period
			SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
					soMessageTemplate, msisdn, soMessageAsJSON, notInMonitoringPeriodMessage, channelName,
					jsonObjectMapper);
			optinNotificationMessage.setTriggerType(optinNotify);
			optinNotificationMessage.setActionType(notify);
			optinNotificationMessage.setActionStatus(success);
			optinNotificationMessage.getMetaInfo().setChannelName(channelName);
			optinNotificationMessage.getMessage().setRequesterChannel(channelName);
			String provisionOfferErorrResponse = setErrorResponse(msisdn, notInMonitoringPeriodMessage);
			exchange.getIn().setHeader("notInMonitoringPeriod", provisionOfferErorrResponse);
			String finalMessageAsJson = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
			exchange.getIn().setHeader("messageSendToDbHandler", createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure));
			exchange.getIn().setHeader("isMOSuccess", false);
			exchange.getIn().setHeader("isInvalidKeyword", false);
			exchange.getIn().setHeader("offerExpired", true);
			exchange.getIn().setBody(finalMessageAsJson);
		}
	}

	/**
	 * Handles optin valid keyword case
	 * 
	 * @param exchange
	 *            Camel Exchange
	 * @param msisdn
	 *            Mobile
	 * @param cacheKeyToRemove
	 *            key to remove
	 * @param soMessage
	 *            SONotificationMessage
	 * @param userOptedKeyword
	 *            SMS text that user send for OPT-IN
	 * @param optinShortCode
	 *            short code received as part of MO
	 * @throws IOException
	 *             ioException
	 * @throws JsonProcessingException
	 *             parse exception
	 * @throws Exception
	 *             Exception
	 * @param jsonNode
	 *            jsonNode
	 */
	private void onReceivingValidMonitoringPeriodInMO(Exchange exchange, String msisdn, String cacheKeyToRemove,
			SOMessage soMessage, String userOptedKeyword, String optinShortCode, JsonNode jsonNode)
			throws IOException, JsonProcessingException, Exception {
		soMessage.setTriggerType(optinResponse);
		soMessage.setActionType(actionTypeOptin);
		soMessage.setActionStatus(success);
		soMessage.setActionResponse(userOptedKeyword);
		// To be sent to Core
		SOMessageUtils.setHeaderValue(soMessage, "subscriberOptinTimestamp", SOMessageUtils.getUnixTimestamp());
		SOMessageUtils.setHeaderValue(soMessage, "subscriberOptinChannel", channelName);
		SONotificationMessage soTemplateNotificationMessage = (SONotificationMessage) soMessage;
		soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
				.getNotificationMessages().get(0).setChannelName(channelName);
		soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
				.getNotificationMessages().get(0).setOptInChannel(channelName);
		soTemplateNotificationMessage.getMetaInfo().setChannelName(channelName);
		soTemplateNotificationMessage.getMessage().setRequesterChannel(channelName);
		soTemplateNotificationMessage.setActionType(optin);
		String messageToDB = jsonObjectMapper.writeValueAsString(soMessage);
		LOG.debug("Message to be send after valid CC: {}", messageToDB);
		String messageToPRG = jsonObjectMapper.writeValueAsString(soMessage);
		LOG.debug("Message to Prg : {}", messageToPRG);
		String provisionOfferResponse = isSuccessProvition(jsonNode);
		exchange.getIn().setHeader("provisionOfferSuccessMsg", provisionOfferResponse);
		exchange.getIn().setHeader("messageToPRG", messageToPRG);
		exchange.getIn().setHeader("isMOSuccess", true);
		exchange.getIn().setHeader("isInvalidKeyword", false);
		exchange.getIn().setBody(messageToDB);
		LOG.debug("Redis object after receiving a valid CC : {}", cacheKeyToRemove);
		swapFulfillmentOptinToProcessing(cacheKeyToRemove, msisdn, optinShortCode);
		// Check this condition
		if (soMessage instanceof SOFulfillmentMessage) {
			exchange.getIn().setHeader("offerAlreadyProvisionedByCore", true);
		}
	}

	/**
	 * Swapping soMessage from optin to processing pool
	 * 
	 * @param optinCacheKey
	 *            redis key used to save optin message
	 * @param msisdn
	 *            mobile number
	 * @param optinShortCode
	 *            short code received as part of MO
	 * @throws Exception
	 *             Exception
	 */
	private void swapFulfillmentOptinToProcessing(String optinCacheKey, String msisdn, String optinShortCode)
			throws Exception {
		String objectToMoveToProcessing = redisConnectionPool.getObjectFromCache(optinCacheKey);
		SONotificationMessage soNotificationMessage = jsonObjectMapper.readValue(objectToMoveToProcessing,
				SONotificationMessage.class);
		SOMessageUtils.setHeaderValue(soNotificationMessage, optinStatus, "PROCESSING");
		String optinCount=SOMessageUtils.getHeaderValue(soNotificationMessage, "OPTIN-COUNT");
	        if(StringUtils.isNotEmpty(optinCount)){
	        	String updatedCount=Integer.toString(Integer.parseInt(SOMessageUtils.getHeaderValue(soNotificationMessage, "OPTIN-COUNT"))+1);
	            SOMessageUtils.setHeaderValue(soNotificationMessage, "OPTIN-COUNT", updatedCount);	
	            LOG.debug("optin updated count {}.", optinCacheKey, updatedCount);
	        }
		objectToMoveToProcessing = jsonObjectMapper.writeValueAsString(soNotificationMessage);
		String processingKey = CacheKeyUtils.generateOptinCacheKeyForProcessingFulfillment(msisdn, soNotificationMessage
				.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getOfferId());
		if (StringUtils.isNotBlank(objectToMoveToProcessing)) {
			// TTL = offer end date - current time
			Long currentTime = Instant.now().toEpochMilli();
			String endDate = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers()
					.get(0).getOfferEndDate();
			int optinTTL = CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) + ttlBuffer;
			redisConnectionPool.putInCache(processingKey, objectToMoveToProcessing, optinTTL);
			String optinKeyword = SOMessageUtils.getOptinKeyword(soNotificationMessage);
			String[] keywords = optinKeyword.split(",");
			for (int i = 0; i < keywords.length; i++) {
				optinCacheKey = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn, keywords[i].toString());
				redisConnectionPool.putInCache(optinCacheKey, objectToMoveToProcessing, optinTTL);
			}
		}
		LOG.debug("Redis: Copied SOMessage from key {} to key {}.", optinCacheKey, processingKey);
	}

	/**
	 * 
	 * Checks whether the current time is in between offer start and end dates
	 * 
	 * @param soMessage
	 *            soNotificationMessage
	 * @return true/false
	 */
	private static boolean isMOInValidTime(SOMessage soMessage) {
		long startTime = Long.parseLong(SOMessageUtils.getOfferStartDate(soMessage));
		long endTime = Long.parseLong(SOMessageUtils.getOfferEndDate(soMessage));
		Calendar calendarStart = Calendar.getInstance();
		calendarStart.setTimeInMillis(startTime);
		Calendar calendarEnd = Calendar.getInstance();
		calendarEnd.setTimeInMillis(endTime);
		Calendar calendarNow = Calendar.getInstance();
		// Get the time in local timezone
		int hour = calendarNow.get(Calendar.HOUR_OF_DAY);
		int minutes = calendarNow.get(Calendar.MINUTE);
		int seconds = calendarNow.get(Calendar.SECOND);
		// create time zone object with UTC and set the timezone
		TimeZone tzone = TimeZone.getTimeZone("UTC");
		calendarNow.setTimeZone(tzone);
		// Set the current time as GMT Time
		calendarNow.set(Calendar.HOUR_OF_DAY, hour);
		calendarNow.set(Calendar.MINUTE, minutes);
		calendarNow.set(Calendar.SECOND, seconds);
		long epochNow = calendarNow.getTimeInMillis();
		calendarNow.setTimeInMillis(epochNow);
		LOG.debug("Calender Start:{} Calender End: {} Calender Now: {}", calendarStart, calendarEnd, calendarNow);
		boolean result = calendarNow.after(calendarStart) && calendarNow.before(calendarEnd);
		LOG.info("isMOInValidTime: {}", result);
		return result;
	}

	/**
	 * Handles invalid optin
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @param soMessageAsJSON
	 *            soMessageAsJSON
	 * @param soMessage
	 *            fetch from cache
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @throws Exception
	 *             Exception
	 */
	private void onReceivingInValidOptinInMO(Exchange exchange, String msisdn, String soMessageAsJSON,
			String userOptedKeyword, SOMessage soMessage) throws Exception {
		SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
				soMessageTemplate, msisdn, soMessageAsJSON, optinInvalidMessage, channelName, jsonObjectMapper);
		LOG.debug("Invalid Optin, Incorrect values");
		optinNotificationMessage.setTriggerType(optinNotify);
		optinNotificationMessage.setActionType(notify);
		optinNotificationMessage.setActionStatus(success);
		optinNotificationMessage.getMetaInfo().setChannelName(channelName);
		optinNotificationMessage.getMessage().setRequesterChannel(channelName);
		String finalMessageAsJson = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
		exchange.getIn().setHeader("messageSendToDbHandler",createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure));
		exchange.getIn().setHeader("isMOSuccess", false);
		exchange.getIn().setHeader("isInvalidKeyword", false);
		exchange.getIn().setHeader("invalidOptinKeyword", true);
		exchange.getIn().setBody(finalMessageAsJson);
		String erorrResponse = setErrorResponse(msisdn, optinInvalidMessage);
		LOG.debug("Invalid CC received with Invalid Keyword {}", erorrResponse);
		exchange.getIn().setHeader("onReceivingInValidOptinInMO", erorrResponse);
	}

	/**
	 * 
	 * This message is to set triggerType, actionType,actionStatus and
	 * actionResponse for OPTINCASE for sending to DB
	 * 
	 * @param messageAsJson
	 *            message created for sending optin notify message
	 * @param userOptedKeyword
	 *            text sent by the user
	 * @param actionStatus
	 *            success/failure
	 * @return Somessag as json for sending to DB handler
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	private String createOptinResponseMessageForDb(String messageAsJson, String userOptedKeyword, String actionStatus)
			throws JsonProcessingException {
		SOMessage soMessageForDB = RawMessageUtils.processRawMessage(messageAsJson, jsonObjectMapper);
		soMessageForDB.setTriggerType(optinResponse);
		soMessageForDB.setActionType(optin);
		soMessageForDB.setActionResponse(userOptedKeyword);
		soMessageForDB.setActionStatus(actionStatus);
		return jsonObjectMapper.writeValueAsString(soMessageForDB);
	}

	/**
	 * 
	 * @param msisdn
	 *            msisdn
	 * @param errorMessage
	 *            errorMessage
	 * @return ErrorResponse
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */

	private String setErrorResponse(String msisdn, String errorMessage) throws JsonProcessingException {
		ObjectNode node = jsonObjectMapper.createObjectNode();
		node.put("msisdn", msisdn);
		node.put("responseCode", "-1");
		node.put("responseMessage", errorMessage);
		return jsonObjectMapper.writeValueAsString(node);
	}

}
