/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.knowesis.sift.orchestrator.cc.domain.CCResponse;
import com.knowesis.sift.orchestrator.cc.domain.Campaign;
import com.knowesis.sift.orchestrator.cc.domain.Offer;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.domain.SOEventTrigger;

/**
 * To Process CC Response
 * 
 * @author SO Development Team
 */
@Component("CCResponseProcessor")
public class CCResponseProcessor implements Processor {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	ObjectMapper jsonObjectMapper;

	@Value("${so.triggerType.fulfill}")
	private String fulfillTrigger;

	@Value("${so.actionType.fulfill}")
	private String fulfillActionType;

	@Value("${so.triggerType.offerNotify}")
	private String offerNotifytriggerType;

	@Value("${so.triggerType.notify}")
	private String offerNotifyActionType;

	@Value("${so.triggerType.nominationNotify}")
	private String nominationNotify;

	@Value("${so.actionType.notify}")
	private String actionType3;
	
	@Value("${so.acq.programId}")
	private String programId;
	

	@Autowired
	RedisConnectionPool redisConnectionPool;

	@Override
	public void process(Exchange exchange) throws Exception {
		CCResponse ccResponse = new CCResponse();
		log.debug("Response from DB" + exchange.getIn().getBody(String.class));
		List<SOEventTrigger> soEventTriggers = jsonObjectMapper.readValue(exchange.getIn().getBody(String.class),
				TypeFactory.defaultInstance().constructCollectionType(List.class, SOEventTrigger.class));
		log.info(soEventTriggers.toString());
		if (soEventTriggers.size() > 0) {
			List<Campaign> campaignList = new ArrayList<Campaign>();
			ccResponse.setResponseCode("200");
			ccResponse.setResponseMessage("OK");
			List<String> campaignCodes = getCampaignCodes(soEventTriggers);
			for (String campaignCode : campaignCodes) {
				getDetailsOfCampaign(campaignList, soEventTriggers, campaignCode);
			}
			ccResponse.setCampaignList(campaignList);
			ccResponse.setMsisdn(soEventTriggers.get(0).getMsisdn());
			Calendar calendarTime = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			String currentSystemTime = dateFormat.format(calendarTime.getTime());
			ccResponse.setCurrentSystemTime(currentSystemTime);
			exchange.getIn().setBody(ccResponse);
		} else {
			ccResponse.setResponseCode("200");
			ccResponse.setResponseMessage("OK");
			Calendar calendarTime = Calendar.getInstance();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			String currentSystemTime = dateFormat.format(calendarTime.getTime());
			ccResponse.setCurrentSystemTime(currentSystemTime);
			List<Campaign> campaignList = new ArrayList<Campaign>();
			ccResponse.setCampaignList(campaignList);
			String msisdn = exchange.getIn().getHeader("MSISDN", String.class);
			ccResponse.setMsisdn(msisdn);
			exchange.getIn().setBody(ccResponse);
			log.info("No Entry found.");
		}
	}

	/**
	 * To get the details of a particular campaign
	 * 
	 * @param campaignList
	 *            campaignList
	 * @param soEventTriggers
	 *            soEventTrigger
	 * @param campaignCode
	 *            campaignCode of the campaign
	 * 
	 * @throws IOException
	 *             IOException
	 * @throws JsonMappingException
	 *             JsonMappingException
	 * @throws ParseException
	 *             ParseException
	 */
	public void getDetailsOfCampaign(List<Campaign> campaignList, List<SOEventTrigger> soEventTriggers,
			String campaignCode) throws JsonMappingException, IOException, ParseException {
		List<SOEventTrigger> soEventTriggersWithSameCampaignCode = new ArrayList<SOEventTrigger>();
		for (SOEventTrigger soEventTrigger : soEventTriggers) {
			if (soEventTrigger.getProgramId().equalsIgnoreCase(campaignCode)) {
				soEventTriggersWithSameCampaignCode.add(soEventTrigger);
			}
		}
		List<String> offerCodes = getOfferCodes(soEventTriggersWithSameCampaignCode);
		if (soEventTriggersWithSameCampaignCode != null && soEventTriggersWithSameCampaignCode.size() > 0) {
			setCampaignDetailsOfEntriesWithSameCampaignCode(soEventTriggersWithSameCampaignCode, campaignList,
					offerCodes, campaignCode);
		}
	}

	/**
	 * To get details of offers in same Campaign
	 * 
	 * @param soEventTriggersWithSameCampaignCode
	 *            soEventTriggersWithSameCampaignCode
	 * @param campaignList
	 *            campaignList
	 * @param offerCodes
	 *            offerCodes
	 * @param campaignCode
	 *            campaignCode
	 * @throws JsonParseException
	 *             JsonParseException
	 * @throws JsonMappingException
	 *             JsonMappingException
	 * @throws IOException
	 *             IOException
	 * @throws ParseException
	 *             ParseException
	 */
	public void setCampaignDetailsOfEntriesWithSameCampaignCode(
			List<SOEventTrigger> soEventTriggersWithSameCampaignCode, List<Campaign> campaignList,
			List<String> offerCodes, String campaignCode)
			throws JsonParseException, JsonMappingException, IOException, ParseException {
		for (String offerCode : offerCodes) {
			Campaign campaign = new Campaign();

			campaign.setCampaignName(campaignCode);
			campaign.setCampaignCode(campaignCode);
			campaign.setCampaignDescription("");
			List<Offer> campaignOfferList = new ArrayList<Offer>();
			Offer campaignOffer = getDetailsOfOffer(soEventTriggersWithSameCampaignCode, offerCode);
			log.info("isNotBlank"+campaignOffer.getFlowId());
			if(StringUtils.isNotBlank(campaignOffer.getFlowId())){
				log.info("isNotBlank passed");
			campaignOfferList.add(campaignOffer);
			campaign.setCampaignOfferList(campaignOfferList);
			campaignList.add(campaign);
			
			}
		}
	}

	/**
	 * To get the details of a particular offer
	 * 
	 * @param soEventTriggersWithSameCampaignCode
	 *            soEventTriggersWithSameCampaignCode
	 * @param offerCode
	 *            offerCode
	 * 
	 * @return campaign campaign
	 * @throws ParseException
	 *             ParseException
	 */

	public Offer getDetailsOfOffer(List<SOEventTrigger> soEventTriggersWithSameCampaignCode, String offerCode)
			throws ParseException {
		List<SOEventTrigger> soEventTriggersWithSameOfferCode = new ArrayList<SOEventTrigger>();
		for (SOEventTrigger soEventTrigger : soEventTriggersWithSameCampaignCode) {
			if (soEventTrigger.getOfferId().equalsIgnoreCase(offerCode)) {
				soEventTriggersWithSameOfferCode.add(soEventTrigger);

			}
		}
		Offer offer = new Offer();
		offer.setOfferID(offerCode);
		offer.setOfferChannel(soEventTriggersWithSameOfferCode.get(0).getChannel());

		setOfferDetailsFromNotificationMessage(soEventTriggersWithSameOfferCode, offer);
		setOfferDetailsFromFulfillmentMessage(soEventTriggersWithSameOfferCode, offer);
		setOfferDetailsFromNominationMessage(soEventTriggersWithSameOfferCode, offer);
		offer.setControlFlag("N");
		return offer;
	}

	/**
	 * To set optinflag
	 * 
	 * @param soEventTriggersWithSameOfferCode
	 *            soEventTriggersWithSameOfferCode
	 * @param offer
	 *            offer
	 */
	public void setOptinFlag(List<SOEventTrigger> soEventTriggersWithSameOfferCode, Offer offer) {
		String nominationOptin = "";
		log.info("optin flag for " + offer);
		for (SOEventTrigger soEventTrigger : soEventTriggersWithSameOfferCode) {
			if (soEventTrigger.getContactDirection() != null && soEventTrigger.getContactDirection().contains("_")) {
				String contactDirection = soEventTrigger.getContactDirection();
				String[] optin = contactDirection.split("_");
				if (optin.length > 3) {
					nominationOptin = optin[1] + "_" + optin[2];
				}
				assignOptinFlagValue(soEventTrigger,offer,optin,nominationOptin);
			} else {
				offer.setOptinFlag("NO");
			}
		}
	}
	
	/**
	 * assign value to Optin Flag
	 * 
	 * @param soEventTrigger soEventTrigger
	 * @param offer offer
	 * @param optin optin arraysplit by '_'
	 * @param nominationOptin nominationOptin
	 */
	public void assignOptinFlagValue(SOEventTrigger soEventTrigger,Offer offer,String[] optin,String nominationOptin){
		if (optin[1].equalsIgnoreCase("optin") || "NOMINATION_OPTIN".equalsIgnoreCase(nominationOptin)) {
			offer.setOptinFlag(checkOfferExist(soEventTrigger));
		} else {
			offer.setOptinFlag("NO");
		}
		
	}

	/**
	 * To check Offer Exist
	 * 
	 * @param soEventTrigger
	 *            soEventTrigger
	 * @return OfferExist or not
	 */
	public String checkOfferExist(SOEventTrigger soEventTrigger) {
		if (soEventTrigger.getOfferEndDate().before(new Date())) {
			return "Expired";
		}
		return "Yes";

	}

	/**
	 * Set values in offer from Notification Message
	 * 
	 * @param soEventTriggersWithSameOfferCode
	 *            soEventTriggersWithSameOfferCode
	 * @param offer
	 *            offer
	 */
	public void setOfferDetailsFromNotificationMessage(List<SOEventTrigger> soEventTriggersWithSameOfferCode,
			Offer offer) {
		offer.setOfferMessage("");
		offer.setOfferContactDate("");
		offer.setOfferStatus("UNDLV");
		offer.setOfferStartDate("");
		offer.setOfferEndDate("");
		offer.setFlowId("");
		offer.setOptinFlag("");
		offer.setOptinShortcode("");
		offer.setOptinKeyword("");
		for (SOEventTrigger soEventTrigger : soEventTriggersWithSameOfferCode) {
			String triggerType = soEventTrigger.getTriggerType();
			String actionType = soEventTrigger.getActionType();
			
			if (triggerType.equalsIgnoreCase(offerNotifytriggerType) && !(actionType.equalsIgnoreCase("DR-NOTIFY"))) { 
																											// triggerType=OFFERNOTIFY
				offer.setOfferMessage(soEventTrigger.getNotificationMessage());
				String offerContactDate = getFormattedDate(soEventTrigger.getTriggerTimestamp());
				offer.setOfferContactDate(offerContactDate);
				offer.setOfferStatus("UNDLV");
				String offerStartDate = getFormattedDate(soEventTrigger.getOfferStartDate());
				offer.setOfferStartDate(offerStartDate);
				String offerEndDate = getFormattedDate(soEventTrigger.getOfferEndDate());
				offer.setOfferEndDate(offerEndDate);
				offer.setFlowId(soEventTrigger.getFlowId());
				setOptinFlag(soEventTriggersWithSameOfferCode, offer);
				offer.setOptinShortcode(getOptinShortcode(soEventTrigger));
				offer.setOptinKeyword("");
				String prgId = soEventTrigger.getProgramId();
				log.debug("program id from DB {}",prgId);
				log.debug("program id from Property {}",programId);
				if(programId.equalsIgnoreCase(prgId)) { //setting  OfferStatus null for acq  
					offer.setOfferStatus("");
				}
				
			}
			setOfferStatusByEvaluatingDRNotify(soEventTrigger, offer, triggerType, actionType);
		}
	}
	
	/**
	 * setOfferStatusByEvaluatingDRNotify 
	 * 
	 * @param soEventTrigger soEventTrigger
	 * @param offer offer
	 * @param triggerType triggerType
	 * @param actionType actionType
	 */
	public void setOfferStatusByEvaluatingDRNotify(SOEventTrigger soEventTrigger,Offer offer,String triggerType,String actionType){
		if (triggerType.equalsIgnoreCase(offerNotifytriggerType) && actionType.equalsIgnoreCase("DR-NOTIFY")) {

			if (soEventTrigger.getActionStatus().equalsIgnoreCase("Success")) {
				offer.setOfferStatus("P");
			} else {
				offer.setOfferStatus("UNDLV");
			}
		}
		
	}

	/**
	 * to get Nomination optin short code
	 * 
	 * @param soEventTrigger
	 *            soEventTrigger
	 * @return optins shortcode
	 */
	public String getNominationOptinShortcode(SOEventTrigger soEventTrigger) {

		if (soEventTrigger.getContactDirection() != null && soEventTrigger.getContactDirection().contains("_")) {
			String contactDirection = soEventTrigger.getContactDirection();
			String[] optin = contactDirection.split("_");
			if (optin.length == 4) {
				String nominationOptin = optin[1] + "_" + optin[2];
				return optin[3].toString();
			}

		}
		return "";
	}

	/**
	 * To ger normal optin shortcode
	 * 
	 * @param soEventTrigger
	 *            soEventTrigger
	 * @return optins shortcode
	 */
	public String getOptinShortcode(SOEventTrigger soEventTrigger) {
		if (soEventTrigger.getContactDirection() != null && soEventTrigger.getContactDirection().contains("_")) {
		    log.info("Contact Direction: {}", soEventTrigger.getContactDirection());
			String contactDirection = soEventTrigger.getContactDirection();
			String[] optin = contactDirection.split("_");
			if (optin.length == 3) {
				return optin[2].toString();
			}

		}
		return "";
	}

	
	/**
	 * Set values in offer from Notification Message
	 * 
	 * @param soEventTriggersWithSameOfferCode
	 *            soEventTriggersWithSameOfferCode
	 * @param offer
	 *            offer
	 * @throws ParseException
	 *             ParseException
	 */

	public void setOfferDetailsFromNominationMessage(List<SOEventTrigger> soEventTriggersWithSameOfferCode, Offer offer)
			throws ParseException {
		offer.setNominationMessage("");
		offer.setNominationShortcode("");
		offer.setNominationStatus(getNominationStatus(soEventTriggersWithSameOfferCode, offer));
		offer.setNominationOfferStartDate("");
		offer.setNominationOfferEndDate("");
		Optional<Timestamp> latestDate = soEventTriggersWithSameOfferCode.stream().filter(a -> a.getTriggerType().equalsIgnoreCase(nominationNotify)).filter(Objects::nonNull)
				.map(u -> u.getOfferEndDate()).max(Date::compareTo);
		Date latestTimestamp=latestDate.isPresent()?latestDate.get():null;
		for (SOEventTrigger soEventTrigger : soEventTriggersWithSameOfferCode) {
			
			if( latestTimestamp ==(soEventTrigger.getOfferEndDate())){
				
			String triggerType = soEventTrigger.getTriggerType();
			String actionType = soEventTrigger.getActionType();
			setNominationDetailsWithDRNotification(soEventTrigger, offer, triggerType, actionType);
			offer.setNominationStatus(getNominationStatus(soEventTriggersWithSameOfferCode, offer));

		}
		}
	}
	/**
	 * setNominationDetailsWithDRNotification
	 * 
	 * @param soEventTrigger soEventTrigger
	 * @param offer offer
	 * @param triggerType triggerType
	 * @param actionType actionType
	 */
	public void setNominationDetailsWithDRNotification(SOEventTrigger soEventTrigger,Offer offer,String triggerType,String actionType){
		if (triggerType.equalsIgnoreCase(nominationNotify)  && !(actionType.equalsIgnoreCase("DR-NOTIFY"))) { // Checks
			offer.setNominationMessage(soEventTrigger.getNotificationMessage());
			offer.setNominationShortcode(getNominationOptinShortcode(soEventTrigger));
			offer.setNominationOfferStartDate(getFormattedDate(soEventTrigger.getOfferStartDate()));
			offer.setNominationOfferEndDate(getFormattedDate(soEventTrigger.getOfferEndDate()));
		}
	}

	
	
	/**
	 * To get Nomination Status
	 * 
	 * @param soEventTriggersWithSameOfferCode
	 *            sEventTriggers With Same OfferCode
	 * @param offer
	 *            offer
	 * @return nomination status
	 */

	public String getNominationStatus(List<SOEventTrigger> soEventTriggersWithSameOfferCode, Offer offer) {
		for (SOEventTrigger soEventTrigger : soEventTriggersWithSameOfferCode) {
			String triggerType = soEventTrigger.getTriggerType();
			String actionType = soEventTrigger.getActionType();
			if (triggerType.equalsIgnoreCase("NOMINATION-NOTIFY") && actionType.equalsIgnoreCase("DR-NOTIFY")) {
				if (soEventTrigger.getActionStatus().equalsIgnoreCase("Success")) {
					return "N";
				} else {
					return "NUNDLV";
				}
			}
		}
		return "NA";
	}

	/**
	 * Converts timestamp to String
	 * 
	 * @param timestamp
	 *            timestamp
	 * @return dateAsString
	 */

	public String getFormattedDate(Timestamp timestamp) {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			return dateFormat.format(timestamp);
		} catch (Exception e) {
			log.debug("Date parsing error");
			return null;
		}
	}

	/**
	 * Set values in offer from Fulfillment Message
	 * 
	 * @param soEventTriggersWithSameOfferCode
	 *            soEventTriggersWithSameOfferCode
	 * @param offer
	 *            offer
	 */

	public void setOfferDetailsFromFulfillmentMessage(List<SOEventTrigger> soEventTriggersWithSameOfferCode,
			Offer offer) {
		offer.setProvisionStatus("");
		offer.setFulfillmentMessage("");
		offer.setProvisionDate("");
		offer.setProvisioningFailureReason("");
		offer.setRewardID("");
		//offer.setOfferStatus(""); removed for cc status getting null
		log.info("Offer status 5",	offer.getOfferStatus());
		List<SOEventTrigger> soEventTriggersWithFulfillmentDetails = new ArrayList<SOEventTrigger>();
		for (SOEventTrigger soEventTrigger : soEventTriggersWithSameOfferCode) {
			String triggerType = soEventTrigger.getTriggerType();
			String actionType = soEventTrigger.getActionType();
			if (triggerType.equalsIgnoreCase(fulfillTrigger) && actionType.equalsIgnoreCase(fulfillActionType)) {
				soEventTriggersWithFulfillmentDetails.add(soEventTrigger);
			}
			if (triggerType.equalsIgnoreCase("FULFILL-NOTIFY")) {
				offer.setFulfillmentMessage(soEventTrigger.getNotificationMessage());
			}
		}
		setSoEventTriggersWithFulfillmentDetails(soEventTriggersWithFulfillmentDetails, offer);
	}
	
	/**
	 * setSoEventTriggersWithFulfillmentDetails
	 * 
	 * @param soEventTriggersWithFulfillmentDetails soEventTriggersWithFulfillmentDetails
	 * @param offer offer
	 */
	public void setSoEventTriggersWithFulfillmentDetails(List<SOEventTrigger> soEventTriggersWithFulfillmentDetails,Offer offer){
		if (soEventTriggersWithFulfillmentDetails.size() < 1) {
			offer.setProvisionStatus("UNKNOWN");
		} else {
			soEventTriggersWithFulfillmentDetails = getLatestFulfillment(soEventTriggersWithFulfillmentDetails, offer);
			getMultipleProductDetails(soEventTriggersWithFulfillmentDetails, offer);
			offer.setOfferStatus("O");

		}
		
	}

	/**
	 * To get latest fulfillment from multiple fulfillment for same offer
	 * 
	 * @param soEventTriggersWithFulfill
	 *            list of soEventTrigger With Fulfillment
	 * @param offer
	 *            offer
	 * @return list of SOEventTrigger
	 */
	public List<SOEventTrigger> getLatestFulfillment(List<SOEventTrigger> soEventTriggersWithFulfill, Offer offer) {
		List<SOEventTrigger> soEventTriggersWithFulfillment = new ArrayList<SOEventTrigger>();
		Date latestDate = soEventTriggersWithFulfill.stream().map(u -> u.getTriggerTimestamp()).max(Date::compareTo)
				.get();
		for (SOEventTrigger soEventTrigger : soEventTriggersWithFulfill) {
			if (soEventTrigger.getTriggerTimestamp().equals(latestDate)) {
				soEventTriggersWithFulfillment.add(soEventTrigger);
			}
		}

		return soEventTriggersWithFulfillment;

	}

	/**
	 * To set setMultipleProductDetails
	 * 
	 * @param soEventTriggersWithFulfillment
	 *            soEventTriggersWithFulfillment
	 * @param offer
	 *            offer
	 */
	public void getMultipleProductDetails(List<SOEventTrigger> soEventTriggersWithFulfillment, Offer offer) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		log.info("soEventTriggersWithFulfillment size" + soEventTriggersWithFulfillment.size());
		if (soEventTriggersWithFulfillment.size() == 1) {
			offer.setOfferStartDate(getFormattedDate(soEventTriggersWithFulfillment.get(0).getOfferStartDate()));
			offer.setOfferEndDate(getFormattedDate(soEventTriggersWithFulfillment.get(0).getOfferEndDate()));
			offer.setFlowId(soEventTriggersWithFulfillment.get(0).getFlowId());
			offer.setRewardID(soEventTriggersWithFulfillment.get(0).getProvisioningOfferId());
			offer.setProvisionStatus(soEventTriggersWithFulfillment.get(0).getActionStatus());
			if(offer.getProvisionStatus().equalsIgnoreCase("SUCCESS")){
				offer.setProvisioningFailureReason("");
			}else {
				offer.setProvisioningFailureReason(soEventTriggersWithFulfillment.get(0).getActionResponse());
			}
			offer.setProvisionDate(dateFormat.format(soEventTriggersWithFulfillment.get(0).getTriggerTimestamp()));
		} else if (soEventTriggersWithFulfillment.size() > 1) {
			List<String> rewordid = new ArrayList<String>();
			List<String> provisionStatus = new ArrayList<String>();
			List<String> provisioningFailureReason = new ArrayList<String>();
			for (SOEventTrigger soEventTrigger : soEventTriggersWithFulfillment) {
				offer.setOfferStartDate(getFormattedDate(soEventTrigger.getOfferStartDate()));
				offer.setOfferEndDate(getFormattedDate(soEventTrigger.getOfferEndDate()));
				offer.setFlowId(soEventTrigger.getFlowId());
				rewordid.add(soEventTrigger.getProvisioningOfferId());
				provisionStatus.add(soEventTrigger.getActionStatus());
				setProvisioningFailureReason(soEventTrigger, provisioningFailureReason);
				offer.setProvisionDate(dateFormat.format(soEventTrigger.getTriggerTimestamp()));
			}
			offer.setRewardID(String.join("|", rewordid));
			offer.setProvisionStatus(String.join("|", provisionStatus));
			offer.setProvisioningFailureReason(String.join("|", provisioningFailureReason));
		}

	}
	
	/**
	 * setProvisioningFailureReason 
	 * 
	 * @param soEventTrigger soEventTrigger
	 * @param provisioningFailureReason provisioningFailureReason
	 */
	public void setProvisioningFailureReason(SOEventTrigger soEventTrigger,List<String> provisioningFailureReason){
		if(soEventTrigger.getActionStatus().equalsIgnoreCase("SUCCESS")){
			provisioningFailureReason.add("");}
		else{
			provisioningFailureReason.add(soEventTrigger.getActionStatus());	
		}
	}

	/**
	 * To fetch the names of campaignCodes
	 * 
	 * @param soEventTriggers
	 *            List of Event triggers
	 * @return campaignCodes List of campaigns
	 */

	public List<String> getCampaignCodes(List<SOEventTrigger> soEventTriggers) {
		List<String> campaignCodes = new ArrayList<String>();
		for (SOEventTrigger soEventTrigger : soEventTriggers) {
			String campaignCode = soEventTrigger.getProgramId();
			if ((!campaignCodes.contains(campaignCode)) && campaignCode != null) {
				campaignCodes.add(campaignCode);
			}
		}
		return campaignCodes;
	}

	/**
	 * To get the offerCodes under a campaign
	 * 
	 * @param soEventTriggersSameCampaignNames
	 *            soEventTriggersSameCampaignNames return offerCodes offerCodes
	 * @return offerCodes offerCodes
	 */
	public List<String> getOfferCodes(List<SOEventTrigger> soEventTriggersSameCampaignNames) {
		List<String> offerCodes = new ArrayList<String>();
		for (SOEventTrigger soEventTrigger : soEventTriggersSameCampaignNames) {
			String offerCode = soEventTrigger.getOfferId();
			if ((!offerCodes.contains(offerCode)) && offerCode != null) {
				offerCodes.add(offerCode);
			}
		}
		return offerCodes;
	}

	/**
	 * 
	 * @param line
	 *            line
	 * @return campaignDesc campaignDesc
	 */
	public String getCampaignDesc(String[] line) {
		String campaignDesc = null;
		for (int i = 0; i < line.length; i++) {
			campaignDesc = campaignDesc + line[i] + "|";
		}
		return campaignDesc;
	}

}
