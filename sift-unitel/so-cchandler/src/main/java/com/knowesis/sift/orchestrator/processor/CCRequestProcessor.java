/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * To Process CC Request
 * 
 * @author SO Development Team
 */

@Component("CCRequestProcessor")
public class CCRequestProcessor implements Processor {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Value("${cc.db.select}")
	String sql;
	
	@Value("${db.request.endpoint.url}")
	private String dbRequestEndpoint;
	
	@Value("${so.triggerType.reminderNotify}")
	private String triggerTypeReminderNotify;
	
	@Value("${so.triggerType.coreReminder}")
	private String triggerTypeCoreReminder;

	
	@Value("${cc.defaultStartDate.numberOfDays}")
	private String numberOfDays;
	
	private int defaultNumberOfDays;
	
	/**
	 * Called when Bean is refreshed by Spring
	 */
	@PostConstruct
	public void onPostConstruct() {
		defaultNumberOfDays = Integer.parseInt(numberOfDays);
	}

	@Override
	public void process(Exchange exchange) throws Exception
	  {
	    HttpServletRequest request = (HttpServletRequest)exchange.getIn().getBody(HttpServletRequest.class);
	    String msisdn = request.getParameter("msisdn");
	    String msisdnWithoutCode = "";
	    if ((StringUtils.isBlank(msisdn)) || (msisdn == null)) {
	      exchange.getIn().setHeader("INVALID", Boolean.valueOf(true));
	      exchange.getIn().setBody(null);
	      log.debug("MSISDN is not found");
	    } else {
	      msisdnWithoutCode = "976" + msisdn;
	    }
	    log.debug("MSISDN {}", msisdn);
	    String tradeStartDate = request.getParameter("startDate");
	    log.debug("STARTDATE {}", tradeStartDate);
	    String tradeEndDate = request.getParameter("endDate");
	    log.debug("ENDDATE {}", tradeEndDate);
	    tradeStartDate = validateStartDate(tradeStartDate);
	    tradeEndDate = validateEndDate(tradeEndDate);
	    String startDate = parseDate(tradeStartDate + "000000");
	    String endDate = parseDate(tradeEndDate + "235959");
	    log.info("Start Date {}", startDate);
	    log.info("End Date {}", endDate);
	    log.info("SQL query {}", sql);
	    exchange.getIn().setBody(sql);
	    exchange.getIn().setHeader("MSISDN", msisdn);
	    exchange.getIn().setHeader("MSISDNWITHCODE", msisdnWithoutCode);
	    exchange.getIn().setHeader("OriginalRequest", sql);
	    exchange.getIn().setHeader("startDate", startDate);
	    exchange.getIn().setHeader("endDate", endDate);
	    
	    exchange.getIn().setHeader("CamelHttpUri", dbRequestEndpoint);
	    exchange.getIn().setHeader("CamelHttpQuery", "args=" + msisdn + "," + startDate + "," + endDate + "," + triggerTypeReminderNotify + "," + triggerTypeCoreReminder);
	  }
	
	/**
	 * 
	 * @param tradeEndDate tradeEndDate
	 * @return endDate
	 */
	public String validateEndDate(String tradeEndDate){
		String endDate = (StringUtils.isBlank(tradeEndDate) || tradeEndDate == null)? getTradeEndDate(): tradeEndDate;
		return endDate;	
	}
	
	/**
	 * To vaidate startDate
	 * 
	 * @param tradeStartDate tradeStartDate
	 * @return startDate
	 */
	public String validateStartDate(String tradeStartDate){
		String startDate = (StringUtils.isBlank(tradeStartDate) || (tradeStartDate == null))? getTradeStartDate() : tradeStartDate;
		return startDate;	
	}
	
	
	/**
	 * To parse date
	 * 
	 * @param date date
	 * @return parsed date
	 * @throws ParseException 
	 * 
	 */
	public String parseDate(String date) throws ParseException {
		Date tradeDate =  new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).parse(date);
		String parsedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(tradeDate);
		return parsedDate;
	}
	
	/**
	 * To set the tradeStartDate if it is null in the request.
	 * @return tradeStartDate
	 */
	
	public String getTradeStartDate(){
		Calendar c = Calendar.getInstance();
	    c.add(Calendar.DATE, -defaultNumberOfDays);
	    DateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
	    String tradeStartDate = df.format(c.getTime());
	    return tradeStartDate;
	}
	
	/**
	 * To set the tradeEndDate if it is null in the request
	 * 
	 * @return tradeEndDate
	 */
	public String getTradeEndDate(){
		Calendar c = Calendar.getInstance(); 
	    DateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
	    String tradeEndDate = df.format(c.getTime());
	    return tradeEndDate;
	}
}
