/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * 
 * Updates AcqTxn table with required information to core
 * 
 * @author SO Development Team
 */
@Component("UpdateAcqTxnProcessor")
public class UpdateAcqTxnProcessor implements Processor {
    
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Value("${so.externalsmsc.updateacqtxn.sql}")
    String acqTxnUpdateSql;
    
    @Override
    public void process(Exchange exchange) throws Exception {
        String message = exchange.getIn().getBody(String.class);
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        if (soMessage != null && soMessage instanceof SONotificationMessage) {
            log.info("Updating AcqTxn table based on offer message: {}", message);
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            String promocode = SOMessageUtils.getOfferPayLoad(soMessage).get("PROMO_CODE");
            String eventId = SOMessageUtils.getEventId(soMessage);
            String flowId = SOMessageUtils.getFlowId(soMessage);
            String programId = SOMessageUtils.getProgramId(soMessage);
            String offerId = SOMessageUtils.getOfferId(soMessage);
            Timestamp offerStartDate = getTimestamp(SOMessageUtils.getOfferStartDate(soMessage));
            Timestamp offerEndDate = getTimestamp(SOMessageUtils.getOfferEndDate(soMessage));
            log.info("Query: {}. Parameters: {}", acqTxnUpdateSql, new Object[] { programId, offerId, eventId, flowId, msisdn, promocode });
            jdbcTemplate.update(acqTxnUpdateSql,
                    new Object[] { programId, offerId, eventId, flowId, offerStartDate, offerEndDate, msisdn, promocode });
        }
    }
    
    
    /**
     * Convert the epoch time to TimeStamp
     * 
     * @param timestampInString timestamp as string
     * @return date as timestamp
     */
    public static Timestamp getTimestamp(String timestampInString) {
        if (StringUtils.isNotBlank(timestampInString) && timestampInString != null) {
            Date date = new Date(Long.parseLong(timestampInString));
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            String formatted = format.format(date);
            Timestamp timeStamp = Timestamp.valueOf(formatted);
            return timeStamp;
        } else {
            return null;
        }
    }
    

}
