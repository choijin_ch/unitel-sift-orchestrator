/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.domain.SOReminderMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.ExternalSMSCUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Processes SMPP response from the SMSC
 * 
 * @author SO Development Team
 */
@Component("MTResponseProcessor")
public class MTResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;
    @Autowired
    RedisConnectionPool redisConnectionPool;

    // Properties added for Unitel
    @Value("${so.triggerType.offerNotify}")
    private String offerNotify;

    @Value("${so.actionType.notify}")
    private String notify;

    @Value("${so.actionStatus.success}")
    private String success;

    @Value("${so.actionStatus.failure}")
    private String failure;

    @Value("${so.triggerType.reminderNotify}")
    private String reminderNotify;
    
    @Value("${so.monitoring.ttl.additionalBufferValue}")
    private int ttlBuffer;


    /**
     * Processes the Smpp Response
     * 
     * @param exchange camel exchange
     * @throws Exception exception
     */
    public void process(Exchange exchange) throws Exception {
        String responseMessage = exchange.getIn().getBody(String.class);
        log.info("Response received from external SMSC {}",responseMessage);
        JsonNode responseMessageJson = jsonObjectMapper.readTree(responseMessage);
        String originalSoMessageJson = exchange.getProperty("OriginalRequestMessage", String.class);
        SOMessage soMessage = RawMessageUtils.processRawMessage(originalSoMessageJson, jsonObjectMapper);
        log.info("Synchronous External SMSC reponse has been received for msisdn: {}",
                SOMessageUtils.getMSISDN(soMessage));

        if(soMessage instanceof SONotificationMessage) {
            soMessage.setTriggerType(offerNotify);
        } else if(soMessage instanceof SOReminderMessage) {
            soMessage.setTriggerType(reminderNotify);
        }
        soMessage.setActionType(notify);
        String value = null;
        if ((responseMessageJson.path("statusCode").asText()).equals("200")) {
            exchange.getIn().setHeader("OFFERNOTIFY", true);
            exchange.getIn().setHeader("RESPONSESTATUS", success);
            soMessage.setActionStatus(success);
            value = jsonObjectMapper.writeValueAsString(soMessage);
            cacheForOptin(soMessage, value);
        } else {
            exchange.getIn().setHeader("RESPONSESTATUS", failure);
            soMessage.setActionStatus(failure);
        }
        exchange.getIn().setHeader("messageToDB", jsonObjectMapper.writeValueAsString(soMessage));
        exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
    }
    
    /**
     * Set redis entry for optin if Optin keyword and optin short code is present
     * 
     * @param soMessage SOMessage
     * @param value JSON form of SOMessage
     * 
     */
    private void cacheForOptin(SOMessage soMessage, String value) {

        if (SOMessageUtils.isSoMessageOptin(soMessage)) {
            String startDate = SOMessageUtils.getOfferStartDate(soMessage);
            String endDate = SOMessageUtils.getOfferEndDate(soMessage);
            int optinTTL = CacheKeyUtils.generateOptinTTL(startDate, endDate) + ttlBuffer;
            String msisdn = SOMessageUtils.getMSISDN(soMessage);

            String optinKeyword = SOMessageUtils.getOptinKeyword(soMessage);
            log.info("optinKeyword" + optinKeyword);
            String[] keywords = optinKeyword.split(",");

            for (int i = 0; i < keywords.length; i++) {
                String cacheKey = ExternalSMSCUtils.generateOptinCacheKeyForExternalSMSC(msisdn, keywords[i].toString());
                redisConnectionPool.putInCache(cacheKey, value, optinTTL);
            }
        }
    }
}
