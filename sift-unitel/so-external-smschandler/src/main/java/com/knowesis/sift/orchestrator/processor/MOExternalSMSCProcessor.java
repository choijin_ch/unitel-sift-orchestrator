/*
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.time.Instant;
import java.util.Calendar;
import java.util.TimeZone;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.ExternalSMSCUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Processes MO requests
 * 
 * @author SO Development Team
 */
@Component("MOExternalSMSCProcessor")
public class MOExternalSMSCProcessor implements Processor {
    private final static Logger LOG = LoggerFactory.getLogger(MOExternalSMSCProcessor.class);

    @Autowired
    ObjectMapper jsonObjectMapper;
    
    @Autowired
    RedisConnectionPool redisConnectionPool;
    
    @Value("${mo.notify.optin.invalidKeyword}")
    private String optinInvalidMessage;
    
    @Value("${mo.notify.optin.ssp.inprogress}")
    private String alreadyOptinSspInProgress;
    
    @Value("${so.notification.template}")
    private String soMessageTemplate;
    
    @Value("${mo.channel.name}")
    private String channelName;
    
    @Value("${so.monitoring.ttl.additionalBufferValue}")
    private int ttlBuffer;
    
    @Value("${so.optin.status.key}")
    private String optinStatus;
    
    @Value("${so.triggerType.optinNotify}")
    private String optinNotify;

    @Value("${so.actionType.notify}")
    private String notify;
    
    @Value("${so.actionStatus.success}")
    private String success;
    
    @Value("${so.actionStatus.failure}")
    private String failure;
    
    @Value("${so.triggerType.optinResponse}")
    private String optinResponse;

    @Value("${so.actionType.optin}")
    private String optin;
    
    @Value("${mo.optin.actionType}")
    private String actionTypeOptin;
    
    @Value("${mo.notify.optin.monitoring.expired}")
    private String notInMonitoringPeriodMessage;
    
    @Value("${mo.notify.optin.externalsmsc.response.success}")
    private String responseCodeSuccess;
    
    @Value("${mo.notify.optin.externalsmsc.response.fail}")
    private String responseCodeFail;
    
    @Value("${mo.notify.optin.externalsmsc.response.field}")
    private String responseCodeField;

    @Override
    public void process(Exchange exchange) throws Exception {
        String requestMessage = exchange.getIn().getBody(String.class);
        LOG.info("Received MO is",requestMessage);
        recivedMOProcess(exchange,requestMessage);
    }

	/**
	 * process the recived MO
	 * 
	 * @param exchange
	 *            exchange
	 * @param requestMessage
	 *            requestMessage body
	 * @throws Exception
	 *             Exception
	 */
	private void recivedMOProcess(Exchange exchange, String requestMessage) throws Exception {
		if (StringUtils.isNotBlank(requestMessage)) {
			ObjectNode requestJson = (ObjectNode) jsonObjectMapper.readTree(requestMessage);
			String smsText = requestJson.get("smsText").asText();
			String msisdn = requestJson.get("msisdn").asText();
			String cacheKey = ExternalSMSCUtils.generateOptinCacheKeyForExternalSMSC(msisdn, smsText);
			String soMessageAsJSON = null;
			if (StringUtils.isNotBlank(cacheKey)) {
				soMessageAsJSON = redisConnectionPool.getObjectFromCache(cacheKey);
			}
			ObjectNode responseJson = jsonObjectMapper.createObjectNode();

			if (soMessageAsJSON == null) {
				responseJson.put(responseCodeField, responseCodeFail);
			} else {

				whenOptinFound(exchange, soMessageAsJSON, msisdn, cacheKey, smsText, responseJson);

			}
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(responseJson));
		}

	}

	/**
	 * process when optin is recived from user
	 * 
	 * @param exchange exchange 
	 * @param soMessageAsJSON soMessageAsJSON
	 * @param msisdn msisdn
	 * @param cacheKeyToRemove cacheKey generated
	 * @param smsText smsText recived from user
	 * @param responseJson responseJson
	 * @throws JsonProcessingException JsonProcessingException
	 * @throws IOException IOException
	 * @throws Exception Exception
	 */
	private void whenOptinFound(Exchange exchange, String soMessageAsJSON, String msisdn, String cacheKeyToRemove,
			String smsText, ObjectNode responseJson) throws JsonProcessingException, IOException, Exception {
		SOMessage soMessage = RawMessageUtils.processRawMessage(soMessageAsJSON, jsonObjectMapper);
		 if (validateUserResponse(soMessage, smsText, "THIRDPARTY_SMS")) { 
	            onReceivingValidOptinInMO(exchange, msisdn, soMessageAsJSON, cacheKeyToRemove, soMessage, smsText,responseJson);
	        } else {
	            onReceivingInValidOptinInMO(exchange, msisdn, soMessageAsJSON, smsText, soMessage,responseJson);
	        }
		
	}
	
	/**
	 * process when valid MO is received
	 * 
	 * @param exchange exchange
	 * @param msisdn msisdn
	 * @param soMessageAsJSON soMessageAsJSON
	 * @param cacheKeyToRemove cacheKeyToRemove
	 * @param soMessage soMessage
	 * @param userOptedKeyword userOptedKeyword
	 * @param responseJson responseJson
	 * @throws JsonProcessingException JsonProcessingException
	 * @throws IOException IOException
	 * @throws Exception Exception
	 */
	private void onReceivingValidOptinInMO(Exchange exchange, String msisdn, String soMessageAsJSON,
            String cacheKeyToRemove, SOMessage soMessage, String userOptedKeyword, ObjectNode responseJson)
            throws JsonProcessingException, IOException, Exception {
        if (isMOInProcessing(soMessage)) {
            SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
                    soMessageTemplate, msisdn, soMessageAsJSON, alreadyOptinSspInProgress, channelName,
                    jsonObjectMapper);
            optinNotificationMessage.setTriggerType(optinNotify);
            optinNotificationMessage.setActionType(notify);
            optinNotificationMessage.setActionStatus(success);
            String finalMessageAsJson = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
            responseJson.put(responseCodeField,responseCodeFail);
            exchange.getIn().setHeader("messageSendToDbHandler",
                    createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure));
            exchange.getIn().setHeader("isMOSuccess", false);
            exchange.getIn().setHeader("isInvalidKeyword", false);
	        exchange.getIn().setHeader("messageToDB", finalMessageAsJson);
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(responseJson));
        } else {
            checkValidMonitoringPeriod(exchange, msisdn, soMessageAsJSON, cacheKeyToRemove, soMessage, userOptedKeyword,responseJson);
        }
    }
	
	/**
	 * check whether received MO in valid time period
	 * 
	 * @param exchange exchange
	 * @param msisdn msisdn
	 * @param soMessageAsJSON soMessageAsJSON
	 * @param cacheKeyToRemove cacheKeyToRemove
	 * @param soMessage soMessage
	 * @param userOptedKeyword userOptedKeyword
	 * @param responseJson responseJson
	 * @throws JsonProcessingException JsonProcessingException
	 * @throws IOException IOException
	 * @throws Exception Exception
	 */
    public void checkValidMonitoringPeriod(Exchange exchange, String msisdn, String soMessageAsJSON,
            String cacheKeyToRemove, SOMessage soMessage, String userOptedKeyword,ObjectNode responseJson)
            throws JsonProcessingException, IOException, Exception {
        if (isMOInValidTime(soMessage)) {
            onReceivingValidMonitoringPeriodInMO(exchange, msisdn, cacheKeyToRemove, soMessage, userOptedKeyword,responseJson);
        } else { // Not in Monitoring period
            SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
                    soMessageTemplate, msisdn, soMessageAsJSON, notInMonitoringPeriodMessage, channelName,
                    jsonObjectMapper);
            optinNotificationMessage.setTriggerType(optinNotify);
            optinNotificationMessage.setActionType(notify);
            optinNotificationMessage.setActionStatus(success);
            String finalMessageAsJson = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
            responseJson.put(responseCodeField, responseCodeFail);
            exchange.getIn().setHeader("messageSendToDbHandler",
                    createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure));
            exchange.getIn().setHeader("isMOSuccess", false);
            exchange.getIn().setHeader("isInvalidKeyword", false);
	        exchange.getIn().setHeader("messageToDB", finalMessageAsJson);
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(responseJson));
        }
    }
	
	 /**
     * Checks whether the offer is being processed
     * 
     * @param soMessage soMessage
     * @return true/false
     */
    public boolean isMOInProcessing(SOMessage soMessage) {
        String optinStat = SOMessageUtils.getHeaderValue(soMessage, optinStatus);
        if (optinStat != null && optinStat.equals("PROCESSING")) {
            return true;
        } else {
            return false;
        }
    }
	 
	 /**
	  * validating the user response
	  * 
	  * @param soMessage soMessage
	  * @param optinKeyword optinKeyword
	  * @param optinChannel optinChannel
	  * @return true or false
	  */
	 private static boolean validateUserResponse(SOMessage soMessage, String optinKeyword, String optinChannel) {
	        LOG.debug("SMS MO Cache Message: {} optinKeyword: {}, optinChannel: {}", soMessage,
	                optinKeyword, optinChannel);
	        String optinKeywords = SOMessageUtils.getOptinKeyword(soMessage);
	        String[] keywords = optinKeywords.split(",");
	        for (int i = 0; i < keywords.length; i++) {
	            if (SOMessageUtils.getOptinChannel(soMessage).equals(optinChannel)
	                    && keywords[i].toString().equalsIgnoreCase(optinKeyword)) {
	                LOG.debug("MO OPTIN Success");
	                return true;
	            }
	        }
	        LOG.debug("MO OPTIN Failed");
	        return false;
	    }
	 
	 /**
	  * Handles optin valid keyword case
	  * 
	  * @param exchange exchange
	  * @param msisdn msisdn
	  * @param cacheKeyToRemove cacheKeyToRemove
	  * @param soMessage soMessage
	  * @param userOptedKeyword userOptedKeyword
	  * @param responseJson responseJson
	  * @throws IOException IOException
	  * @throws JsonProcessingException JsonProcessingException
	  * @throws Exception Exception
	  */
	    private void onReceivingValidMonitoringPeriodInMO(Exchange exchange, String msisdn, String cacheKeyToRemove,
	            SOMessage soMessage, String userOptedKeyword,ObjectNode responseJson)
	            throws IOException, JsonProcessingException, Exception {
	        soMessage.setTriggerType(optinResponse);
	        soMessage.setActionType(actionTypeOptin);
	        soMessage.setActionStatus(success);
	        soMessage.setActionResponse(userOptedKeyword);
	        // To be sent to Core
	        SOMessageUtils.setHeaderValue(soMessage, "subscriberOptinTimestamp", SOMessageUtils.getUnixTimestamp());
	        SOMessageUtils.setHeaderValue(soMessage, "subscriberOptinChannel", "THIRDPARTY_SMS");
	        LOG.debug("SOMessage on receiving a valid keyword in MO: {}", soMessage);
	        String messageToDB = jsonObjectMapper.writeValueAsString(soMessage);
	        LOG.debug("Message to be send after valid MO: {}", messageToDB);
	        soMessage.setActionType(optin);
	        String messageToPRG = jsonObjectMapper.writeValueAsString(soMessage);
	        responseJson.put(responseCodeField, responseCodeSuccess);
	        exchange.getIn().setHeader("messageToPRG", messageToPRG);
	        exchange.getIn().setHeader("messageToDB", messageToDB);
	        exchange.getIn().setHeader("isMOSuccess", true);
	        exchange.getIn().setHeader("isInvalidKeyword", false);
	        LOG.debug("response json: {}  converted :{}",responseJson,jsonObjectMapper.writeValueAsString(responseJson));
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(responseJson));
	        LOG.debug("Redis object after receiving a valid MO: {}", cacheKeyToRemove);
	        swapFulfillmentOptinToProcessing(cacheKeyToRemove, msisdn);
	        // Check this condition
	        if (soMessage instanceof SOFulfillmentMessage) {
	            exchange.getIn().setHeader("offerAlreadyProvisionedByCore", true);
	        }
	    }
	    
	    /**
	     * Swapping soMessage from optin to processing pool
	     * 
	     * @param optinCacheKey redis key used to save optin message
	     * @param msisdn mobile number
	     * @throws Exception Exception
	     */
	    private void swapFulfillmentOptinToProcessing(String optinCacheKey, String msisdn)
	            throws Exception {
	        String objectToMoveToProcessing = redisConnectionPool.getObjectFromCache(optinCacheKey);
	        SONotificationMessage soNotificationMessage = jsonObjectMapper.readValue(objectToMoveToProcessing,
	                SONotificationMessage.class);
	        SOMessageUtils.setHeaderValue(soNotificationMessage, optinStatus, "PROCESSING");
	        objectToMoveToProcessing = jsonObjectMapper.writeValueAsString(soNotificationMessage);
	        String processingKey = CacheKeyUtils.generateOptinCacheKeyForProcessingFulfillment(msisdn, soNotificationMessage
	                .getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getOfferId());
	        if (StringUtils.isNotBlank(objectToMoveToProcessing)) {
	            // TTL = offer end date - current time
	            Long currentTime = Instant.now().toEpochMilli();
	            String endDate = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers()
	                    .get(0).getOfferEndDate();
	            int optinTTL = CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) + ttlBuffer;
	            redisConnectionPool.putInCache(processingKey, objectToMoveToProcessing, optinTTL);
	            String optinKeyword = SOMessageUtils.getOptinKeyword(soNotificationMessage);
	            String[] keywords = optinKeyword.split(",");
	            for (int i = 0; i < keywords.length; i++) {
	                optinCacheKey = ExternalSMSCUtils.generateOptinCacheKeyForExternalSMSC(msisdn, keywords[i].toString());
	                redisConnectionPool.putInCache(optinCacheKey, objectToMoveToProcessing, optinTTL);
	            }
	        }
	        LOG.debug("Redis: Copied SOMessage from key {} to key {}.", optinCacheKey, processingKey);
	    }
	    
	    
	    /**
	     * Handles invalid optin
	     * 
	     * @param exchange exchange
	     * @param msisdn msisdn
	     * @param soMessageAsJSON soMessageAsJSON
	     * @param soMessage fetch from cache
	     * @param userOptedKeyword userOptedKeyword
	     * @param responseJson responseJson
	     * @throws Exception Exception
	     */
	    private void onReceivingInValidOptinInMO(Exchange exchange, String msisdn, String soMessageAsJSON,
	            String userOptedKeyword, SOMessage soMessage, ObjectNode responseJson) throws Exception {
	        SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
	                soMessageTemplate, msisdn, soMessageAsJSON, optinInvalidMessage, channelName, jsonObjectMapper);
	        LOG.debug("Invalid Optin, Incorrect values");
	        optinNotificationMessage.setTriggerType(optinNotify);
	        optinNotificationMessage.setActionType(notify);
	        optinNotificationMessage.setActionStatus(success);
	        String finalMessageAsJson = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
	        responseJson.put(responseCodeField, responseCodeFail);
	        exchange.getIn().setHeader("messageSendToDbHandler",
	                createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure));
	        exchange.getIn().setHeader("isMOSuccess", false);
	        exchange.getIn().setHeader("isInvalidKeyword", false);
	        exchange.getIn().setHeader("messageToDB", finalMessageAsJson);
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(responseJson));
	    }
	 
	 /**
	     * 
	     * This message is to set triggerType, actionType,actionStatus and actionResponse for OPTINCASE
	     * for sending to DB
	     * 
	     * @param messageAsJson message created for sending optin notify message
	     * @param userOptedKeyword text sent by the user
	     * @param actionStatus success/failure
	     * @return Somessag as json for sending to DB handler
	     * @throws JsonProcessingException JsonProcessingException
	     */
	    private String createOptinResponseMessageForDb(String messageAsJson, String userOptedKeyword, String actionStatus)
	            throws JsonProcessingException {
	        SOMessage soMessageForDB = RawMessageUtils.processRawMessage(messageAsJson, jsonObjectMapper);
	        soMessageForDB.setTriggerType(optinResponse);
	        soMessageForDB.setActionType(actionTypeOptin);
	        soMessageForDB.setActionResponse(userOptedKeyword);
	        soMessageForDB.setActionStatus(actionStatus);
	        return jsonObjectMapper.writeValueAsString(soMessageForDB);
	    }
	    
	    
	    /**
	     * 
	     * Checks whether the current time is in between offer start and end dates
	     * 
	     * @param soMessage soNotificationMessage
	     * @return true/false
	     */
	    private static boolean isMOInValidTime(SOMessage soMessage) {
	        long startTime = Long.parseLong(SOMessageUtils.getOfferStartDate(soMessage));
	        long endTime = Long.parseLong(SOMessageUtils.getOfferEndDate(soMessage));
	        Calendar calendarStart = Calendar.getInstance();
	        calendarStart.setTimeInMillis(startTime);
	        Calendar calendarEnd = Calendar.getInstance();
	        calendarEnd.setTimeInMillis(endTime);
	        Calendar calendarNow = Calendar.getInstance();
	        // Get the time in local timezone
	        int hour = calendarNow.get(Calendar.HOUR_OF_DAY);
	        int minutes = calendarNow.get(Calendar.MINUTE);
	        int seconds = calendarNow.get(Calendar.SECOND);
	        // create time zone object with UTC and set the timezone
	        TimeZone tzone = TimeZone.getTimeZone("UTC");
	        calendarNow.setTimeZone(tzone);
	        // Set the current time as GMT Time
	        calendarNow.set(Calendar.HOUR_OF_DAY, hour);
	        calendarNow.set(Calendar.MINUTE, minutes);
	        calendarNow.set(Calendar.SECOND, seconds);
	        long epochNow = calendarNow.getTimeInMillis();
	        calendarNow.setTimeInMillis(epochNow);
	        LOG.debug("Calender Start:{} Calender End: {} Calender Now: {}", calendarStart, calendarEnd, calendarNow);
	        boolean result = calendarNow.after(calendarStart) && calendarNow.before(calendarEnd);
	        LOG.info("isMOInValidTime: {}", result);
	        return result;
	    }

}
