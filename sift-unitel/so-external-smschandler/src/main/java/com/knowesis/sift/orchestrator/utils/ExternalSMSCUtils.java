package com.knowesis.sift.orchestrator.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processes MO requests
 * 
 * @author SO Development Team
 */
public class ExternalSMSCUtils {

	private static final Logger LOG = LoggerFactory.getLogger(ExternalSMSCUtils.class);

	/**
	 * Generate key for optin cases
	 * 
	 * @param msisdn
	 *            msisdn
	 * @param optinKeyword
	 *            optinKeyword
	 * @return cachekey
	 */
	public static String generateOptinCacheKeyForExternalSMSC(String msisdn, String optinKeyword) {

		String key = null;
		if (StringUtils.isNotBlank(msisdn) && StringUtils.isNotBlank(optinKeyword)) {
			StringBuilder builder = new StringBuilder();
			builder.append("OPTIN-").append(msisdn).append("-").append(optinKeyword.toUpperCase());
			key = builder.toString();
		}
		LOG.debug("Generated Optin cache key {}", key);
		return key;
	}

}
