/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepared SMPP request when a SO message comes to the queue
 * 
 * @author SO Development Team
 */
@Component("MTRequestProcessor")
public class MTRequestProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${external.smsc.smstype}")
    private String smsType;
    
    @Value("${so.test.enableWhiteListing}")
    private Boolean enableWhiteListing;
    
    @Value("${so.test.whiteListedMSISDNs}")
    private String whiteListedMSISDNs;

    List<String> whiteListedMSISDNList;
    
    @Value("${external.smsc.default.shortcode}")
    private String defaultShortCode;

    /**
     * Called when Bean is refreshed by Spring
     */
    @PostConstruct
    public void onPostConstruct() {
        whiteListedMSISDNList = new ArrayList<>();
        String[] keywords = StringUtils.split(whiteListedMSISDNs, ',');
        whiteListedMSISDNList = Arrays.asList(keywords);
    }

    /**
     * Processes the So messages from queue and prepares SMPP requests
     * 
     * @param exchange camel exchange
     * @throws Exception exception
     */
    public void process(Exchange exchange) throws Exception {
        String message = exchange.getIn().getBody(String.class);
        exchange.getIn().removeHeaders("CamelHttp*");
        exchange.setProperty("OriginalRequestMessage", message);
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        String msisdn = SOMessageUtils.getMSISDN(soMessage);
        boolean isWhiteListedMsisdnOrProduction = SOMessageUtils.checkMsisdnWhiteListCondition(msisdn, enableWhiteListing,
                whiteListedMSISDNList);
        if (soMessage != null && isWhiteListedMsisdnOrProduction) {
            String textToBeSent = SOMessageUtils.getTextToBeSent(soMessage);
            String shortCode = SOMessageUtils.getSmscShortCode(soMessage);
            if(StringUtils.isBlank(shortCode)) {
                shortCode = defaultShortCode;
            }
            ObjectNode externalSmscMsgNode = jsonObjectMapper.createObjectNode();
            externalSmscMsgNode.put("phoneno", msisdn);
            externalSmscMsgNode.put("specnum", shortCode);
            externalSmscMsgNode.put("text", textToBeSent);
            exchange.getIn().setHeader("msisdn", msisdn);
            String requestString = jsonObjectMapper.writeValueAsString(externalSmscMsgNode);
            log.info("External SMSC request for msisdn {}: {}", msisdn, requestString);
            exchange.getIn().setBody(requestString);
            exchange.getIn().setHeader("CamelSmppDestAddr", msisdn);
        }
        else {
            exchange.getIn().setHeader("msisdn", msisdn);
        }
    }

}
