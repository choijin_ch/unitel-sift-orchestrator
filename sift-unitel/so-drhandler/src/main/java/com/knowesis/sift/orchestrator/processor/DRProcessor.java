/*
 * Copyright (c) 2015, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.time.Instant;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Process the MO in case of DR
 * 
 * @author SO Development Team
 */
@Component("DRProcessor")
public class DRProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    RedisConnectionPool redisConnectionPool;

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${dr.actionType.failed}")
    private String drFailedActonType;

    // SO unitel actionTypes

    @Value("${so.triggerType.offerNotify}")
    private String offerNotify;

    @Value("${so.actionType.drNotify}")
    private String drNotify;
    
    @Value("${so.fulfill.actionType.extendExpiryNotify}")
    private String extendExpiryNotify;

    @Value("${so.monitoring.ttl.additionalBufferValue}")
    private int ttlBuffer;
    
    @Override
    public void process(Exchange exchange) throws Exception {

        processDRMessage(exchange);
    }

	/**
	 * Process MT Message
	 * 
	 * @param exchange
	 *            camel exchange
	 * @throws IOException
	 *             Handle IO exception
	 * @throws JsonMappingException
	 *             Handle JsonMapping exception
	 * @throws JsonParseException
	 *             Handle JsonParse exception
	 */
	private void processDRMessage(Exchange exchange) throws JsonParseException, JsonMappingException, IOException {
		String camelSmppId = (String) exchange.getIn().getHeader("CamelSmppId");
		String monitoredKey = "SMSMT-" + camelSmppId;
		String soMessageAsJSON = redisConnectionPool.getObjectFromCache(monitoredKey);

		/**
		 * DELIVRD: Message is delivered to destination EXPIRED: Message
		 * validity period has expired. DELETED: Message has been deleted.
		 * UNDELIV: Message is undeliverable ACCEPTD: Message is in accepted
		 * state (i.e. has been manually read on behalf of the subscriber by
		 * customer service) UNKNOWN: Message is in invalid state REJECTD:
		 * Message is in a rejected state
		 */

		log.debug("Orignal request fetched with Cache Key: {}, value: {}", camelSmppId, soMessageAsJSON);

		if (soMessageAsJSON != null) {
			redisConnectionPool.delObjectFromCache(monitoredKey);
			log.debug("DR key - {} is removed from Cache.", monitoredKey);
			String deliveryStatus = (String) exchange.getIn().getHeader("DRStatus");
			SOMessage soMessage = RawMessageUtils.processRawMessage(soMessageAsJSON, jsonObjectMapper);
			if (soMessage != null) {
				processSuccessfullyRecivedDR(exchange, soMessage, deliveryStatus, monitoredKey);
				return;
			}
		}
		// soMessage or soMessage is null
		exchange.getIn().setHeader("PROPOGATE-DR", "false");
		exchange.getIn().setBody("DR-NOT-MAPPED:" + camelSmppId);

	}

	/**
	 * process all DR recived with keys found in redis
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 * @param deliveryStatus
	 *            deliveryStatus
	 * @param monitoredKey
	 *            monitoredKey
	 * @throws JsonParseException
	 *             JsonParseException
	 * @throws JsonMappingException
	 *             JsonMappingException
	 * @throws IOException
	 *             IOException
	 */
	private void processSuccessfullyRecivedDR(Exchange exchange, SOMessage soMessage, String deliveryStatus,
			String monitoredKey) throws JsonParseException, JsonMappingException, IOException {
		String msisdn = SOMessageUtils.getMSISDN(soMessage);
		exchange.getIn().setHeader("msisdn", msisdn);
		// Action type for Register Action
		soMessage.setActionType(drFailedActonType);
		soMessage = setActionStatus(soMessage, deliveryStatus, monitoredKey);

		String doubleConfirmation = SOMessageUtils.getHeaderValue(soMessage, "DOUBLECONFIRMATION");

		String actionType = SOMessageUtils.getActionType(soMessage);

		if (deliveryStatus.equals("UNDELIV")) {
			processUndeliveredMessages(exchange, soMessage, actionType, msisdn, monitoredKey, doubleConfirmation);
		} else {
			exchange.getIn().setHeader("DELIVERED", "true");
		}
		// Action type for DB entry
		soMessage.setActionType(drNotify);
		exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
		return;
	}

	/**
	 * process undelivered messages
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 * @param actionType
	 *            actionType
	 * @param msisdn
	 *            msisdn
	 * @param monitoredKey
	 *            monitoredKey
	 * @param doubleConfirmation
	 *            doubleConfirmation
	 * @throws IOException
	 *             IOException
	 */
	private void processUndeliveredMessages(Exchange exchange, SOMessage soMessage, String actionType, String msisdn,
			String monitoredKey, String doubleConfirmation) throws IOException {
		if ((offerNotify.equalsIgnoreCase(soMessage.getTriggerType()))
				|| (actionType != null && extendExpiryNotify.equalsIgnoreCase(actionType))) {
			log.info("DR failed for MSISDN: {}. Sending to CORE", msisdn);
			exchange.getIn().setHeader("OFFERNOTIFY", true);
			String messageToPrg = jsonObjectMapper.writeValueAsString(soMessage);
			exchange.getIn().setHeader("messageToPrg", messageToPrg);
			removeOptinCacheObject(exchange, soMessage, monitoredKey);
		} else if (doubleConfirmation != null) {
			log.debug("DR failed removing second level key and updating first level keys");
			exchange.getIn().setHeader("OFFERNOTIFY", false);
			deleteMultiLevelKeyAndRemoveOptinStatus(exchange, msisdn, soMessage);
		}
	}

	/**
	 * deleteMultiLevelKeyAndRemoveOptinStatus if DR fails
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @param soMessage
	 *            soMessage
	 */
	private void deleteMultiLevelKeyAndRemoveOptinStatus(Exchange exchange, String msisdn, SOMessage soMessage) {
		String optinShortCode = SOMessageUtils.getOptinShortCode(soMessage);
		String[] optinKeyWordForFirstLevel = SOMessageUtils.getHeaderValue(soMessage, "OptinKeyWord").split(",");
		String multiLevelCacheKey = CacheKeyUtils.generateMultiLevelOptinCacheKey(optinShortCode, msisdn);
		redisConnectionPool.delObjectFromCache(multiLevelCacheKey);//delete multilevelKey
		String optinCacheKey = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn,
				optinKeyWordForFirstLevel[0]);
		String soNotificationMessageAsJson = redisConnectionPool.getObjectFromCache(optinCacheKey);
		if (soNotificationMessageAsJson != null) {
			try {
				SONotificationMessage soNotificationMessageForUpdatedRedis = jsonObjectMapper
						.readValue(soNotificationMessageAsJson, SONotificationMessage.class);
				SOMessageUtils.removeHeaderValue(soNotificationMessageForUpdatedRedis, "optinStatus");
				SOMessageUtils.removeHeaderValue(soNotificationMessageForUpdatedRedis, "DOUBLECONFIRMATION");
				SOMessageUtils.removeHeaderValue(soNotificationMessageForUpdatedRedis, "OptinKeyWord");
				Long currentTime = Instant.now().toEpochMilli();
				String endDate = soNotificationMessageForUpdatedRedis.getMessage().getRequesterLocation().getLocations()
						.get(0).getOffers().get(0).getOfferEndDate();
				int optinTTL = CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) + ttlBuffer;
				for (int i = 0; i < optinKeyWordForFirstLevel.length; i++) {
					String updatedCacheKeyForOptin = CacheKeyUtils.generateOptinCacheKey(
							SOMessageUtils.getOptinShortCode(soNotificationMessageForUpdatedRedis), msisdn,
							optinKeyWordForFirstLevel[i].toString());
					log.debug("key processing: {}", optinKeyWordForFirstLevel[i].toString());
					redisConnectionPool.putInCache(updatedCacheKeyForOptin,
							jsonObjectMapper.writeValueAsString(soNotificationMessageForUpdatedRedis), optinTTL);
				}
			} catch (Exception exception) {
				log.debug("Exception occured in deleteMultiLevelKeyAndRemoveOptinStatus is:{}", exception);
			}
		}
	}

	/**
     * set Delivery status
     * 
     * @param soMessage soMessage
     * 
     * @param deliveryStatus delivery status got from smsc
     * 
     * @param monitoredKey Key to map CamelSmppId
     * 
     * @return soMessage with delivery status set
     */
    private SOMessage setActionStatus(SOMessage soMessage, String deliveryStatus, String monitoredKey) {
        if (deliveryStatus.equals("DELIVRD")) {
            soMessage.setActionStatus("SUCCESS");
        } else if (deliveryStatus.equals("UNDELIV")) {
            soMessage.setActionStatus("FAILURE");
        }
        return soMessage;
    }

    /**
     * 
     * Checks whether the delivery failed messages is OPTIN and removes it
     * 
     * @param exchange camel exchage
     * @param soMessage message from cache
     * @param monitoredKey to map camel smppId
     * @throws IOException IOException 
     * @throws JsonMappingException JsonMappingException 
     * @throws JsonParseException JsonParseException 
     */
	private void removeOptinCacheObject(Exchange exchange, SOMessage soMessage, String monitoredKey)
			throws JsonParseException, JsonMappingException, IOException {
		if (SOMessageUtils.isSoMessageOptin(soMessage)) {
			String msisdn = SOMessageUtils.getMSISDN(soMessage);
			String optinKeyword = SOMessageUtils.getOptinKeyword(soMessage);
			String[] keywords = optinKeyword.split(",");
			log.debug("Somessage for processing is:{}", soMessage);
			/*
			 * if(SOMessageUtils.getHeaderValue(soMessage, "DOUBLECONFIRMATION")
			 * != null){ log.
			 * debug("removing second level key and updating first level keys");
			 * exchange.getIn().setHeader("OFFERNOTIFY", false); String[]
			 * optinKeyWordForFirstLevel =
			 * SOMessageUtils.getHeaderValue(soMessage,
			 * "OptinKeyWord").split(","); String cacheKey =
			 * CacheKeyUtils.generateOptinCacheKey(SOMessageUtils.
			 * getOptinShortCode(soMessage), msisdn,
			 * optinKeyWordForFirstLevel[0].toString()); String
			 * messageOfFirstLevelAsJson =
			 * redisConnectionPool.getObjectFromCache(cacheKey);
			 * SONotificationMessage soNotificationMessageForUpdatedRedis =
			 * jsonObjectMapper.readValue(messageOfFirstLevelAsJson,
			 * SONotificationMessage.class); SOMessageUtils.removeHeaderValue(
			 * soNotificationMessageForUpdatedRedis, "optinStatus");
			 * SOMessageUtils.removeHeaderValue(
			 * soNotificationMessageForUpdatedRedis, "DOUBLECONFIRMATION");
			 * SOMessageUtils.removeHeaderValue(
			 * soNotificationMessageForUpdatedRedis, "OptinKeyWord"); Long
			 * currentTime = Instant.now().toEpochMilli(); String endDate =
			 * soNotificationMessageForUpdatedRedis.getMessage().
			 * getRequesterLocation().getLocations().get(0)
			 * .getOffers().get(0).getOfferEndDate(); int optinTTL =
			 * CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) +
			 * ttlBuffer; for (int i = 0; i < optinKeyWordForFirstLevel.length;
			 * i++) { String updatedCacheKeyForOptin =
			 * CacheKeyUtils.generateOptinCacheKey(SOMessageUtils.
			 * getOptinShortCode(soNotificationMessageForUpdatedRedis), msisdn,
			 * optinKeyWordForFirstLevel[i].toString());
			 * log.debug("key processing: {}",optinKeyWordForFirstLevel[i].
			 * toString());
			 * redisConnectionPool.putInCache(updatedCacheKeyForOptin,
			 * jsonObjectMapper.writeValueAsString(
			 * soNotificationMessageForUpdatedRedis), optinTTL); } }
			 */
			for (int i = 0; i < keywords.length; i++) {
				String cacheKey = CacheKeyUtils.generateOptinCacheKey(SOMessageUtils.getOptinShortCode(soMessage),
						msisdn, keywords[i].toString());
				redisConnectionPool.delObjectFromCache(cacheKey);
				log.debug("DR is failed for MSISDN {} with monitored Key {} , Removing OPTIN cache object:{}", msisdn,
						monitoredKey, cacheKey);
			}
		}
	}

}
