/*
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.exceptions;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JMSExceptionHandler
 * 
 * @author SO Development Team
 */
public class JMSExceptionHandler implements ExceptionListener {

	/*
	 * Connection Exception listener. This is called when connection breaks and
	 * no reconnect attempts are performed by MQ client runtime.
	 */
	private Logger log = LoggerFactory.getLogger(JMSExceptionHandler.class);
	/**
	 * For handling the exceptions
	 * @param e Exception
	 */
	public void onException(JMSException e) {

		log.info("JMS Reconnect failed.  Shutting down the connection ...");

		/**
		 * Set this flag to false so that the run() method will exit.
		 */

		log.error("JMS Reconnect failed.  Shutting down the connection ...", e);
	}

}
