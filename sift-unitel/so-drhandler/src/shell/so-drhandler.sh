#! /bin/bash

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#title           :so-drhandler.sh
#description     :This script manages ${project.artifactId} Process.
#author          :SIFT SO Team
#version         :${project.version}    
#usage           :bash so-drhandler.sh
#handler_version :${project.version}-release
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

source ./so-env.sh

: "${JAVA_HOME?JAVA_HOME not set}"
: "${SO_HOME?SO_HOME not set}"

# ***********************************************
SCRIPT_HOME=$(dirname "$0")

echo "${handler-name}-${project.version}"
export API_CONF=$SO_HOME'/conf'
export LOG_HOME=$SO_HOME'/log'
export LOGBACK_XML=$API_CONF'/${handler-name}-logback.xml'
export FLOW_LOC=$SO_HOME'/flow'
export CAMEL_ENCRYPTION_PASSWORD=secret

# OS specific support.
cygwin=false
case "`uname`" in
   CYGWIN*) cygwin=true ;;
esac

if $cygwin ; then
    echo 'cygwin shell detected'
    API_CONF=`cygpath -w "$API_CONF"`
    LOG_HOME=`cygpath -w "$LOG_HOME"`
    LOGBACK_XML=`cygpath -w "$LOGBACK_XML"`
    FLOW_LOC=`cygpath -w "$FLOW_LOC"`
fi

CP=$(echo ../lib/*.jar | tr ' ' ':')
export CLASSPATH=$API_CONF':'$CP':'$FLOW_LOC

ARGS='-Dlogback.configurationFile='$LOGBACK_XML' -DCONFIG_HOME='$API_CONF' -DLOG_HOME='$LOG_HOME' -jar '$FLOW_LOC'/${project.artifactId}-${project.version}.jar'
DAEMON=$JAVA_HOME/bin/java

case "$1" in
start)
    (
		pid=`pgrep -f '.+${project.artifactId}-.+.jar'`
		if [ ! -z $pid ]; then 
			echo "process found with pid "$pid
			echo "use $0 stop"
		else 
			echo 'Starting...'
    		$DAEMON $ARGS > $LOG_HOME/${handler-name}_sysout.log 2>&1
			echo $!
		fi
	) & 
;;

status)
	pid=`pgrep -f '.+${project.artifactId}-.+.jar'`
	if [ ! -z $pid ]; then 
		echo "process found with pid "$pid
	else 
		echo "process not found"
	fi
;;

stop)
	pid=`pgrep -f '.+${project.artifactId}-.+.jar'`
	if [ ! -z $pid ]; then 
		echo "stopping ..."$pid
		pkill -f '.+${project.artifactId}-.+.jar'
	else 
		echo "process not found"
	fi
;;

kill)
	pid=`pgrep -f '.+${project.artifactId}-.+.jar'`
	if [ ! -z $pid ]; then 
		echo "killing ..."$pid
		pkill -9 -f '.+${project.artifactId}-.+.jar'
	else 
		echo "process not found"
	fi
;;

log)
	tail -f $LOG_HOME/${handler-name}.log
;;

restart)
    $0 stop
    $0 start
;;

*)
    echo "Usage: $0 {status|start|stop}"
    exit 1
esac
unset CAMEL_ENCRYPTION_PASSWORD
