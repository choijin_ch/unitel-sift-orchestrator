/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Represent a row of Payload table in SO database
 * 
 * @author SO Development Team
 */
public class SOEventPayload implements Serializable {

    private static final long serialVersionUID = 1L;

    private String flowId;

    private String payloadFieldName;

    private String payloadFieldValue;

    private String payloadFieldDatatype;

    private String triggerType;

    private String actionType;

    private Timestamp createdOn;
    
    private Timestamp updatedOn;

	/**
	 * @return the flowId
	 */
	public String getFlowId() {
		return flowId;
	}

	/**
	 * @param flowId the flowId to set
	 */
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	/**
	 * @return the payloadFieldName
	 */
	public String getPayloadFieldName() {
		return payloadFieldName;
	}

	/**
	 * @param payloadFieldName the payloadFieldName to set
	 */
	public void setPayloadFieldName(String payloadFieldName) {
		this.payloadFieldName = payloadFieldName;
	}

	/**
	 * @return the payloadFieldValue
	 */
	public String getPayloadFieldValue() {
		return payloadFieldValue;
	}

	/**
	 * @param payloadFieldValue the payloadFieldValue to set
	 */
	public void setPayloadFieldValue(String payloadFieldValue) {
		this.payloadFieldValue = payloadFieldValue;
	}

	/**
	 * @return the payloadFieldDatatype
	 */
	public String getPayloadFieldDatatype() {
		return payloadFieldDatatype;
	}

	/**
	 * @param payloadFieldDatatype the payloadFieldDatatype to set
	 */
	public void setPayloadFieldDatatype(String payloadFieldDatatype) {
		this.payloadFieldDatatype = payloadFieldDatatype;
	}

	/**
	 * @return the triggerType
	 */
	public String getTriggerType() {
		return triggerType;
	}

	/**
	 * @param triggerType the triggerType to set
	 */
	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}

	/**
	 * @return the actionType
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param actionType the actionType to set
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	/**
	 * @return the createdOn
	 */
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}


	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	 @Override
	public String toString() {
	      return new ToStringBuilder(this).append("flowId", flowId).append("payloadFieldName", payloadFieldName)
	                .append("payloadFieldValue", payloadFieldValue).append("payloadFieldDatatype", payloadFieldDatatype).append("triggerType", triggerType)
	                .append("actionType", actionType).append("createdOn", createdOn)
	                .append("updatedOn", updatedOn).toString();

	}

}