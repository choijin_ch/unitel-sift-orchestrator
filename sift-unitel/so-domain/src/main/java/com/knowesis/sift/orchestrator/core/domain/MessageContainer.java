/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.core.domain;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO for Event_Payload
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageContainer implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("channelName")
    private String channelName;

    @JsonProperty("contactWindows")
    private String contactWindows;

    @JsonProperty("shortCode")
    private String shortCode;

    @JsonProperty("optInShortCode")
    private String optInShortCode;

    @JsonProperty("optInKeyWord")
    private String optInKeyWord;

    @JsonProperty("optInChannel")
    private String optInChannel;

    @JsonProperty("messages")
    private List<Message> messages;

    /**
     * Getter for ChannelName
     * 
     * @return the ChannelName
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * Setter for ChannelName
     * 
     * @param ChannelName the ChannelName to set
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    /**
     * Getter for ContactWindows
     * 
     * @return the ContactWindows
     */
    public String getContactWindows() {
        return contactWindows;
    }

    /**
     * Setter for ContactWindows
     * 
     * @param ContactWindows the ContactWindows to set
     */
    public void setContactWindows(String contactWindows) {
        this.contactWindows = contactWindows;
    }

    /**
     * Getter for ShortCode
     * 
     * @return the ShortCode
     */
    public String getShortCode() {
        return shortCode;
    }

    /**
     * Setter for ShortCode
     * 
     * @param ShortCode the ShortCode to set
     */
    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    /**
     * Getter for OptInShortCode
     * 
     * @return the OptInShortCode
     */
    public String getOptInShortCode() {
        return optInShortCode;
    }

    /**
     * Setter for OptInShortCode
     * 
     * @param OptInShortCode the OptInShortCode to set
     */
    public void setOptInShortCode(String optInShortCode) {
        this.optInShortCode = optInShortCode;
    }

    /**
     * Getter for OptInKeyWord
     * 
     * @return the OptInKeyWord
     */
    public String getOptInKeyWord() {
        return optInKeyWord;
    }

    /**
     * Setter for OptInKeyWord
     * 
     * @param OptInKeyWord the OptInKeyWord to set
     */
    public void setOptInKeyWord(String optInKeyword) {
        this.optInKeyWord = optInKeyword;
    }

    /**
     * Getter for OptInChannel
     * 
     * @return the OptInChannel
     */
    public String getOptInChannel() {
        return optInChannel;
    }

    /**
     * Setter for OptInChannel
     * 
     * @param OptInChannel the OptInChannel to set
     */
    public void setOptInChannel(String optInChannel) {
        this.optInChannel = optInChannel;
    }

    /**
     * Getter for Message
     * 
     * @return the Message
     */
    public List<Message> getMessages() {
        return messages;
    }

    /**
     * Setter for Message
     * 
     * @param Message the Message to set
     */
    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("channelName", channelName)
                .append("contactWindows", contactWindows).append("shortCode", shortCode)
                .append("optInShortCode", optInShortCode).append("optInKeyword", optInKeyWord)
                .append("optInChannel", optInChannel).append("messages", messages).toString();
    }
}
