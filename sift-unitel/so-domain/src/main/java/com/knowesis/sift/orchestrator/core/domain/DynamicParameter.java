/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.core.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents Dynamic Properties in Core fulfillment Event
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DynamicParameter implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("rewardValidityHours")
    private Integer rewardValidityHours;
    
    @JsonProperty("rewardValidityDays")
    private Integer rewardValidityDays;

    @JsonProperty("operatorType")
    private String operatorType;

    @JsonProperty("type")
    private String type;

    @JsonProperty("value")
    private String value;

    @JsonProperty("key")
    private String key;

    @JsonProperty("resolvedValue")
    private String resolvedValue;
    

    @JsonProperty("targetSystem")
    private String targetSystem;

    /**
     * Getter for rewardValidityHours
     * 
     * @return the rewardValidityHours
     */
    public Integer getRewardValidityHours() {
		return rewardValidityHours;
	}

    /**
     * Setter for rewardValidityHours
     * 
     * @param rewardValidityHours the rewardValidityHours to set
     */
	public void setRewardValidityHours(Integer rewardValidityHours) {
		this.rewardValidityHours = rewardValidityHours;
	}

	/**
     * Getter for rewardValidityDays
     * 
     * @return the rewardValidityDays
     */
    public Integer getRewardValidityDays() {
        return rewardValidityDays;
    }

    /**
     * Setter for rewardValidityDays
     * 
     * @param rewardValidityDays the rewardValidityDays to set
     */
    public void setRewardValidityDays(Integer rewardValidityDays) {
        this.rewardValidityDays = rewardValidityDays;
    }

    /**
     * Getter for operatorType
     * 
     * @return the operatorType
     */
    public String getOperatorType() {
        return operatorType;
    }

    /**
     * Setter for operatorType
     * 
     * @param operatorType the operatorType to set
     */
    public void setOperatorType(String operatorType) {
        this.operatorType = operatorType;
    }

    /**
     * Getter for type
     * 
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Setter for type
     * 
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Getter for value
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter for value
     * 
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Getter for key
     * 
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Setter for key
     * 
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Getter for resolvedValue
     * 
     * @return the resolvedValue
     */
    public String getResolvedValue() {
        return resolvedValue;
    }

    /**
     * Setter for resolvedValue
     * 
     * @param resolvedValue the resolvedValue to set
     */
    public void setResolvedValue(String resolvedValue) {
        this.resolvedValue = resolvedValue;
    }

    /**
     * Getter for targetSystem
     * 
     * @return the targetSystem
     */
    public String getTargetSystem() {
		return targetSystem;
	}
    /**
     * Setter for targetSystem
     * 
     * @param targetSystem the targetSystem to set
     */
	public void setTargetSystem(String targetSystem) {
		this.targetSystem = targetSystem;
	}

	/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("rewardValidityDays", rewardValidityDays).append("operatorType", operatorType)
                .append("type", type).append("value", value).append("key", key).append("resolvedValue", resolvedValue)
                .append("targetSystem", targetSystem).toString();
    }

}
