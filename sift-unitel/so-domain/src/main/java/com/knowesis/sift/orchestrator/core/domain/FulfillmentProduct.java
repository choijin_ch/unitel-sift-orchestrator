/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.core.domain;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represent FulfillmentProduct in fulfillment event
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FulfillmentProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("productId")
    private String productId;

    @JsonProperty("targetSystem")
    private String targetSystem;

    @JsonProperty("productCriteria")
    private String productCriteria;

    @JsonProperty("dynamicParameters")
    private List<DynamicParameter> dynamicParameters;

    /**
     * Getter for productId
     * 
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Setter for productId
     * 
     * @param productId the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * Getter for targetSystem
     * 
     * @return the targetSystem
     */
    public String getTargetSystem() {
        return targetSystem;
    }

    /**
     * Setter for targetSystem
     * 
     * @param targetSystem the targetSystem to set
     */
    public void setTargetSystem(String targetSystem) {
        this.targetSystem = targetSystem;
    }

    /**
     * Getter for productCriteria
     * 
     * @return the productCriteria
     */
    public String getProductCriteria() {
        return productCriteria;
    }

    /**
     * Setter for productCriteria
     * 
     * @param productCriteria the productCriteria to set
     */
    public void setProductCriteria(String productCriteria) {
        this.productCriteria = productCriteria;
    }

    /**
     * Getter for dynamicParameters
     * 
     * @return the dynamicParameters
     */
    public List<DynamicParameter> getDynamicParameters() {
        return dynamicParameters;
    }

    /**
     * Setter for dynamicParameters
     * 
     * @param dynamicParameters the dynamicParameters to set
     */
    public void setDynamicParameters(List<DynamicParameter> dynamicParameters) {
        this.dynamicParameters = dynamicParameters;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("productId", productId)
                .append("targetSystem", targetSystem).append("dynamicParameters", dynamicParameters)
                .append("productCriteria", productCriteria).toString();
    }

}
