/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Base class for payload in SO
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SOMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("metaInfo")
    private MetaInfo metaInfo;

    @JsonProperty("ACTION_STATUS")
    private String actionStatus;
    
    @JsonProperty("eventResponse")
    private SOEventResponse eventResponse;

    @JsonProperty("TRIGGER_TYPE")
    private String triggerType;

    @JsonProperty("ACTION_TYPE")
    private String actionType;
    
    @JsonProperty("ACTION_RESPONSE")
    private String actionResponse;

    /**
     * Getter for metaInfo
     * 
     * @return the metaInfo
     */
    public MetaInfo getMetaInfo() {
        return metaInfo;
    }

    /**
     * Setter for metaInfo
     * 
     * @param metaInfo the metaInfo to set
     */
    public void setMetaInfo(MetaInfo metaInfo) {
        this.metaInfo = metaInfo;
    }

    /**
     * Getter for actionStatus
     * 
     * @return the actionStatus
     */

    public String getActionStatus() {
        return actionStatus;
    }

    /**
     * Setter for actionStatus
     * 
     * @param actionStatus the actionStatus to set
     */

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    /**
     * Getter for triggerType
     * 
     * @return the triggerType
     */

    public String getTriggerType() {
        return triggerType;
    }

    /**
     * Setter for triggerType
     * 
     * @param triggerType the triggerType to set
     */
    public void setTriggerType(String triggerType) {
        this.triggerType = triggerType;
    }

    /**
     * Getter for actionType
     * 
     * @return the actionType
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Setter for actionType
     * 
     * @param actionType the actionType to set
     */
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionResponse() {
        return actionResponse;
    }

    public void setActionResponse(String actionResponse) {
        this.actionResponse = actionResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        return new ToStringBuilder(this).append("metaInfo", metaInfo).append("triggerType", triggerType)
                .append("actionType", actionType).append("actionStatus", actionStatus).toString();
    }
}
