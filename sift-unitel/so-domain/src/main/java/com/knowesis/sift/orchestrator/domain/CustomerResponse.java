/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Specifies the customer response from MO-OPTIN flow
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("responseChannel")
    private String responseChannel;

    @JsonProperty("reponseText")
    private String reponseText;
    
    @JsonProperty("reponseReferenceNumber")
    private String reponseReferenceNumber;
    
    @JsonProperty("reponseShortCode")
    private String reponseShortCode;

   

    /**
     * Getter for responseChannel
     * @return the responseChannel
     */
    public String getResponseChannel() {
        return responseChannel;
    }

    /**
     * Setter for responseChannel
     * @param responseChannel the responseChannel to set
     */
    public void setResponseChannel(String responseChannel) {
        this.responseChannel = responseChannel;
    }


    /**
     * Getter for reponseText
     * @return the reponseText
     */
    public String getReponseText() {
        return reponseText;
    }

    /**
     * Setter for reponseText
     * @param reponseText the reponseText to set
     */
    public void setReponseText(String reponseText) {
        this.reponseText = reponseText;
    }

    /**
     * Getter for reponseReferenceNumber
     * @return the reponseReferenceNumber
     */
    public String getReponseReferenceNumber() {
        return reponseReferenceNumber;
    }

    /**
     * Setter for reponseReferenceNumber
     * @param reponseReferenceNumber the reponseReferenceNumber to set
     */
    public void setReponseReferenceNumber(String reponseReferenceNumber) {
        this.reponseReferenceNumber = reponseReferenceNumber;
    }

    /**
     * Getter for reponseShortCode
     * @return the reponseShortCode
     */
    public String getReponseShortCode() {
        return reponseShortCode;
    }

    /**
     * Setter for reponseShortCode
     * @param reponseShortCode the reponseShortCode to set
     */
    public void setReponseShortCode(String reponseShortCode) {
        this.reponseShortCode = reponseShortCode;
    }
    
    @Override
    public String toString() {
        return "CustomerResponse [responseChannel=" + responseChannel + ", reponseText=" + reponseText
                + ", reponseReferenceNumber=" + reponseReferenceNumber + ", reponseShortCode=" + reponseShortCode + "]";
    }

}
