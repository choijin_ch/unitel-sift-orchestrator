/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.core.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO for Event_Payload
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Offers implements Serializable  {

	private static final long serialVersionUID = 1L;

	@JsonProperty("MSISDN")
	private String msisdn;

	@JsonProperty("SUBSCRIBER_ID")
	private String subscriberId;

	@JsonProperty("CATEGORY")
	private String category;

	@JsonProperty("PROGRAM_ID")
	private String programId;

	@JsonProperty("OFFER_ID")
	private String offerId;

	@JsonProperty("EVENT_ID")
	private String eventId;

	@JsonProperty("EVENT_TIMESTAMP")
	private String eventTimeStamp;

	@JsonProperty("OFFER_DESCRIPTION")
	private String offerDescription;

	@JsonProperty("OFFER_TYPE")
	private String offerType;

	@JsonProperty("OFFER_SCORE")
	private String offerScore;

	@JsonProperty("APPLICABLE_LOCATIONS")
	private String applicableLocations;

	@JsonProperty("NEW_OFFER_FLAG")
	private String newOfferFlag;

	@JsonProperty("EVENT_PAYLOAD")
	private List<EventPayload> eventPayload;

	@JsonProperty("NOTIFICATION_MESSAGES")
	private List<MessageContainer> notificationMessages;

	@JsonProperty("OFFER_START_DATE")
	private String offerStartDate;

	@JsonProperty("OFFER_END_DATE")
	private String offerEndDate;

	@JsonProperty("OFFER_PAYLOAD")
	private Map<String, String> offerPayload;

	@JsonProperty("FLOW_ID")
	private String flowId;

	@JsonProperty("IS_CONTROL")
	private String isControl;

	@JsonProperty("IS_SIMULATED")
	private String isSimulated;

	@JsonProperty("COUNT_AS_CONTACT")
	private String countAsContact;

	@JsonProperty("PARTICIPATION_TYPE")
	private String participationType;
	
	@JsonProperty("FULFILMENT_TYPE")
	private String fulfillmentType;
	
	@JsonProperty("ORIG_REQUEST_ID")
	private String origRequestId;
	
	@JsonProperty("OFFER_END_DATE_LTZ")
	private String offerEndDateLTZ;
	
	@JsonProperty("SELECTED_OFFER_CATEGORY") 
	private String selectedOfferCategory;
	
	@JsonProperty("FULFILLMENT_STATUS")
	private String fulfillmentStatus;

	/**
	 * Getter for Msisdn
	 * 
	 * @return the Msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * Setter for Msisdn
	 * 
	 * @param Msisdn
	 *            the Msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * Getter for SubscriberId
	 * 
	 * @return the SubscriberId
	 */
	public String getSubscriberId() {
		return subscriberId;
	}

	/**
	 * Setter for SubscriberId
	 * 
	 * @param SubscriberId
	 *            the SubscriberId to set
	 */
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	/**
	 * Getter for Category
	 * 
	 * @return the Category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * Setter for Category
	 * 
	 * @param Category
	 *            the Category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * Getter for ProgramId
	 * 
	 * @return the ProgramId
	 */
	public String getProgramId() {
		return programId;
	}

	/**
	 * Setter for ProgramId
	 * 
	 * @param ProgramId
	 *            the ProgramId to set
	 */
	public void setProgramId(String programId) {
		this.programId = programId;
	}

	/**
	 * Getter for OfferId
	 * 
	 * @return the OfferId
	 */
	public String getOfferId() {
		return offerId;
	}

	/**
	 * Setter for OfferId
	 * 
	 * @param OfferId
	 *            the OfferId to set
	 */
	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	/**
	 * Getter for EventId
	 * 
	 * @return the EventId
	 */
	public String getEventId() {
		return eventId;
	}

	/**
	 * Setter for EventId
	 * 
	 * @param EventId
	 *            the EventId to set
	 */
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	/**
	 * Getter for EventTimeStamp
	 * 
	 * @return the EventTimeStamp
	 */
	public String getEventTimeStamp() {
		return eventTimeStamp;
	}

	/**
	 * Setter for EventTimeStamp
	 * 
	 * @param EventTimeStamp
	 *            the EventTimeStamp to set
	 */
	public void setEventTimeStamp(String eventTimeStamp) {
		this.eventTimeStamp = eventTimeStamp;
	}

	/**
	 * Getter for OfferDescription
	 * 
	 * @return the OfferDescription
	 */
	public String getOfferDescription() {
		return offerDescription;
	}

	/**
	 * Setter for OfferDescription
	 * 
	 * @param OfferDescription
	 *            the OfferDescription to set
	 */
	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}

	/**
	 * Getter for OfferType
	 * 
	 * @return the OfferType
	 */
	public String getOfferType() {
		return offerType;
	}

	/**
	 * Setter for OfferType
	 * 
	 * @param OfferType
	 *            the OfferType to set
	 */
	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	/**
	 * Getter for OfferScore
	 * 
	 * @return the OfferScore
	 */
	public String getOfferScore() {
		return offerScore;
	}

	/**
	 * Setter for OfferScore
	 * 
	 * @param OfferScore
	 *            the OfferScore to set
	 */
	public void setOfferScore(String offerScore) {
		this.offerScore = offerScore;
	}

	/**
	 * Getter for ApplicableLocations
	 * 
	 * @return the ApplicableLocations
	 */
	public String getApplicableLocations() {
		return applicableLocations;
	}

	/**
	 * Setter for ApplicableLocations
	 * 
	 * @param ApplicableLocations
	 *            the ApplicableLocations to set
	 */
	public void setApplicableLocations(String applicableLocations) {
		this.applicableLocations = applicableLocations;
	}

	/**
	 * Getter for NewOfferFlag
	 * 
	 * @return the NewOfferFlag
	 */
	public String getNewOfferFlag() {
		return newOfferFlag;
	}

	/**
	 * Setter for NewOfferFlag
	 * 
	 * @param NewOfferFlag
	 *            the NewOfferFlag to set
	 */
	public void setNewOfferFlag(String newOfferFlag) {
		this.newOfferFlag = newOfferFlag;
	}

	/**
	 * Getter for EventPayload
	 * 
	 * @return the EventPayload
	 */
	public List<EventPayload> getEventPayload() {
		return eventPayload;
	}

	/**
	 * Setter for EventPayload
	 * 
	 * @param EventPayload
	 *            the EventPayload to set
	 */
	public void setEventPayload(List<EventPayload> eventPayload) {
		this.eventPayload = eventPayload;
	}

	/**
	 * Getter for MessageContainer
	 * 
	 * @return the MessageContainer
	 */
	public List<MessageContainer> getNotificationMessages() {
		return notificationMessages;
	}

	/**
	 * Setter for MessageContainer
	 * 
	 * @param MessageContainer
	 *            the MessageContainer to set
	 */
	public void setNotificationMessages(List<MessageContainer> notificationMessages) {
		this.notificationMessages = notificationMessages;
	}

	/**
	 * Getter for OfferStartDate
	 * 
	 * @return the OfferStartDate
	 */
	public String getOfferStartDate() {
		return offerStartDate;
	}

	/**
	 * Setter for OfferStartDate
	 * 
	 * @param OfferStartDate
	 *            the OfferStartDate to set
	 */
	public void setOfferStartDate(String offerStartDate) {
		this.offerStartDate = offerStartDate;
	}

	/**
	 * Getter for OfferEndDate
	 * 
	 * @return the OfferEndDate
	 */
	public String getOfferEndDate() {
		return offerEndDate;
	}

	/**
	 * Setter for OfferEndDate
	 * 
	 * @param OfferEndDate
	 *            the OfferEndDate to set
	 */
	public void setOfferEndDate(String offerEndDate) {
		this.offerEndDate = offerEndDate;
	}

	/**
	 * Getter for OfferPayload
	 * 
	 * @return the OfferPayload
	 */
	public Map<String, String> getOfferPayload() {
		return offerPayload;
	}

	/**
	 * Setter for OfferPayload
	 * 
	 * @param OfferPayload
	 *            the OfferPayload to set
	 */
	public void setOfferPayload(Map<String, String> offerPayload) {
		this.offerPayload = offerPayload;
	}

	/**
	 * Getter for FlowId
	 * 
	 * @return the FlowId
	 */
	public String getFlowId() {
		return flowId;
	}

	/**
	 * Setter for FlowId
	 * 
	 * @param FlowId
	 *            the FlowId to set
	 */
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	/**
	 * Getter for IsControl
	 * 
	 * @return the IsControl
	 */
	public String getIsControl() {
		return isControl;
	}

	/**
	 * Setter for IsControl
	 * 
	 * @param IsControl
	 *            the IsControl to set
	 */
	public void setIsControl(String isControl) {
		this.isControl = isControl;
	}

	/**
	 * Getter for IsSimulated
	 * 
	 * @return the IsSimulated
	 */
	public String getIsSimulated() {
		return isSimulated;
	}

	/**
	 * Setter for IsSimulated
	 * 
	 * @param IsSimulated
	 *            the IsSimulated to set
	 */
	public void setIsSimulated(String isSimulated) {
		this.isSimulated = isSimulated;
	}

	/**
	 * Getter for CountAsContact
	 * 
	 * @return the CountAsContact
	 */
	public String getCountAsContact() {
		return countAsContact;
	}

	/**
	 * Setter for CountAsContact
	 * 
	 * @param CountAsContact
	 *            the CountAsContact to set
	 */
	public void setCountAsContact(String countAsContact) {
		this.countAsContact = countAsContact;
	}

	/**
	 * Getter for ParticipationType
	 * 
	 * @return the ParticipationType
	 */
	public String getParticipationType() {
		return participationType;
	}

	/**
	 * Setter for ParticipationType
	 * 
	 * @param ParticipationType
	 *            the ParticipationType to set
	 */
	public void setParticipationType(String participationType) {
		this.participationType = participationType;
	}

	/**
	 * Getter for FulfillmentType
	 * 
	 * @return the FulfillmentType
	 */
	public String getFulfillmentType() {
		return fulfillmentType;
	}

	/**
	 * Setter for FulfillmentType
	 * 
	 * @param FulfillmentType
	 *            the FulfillmentType to set
	 */
	public void setFulfillmentType(String fulfillmentType) {
		this.fulfillmentType = fulfillmentType;
	}

	/**
	 * Getter for OrigRequestId
	 * 
	 * @return the OrigRequestId
	 */
	public String getOrigRequestId() {
		return origRequestId;
	}

	/**
	 * Setter for OrigRequestId
	 * 
	 * @param OrigRequestId
	 *            the OrigRequestId to set
	 */
	public void setOrigRequestId(String origRequestId) {
		this.origRequestId = origRequestId;
	}

	/**
	 * Getter for OfferEndDateLTZ
	 * 
	 * @return the OfferEndDateLTZ
	 */
	public String getOfferEndDateLTZ() {
		return offerEndDateLTZ;
	}

	/**
	 * Setter for OfferEndDateLTZ
	 * 
	 * @param OfferEndDateLTZ
	 *            the OfferEndDateLTZ to set
	 */
	public void setOfferEndDateLTZ(String offerEndDateLTZ) {
		this.offerEndDateLTZ = offerEndDateLTZ;
	}

	/**
	 * Getter for SelectedOfferCategory
	 * 
	 * @return the SelectedOfferCategory
	 */
	public String getSelectedOfferCategory() {
		return selectedOfferCategory;
	}

	/**
	 * Setter for SelectedOfferCategory
	 * 
	 * @param SelectedOfferCategory
	 *            the SelectedOfferCategory to set
	 */
	public void setSelectedOfferCategory(String selectedOfferCategory) {
		this.selectedOfferCategory = selectedOfferCategory;
	}

	/**
	 * Getter for FulfillmentStatus
	 * 
	 * @return the FulfillmentStatus
	 */
	public String getFulfillmentStatus() {
		return fulfillmentStatus;
	}

	/**
	 * Setter for FulfillmentStatus
	 * 
	 * @param FulfillmentStatus
	 *            the FulfillmentStatus to set
	 */
	public void setFulfillmentStatus(String fulfillmentStatus) {
		this.fulfillmentStatus = fulfillmentStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("MSISDN", msisdn)
				.append("SUBSCRIBER_ID", subscriberId).append("CATEGORY", category).append("PROGRAM_ID", programId)
				.append("OFFER_ID", offerId).append("EVENT_ID", eventId).append("EVENT_TIMESTAMP", eventTimeStamp)
				.append("OFFER_DESCRIPTION", offerDescription).append("OFFER_TYPE", offerType)
				.append("OFFER_SCORE", offerScore).append("APPLICABLE_LOCATIONS", applicableLocations)
				.append("NEW_OFFER_FLAG", newOfferFlag).append("EVENT_PAYLOAD", eventPayload)
				.append("NOTIFICATION_MESSAGES", notificationMessages).append("OFFER_START_DATE", offerStartDate)
				.append("OFFER_END_DATE", offerEndDate).append("OFFER_PAYLOAD", offerPayload).append("FLOW_ID", flowId)
				.append("IS_CONTROL", isControl).append("IS_SIMULATED", isSimulated)
				.append("COUNT_AS_CONTACT", countAsContact).append("PARTICIPATION_TYPE", participationType)
				.append("FULFILMENT_TYPE",fulfillmentType).append("ORIG_REQUEST_ID",origRequestId)
				.append("OFFER_END_DATE_LTZ",offerEndDateLTZ).append("SELECTED_OFFER_CATEGORY",selectedOfferCategory)
				.append("FULFILLMENT_STATUS",fulfillmentStatus).toString();
	}

}
