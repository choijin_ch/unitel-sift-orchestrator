/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Specifies the header information in payload (transactional attributes)
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetaInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("serviceName")
    private String serviceName;

    @JsonProperty("channelName")
    private String channelName;

    // this should be a map for easy processing
    @JsonProperty("headers")
    private List<Header> headers;

    @JsonProperty("correlationId")
    private String correlationId;

    @JsonProperty("stepName")
    private String stepName;

    /**
     * Getter for serviceName
     * 
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Setter for serviceName
     * 
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * Getter for channelName
     * 
     * @return the channelName
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * Setter for channelName
     * 
     * @param channelName the channelName to set
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    /**
     * Getter for headers
     * 
     * @return the headers
     */
    public List<Header> getHeaders() {
        return headers;
    }

    /**
     * Setter for headers
     * 
     * @param headers the headers to set
     */
    public void setHeaders(List<Header> headers) {
        this.headers = headers;
    }

    /**
     * Getter for correlationId
     * 
     * @return the correlationId
     */
    public String getCorrelationId() {
        return correlationId;
    }

    /**
     * Setter for correlationId
     * 
     * @param correlationId the correlationId to set
     */
    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    /**
     * Getter for stepName
     * 
     * @return the stepName
     */
    public String getStepName() {
        return stepName;
    }

    /**
     * Setter for stepName
     * 
     * @param stepName the stepName to set
     */
    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("serviceName", serviceName).append("channelName", channelName)
                .append("headers", headers).append("correlationId", correlationId).append("stepName", stepName)
                .toString();
    }

}
