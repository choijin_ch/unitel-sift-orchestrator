/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.core.domain;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represent Core fulfillment Event
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class SoRegisterAction implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("PACKAGEID")
    private String packageId;

    @JsonProperty("MSISDN")
    private String msisdn;

    @JsonProperty("EVENTID")
    private String eventId;

    @JsonProperty("FLOW_ID")
    private String flowId;

    @JsonProperty("SUBSCRIBER_ID")
    private String subscriberId;

    @JsonProperty("RECORD_TYPE")
    private String recordType;

    @JsonProperty("PROGRAM_CODE")
    private String programCode;

    @JsonProperty("TIMESTAMP")
    private String timeStamp;

    @JsonProperty("OFFER_CODE")
    private String offerCode;

    @JsonProperty("OPTIN_SHORTCODE")
    private String optinShortCode;

    @JsonProperty("CHANNEL_TYPE")
    private String channelType;

    @JsonProperty("ACTION_TYPE")
    private String actionType;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("NOMINATOR_MSISDN")
    private String nominatorMsisdn;
    
    @JsonProperty("NOMINATION_COUNT")
    private String nominationCount;
    
    @JsonProperty("NOMINATION_RECEIVEDCOUNT")
    private String nominationReceivedCount;

    @JsonProperty("FULFILLMENT_PRODUCTS")
    private List<FulfillmentProduct> fulfillmentProducts;

    /**
     * Getter for PackageId
     * 
     * @return the PackageId
     */
    public String getPackageId() {
        return packageId;
    }

    /**
     * Setter for PackageId
     * 
     * @param PackageId the PackageId to set
     */
    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    /**
     * Getter for Msisdn
     * 
     * @return the Msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Setter for Msisdn
     * 
     * @param Msisdn the Msisdn to set
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * Getter for EventId
     * 
     * @return the EventId
     */
    public String getEventId() {
        return eventId;
    }

    /**
     * Setter for EventId
     * 
     * @param EventId the EventId to set
     */
    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    /**
     * Getter for FlowId
     * 
     * @return the FlowId
     */
    public String getFlowId() {
        return flowId;
    }

    /**
     * Setter for FlowId
     * 
     * @param FlowId the FlowId to set
     */
    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    /**
     * Getter for SubscriberId
     * 
     * @return the SubscriberId
     */
    public String getSubscriberId() {
        return subscriberId;
    }

    /**
     * Setter for SubscriberId
     * 
     * @param SubscriberId the SubscriberId to set
     */
    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }

    /**
     * Getter for RecordType
     * 
     * @return the RecordType
     */
    public String getRecordType() {
        return recordType;
    }

    /**
     * Setter for RecordType
     * 
     * @param RecordType the RecordType to set
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    /**
     * Getter for ProgramCode
     * 
     * @return the ProgramCode
     */
    public String getProgramCode() {
        return programCode;
    }

    /**
     * Setter for ProgramCode
     * 
     * @param ProgramCode the ProgramCode to set
     */
    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    /**
     * Getter for TimeStamp
     * 
     * @return the TimeStamp
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * Setter for TimeStamp
     * 
     * @param TimeStamp the TimeStamp to set
     */
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * Getter for OfferCode
     * 
     * @return the OfferCode
     */
    public String getOfferCode() {
        return offerCode;
    }

    /**
     * Setter for OfferCode
     * 
     * @param OfferCode the OfferCode to set
     */
    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }

    /**
     * Getter for OptinShortCode
     * 
     * @return the OptinShortCode
     */
    public String getOptinShortCode() {
        return optinShortCode;
    }

    /**
     * Setter for OptinShortCode
     * 
     * @param OptinShortCode the OptinShortCode to set
     */
    public void setOptinShortCode(String optinShortCode) {
        this.optinShortCode = optinShortCode;
    }

    /**
     * Getter for ChannelType
     * 
     * @return the ChannelType
     */
    public String getChannelType() {
        return channelType;
    }

    /**
     * Setter for ChannelType
     * 
     * @param ChannelType the ChannelType to set
     */
    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    /**
     * Getter for ActionType
     * 
     * @return the ActionType
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Setter for ActionType
     * 
     * @param ActionType the ActionType to set
     */
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    /**
     * Getter for Status
     * 
     * @return the Status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter for Status
     * 
     * @param Status the Status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Getter for NominatorMsisdn
     * 
     * @return the NominatorMsisdn
     */
    public String getNominatorMsisdn() {
        return nominatorMsisdn;
    }

    /**
     * Setter for NominatorMsisdn
     * 
     * @param NominatorMsisdn the NominatorMsisdn to set
     */
    public void setNominatorMsisdn(String nominatorMsisdn) {
        this.nominatorMsisdn = nominatorMsisdn;
    }

    public String getNominationCount() {
        return nominationCount;
    }

    public void setNominationCount(String nominationCount) {
        this.nominationCount = nominationCount;
    }

    public String getNominationReceivedCount() {
        return nominationReceivedCount;
    }

    public void setNominationReceivedCount(String nominationReceivedCount) {
        this.nominationReceivedCount = nominationReceivedCount;
    }

    /**
     * Getter for FulfillmentProducts
     * 
     * @return the FulfillmentProducts
     */
    public List<FulfillmentProduct> getFulfillmentProducts() {
        return fulfillmentProducts;
    }

    /**
     * Setter for FulfillmentProducts
     * 
     * @param FulfillmentProduct the FulfillmentProducts to set
     */
    public void setFulfillmentProducts(List<FulfillmentProduct> fulfillmentProducts) {
        this.fulfillmentProducts = fulfillmentProducts;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("PACKAGEID", packageId)
                .append("MSISDN", msisdn).append("EVENTID", eventId).append("FLOW_ID", flowId)
                .append("SUBSCRIBER_ID", subscriberId).append("RECORD_TYPE", recordType)
                .append("PROGRAM_CODE", recordType).append("PROGRAM_CODE", programCode).append("TIMESTAMP", timeStamp)
                .append("OFFER_CODE", offerCode).append("OPTIN_SHORTCODE", optinShortCode)
                .append("CHANNEL_TYPE", channelType).append("ACTION_TYPE", actionType).append("STATUS", status)
                .append("NOMINATOR_MSISDN", nominatorMsisdn).append("FULFILLMENT_PRODUCTS", fulfillmentProducts)
                .toString();
    }
}
