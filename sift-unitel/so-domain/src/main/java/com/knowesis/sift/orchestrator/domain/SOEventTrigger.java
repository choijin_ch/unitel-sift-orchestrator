/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Represent a row of EventTriggerRecord table in SO database
 * 
 * @author SO Development Team
 */
public class SOEventTrigger implements Serializable {

    private static final long serialVersionUID = 1L;

    private String msisdn;

    private String offerId;

    private String programId;

    private String triggerId;

    private String flowId;

    private Double monitoredValue;

    private Double actualResponseValue;

    private String provisionedValue;

    private String provisioningSystem;

    private String provisioningOfferId;

    private Timestamp triggerTimestamp;

    private String notificationMessage;

    private String actionResponse;

    private String triggerType;

    private String actionType;

    private String channel;

    private String actionStatus;

    private String isSimulated;

    private Timestamp createdOn;

    private String isControl;

    private Timestamp updatedOn;

    private Timestamp offerStartDate;

    private Timestamp offerEndDate;

    private String subscriptionId;

    private String contactDirection;
    
	/**
     * Getter for msisdn
     * 
     * @return the msisdn
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Setter for msisdn
     * 
     * @param msisdn the msisdn to set
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    /**
     * Getter for offerId
     * 
     * @return the offerId
     */
    public String getOfferId() {
        return offerId;
    }

    /**
     * Setter for offerId
     * 
     * @param offerId the offerId to set
     */
    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    /**
     * Getter for programId
     * 
     * @return the programId
     */
    public String getProgramId() {
        return programId;
    }

    /**
     * Setter for programId
     * 
     * @param programId the programId to set
     */
    public void setProgramId(String programId) {
        this.programId = programId;
    }

    /**
     * Getter for triggerId
     * 
     * @return the triggerId
     */
    public String getTriggerId() {
        return triggerId;
    }

    /**
     * Setter for triggerId
     * 
     * @param triggerId the triggerId to set
     */
    public void setTriggerId(String triggerId) {
        this.triggerId = triggerId;
    }

    /**
     * Getter for flowId
     * 
     * @return the flowId
     */
    public String getFlowId() {
        return flowId;
    }

    /**
     * Setter for flowId
     * 
     * @param flowId the flowId to set
     */
    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    /**
     * Getter for monitoredValue
     * 
     * @return the monitoredValue
     */
    public Double getMonitoredValue() {
        return monitoredValue;
    }

    /**
     * Setter for monitoredValue
     * 
     * @param monitoredValue the monitoredValue to set
     */
    public void setMonitoredValue(Double monitoredValue) {
        this.monitoredValue = monitoredValue;
    }

    /**
     * Getter for actualResponseValue
     * 
     * @return the actualResponseValue
     */
    public Double getActualResponseValue() {
        return actualResponseValue;
    }

    /**
     * Setter for actualResponseValue
     * 
     * @param actualResponseValue the actualResponseValue to set
     */
    public void setActualResponseValue(Double actualResponseValue) {
        this.actualResponseValue = actualResponseValue;
    }

    /**
     * Getter for provisionedValue
     * 
     * @return the provisionedValue
     */
    public String getProvisionedValue() {
        return provisionedValue;
    }

    /**
     * Setter for provisionedValue
     * 
     * @param provisionedValue the provisionedValue to set
     */
    public void setProvisionedValue(String provisionedValue) {
        this.provisionedValue = provisionedValue;
    }

    /**
     * Getter for provisioningOfferId
     * 
     * @return the provisioningOfferId
     */

    public String getProvisioningOfferId() {
        return provisioningOfferId;
    }

    /**
     * Setter for provisioningOfferId
     * 
     * @param provisioningOfferId the provisioningOfferId to set
     */
    public void setProvisioningOfferId(String provisioningOfferId) {
        this.provisioningOfferId = provisioningOfferId;
    }

    /**
     * Getter for provisioningSystem
     * 
     * @return the provisioningSystem
     */
    public String getProvisioningSystem() {
        return provisioningSystem;
    }

    /**
     * Setter for provisioningSystem
     * 
     * @param provisioningSystem the provisioningSystem to set
     */
    public void setProvisioningSystem(String provisioningSystem) {
        this.provisioningSystem = provisioningSystem;
    }

    /**
     * Getter for triggerTimestamp
     * 
     * @return the triggerTimestamp
     */
    public Timestamp getTriggerTimestamp() {
        return triggerTimestamp;
    }

    /**
     * Setter for triggerTimestamp
     * 
     * @param triggerTimestamp the triggerTimestamp to set
     */
    public void setTriggerTimestamp(Timestamp triggerTimestamp) {
        this.triggerTimestamp = triggerTimestamp;
    }

    /**
     * Getter for notificationMessage
     * 
     * @return the notificationMessage
     */
    public String getNotificationMessage() {
        return notificationMessage;
    }

    /**
     * Setter for notificationMessage
     * 
     * @param notificationMessage the notificationMessage to set
     */
    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    /**
     * Getter for actionResponse
     * 
     * @return the actionResponse
     */
    public String getActionResponse() {
        return actionResponse;
    }

    /**
     * Setter for actionResponse
     * 
     * @param actionResponse the actionResponse to set
     */
    public void setActionResponse(String actionResponse) {
        this.actionResponse = actionResponse;
    }

    /**
     * Getter for triggerType
     * 
     * @return the triggerType
     */
    public String getTriggerType() {
        return triggerType;
    }

    /**
     * Setter for triggerType
     * 
     * @param triggerType the triggerType to set
     */
    public void setTriggerType(String triggerType) {
        this.triggerType = triggerType;
    }

    /**
     * Getter for actionType
     * 
     * @return the actionType
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Setter for actionType
     * 
     * @param actionType the actionType to set
     */
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    /**
     * Getter for channel
     * 
     * @return the channel
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Setter for msisdn
     * 
     * @param channel the channel to set
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * Getter for actionStatus
     * 
     * @return the actionStatus
     */
    public String getActionStatus() {
        return actionStatus;
    }

    /**
     * Setter for actionStatus
     * 
     * @param actionStatus the actionStatus to set
     */
    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    /**
     * Getter for isSimulated
     * 
     * @return the isSimulated
     */
    public String getIsSimulated() {
        return isSimulated;
    }

    /**
     * Setter for isSimulated
     * 
     * @param isSimulated the isSimulated to set
     */
    public void setIsSimulated(String isSimulated) {
        this.isSimulated = isSimulated;
    }

    /**
     * Getter for createdOn
     * 
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * Setter for createdOn
     * 
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Getter for isControl
     * 
     * @return the isControl
     */
    public String getIsControl() {
        return isControl;
    }

    /**
     * Setter for isControl
     * 
     * @param isControl the isControl to set
     */
    public void setIsControl(String isControl) {
        this.isControl = isControl;
    }

    /**
     * Getter for updatedOn
     * 
     * @return the updatedOn
     */
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    /**
     * Setter for updatedOn
     * 
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * Getter for offerStartDate
     * 
     * @return offerStartDate the offerStartDate to return
     */
    public Timestamp getOfferStartDate() {
        return offerStartDate;
    }

    /**
     * Setter for offerStartDate
     * 
     * @param offerStartDate the offerStartDate to set
     */
    public void setOfferStartDate(Timestamp offerStartDate) {
        this.offerStartDate = offerStartDate;
    }

    /**
     * Getter for offerEndDate
     * 
     * @return offerEndDate the offerEndDate
     */
    public Timestamp getOfferEndDate() {
        return offerEndDate;
    }

    /**
     * Setter for offerEndDate
     * 
     * @param offerEndDate the offerEndDate to set
     */
    public void setOfferEndDate(Timestamp offerEndDate) {
        this.offerEndDate = offerEndDate;
    }
    
    /**
     * Getter for SubscriptionId
     * 
     * @return SubscriptionId the SubscriptionId
     */
    public String getSubscriptionId() {
        return subscriptionId;
    }
    
    /**
     * Setter for SubscriptionId 
     * 
     * @param SubscriptionId the SubscriptionId
     */
    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    /**
     * Getter for contactDirection
     * 
     * @return contactDirection the contactDirection
     */
    public String getContactDirection() {
        return contactDirection;
    }

    /**
     * Setter for setContactDirection
     * 
     * @param contactDirection the contactDirection to set
     */
    public void setContactDirection(String contactDirection) {
        this.contactDirection = contactDirection;
    }


    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("msisdn", msisdn).append("offerId", offerId)
                .append("programId", programId).append("triggerId", triggerId).append("flowId", flowId)
                .append("monitoredValue", monitoredValue).append("actualResponseValue", actualResponseValue)
                .append("provisionedValue", provisionedValue).append("provisioningSystem", provisioningSystem)
                .append("triggerTimestamp", triggerTimestamp).append("notificationMessage", notificationMessage)
                .append("actionResponse", actionResponse).append("triggerType", triggerType)
                .append("actionType", actionType).append("channel", channel).append("actionStatus", actionStatus)
                .append("isSimulated", isSimulated).append("createdOn", createdOn).append("isControl", isControl)
                .append("updatedOn", updatedOn).append("offerStartDate", offerStartDate)
                .append("offerEndDate", offerEndDate).append("subscriptionId", subscriptionId)
                .append("contactDirection", contactDirection).toString();

    }

}
