/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.core.domain;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO for Event_Payload
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Locations implements Serializable,Cloneable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("id")
    private String id;
    
    @JsonProperty("numOffers")
    private String numOffers;
    
    @JsonProperty("SHOW_EXISTING_OFFERS")
    private String showExistingOffers;

    @JsonProperty("OFFERS")
    private List<Offers> offers;

    @JsonProperty("FULFILMENTS")
    private List<Fulfillments> fulfilments;

    @JsonProperty("REMINDERS")
    private List<Reminders> reminders;

    /**
     * Getter for Id
     * 
     * @return the Id
     */
    public String getId() {
        return id;
    }

    /**
     * Setter for Id
     * 
     * @param Id the Id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter for numOffers
     * 
     * @return the numOffers
     */
    public String getNumOffers() {
		return numOffers;
	}

    /**
     * Setter for numOffers
     * 
     * @param numOffers the numOffers to set
     */
	public void setNumOffers(String numOffers) {
		this.numOffers = numOffers;
	}

	 /**
     * Getter for Reminders
     * 
     * @return the Reminders
     */
	public String getShowExistingOffers() {
		return showExistingOffers;
	}
	
	/**
     * Setter for ShowExistingOffers
     * 
     * @param ShowExistingOffers the ShowExistingOffers to set
     */
	public void setShowExistingOffers(String showExistingOffers) {
		this.showExistingOffers = showExistingOffers;
	}

	/**
     * Getter for Offers
     * 
     * @return the Offers
     */
    public List<Offers> getOffers() {
        return offers;
    }

    /**
     * Setter for Offers
     * 
     * @param Offers the Offers to set
     */
    public void setOffers(List<Offers> offers) {
        this.offers = offers;
    }

    /**
     * Getter for Fulfillments
     * 
     * @return the Fulfillments
     */
    public List<Fulfillments> getFulfilments() {
        return fulfilments;
    }

    /**
     * Setter for Fulfillments
     * 
     * @param Fulfillments the Fulfillments to set
     */
    public void setFulfilments(List<Fulfillments> fulfilments) {
        this.fulfilments = fulfilments;
    }

    /**
     * Getter for Reminders
     * 
     * @return the Reminders
     */
    public List<Reminders> getReminders() {
        return reminders;
    }

    /**
     * Setter for Reminders
     * 
     * @param Reminders the Reminders to set
     */
    public void setReminders(List<Reminders> reminders) {
        this.reminders = reminders;
    }

    public Object clone()throws CloneNotSupportedException{  
    	 return (Locations)super.clone();  
    	   }
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("ID", id)
        		.append("numOffers", numOffers).append("SHOW_EXISTING_OFFERS", showExistingOffers)
        		.append("OFFERS", offers).append("FULFILMENTS", fulfilments).append("REMINDERS", reminders).toString();
    }
}
