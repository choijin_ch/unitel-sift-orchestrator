/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This DTO is used by MT/MO handler to populate response of Notification or Optin
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SONotificationEventResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    /** camelSmppId */
    @JsonProperty("referenceNumber")
    private String referenceNumber;

    /** keyword */
    @JsonProperty("responseText")
    private String responseText;

    /** shortcode */
    @JsonProperty("responseCode")
    private String responseCode;

    /**
     * Getter for referenceNumber
     * 
     * @return the referenceNumber
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Setter for referenceNumber
     * 
     * @param referenceNumber the referenceNumber to set
     */
    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    /**
     * Getter for responseText
     * 
     * @return the responseText
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * Setter for responseText
     * 
     * @param responseText the responseText to set
     */
    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    /**
     * Getter for responseCode
     * 
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Setter for responseCode
     * 
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("referenceNumber", referenceNumber).append("responseText", responseText)
                .append("responseCode", responseCode).toString();
    }
}
