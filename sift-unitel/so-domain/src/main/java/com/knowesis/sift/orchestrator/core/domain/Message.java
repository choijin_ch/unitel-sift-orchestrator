/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.core.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO for Event_Payload
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("lang")
    private String lang;

    @JsonProperty("contentId")
    private String contentId;

    @JsonProperty("textTobeSent")
    private String textTobeSent;

    /**
     * Getter for Lang
     * 
     * @return the Lang
     */
    public String getLang() {
        return lang;
    }

    /**
     * Setter for Lang
     * 
     * @param Lang the Lang to set
     */
    public void setLang(String lang) {
        this.lang = lang;
    }

    /**
     * Getter for ContentId
     * 
     * @return the ContentId
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * Setter for ContentId
     * 
     * @param ContentId the ContentId to set
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    /**
     * Getter for TextTobeSent
     * 
     * @return the TextTobeSent
     */
    public String getTextTobeSent() {
        return textTobeSent;
    }

    /**
     * Setter for TextTobeSent
     * 
     * @param TextTobeSent the TextTobeSent to set
     */
    public void setTextTobeSent(String textTobeSent) {
        this.textTobeSent = textTobeSent;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("lang", lang)
                .append("contentId", contentId).append("textTobeSent", textTobeSent).toString();
    }
}
