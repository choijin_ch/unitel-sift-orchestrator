/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.knowesis.sift.orchestrator.core.domain.CoreNotificationMessage;

/**
 * Represents Notification in SO
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SONotificationMessage extends SOMessage {

    private static final long serialVersionUID = 1L;

    @JsonProperty("message")
    private CoreNotificationMessage message;

    /**
     * Getter for message
     * 
     * @return the message
     */
    public CoreNotificationMessage getMessage() {
        return message;
    }

    /**
     * Setter for message
     * 
     * @param message the message to set
     */
    public void setMessage(CoreNotificationMessage message) {
        this.message = message;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.knowesis.sift.orchestrator.domain.SOMessage#toString()
     */
    @Override
    public String toString() {

        return new ToStringBuilder(this).appendSuper(super.toString()).append("message", message).toString();
    }

}
