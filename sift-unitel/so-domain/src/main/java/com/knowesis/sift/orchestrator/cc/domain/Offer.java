package com.knowesis.sift.orchestrator.cc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents Offer 
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Offer {
	
	@JsonProperty("offerID")
	private String offerID;
	
	@JsonProperty("offerChannel")
	private String offerChannel;
	
	@JsonProperty("offerMessage")
	private String offerMessage;
	
	@JsonProperty("offerContactDate")
	private String offerContactDate;
	
	@JsonProperty("offerStatus")
	private String offerStatus;
	
	@JsonProperty("offerStartDate")
	private String offerStartDate;
	
	@JsonProperty("offerEndDate")
	private String offerEndDate;
	
	@JsonProperty("flowId")
	private String flowId;

	@JsonProperty("provisionDate")
    private String provisionDate;
    
	@JsonProperty("rewardID")
    private String rewardID;
    
	@JsonProperty("optinFlag")
    private String optinFlag;
    
	@JsonProperty("optinKeyword")
    private String optinKeyword;
	
	@JsonProperty("optinShortcode")
    private String optinShortcode;
	
	@JsonProperty("controlFlag")
    private String controlFlag;

	@JsonProperty("provisioningFailureReason")
    private String provisioningFailureReason;

	@JsonProperty("provisionStatus")
	private String provisionStatus;
	
	@JsonProperty("nominationMessage")
	private String nominationMessage;
	
	@JsonProperty("nominationStatus")
	private String nominationStatus;
	
	@JsonProperty("nominationShortcode")
	private String nominationShortcode;
	
	@JsonProperty("nominationOfferStartDate")
	private String nominationOfferStartDate;
	
	@JsonProperty("nominationOfferEndDate")
	private String nominationOfferEndDate;
	
	@JsonProperty("fulfillmentMessage")
	private String fulfillmentMessage;

	/**
	 * @return the offerStartDate
	 */
	public String getOfferStartDate() {
		return offerStartDate;
	}

	/**
	 * @param offerStartDate the offerStartDate to set
	 */
	public void setOfferStartDate(String offerStartDate) {
		this.offerStartDate = offerStartDate;
	}

	/**
	 * @return the provisionDate
	 */
	public String getProvisionDate() {
		return provisionDate;
	}

	/**
	 * @param provisionDate the provisionDate to set
	 */
	public void setProvisionDate(String provisionDate) {
		this.provisionDate = provisionDate;
	}

	/**
	 * @return the offerMessage
	 */
	public String getOfferMessage() {
		return offerMessage;
	}

	/**
	 * @param offerMessage the offerMessage to set
	 */
	public void setOfferMessage(String offerMessage) {
		this.offerMessage = offerMessage;
	}

	/**
	 * @return the offerEndDate
	 */
	public String getOfferEndDate() {
		return offerEndDate;
	}

	/**
	 * @param offerEndDate the offerEndDate to set
	 */
	public void setOfferEndDate(String offerEndDate) {
		this.offerEndDate = offerEndDate;
	}

	/**
	 * @return the offerStatus
	 */
	public String getOfferStatus() {
		return offerStatus;
	}

	/**
	 * @param offerStatus the offerStatus to set
	 */
	public void setOfferStatus(String offerStatus) {
		this.offerStatus = offerStatus;
	}

	/**
	 * @return the offerChannel
	 */
	public String getOfferChannel() {
		return offerChannel;
	}

	/**
	 * @param offerChannel the offerChannel to set
	 */
	public void setOfferChannel(String offerChannel) {
		this.offerChannel = offerChannel;
	}

	/**
	 * @return the offerContactDate
	 */
	public String getOfferContactDate() {
		return offerContactDate;
	}

	/**
	 * @param offerContactDate the offerContactDate to set
	 */
	public void setOfferContactDate(String offerContactDate) {
		this.offerContactDate = offerContactDate;
	}

	/**
	 * @return the provisioningFailureReason
	 */
	public String getProvisioningFailureReason() {
		return provisioningFailureReason;
	}

	/**
	 * @param provisioningFailureReason the provisioningFailureReason to set
	 */
	public void setProvisioningFailureReason(String provisioningFailureReason) {
		this.provisioningFailureReason = provisioningFailureReason;
	}

	/**
	 * @return the rewardID
	 */
	public String getRewardID() {
		return rewardID;
	}

	/**
	 * @param rewardID the rewardID to set
	 */
	public void setRewardID(String rewardID) {
		this.rewardID = rewardID;
	}

	

	public String getOptinFlag() {
		return optinFlag;
	}

	public void setOptinFlag(String optinFlag) {
		this.optinFlag = optinFlag;
	}

	public String getOptinKeyword() {
		return optinKeyword;
	}

	public void setOptinKeyword(String optinKeyword) {
		this.optinKeyword = optinKeyword;
	}

	public String getOptinShortcode() {
		return optinShortcode;
	}

	public void setOptinShortcode(String optinShortcode) {
		this.optinShortcode = optinShortcode;
	}

	/**
	 * @return the offerID
	 */
	public String getOfferID() {
		return offerID;
	}

	/**
	 * @param offerCode the offerID to set
	 */
	public void setOfferID(String offerID) {
		this.offerID = offerID;
	}

	/**
	 * @return the flowId
	 */
	public String getFlowId() {
		return flowId;
	}

	/**
	 * @param flowId the flowId to set
	 */
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}

	/**
	 * @return the controlFlag
	 */
	public String getControlFlag() {
		return controlFlag;
	}

	/**
	 * @param controlFlag the controlFlag to set
	 */
	public void setControlFlag(String controlFlag) {
		this.controlFlag = controlFlag;
	}

	public String getProvisionStatus() {
		return provisionStatus;
	}

	public void setProvisionStatus(String provisionStatus) {
		this.provisionStatus = provisionStatus;
	}
	
	

	public String getNominationMessage() {
		return nominationMessage;
	}

	public void setNominationMessage(String nominationMessage) {
		this.nominationMessage = nominationMessage;
	}

	public String getNominationStatus() {
		return nominationStatus;
	}

	public void setNominationStatus(String nominationStatus) {
		this.nominationStatus = nominationStatus;
	}

	public String getNominationShortcode() {
		return nominationShortcode;
	}

	public void setNominationShortcode(String nominationShortcode) {
		this.nominationShortcode = nominationShortcode;
	}

	public String getNominationOfferStartDate() {
		return nominationOfferStartDate;
	}

	public void setNominationOfferStartDate(String nominationOfferStartDate) {
		this.nominationOfferStartDate = nominationOfferStartDate;
	}

	public String getNominationOfferEndDate() {
		return nominationOfferEndDate;
	}

	public void setNominationOfferEndDate(String nominationOfferEndDate) {
		this.nominationOfferEndDate = nominationOfferEndDate;
	}

	
	public String getFulfillmentMessage() {
		return fulfillmentMessage;
	}

	public void setFulfillmentMessage(String fulfillmentMessage) {
		this.fulfillmentMessage = fulfillmentMessage;
	}

	/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("offerID", offerID).append("offerChannel", offerChannel)
                .append("offerMessage", offerMessage).append("offerContactDate", offerContactDate)
                .append("offerStatus", offerStatus).append("offerStartDate", offerStartDate)
                .append("offerEndDate", offerEndDate).append("flowId", flowId).append("provisionDate", provisionDate)
                .append("rewardID", rewardID).append("optinFlag", optinFlag).append("optinKeyword", optinKeyword).append("optinShortcode", optinShortcode).append("controlFlag", controlFlag)
                .append("provisioningFailureReason", provisioningFailureReason)
                .append("provisionStatus", provisionStatus).append("nominationMessage", nominationMessage).append("nominationStatus", nominationStatus).append("nominationShortcode", nominationShortcode).append("nominationOfferStartDate", nominationOfferStartDate).append("nominationOfferEndDate", nominationOfferEndDate).append("fulfillmentMessage", fulfillmentMessage).toString();
    }



}
