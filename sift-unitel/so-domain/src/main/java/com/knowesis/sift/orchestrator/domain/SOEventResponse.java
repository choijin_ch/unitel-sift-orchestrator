/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This DTO is used by PRG and possibly by Core
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SOEventResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    /** SUCCESS/FAILURE: overall status of the event */
    @JsonProperty("status")
    private String status;

    /** high level description in case of error */
    @JsonProperty("description")
    private String description;

    /** SSP/SO/SMSC: in case of failure, where was the failure */
    @JsonProperty("category")
    private String category;

    @JsonProperty("fulfillmentResponse")
    private SOFulfillmentEventResponse fulfillmentResponse;

    @JsonProperty("notificationResponse")
    private SONotificationEventResponse notificationResponse;

    /**
     * Getter for status
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter for status
     * 
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Getter for description
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for description
     * 
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for category
     * 
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Setter for category
     * 
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Getter for fulfillmentResponse
     * 
     * @return the fulfillmentResponse
     */
    public SOFulfillmentEventResponse getFulfillmentResponse() {
        return fulfillmentResponse;
    }

    /**
     * Setter for fulfillmentResponse
     * 
     * @param fulfillmentResponse the fulfillmentResponse to set
     */
    public void setFulfillmentResponse(SOFulfillmentEventResponse fulfillmentResponse) {
        this.fulfillmentResponse = fulfillmentResponse;
    }

    /**
     * Getter for notificationResponse
     * 
     * @return the notificationResponse
     */
    public SONotificationEventResponse getNotificationResponse() {
        return notificationResponse;
    }

    /**
     * Setter for notificationResponse
     * 
     * @param notificationResponse the notificationResponse to set
     */
    public void setNotificationResponse(SONotificationEventResponse notificationResponse) {
        this.notificationResponse = notificationResponse;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("category", category)
                .append("description", description).append("fulfillmentResponse", fulfillmentResponse)
                .append("notificationResponse", notificationResponse).toString();
    }
}
