/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.knowesis.sift.orchestrator.core.domain;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents Fulfillment Message in Core Fulfillment Event
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OfferPayload implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("NOMINATION_MESSAGE")
	private String nominationMessage;

	@JsonProperty("NOMINATION_SHORT_CODE")
	private String nominationShortCode;

	@JsonProperty("NOMINATION_COUNT")
	private String nominationCount;

	@JsonProperty("NOMINATION_VALIDITY")
	private String nominationValidity;
	
    /**
     * Getter for NominationMessage
     * 
     * @return the NominationMessage
     */
	public String getNominationMessage() {
		return nominationMessage;
	}

	 /**
		 * Setter for NominationMessage
		 * 
		 * @param NominationMessage
		 *            the NominationMessage to set
		 */
	public void setNominationMessage(String nominationMessage) {
		this.nominationMessage = nominationMessage;
	}

    /**
     * Getter for NominationShortCode
     * 
     * @return the NominationShortCode
     */
	public String getNominationShortCode() {
		return nominationShortCode;
	}

	 /**
		 * Setter for NominationShortCode
		 * 
		 * @param NominationShortCode
		 *            the NominationShortCode to set
		 */
	public void setNominationShortCode(String nominationShortCode) {
		this.nominationShortCode = nominationShortCode;
	}

    /**
     * Getter for NominationCount
     * 
     * @return the NominationCount
     */
	public String getNominationCount() {
		return nominationCount;
	}

	 /**
		 * Setter for NominationCount
		 * 
		 * @param NominationCount
		 *            the NominationCount to set
		 */
	public void setNominationCount(String nominationCount) {
		this.nominationCount = nominationCount;
	}

    /**
     * Getter for NominationValidity
     * 
     * @return the NominationValidity
     */
	public String getNominationValidity() {
		return nominationValidity;
	}

	 /**
		 * Setter for NominationValidity
		 * 
		 * @param NominationValidity
		 *            the NominationValidity to set
		 */
	public void setNominationValidity(String nominationValidity) {
		this.nominationValidity = nominationValidity;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("NOMINATION_MESSAGE", nominationMessage)
				.append("NOMINATION_SHORT_CODE", nominationShortCode).append("NOMINATION_COUNT", nominationCount)
				.append("NOMINATION_VALIDITY", nominationValidity).toString();
	}
}
