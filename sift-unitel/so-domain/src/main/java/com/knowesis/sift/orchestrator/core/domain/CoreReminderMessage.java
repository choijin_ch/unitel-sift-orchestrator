/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.core.domain;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents Core Reminder Message
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoreReminderMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("CUSTOMER_IDENTIFIER")
    private CustomerIdentifier customerIdentifier;

    @JsonProperty("REQUEST_ID")
    private String requestId;

    @JsonProperty("CONTEXT_DATA")
    private Map<String, String> contextData;

    @JsonProperty("REQUESTER_LOCATION")
    private RequesterLocation requesterLocation;

    @JsonProperty("REQUESTER_CHANNEL")
    private String requesterChannel;

    @JsonProperty("REQUESTER_APPLICATION")
    private String requesterApplication;

    @JsonProperty("REQUESTER_ZONE")
    private String requesterZone;

    /**
     * Getter for Status
     * 
     * @return the Status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter for Status
     * 
     * @param Status the Status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Getter for CustomerIdentifier
     * 
     * @return the CustomerIdentifier
     */
    public CustomerIdentifier getCustomerIdentifier() {
        return customerIdentifier;
    }

    /**
     * Setter for CustomerIdentifier
     * 
     * @param CustomerIdentifier the CustomerIdentifier to set
     */
    public void setCustomerIdentifier(CustomerIdentifier customerIdentifier) {
        this.customerIdentifier = customerIdentifier;
    }

    /**
     * Getter for ContextData
     * 
     * @return the ContextData
     */
    public Map<String, String> getContextData() {
        return contextData;
    }

    /**
     * Setter for ContextData
     * 
     * @param ContextData the ContextData to set
     */
    public void setContextData(Map<String, String> contextData) {
        this.contextData = contextData;
    }

    /**
     * Getter for RequestId
     * 
     * @return the RequestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Setter for RequestId
     * 
     * @param RequestId the RequestId to set
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Getter for RequesterLocation
     * 
     * @return the RequesterLocation
     */
    public RequesterLocation getRequesterLocation() {
        return requesterLocation;
    }

    /**
     * Setter for RequesterLocation
     * 
     * @param RequesterLocation the RequesterLocation to set
     */
    public void setRequesterLocation(RequesterLocation requesterLocation) {
        this.requesterLocation = requesterLocation;
    }

    /**
     * Getter for RequesterChannel
     * 
     * @return the RequesterChannel
     */
    public String getRequesterChannel() {
        return requesterChannel;
    }

    /**
     * Setter for RequesterChannel
     * 
     * @param RequesterChannel the RequesterChannel to set
     */
    public void setRequesterChannel(String requesterChannel) {
        this.requesterChannel = requesterChannel;
    }

    /**
     * Getter for RequesterApplication
     * 
     * @return the RequesterApplication
     */
    public String getRequesterApplication() {
        return requesterApplication;
    }

    /**
     * Setter for RequesterApplication
     * 
     * @param RequesterApplication the RequesterApplication to set
     */
    public void setRequesterApplication(String requesterApplication) {
        this.requesterApplication = requesterApplication;
    }

    /**
     * Getter for RequesterZone
     * 
     * @return the RequesterZone
     */
    public String getRequesterZone() {
        return requesterZone;
    }

    /**
     * Setter for RequesterZone
     * 
     * @param RequesterZone the RequesterZone to set
     */
    public void setRequesterZone(String requesterZone) {
        this.requesterZone = requesterZone;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("STATUS", status)
                .append("CUSTOMER_IDENTIFIER", customerIdentifier).append("CONTEXT_DATA", contextData)
                .append("REQUEST_ID", requestId).append("REQUESTER_LOCATION", requesterLocation)
                .append("REQUESTER_CHANNEL", requesterChannel).append("REQUESTER_APPLICATION", requesterApplication)
                .append("REQUESTER_ZONE", requesterZone).toString();
    }

}
