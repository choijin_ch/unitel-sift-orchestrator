/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.core.domain;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO for Event_Payload
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequesterLocation implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("LOCATIONS")
    private List<Locations> locations;

    /**
     * Getter for Locations
     * 
     * @return the Locations
     */
    public List<Locations> getLocations() {
        return locations;
    }

    /**
     * Setter for Locations
     * 
     * @param Locations the Locations to set
     */
    public void setLocations(List<Locations> locations) {
        this.locations = locations;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("LOCATIONS", locations).toString();
    }

}
