package com.knowesis.sift.orchestrator.cc.domain;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents CCResponse
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CCResponse {

	@JsonProperty("responseCode")
	private String responseCode;

	@JsonProperty("responseMessage")
	private String responseMessage;

	@JsonProperty("msisdn")
	private String msisdn;

	@JsonProperty("campaignList")
	private List<Campaign> campaignList;

	@JsonProperty("currentSystemTime")
	private String currentSystemTime;

	/**
	 * @return the responseMessage
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * @param responseMessage
	 *            the responseMessage to set
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode
	 *            the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @param msisdn
	 *            the msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * @return the campaignList
	 */
	public List<Campaign> getCampaignList() {
		return campaignList;
	}

	/**
	 * @param campaignList
	 *            the campaignList to set
	 */
	public void setCampaignList(List<Campaign> campaignList) {
		this.campaignList = campaignList;
	}

	/**
	 * @return the currentSystemTime
	 */
	public String getCurrentSystemTime() {
		return currentSystemTime;
	}

	/**
	 * @param currentSystemTime
	 *            the currentSystemTime to set
	 */
	public void setCurrentSystemTime(String currentSystemTime) {
		this.currentSystemTime = currentSystemTime;
	}
	
	  /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("responseCode", responseCode).append("responseMessage", responseMessage)
                .append("msisdn", msisdn).append("campaignList", campaignList).append("currentSystemTime", currentSystemTime).toString();
    }
}
