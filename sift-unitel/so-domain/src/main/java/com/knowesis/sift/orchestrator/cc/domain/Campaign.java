package com.knowesis.sift.orchestrator.cc.domain;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents Campaign
 * 
 * @author SO Development Team
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Campaign {
	
	@JsonProperty("campaignCode")
	private String campaignCode;
	
	@JsonProperty("campaignName")
	private String campaignName;

	@JsonProperty("campaignDescription")
    private String campaignDescription;

	@JsonProperty("campaignOfferList")
    private List<Offer> campaignOfferList;

	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}

	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	/**
	 * @return the campaignDescription
	 */
	public String getCampaignDescription() {
		return campaignDescription;
	}

	/**
	 * @param campaignDescription the campaignDescription to set
	 */
	public void setCampaignDescription(String campaignDescription) {
		this.campaignDescription = campaignDescription;
	}

	/**
	 * @return the campaignOfferList
	 */
	public List<Offer> getCampaignOfferList() {
		return campaignOfferList;
	}

	/**
	 * @param campaignOfferList the campaignOfferList to set
	 */
	public void setCampaignOfferList(List<Offer> campaignOfferList) {
		this.campaignOfferList = campaignOfferList;
	}

	/**
	 * @return the campaignCode
	 */
	public String getCampaignCode() {
		return campaignCode;
	}

	/**
	 * @param campaignCode the campaignCode to set
	 */
	public void setCampaignCode(String campaignCode) {
		this.campaignCode = campaignCode;
	}
	  /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("campaignCode", campaignCode).append("campaignName", campaignName)
                .append("campaignDescription", campaignDescription).append("campaignOfferList", campaignOfferList)
                .toString();
    }

}
