/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import static java.util.Collections.reverseOrder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.core.online.V3.PandamaticOnline;
import com.knowesis.sift.orchestrator.core.domain.Locations;
import com.knowesis.sift.orchestrator.core.domain.Offers;
import com.knowesis.sift.orchestrator.core.domain.RequesterLocation;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * 
 * Processes Inbound Requests
 * 
 * @author SO Development Team
 */
@Component("InboundRequestProcessor")
public class InboundRequestProcessor implements Processor {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	ObjectMapper mapper;

	@Autowired
	Gson gson;

	@Autowired
	PandamaticOnline pandamaticOnline;

	@Value("${so.notification.template}")
	private String soMessageTemplate;

	@Value("${so.inbound.offer.count}")
	private int noOfMessagetoPass;

	@Value("${so.triggerType.offerNotify}")
	private String offerNotify;

	@Value("${so.actionType.notify}")
	private String notify;
	
	@Value("${so.inbound.actionresponse.failure}")
	private String actionResponseFailure;

	@Override
	public void process(Exchange exchange) throws Exception {

		String message = exchange.getIn().getBody(String.class);
		JsonParser parser = new JsonParser();
		JsonObject request = new JsonObject();
		log.info("get-offer Request from ussd APP :{}", message);
		request = (JsonObject) parser.parse(message);
		JsonObject result = pandamaticOnline.handleOnlineRequest(request);
		// JsonParser().parse(readFileAsString("/home/vipin/KNOWESIS/PROJECT/UNITEL/TestSpace/Test/response.json")));
		// JsonNode responseJsonNode = mapper.readTree(new File("/home/seqato-lap2/Ajay/UNITEL/msg/response.json"));
		JsonNode responseJsonNode = mapper.readTree(gson.toJson(result));
		log.info("response from siftonline:{}", gson.toJson(result));
		String responseToJetty = prepareMessageToUSSD(exchange, responseJsonNode);
		exchange.getIn().setHeader("OriginalRequestMessage", message);
		log.info("Get-offer response after prioritisation: {}", responseToJetty);
		exchange.getIn().setBody(responseToJetty);
	}

	/**
	 * prepareMessageToUSSD
	 * 
	 * @param exchange
	 *            exchange
	 * @param responseJsonNode
	 *            responseJsonNode
	 * @return jsonString
	 * @throws IOException
	 *             IOException
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 * @throws CloneNotSupportedException
	 *             CloneNotSupportedException
	 */
	private String prepareMessageToUSSD(Exchange exchange, JsonNode responseJsonNode)
			throws JsonProcessingException, IOException, CloneNotSupportedException {

		String requesterLocationNode = mapper.writeValueAsString(responseJsonNode.path("REQUESTER_LOCATION"));
		log.info("requesterLocationNode:{}", requesterLocationNode);
		RequesterLocation requesterLocation = mapper.readValue(requesterLocationNode, RequesterLocation.class);
		//Map with key as offerscore and value as  list of locations, Single offers on each location.
		Map<Double, List<Locations>> offerScoreMultimap = new LinkedHashMap<>();
		//for mapping offerscore with location list
		for (Locations locationNode : requesterLocation.getLocations()) {
			if (locationNode.getOffers() != null && locationNode.getOffers().size() > 0) {
				setOfferScoreWithLocations(offerScoreMultimap, locationNode);
			}
		}
		//Iterating the offerscore map to Exclude control group offers 
		eliminateControlGroupoffers(offerScoreMultimap);
		log.debug("offerScoreMap After removing Control group:{}", offerScoreMultimap);
		// sorting based on offer score's desending order
		List<Map.Entry<Double, List<Locations>>> sortedOfferMap = offerScoreMultimap.entrySet().stream()
				.sorted(reverseOrder(Map.Entry.comparingByKey())).collect(Collectors.toList());

		log.debug("offerScoreMap After sorting :{}", sortedOfferMap);
		ObjectNode objectNode = (ObjectNode) responseJsonNode.path("REQUESTER_LOCATION");
		objectNode.remove("LOCATIONS");
        //To set offermessage after Prioritisation
		RequesterLocation modfiedRequesterLocation = offersToUssd(exchange, offerScoreMultimap, sortedOfferMap,
				requesterLocation, responseJsonNode);
		log.info("modfiedRequesterLocation :{}", modfiedRequesterLocation);
		JsonNode newRequesterLocation = mapper.valueToTree(modfiedRequesterLocation);

		ObjectNode rootNode = (ObjectNode) responseJsonNode;
		rootNode.remove("REQUESTER_LOCATION");
		rootNode.set("REQUESTER_LOCATION", newRequesterLocation);
		log.debug("rootNode:{}", rootNode.toString().replace("\\", ""));
		return rootNode.toString();
	}
/**
 * This is to exclude ControlGroup offers
 * @param offerScoreMultimap map
 */
	private void eliminateControlGroupoffers(Map<Double, List<Locations>> offerScoreMultimap) {
		Iterator<Entry<Double, List<Locations>>> it = offerScoreMultimap.entrySet().iterator();
		while (it.hasNext()) {
			Locations tmpLocations = it.next().getValue().get(0);
			if (tmpLocations.getOffers().get(0).getIsControl() == "true"
					|| tmpLocations.getOffers().get(0).getIsSimulated() == "true") {
				it.remove();
			}
		}
		
	}

	/**
	 * prepare message to sent to ussd
	 * 
	 * @param exchange
	 *            exchange
	 * @param offerScoreMultimap
	 *            offerScoreMap
	 * @param sortedofferscoreMultimapList
	 *            offerScoreKeyList
	 * @param requesterLocation
	 *            reqstrloc
	 * @param responseJsonNode
	 *            responseJsonNode
	 * @return jsonString
	 * @throws IOException IOException
	 */
	private RequesterLocation offersToUssd(Exchange exchange, Map<Double, List<Locations>> offerScoreMultimap,
			List<Entry<Double, List<Locations>>> sortedofferscoreMultimapList, RequesterLocation requesterLocation,
			JsonNode responseJsonNode) throws IOException {
		List<String> idList = new ArrayList<String>();
		//collecting the location id in a list
		for (Entry<Double, List<Locations>> sortOfferScoreMultimap : sortedofferscoreMultimapList) {
			if (!idList.contains(sortOfferScoreMultimap.getValue().get(0).getId())) {
				idList.add(sortOfferScoreMultimap.getValue().get(0).getId());
			}
		}
		log.info("Availble id's :{}", idList);
		List<Locations> locationList = new ArrayList<Locations>();
		//grouping locations based on id 
	    groupLocationWithId(exchange,idList,sortedofferscoreMultimapList,responseJsonNode,locationList);
		
		requesterLocation.setLocations(locationList);

		return requesterLocation;
	}
/**
 * 
 * @param exchange camel exchange
 * @param idList list of id
 * @param sortedofferscoreMultimapList sorted offers map
 * @param responseJsonNode response JsonNode
 * @param locationList location List
 */
	private void groupLocationWithId(Exchange exchange, List<String> idList,
			List<Entry<Double, List<Locations>>> sortedofferscoreMultimapList, JsonNode responseJsonNode, List<Locations> locationList) {
		for (int i = 0; i < idList.size(); i++) {
			
			List<Locations> tempLocationList = new ArrayList<>();
			int numOffers = 0;
			for (Entry<Double, List<Locations>> sortOfferScoreMultimap : sortedofferscoreMultimapList) {
				for (Locations locations : sortOfferScoreMultimap.getValue()) {
					if (idList.get(i).equalsIgnoreCase(sortOfferScoreMultimap.getValue().get(0).getId())) {
						tempLocationList.add(locations);
						numOffers = Integer.parseInt(sortOfferScoreMultimap.getValue().get(0).getNumOffers());
					}
				}

			}
			locationList.add(setPrioritisedLocation(exchange, tempLocationList, numOffers, responseJsonNode));

		}
		
	}

	/**
	 * setPrioritisedLocation
	 * 
	 * @param exchange
	 *            exchange
	 * @param tempLocationList
	 *            tempLocationList
	 * @param numOffers
	 *            numOffers
	 * @param responseJsonNode
	 *            responseJsonNode
	 * @return prioritisedLocation
	 * @throws IOException
	 *             IOException
	 */
	private Locations setPrioritisedLocation(Exchange exchange, List<Locations> tempLocationList, int numOffers,
			JsonNode responseJsonNode) {
		List<Offers> prioritisedOffers = new ArrayList<Offers>();
		List<String> registeractionLocation = new ArrayList<String>();
		int noMessagePassed = 0;
		noMessagePassed = (numOffers <= tempLocationList.size()) ? numOffers : tempLocationList.size();

		for (int i = 0; i < noMessagePassed; i++) {
			prioritisedOffers.add(tempLocationList.get(i).getOffers().get(0));
		}
		log.debug("Register Action message List :{}", registeractionLocation);
		exchange.getIn().setHeader("MessagesForRA", registeractionLocation);
	     // need to check whether the below line working properly 
		//tempLocationList.remove("OFFERS");
		tempLocationList.get(0).setOffers(prioritisedOffers);

		return tempLocationList.get(0);

	}

	/**
	 * 
	 * create SomessageForControlGroupAndRA
	 * 
	 * @param locationExcludedAfterPrioritisation
	 *            locationExcludedAfterPrioritisation
	 * @param responseJsonNode
	 *            responseJsonNode
	 * @return message as jsonString
	 * @throws IOException
	 *             IOException
	 */
	private String createSomessageForRA(Locations locationExcludedAfterPrioritisation, JsonNode responseJsonNode)
			throws IOException {

		ObjectNode rootLocationNode = (ObjectNode) responseJsonNode.get("REQUESTER_LOCATION");
		rootLocationNode.remove("LOCATIONS");
		JsonNode locationsNode = mapper.valueToTree(locationExcludedAfterPrioritisation);

		rootLocationNode.set("LOCATIONS", locationsNode);
		SONotificationMessage soNotificationMessage = new SONotificationMessage();
		soNotificationMessage.setTriggerType(offerNotify);
		soNotificationMessage.setActionType(notify);
		soNotificationMessage.setActionResponse(actionResponseFailure); 
		soNotificationMessage.setMetaInfo(SOMessageUtils.createDefaultMetaInfo("INBOUND-NOTIFY", "notify",
				"Inbound-Request", "USSD", "actionType"));
		soNotificationMessage.setActionStatus("success");
		ObjectNode soNotificationMessageNode = mapper.valueToTree(soNotificationMessage);
		soNotificationMessageNode.set("message", responseJsonNode);
		String soNotificationMessageAsString = mapper.writeValueAsString(soNotificationMessageNode);
		log.debug("soNotificationMessage for RA:{}", soNotificationMessageAsString);
		return soNotificationMessageAsString;
	}

	/**
	 * 
	 * Reading mock inbound response from file
	 * 
	 * @param filePath
	 *            filePath
	 * @return json asString
	 * @throws IOException
	 *             IOException
	 */
	private String readFileAsString(String filePath) throws IOException {
		StringBuffer fileData = new StringBuffer();
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
		}
		reader.close();
		return fileData.toString();
	}

	/**
	 * setOfferScore
	 * 
	 * @param offerMap
	 *            offerMap
	 * @param key
	 *            key
	 * @param value
	 *            value
	 */
	public static void setofferScoreMultimap(Map<Double, List<Locations>> offerMap, Double key, Locations value) {
		if (offerMap.get(key) == null) {
			List<Locations> list = new ArrayList<>();
			list.add(value);
			offerMap.put(key, list);
		} else {
			offerMap.get(key).add(value);
		}
	}
	
	/**
	 * This is to map offerscore with location list
	 * 
	 * @param offerScoreMultimap
	 *            map with offerscore and locations list
	 * @param locationNode
	 *            locations
	 * 
	 * @throws CloneNotSupportedException Exception
	 */
	public void setOfferScoreWithLocations(Map<Double, List<Locations>> offerScoreMultimap, Locations locationNode)
			throws CloneNotSupportedException {

		for (Offers offerNode : locationNode.getOffers()) {
			double offerScore = Double.parseDouble(offerNode.getOfferScore());
			log.debug("offerScore :{} offernode  :{}", offerScore, offerNode);
			if (Integer.parseInt(locationNode.getNumOffers()) > 1) {
				Locations tempLocationNode = (Locations) locationNode.clone();
				tempLocationNode.setOffers(null);
				ArrayList<Offers> tempOfferArrayList = new ArrayList<Offers>();
				tempOfferArrayList.add(offerNode);
				tempLocationNode.setOffers(tempOfferArrayList);
				setofferScoreMultimap(offerScoreMultimap, offerScore, tempLocationNode);
				tempOfferArrayList = null;
				tempLocationNode = null;
				log.debug("offerScoremap in loop:{}", offerScoreMultimap);
			} else {
				setofferScoreMultimap(offerScoreMultimap, offerScore, locationNode);
			}
		}
	}

	/**
	 * This is to list locations for RA
	 * 
	 * @param tempLocationList
	 *            map with offerscore and locations list
	 *            
	 * @param numOffers
	 *            number of offers 
	 *            
	 * @param responseJsonNode
	 *            responseJsonNode  
	 *            
	 * @param registeractionLocation
	 *            list of locations for RA
	 *                                 
	 * @throws IOException 
	 * 
	 */
	public void setRALocationList(List<Locations> tempLocationList, int numOffers,
			JsonNode responseJsonNode,List<String> registeractionLocation ) throws IOException
			{

		if (numOffers < tempLocationList.size()) {
			for (int i = numOffers; i < tempLocationList.size(); i++) {
				if (tempLocationList.get(i).getOffers().get(0).getNewOfferFlag().equalsIgnoreCase("true")) {
					log.info("tempLocationList:{}", tempLocationList.get(i));

					registeractionLocation.add(createSomessageForRA(tempLocationList.get(i), responseJsonNode));

				}
			}

		}
	}
}
