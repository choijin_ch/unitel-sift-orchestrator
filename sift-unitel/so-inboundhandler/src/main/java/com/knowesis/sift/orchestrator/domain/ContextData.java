package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/**
 * @author SO Development Team
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContextData implements Serializable{
    
	    @JsonProperty("ACCOUNT_TYPE")
        private String accountType;
	    @JsonProperty("AGENT_CODE")
	    private String agentCode;
	    @JsonProperty("RTR_CODE")
	    private String rtrCode;
	    @JsonProperty("BALANCE")
	    private String balance;
	    @JsonProperty("INTEREST")
	    private String interest;
	    
	    
		public String getRtrCode() {
			return rtrCode;
		}
		public void setRtrCode(String rtrCode) {
			this.rtrCode = rtrCode;
		}
		public String getBalance() {
			return balance;
		}
		public void setBalance(String balance) {
			this.balance = balance;
		}
		public String getAgentCode() {
			return agentCode;
		}
		public void setAgentCode(String agentCode) {
			this.agentCode = agentCode;
		}
		public String getAccountType() {
			return accountType;
		}
		public void setAccountType(String accountType) {
			this.accountType = accountType;
		}
		public String getInterest() {
			return interest;
		}
		public void setInterest(String interest) {
			this.interest = interest;
		}
		
	    
	    
	 
}
