package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author SO Development Team
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequesterLocation  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("LOCATIONS")
	private List<Location> locations;
    

    
 


	public List<Location> getLocations() {
		return locations;
	}



	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RequesterLocation [ LOCATIONS");
		builder.append(locations);
		builder.append("]");
		return builder.toString();
	}
    
}
