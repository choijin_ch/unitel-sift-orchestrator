package com.knowesis.sift.orchestrator.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Confirms requests
 * 
 * @author SO Development Team
 *
 */
@Component("InboundDBProcessor")
public class InboundDBProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    RedisConnectionPool redisConnectionPool;

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${so.inbound.redis.ttl}")
    int inboundRedisTtl;

    @Value("${so.notification.template}")
    private String soMessageTemplate;
    
    @Value("${so.inbound.getOffer.triggerType}")
    private String offerNotify;
    
    @Value("${so.inbound.getOffer.actionType}")
    private String notify;
   
    
    @Value("${so.actionStatus.success}")
    private String success;

    @Override
    public void process(Exchange exchange) throws Exception {
        String coreResponse = exchange.getIn().getBody(String.class);
        JsonNode coreResponseJson = jsonObjectMapper.readTree(coreResponse);
        List<String> soNotificationMessages = createSONotificationMessageList(coreResponseJson);
        log.info("Total Offer found: {}", soNotificationMessages.size());
        exchange.getIn().setBody(soNotificationMessages);
    }

    /**
     * 
     * Creates SONotificationMessages for redis entry and db entry
     * 
     * @param coreResponseJson coreResponseJson
     * @return SONotificationMessage list
     * @throws JsonProcessingException JsonProcessingException
     */
    private List<String> createSONotificationMessageList(JsonNode coreResponseJson) throws JsonProcessingException {
        List<String> soNotificationMessages = new ArrayList<String>();
        JsonNode requesterLocationNode = coreResponseJson.get("REQUESTER_LOCATION");
        for (JsonNode locationNode : requesterLocationNode.get("LOCATIONS")) {
            String locationId = locationNode.get("id").asText();
            if (locationNode.has("OFFERS") && locationNode.get("OFFERS").get(0) != null) {
                log.info("Offer Node size {}", locationNode.get("OFFERS").size());
                for (JsonNode offerNode : locationNode.get("OFFERS")) {
                    log.info("offerNode {}", offerNode);
                    soNotificationMessages.add(createSoNotificationMessage(coreResponseJson, offerNode,locationId));
                }
            }
        }
        return soNotificationMessages;
    }

	/**
	 * 
	 * Creates SONotificationMessage
	 * 
	 * @param coreResponseJson
	 *            coreResponseJson
	 * @param offerNode
	 *            offerNode
	 * @param locationId
	 *            locationId
	 * @return soNotificationMessageS String
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
    private String createSoNotificationMessage(JsonNode coreResponseJson, JsonNode offerNode, String locationId)
            throws JsonProcessingException {

        SONotificationMessage soNotificationMessage = SOMessageObjectFactory
                .createSONotificationMessageForInbound(soMessageTemplate, coreResponseJson, offerNode, jsonObjectMapper,locationId);
        soNotificationMessage.setMetaInfo(SOMessageUtils.createDefaultMetaInfo("INBOUND-NOTIFY", "notify",
                "Inbound-Request", "USSD", "actionType"));
        soNotificationMessage.setTriggerType(offerNotify);
        soNotificationMessage.setActionType(notify);
       
        soNotificationMessage.setActionStatus(success);
        String msisdn = SOMessageUtils.getMSISDN(soNotificationMessage);
        String flowId = SOMessageUtils.getFlowId(soNotificationMessage);
        String inboundOfferCacheKey = CacheKeyUtils.generateInboundCacheKey(msisdn,flowId);
        String soNotificationMessageAsString = jsonObjectMapper.writeValueAsString(soNotificationMessage);
        log.info("soMessageAsJson: {}", soNotificationMessageAsString);
        redisConnectionPool.putInCache(inboundOfferCacheKey, soNotificationMessageAsString, inboundRedisTtl);
        return soNotificationMessageAsString;
    }

}
