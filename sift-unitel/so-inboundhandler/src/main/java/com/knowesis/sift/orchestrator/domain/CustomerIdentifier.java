package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author SO Development Team
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerIdentifier implements Serializable
{
	private static final long serialVersionUID = 1L;
@JsonProperty("MSISDN")
private String msisdn;



public String getMsisdn() {
	return msisdn;
}



public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
}



/*
 * (non-Javadoc)
 *
 * @see java.lang.Object#toString()
 */
@Override
public String toString() {
StringBuilder builder = new StringBuilder();
builder.append("CustomerIdentifier [MSISDN=");
builder.append(msisdn);
builder.append("]");
return builder.toString();
}
}


