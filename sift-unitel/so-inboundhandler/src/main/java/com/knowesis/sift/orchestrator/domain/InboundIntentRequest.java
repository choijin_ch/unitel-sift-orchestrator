package com.knowesis.sift.orchestrator.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author SO Development Team
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class InboundIntentRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("RECORD_TYPE")
	private String recordType;
	@JsonProperty("CUSTOMER_IDENTIFIER")
	private CustomerIdentifier customerIdentifier;
	@JsonProperty("REQUEST_ID")
	private String requestId;
	@JsonProperty("INTENT")
	private String intent;
	@JsonProperty("CONTEXT_DATA")
	private ContextData contextData;
	@JsonProperty("REQUESTER_CHANNEL")
	private String requesterChannel;
	@JsonProperty("REQUESTER_APPLICATION")
	private String requesterApplication;
	@JsonProperty("REQUESTER_ZONE")
	private String requesterZone;
	@JsonProperty("REQUESTER_LOCATION")
	private RequesterLocation requesterLocation;
	
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public CustomerIdentifier getCustomerIdentifier() {
		return customerIdentifier;
	}
	public void setCustomerIdentifier(CustomerIdentifier customerIdentifier) {
		this.customerIdentifier = customerIdentifier;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	
	public ContextData getContextData() {
		return contextData;
	}
	public void setContextData(ContextData contextData) {
		this.contextData = contextData;
	}
	
	
	public String getRequesterChannel() {
		return requesterChannel;
	}
	public void setRequesterChannel(String requesterChannel) {
		this.requesterChannel = requesterChannel;
	}
	public String getRequesterApplication() {
		return requesterApplication;
	}
	public void setRequesterApplication(String requesterApplication) {
		this.requesterApplication = requesterApplication;
	}
	public String getRequesterZone() {
		return requesterZone;
	}
	public void setRequesterZone(String requesterZone) {
		this.requesterZone = requesterZone;
	}
	public RequesterLocation getRequesterLocation() {
		return requesterLocation;
	}
	public void setRequesterLocation(RequesterLocation requesterLocation) {
		this.requesterLocation = requesterLocation;
	}
    
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UssdOfferRequest [ RECORD_TYPE=");
		builder.append(recordType);
		builder.append(", CUSTOMER_IDENTIFIER=");
		builder.append(customerIdentifier);
		builder.append(", REQUEST_ID=");
		builder.append(requestId);
		builder.append(", REQUESTER_ZONE=");
		builder.append(requesterZone);
		builder.append(", REQUESTER_LOCATION=");
		builder.append(requesterLocation);
		builder.append(", CONTEXT_DATA =");
		builder.append(contextData);
		builder.append("]");
		return builder.toString();
	}
}
