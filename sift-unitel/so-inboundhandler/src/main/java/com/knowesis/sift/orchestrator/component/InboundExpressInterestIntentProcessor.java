/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.core.online.V3.PandamaticOnline;
import com.knowesis.sift.orchestrator.domain.InboundIntentRequest;
import com.knowesis.sift.orchestrator.domain.Location;
import com.knowesis.sift.orchestrator.domain.RequesterLocation;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * 
 * Processes Inbound Express-Interest intent
 * 
 * @author SO Development Team
 */
@Component("InboundExpressInterestIntentProcessor")
public class InboundExpressInterestIntentProcessor implements Processor {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	PandamaticOnline pandamaticOnline;

	@Autowired
	ObjectMapper jsonObjectMapper;

	@Autowired
	RedisConnectionPool redisConnectionPool;

	@Value("${so.inbound.intent.actionresponse}")
	private String actionResponse;

	@Value("${so.inbound.interest.actionType}")
	private String actionType;

	@Value("${so.inbound.interest.triggerType}")
	private String triggerType;

	@Value("${so.inbound.compare.participationType}")
	private String participationType;

	@Value("${so.inbound.compare.newOfferFlag}")
	private String newOfferFlagValue;

	@Override
	public void process(Exchange exchange) throws Exception {
		String message = exchange.getIn().getBody(String.class);
		JsonParser parser = new JsonParser();
		JsonObject request = new JsonObject();
		String resultString = "";
		log.info("Express Interest intent request :{}", message);
		exchange.getIn().setHeader("OriginalRequestMessage", message);
		List<String> offerList = getofferFromCache(message);
		String filteredrequest = intentRequestFilter(message, offerList);
		if (!StringUtils.isBlank(filteredrequest)) {
			request = (JsonObject) parser.parse(filteredrequest);
			JsonObject result = pandamaticOnline.handleOnlineRequest(request);
			resultString = result.toString();
			log.info("Response from core for inbound Express-Interest intent request: {}", resultString);
		}
		exchange.getIn().setHeader("OfferMessagesToDB", offerList);

		exchange.getIn().setBody(resultString);
	}

	/**
	 * 
	 * @param message
	 *            intent request
	 * @return offers from cache
	 * @throws JsonParseException
	 *             Exception
	 * @throws JsonMappingException
	 *             Exception
	 * @throws IOException
	 *             Exception
	 */
	private List<String> getofferFromCache(String message)
			throws JsonParseException, JsonMappingException, IOException {
		InboundIntentRequest inboundIntentRequest = jsonObjectMapper.readValue(message, InboundIntentRequest.class);
		List<String> soNotificationMessages = new ArrayList<String>();
		String msisdn = inboundIntentRequest.getCustomerIdentifier().getMsisdn();
		ArrayList<Location> locationList = (ArrayList<Location>) inboundIntentRequest.getRequesterLocation()
				.getLocations();
		for (Location location : locationList) {
			List<String> flowIdList = location.getFlowId();
			for (String flowId : flowIdList) {

				String inboundOfferCacheKey = CacheKeyUtils.generateInboundCacheKey(msisdn, flowId);
				String offerNotificationmessage = redisConnectionPool.getObjectFromCache(inboundOfferCacheKey);
				soNotificationMessages.add(setReportingParams(offerNotificationmessage));
			}
		}
		return soNotificationMessages;

	}

	/**
	 * 
	 * @param message
	 *            notification message
	 * @return soNotificationMessageAsString message
	 * @throws JsonParseException
	 *             Exception
	 * @throws JsonMappingException
	 *             Exception
	 * @throws IOException
	 *             Exception
	 */
	private String setReportingParams(String message) throws JsonParseException, JsonMappingException, IOException {
		SONotificationMessage soNotificationMessage = jsonObjectMapper.readValue(message, SONotificationMessage.class);
		soNotificationMessage.setActionResponse(actionResponse);
		soNotificationMessage.setActionType(actionType);
		soNotificationMessage.setTriggerType(triggerType);
		soNotificationMessage.setMetaInfo(SOMessageUtils.createDefaultMetaInfo(actionType, "notify",
				"Inbound-Intent-ExpressInterest", "USSD", "actionType"));
		String soNotificationMessageAsString = jsonObjectMapper.writeValueAsString(soNotificationMessage);
		return soNotificationMessageAsString;

	}

	/**
	 * To Filter the flow id based on the condition 
	 * 
	 * Condition is 
	 * @param message
	 *            Intent request
	 * @param offerList
	 *            offers fetched from redis
	 * @return intent request if it meets conditions
	 * @throws JsonParseException
	 *             Exception
	 * @throws JsonMappingException
	 *             Exception
	 * @throws IOException
	 *             Exception
	 */
	private String intentRequestFilter(String message, List<String> offerList)
			throws JsonParseException, JsonMappingException, IOException {
		InboundIntentRequest intentRequest = jsonObjectMapper.readValue(message, InboundIntentRequest.class);
		HashMap<String, List<String>> idAndFlowidList = new HashMap<String, List<String>>();
		for (String offer : offerList) {
			SONotificationMessage soNotificationMessage = jsonObjectMapper.readValue(offer,
					SONotificationMessage.class);
			String offerParticipationType = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0)
					.getOffers().get(0).getParticipationType();
			String newOfferFlag = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0)
					.getOffers().get(0).getNewOfferFlag();
			if (offerParticipationType.equalsIgnoreCase(participationType)
					&& newOfferFlag.equalsIgnoreCase(newOfferFlagValue)) {
				addFlowidAndIdToList(soNotificationMessage, idAndFlowidList);
			}
		}
		if (!idAndFlowidList.isEmpty()) {
			intentRequest.setRequesterLocation(updateRequest(idAndFlowidList));
			String intentRequestString = jsonObjectMapper.writeValueAsString(intentRequest);
			log.info("Express Interest intent request after filtration:{}", intentRequestString);
			return intentRequestString;
		}
		return null;

	}

	/**
	 * To update the request location after filtration  based on the condition
	 * 
	 * @param idAndFlowidList
	 *            list with id and flowid which meets the condition
	 * @return loaction with id and flowid from the list
	 */
	private RequesterLocation updateRequest(HashMap<String, List<String>> idAndFlowidList) {
		RequesterLocation requesterLocation = new RequesterLocation();
		List<Location> locations = new ArrayList<Location>();
		for (String id : idAndFlowidList.keySet()) {
			Location location = new Location();
			location.setFlowId(idAndFlowidList.get(id));
			location.setId(id);
			locations.add(location);
		}
		requesterLocation.setLocations(locations);
		log.debug("Updated Location after filtration :{}", requesterLocation);
		return requesterLocation;
	}

	
	/**
	 * To add Flowid and Id to the list
	 * 
	 * @param soNotificationMessage
	 *            so notification message
	 * @param idAndFlowidList
	 *            Hash Map
	 */
	private void addFlowidAndIdToList(SONotificationMessage soNotificationMessage,
			HashMap<String, List<String>> idAndFlowidList) {
		String id = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getId();
		String flowId = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers()
				.get(0).getFlowId();
		if (idAndFlowidList.containsKey(id)) {

			List<String> flowIdList = idAndFlowidList.get(id);
			flowIdList.add(flowId);
		} else {
			List<String> flowIdList = new ArrayList<String>();
			flowIdList.add(flowId);
			idAndFlowidList.put(id, flowIdList);
		}
	}

}
