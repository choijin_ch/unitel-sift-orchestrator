/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knowesis.sift.core.online.V3.PandamaticOnline;
import com.knowesis.sift.orchestrator.domain.InboundIntentRequest;
import com.knowesis.sift.orchestrator.domain.Location;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;


/**
 * 
 * Processes Inbound Reject intent
 * 
 * @author SO Development Team
 */
@Component("InboundRejectIntentProcessor")
public class InboundRejectIntentProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    PandamaticOnline pandamaticOnline;
    @Autowired
    ObjectMapper jsonObjectMapper;
    
    @Autowired
    RedisConnectionPool redisConnectionPool;
    
    
    @Value("${so.inbound.intent.actionresponse}")
    private String actionResponse;
    
    @Value("${so.inbound.reject.actionType}")
    private String actionType;
    
    @Value("${so.inbound.reject.triggerType}")
    private String triggerType;
    
    @Override
    public void process(Exchange exchange) throws Exception {
        String message = exchange.getIn().getBody(String.class);
        JsonParser parser = new JsonParser();
        JsonObject request = new JsonObject();
        request = (JsonObject) parser.parse(message);
        log.info("Reject intent request :{}",message);
        pandamaticOnline.handleOnlineRequest(request);
        List<String> offerListDB = getofferFromCache(message);
        exchange.getIn().setHeader("OfferMessagesToDB", offerListDB);
        exchange.getIn().setHeader("OriginalRequestMessage", message);
    }
    /**
     * 
     * @param message message
     * @return offers message
     * @throws JsonParseException Exception
     * @throws JsonMappingException Exception
     * @throws IOException Exception
     */
    private List<String> getofferFromCache(String message) throws JsonParseException, JsonMappingException, IOException {
		 InboundIntentRequest inboundIntentRequest = jsonObjectMapper.readValue(message,
	        		InboundIntentRequest.class);
		 List<String> soNotificationMessages = new ArrayList<String>();
		 String msisdn=inboundIntentRequest.getCustomerIdentifier().getMsisdn();
		 ArrayList<Location> locationList=(ArrayList<Location>) inboundIntentRequest.getRequesterLocation().getLocations();
		 for(Location location:locationList){
			 List<String> flowIdList=location.getFlowId();
			 for(String flowId:flowIdList){
				  
				 String inboundOfferCacheKey = CacheKeyUtils.generateInboundCacheKey(msisdn,flowId);
				String offerNotificationmessage= redisConnectionPool.getObjectFromCache(inboundOfferCacheKey);
				soNotificationMessages.add(setReportingParams(offerNotificationmessage));
			 }
		 }
		return soNotificationMessages;
		
    
	}
    /**
     * 
     * @param message soNotificationMessage
     * @return soNotificationMessage Message
     * @throws JsonParseException Exception
     * @throws JsonMappingException Exception
     * @throws IOException Exception
     */
	private String setReportingParams(String message) throws JsonParseException, JsonMappingException, IOException  {
		 SONotificationMessage soNotificationMessage=jsonObjectMapper.readValue(message,
				 SONotificationMessage.class);
		 soNotificationMessage.setActionResponse(actionResponse);
		 soNotificationMessage.setActionType(actionType);
		 soNotificationMessage.setTriggerType(triggerType);
		 soNotificationMessage.setMetaInfo(SOMessageUtils.createDefaultMetaInfo(actionType, "notify",
	                "Inbound-Intent-REJECT", "USSD", "actionType"));
		 String soNotificationMessageAsString = jsonObjectMapper.writeValueAsString(soNotificationMessage);
		 return soNotificationMessageAsString;

   
	}
}
