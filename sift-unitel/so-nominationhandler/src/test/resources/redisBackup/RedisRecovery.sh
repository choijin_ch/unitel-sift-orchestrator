#!/bin/bash
echo "started recovering....";
declare -a recoverKeyArray
count=0
#file location where we have the backup file
file="/home/vipin/Desktop/myredisbackupfile.txt"

while read line
do
	value=""
       	linearray=($(awk -F "--->" '{$1=$1} 1' <<<"${line}"))
	arraylength=${#linearray[@]}
	key=${linearray[1]}
	ttl=${linearray[4]}
	for (( i=7; i<${arraylength}+1; i++ ))
	do
  		value=$value${linearray[$i]}
	done
	redis-cli set "$key" "$value"
	redis-cli expire "$key" "$ttl" 
	echo "value inserted for key ${linearray[1]} with ttl ${linearray[4]}" >> insertedRedisKey.txt	
done <"$file"
