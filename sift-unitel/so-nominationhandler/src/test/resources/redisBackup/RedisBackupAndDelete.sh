#!/bin/bash
echo "started deleting....";
declare -a keyArray
declare -a delArray
keyArray=( $(redis-cli KEYS "NOMINATION*") )

#redis ttl upperlimit in milliseconds
UPPER=839345

#redis ttl lowerlimit in milliseconds
LOWER=148000

count=0
del=0
delCount=0
for key in "${keyArray[@]}"  
do
   
   ttl=$(redis-cli ttl "$key")
	echo "key $key ttl $ttl"
	 if (( $ttl > $LOWER && $ttl < $UPPER ))
	then 
		count=$(( $count + 1 ))
		delArray[$del]=$key
		del=$(( $del + 1 ))
	fi
done 

echo "the number of records found : $count"
echo "Do you want to continue (y)"
read reply

if (( reply == 'y' ))
then 
	echo "ok"
	for key in "${delArray[@]}"  
	do
		  ttl=$(redis-cli ttl "$key")
     		  value=$(redis-cli get "$key")
		  echo "key ---> $key ::: ttl---> $ttl ::: value---> $value" >> myredisbackupfile.txt 
		  redis-cli DEL "$key"
		  delCount=$(( $delCount + 1 ))
	done
echo "$delCount number of redis records deleted..."
fi
