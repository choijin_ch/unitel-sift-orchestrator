/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.core.domain.Message;
import com.knowesis.sift.orchestrator.core.domain.MessageContainer;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;
import com.knowesis.sift.orchestrator.commons.CommonFunctions;

/**
 * 
 * Processes Fulfillment Message for NominationFailureNotification
 * 
 * @author SO Development Team
 */
@Component("NotifyNominatorProcessor")
public class NotifyNominatorProcessor implements Processor {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	ObjectMapper jsonObjectMapper;

	@Autowired
	RedisConnectionPool redisConnectionPool;

	@Value("${so.actionType.notify}")
	private String notify;

	@Value("${so.triggerType.notifyNominator}")
	private String notifyNominator;

	@Value("${so.actionStatus.success}")
	private String success;

	@Value("${so.messsage.notifyNominatorFailure}")
	private String notifyNominatorFailureMesssage;

	@Value("${so.messsage.notifyNominatorSuccess}")
	private String notifyNominatorSuccessMesssage;
	
	@Value("${so.notification.template}")
	private String soMessageTemplate;

	@Override
	public void process(Exchange exchange) throws Exception {
		String message = exchange.getIn().getBody(String.class);
		log.info("Inside NotifyNominatorProcessor() {}",message);
		SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
		SOFulfillmentMessage nomineeFulfillmentMessage = (SOFulfillmentMessage) soMessage;
		List<FulfillmentProduct> fulfillmentProducts = SOMessageUtils.getFulfillmentProducts(nomineeFulfillmentMessage);
		FulfillmentProduct nomineeFulfillmentProduct = SOMessageUtils.getNominationProduct(fulfillmentProducts);
		Map<String, String> dynamicParameterMap = SOMessageUtils
				.getFulfillmentProductDynamicParametersAsMap(nomineeFulfillmentProduct);
		String msisdn = dynamicParameterMap.get("NOMINATOR_NUMBER");
		String failureMessage = dynamicParameterMap.get("NOMINATOR_FAILUREMESSAGE");
		String successMessage = dynamicParameterMap.get("NOMINATOR_SUCCESSMESSAGE");
		String nominationShortCode = dynamicParameterMap.get("NOMINATION_SHORT_CODE");
		String subscribtionId = SOMessageUtils.getSubscriptionId(nomineeFulfillmentMessage);
		
		String messageToSend = null;
		if (subscribtionId.equals("0")) {
			log.info(" Sending nominator({}) register action and nominee({}) fulfillment failed register action",
					msisdn, SOMessageUtils.getMSISDN(soMessage));
			messageToSend = setTextBeSendForFailure(failureMessage);
			setNotiFyNominationForSubIdZero(exchange, msisdn, messageToSend, nominationShortCode,
					nomineeFulfillmentMessage);

		} else if (subscribtionId != "0") {
			log.info(" Sending fulfillment Success Notification to ({})", SOMessageUtils.getMSISDN(soMessage));
			messageToSend = setTextBeSendForSuccess(successMessage);
			setNotiFyNominationForSubIdNotZero(exchange, msisdn, messageToSend, nominationShortCode,
					nomineeFulfillmentMessage);
		}
	}

	/**
	 * Set text be send as generic or from message
	 * 
	 * @param successMessage successMessage from core
	 * @return message
	 */
	private String setTextBeSendForSuccess(String successMessage) {
		String messageToSend = null;
		if (StringUtils.isNotBlank(successMessage)) {
			messageToSend = successMessage;
		} else {
			messageToSend = notifyNominatorSuccessMesssage;
		}
		return messageToSend;
	}
	
	/**
	 * Set text be send as generic or from message
	 * 
	 * @param failureMessage failureMessage from core
	 * @return message
	 */
	private String setTextBeSendForFailure(String failureMessage) {
		String messageToSend = null;
		if (StringUtils.isNotBlank(failureMessage)) {
			messageToSend = failureMessage;
		} else {
			messageToSend = notifyNominatorFailureMesssage;
		}
		return messageToSend;
	}

	/**
	 * process to notify when subId is not zero
	 * 
	 * @param exchange exchange
	 * @param msisdn msisdn
	 * @param messageToSend messageToSend
	 * @param nominationShortCode nominationShortCode
	 * @param nomineeFulfillmentMessage nomineeFulfillmentMessage
	 */
	private void setNotiFyNominationForSubIdNotZero(Exchange exchange, String msisdn, String messageToSend,
			String nominationShortCode, SOFulfillmentMessage nomineeFulfillmentMessage) {
		try {
			nomineeFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0).setMsisdn(msisdn);
			Message messageToSet = new Message();
			messageToSet.setTextTobeSent(messageToSend);
			List<Message> messageList = new ArrayList<Message>();
			messageList.add(messageToSet);
			MessageContainer container = new MessageContainer();
			container.setMessages(messageList);
			container.setChannelName("sms");
			container.setShortCode(nominationShortCode);
			List<MessageContainer> messageConatinerList = new ArrayList<MessageContainer>();
			messageConatinerList.add(container);
			nomineeFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.setFulfillmentMessages(messageConatinerList);
			nomineeFulfillmentMessage.setTriggerType(notifyNominator);
			nomineeFulfillmentMessage.setActionType(notify);
			nomineeFulfillmentMessage.setActionStatus(success);
			exchange.getIn().setHeader("nomineeFulfillmentMessageForMT",
					jsonObjectMapper.writeValueAsString(nomineeFulfillmentMessage));
			exchange.getIn().setBody(null);
			return;
		} catch (Exception exception) {
			log.debug("Exception occured in NotifyNominator processor for sub not zero", exception);
		}
	}

	/**
	 * process when subscriberId is zero
	 * 
	 * @param exchange exchange
	 * @param msisdn msisdn
	 * @param messageToSend messageToSend
	 * @param nominationShortCode nominationShortCode
	 * @param nomineeFulfillmentMessage nomineeFulfillmentMessage
	 */
	private void setNotiFyNominationForSubIdZero(Exchange exchange, String msisdn, String messageToSend,
			String nominationShortCode, SOFulfillmentMessage nomineeFulfillmentMessage) {
		try {
			String nominatorCacheKey = CacheKeyUtils.generateNominationCacheKey(msisdn, nominationShortCode);
			String nominatorRedisMessage = CommonFunctions.reduceNominationCountInRedis(nominatorCacheKey,
					notifyNominator, redisConnectionPool, jsonObjectMapper, log);
			if (nominatorRedisMessage != "nomination redis key not modified") {
				SOMessage soNominatorRedisMessage = RawMessageUtils.processRawMessage(nominatorRedisMessage,
						jsonObjectMapper);
				SOMessageUtils.setHeaderValue(soNominatorRedisMessage, "NOMINEE-NUMBER",
						SOMessageUtils.getMSISDN(nomineeFulfillmentMessage));
				SOMessageUtils.setHeaderValue(soNominatorRedisMessage, "NOMINATOR-NUMBER", msisdn);
				soNominatorRedisMessage.setActionType(notifyNominator);
				SOFulfillmentMessage nomineeFulfillmentMessageForMT = nomineeFulfillmentMessage;
				String mtMessage = jsonObjectMapper.writeValueAsString(nomineeFulfillmentMessageForMT);
				SONotificationMessage soNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
						soMessageTemplate, msisdn, mtMessage, messageToSend, "SMS", jsonObjectMapper);
				Message messageToSet = new Message();
				messageToSet.setTextTobeSent(messageToSend);
				List<Message> messageList = new ArrayList<Message>();
				messageList.add(messageToSet);
				MessageContainer container = new MessageContainer();
				container.setMessages(messageList);
				container.setChannelName("sms");
				container.setShortCode(nominationShortCode);
				// container.setOptInKeyWord(SOMessageUtils.getMSISDN(soMessage));
				List<MessageContainer> messageConatinerList = new ArrayList<MessageContainer>();
				messageConatinerList.add(container);
				soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
						.setNotificationMessages(messageConatinerList);
				soNotificationMessage.setTriggerType(notifyNominator);
				soNotificationMessage.setActionType(notify);
				soNotificationMessage.setActionStatus(success);
				exchange.getIn().setHeader("nomineeFulfillmentMessageForMT",
						jsonObjectMapper.writeValueAsString(soNotificationMessage));
				exchange.getIn().setHeader("nominatorMsisdn", msisdn);
				exchange.getIn().setBody(soNominatorRedisMessage);
				return;
			}
		} catch (Exception exception) {
			log.debug("Exception occured in NotifyNominator processor for sub zero", exception);
		}
	}
}
