/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.commons.CommonFunctions;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.NominationUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Handles nomination related stuffs
 * 
 * @author SO Development Team
 */
@Component("NominationRequestProcessor")
public class NominationRequestProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Autowired
    RedisConnectionPool redisConnectionPool;

    @Value("${so.notification.template}")
    private String soMessageTemplate;

    @Value("${so.triggerType.nominationNotify}")
    private String nominationNotify;

    @Value("${so.actionType.notify}")
    private String notify;

    @Value("${so.actionStatus.success}")
    private String success;
    
	private  long aDayInMilliseconds = 86400000;

   	private  long aDayInSeconds = 86400;

	@Override
	public void process(Exchange exchange) throws Exception {
		String message = exchange.getIn().getBody(String.class);
		log.info("Nomination Message recieved: {}", message);
		SOFulfillmentMessage soFulfillmentMessage = (SOFulfillmentMessage) RawMessageUtils.processRawMessage(message,
				jsonObjectMapper);
		removeUnnecessaryHeaderValues(soFulfillmentMessage);
		FulfillmentProduct nominationFulfillmenProduct = SOMessageUtils
				.getNominationProduct(SOMessageUtils.getFulfillmentProducts(soFulfillmentMessage));
		log.debug("Nomination Fulfillment Product: {}", nominationFulfillmenProduct);
		Map<String, String> nominationDynamicParameterMap = SOMessageUtils
				.getFulfillmentProductDynamicParametersAsMap(nominationFulfillmenProduct);
		String msisdn = SOMessageUtils.getMSISDN(soFulfillmentMessage);
		String nominationShortCode = nominationDynamicParameterMap.get("NOMINATION_SHORT_CODE");
		String nominationValidity = nominationDynamicParameterMap.get("NOMINATION_VALIDITY");
		String nominationCount = nominationDynamicParameterMap.get("NOMINATION_COUNT");
		String nominationCacheKey = CacheKeyUtils.generateNominationCacheKey(msisdn, nominationShortCode);
		if (StringUtils.isNumeric(nominationCount) && StringUtils.isNumeric(nominationValidity)) {
			Long initialValidityTimeForDB = aDayInMilliseconds * Long.parseLong(nominationValidity)
					- (NominationUtils.getPastMillisecondForToday() + 500); // 500 milliseconds added for neglecting the processing time
			int initialValidityTime = (int) (aDayInSeconds * Integer.parseInt(nominationValidity)
					- Math.round((float) NominationUtils.getPastMillisecondForToday() / 1000));
			String timeStampArray = "";
			if (StringUtils.isNotBlank(nominationCacheKey)) {
				timeStampArray = nominationKeyUpdation(soFulfillmentMessage, msisdn, nominationCount,
						initialValidityTime, nominationCacheKey, initialValidityTimeForDB);
				if (StringUtils.isNotBlank(timeStampArray)) {
					String offerStartTimestamp = timeStampArray.split(":")[0];
					String offerEndTimestamp = timeStampArray.split(":")[1];
					log.info("Nomination offer start timestamp: {} and end timestamp: {}", offerStartTimestamp,
							offerEndTimestamp);
					String nominationMessage = nominationDynamicParameterMap.get("NOMINATION_MESSAGE");
					sendNominationMessage(exchange, msisdn, message, nominationMessage, nominationShortCode,
							offerStartTimestamp, offerEndTimestamp);
					return;
				}
			}
			exchange.getIn().setBody(null);
		}
	}

	/**
	 * Nomination Key generation/Updation
	 * 
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @param msisdn
	 *            msisdn
	 * @param nominationCount
	 *            nominationCount
	 * @param initialValidityTime
	 *            initialValidityTime
	 * @param nominationCacheKey
	 *            nominationCacheKey
	 * @param initialValidityTimeForDB
	 *            initialValidityTimeForDB
	 * @return timeStampArray
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	private String nominationKeyUpdation(SOFulfillmentMessage soFulfillmentMessage, String msisdn,
			String nominationCount, int initialValidityTime, String nominationCacheKey, Long initialValidityTimeForDB) throws JsonProcessingException {
		String existingNominationMessage = redisConnectionPool.getObjectFromCache(nominationCacheKey);
		String timeStampArray = "";
		if (StringUtils.isNotBlank(existingNominationMessage)) {
			timeStampArray = updateExistingNominationKey(soFulfillmentMessage, existingNominationMessage,
					msisdn, nominationCount, initialValidityTime, nominationCacheKey, initialValidityTimeForDB);
		} else {
			timeStampArray = createNominationKey(soFulfillmentMessage, msisdn, nominationCacheKey,
					nominationCount, initialValidityTime, initialValidityTimeForDB);
		}
		return timeStampArray;
	}

	/**
	 * create nomination cacheKey for initial time
	 * 
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @param msisdn
	 *            msisdn
	 * @param nominationCacheKey
	 *            nominationCacheKey
	 * @param nominationCount
	 *            nominationCount
	 * @param initialValidityTime
	 *            initialValidityTime
	 * @param initialValidityTimeForDB
	 *            initialValidityTimeForDB
	 * @return timeStampArray
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	private String createNominationKey(SOFulfillmentMessage soFulfillmentMessage, String msisdn,
			String nominationCacheKey, String nominationCount, int initialValidityTime, Long initialValidityTimeForDB) throws JsonProcessingException {
		log.info("Generated nomination redis cache key for MSISDN {} is {}", msisdn, nominationCacheKey);
		SOMessageUtils.setHeaderValue(soFulfillmentMessage, "TOTAL-NOMINATION-COUNT", nominationCount);
		SOMessageUtils.setHeaderValue(soFulfillmentMessage, "CURRENT-NOMINAION-COUNT", "0");
		String nominationStartTime = SOMessageUtils.getUnixTimestamp();
		SOMessageUtils.setHeaderValue(soFulfillmentMessage, "NOMINATION-START-TIME", nominationStartTime);
		SOMessageUtils.setHeaderValue(soFulfillmentMessage, "INITIAL-VALIDITY-TIME", "" + initialValidityTime);
		String offerStartTimestamp = nominationStartTime;
		String offerEndTimestamp = "" + (Long.parseLong(nominationStartTime) + initialValidityTimeForDB);
		log.info("Initial TTL for nomination cache key {} is {}", nominationCacheKey, initialValidityTime);
		redisConnectionPool.putInCache(nominationCacheKey, jsonObjectMapper.writeValueAsString(soFulfillmentMessage),
				initialValidityTime);
		return offerStartTimestamp+":"+offerEndTimestamp;
	}

	/**
	 * prepare and send nomination notification message
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @param message
	 *            message
	 * @param nominationMessage
	 *            nominationMessage
	 * @param nominationShortCode
	 *            nominationShortCode
	 * @param offerStartTimestamp
	 *            offerStartTimestamp
	 * @param offerEndTimestamp
	 *            offerEndTimestamp
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 * @throws IOException
	 *             IOException
	 */
	private void sendNominationMessage(Exchange exchange, String msisdn, String message, String nominationMessage,
			String nominationShortCode, String offerStartTimestamp, String offerEndTimestamp)
			throws JsonProcessingException, IOException {
		if (StringUtils.isNotBlank(nominationMessage)) {
			SONotificationMessage soNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
					soMessageTemplate, msisdn, message, nominationMessage, "SMS", jsonObjectMapper);
			// taking from templates created by us so no chance of NPE
			soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
					.getNotificationMessages().get(0).setShortCode(nominationShortCode);
			soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
					.setOfferStartDate(offerStartTimestamp);
			soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
					.setOfferEndDate(offerEndTimestamp);
			soNotificationMessage.setTriggerType(nominationNotify);
			soNotificationMessage.setActionType(notify);
			soNotificationMessage.setActionStatus(success);
			exchange.getIn().setHeader("msisdn", msisdn);
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soNotificationMessage));
			return;
		} else {
			exchange.getIn().setBody(null);
		}
	}

	/**
	 * update existing nomination cache key for higher validity
	 * 
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @param existingNominationMessage
	 *            existingNominationMessage
	 * @param msisdn
	 *            msisdn
	 * @param nominationCount
	 *            nominationCount
	 * @param initialValidityTime
	 *            initialValidityTime
	 * @param nominationCacheKey
	 *            nominationCacheKey
	 * @param initialValidityTimeForDB
	 *            initialValidityTimeForDB
	 * @return timeStampArray
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	private String updateExistingNominationKey(SOFulfillmentMessage soFulfillmentMessage,
			String existingNominationMessage, String msisdn, String nominationCount, int initialValidityTime,
			String nominationCacheKey, Long initialValidityTimeForDB) throws JsonProcessingException {
		SOFulfillmentMessage existingSoFulfillmentMessage = (SOFulfillmentMessage) RawMessageUtils
				.processRawMessage(existingNominationMessage, jsonObjectMapper);
		int redisValidityForExistingMessage = CommonFunctions
				.getRedisValidityForNomination(existingSoFulfillmentMessage);
		int totalNominationCountForExisting = getTotalNominationCountForExisting(existingSoFulfillmentMessage);
		int updatedTotalNominationCountForExisting = totalNominationCountForExisting
				+ Integer.parseInt(nominationCount);
		String timeStampArray = "";
		if (redisValidityForExistingMessage > initialValidityTime) {
			log.info(
					"Redis validity of existing message is greater than new nomination message. Updating TOTAL-NOMINATION-COUNT {} of exising message",
					totalNominationCountForExisting);
			log.info("Updated TOTAL-NOMINATION-COUNT: {}", updatedTotalNominationCountForExisting);
			SOMessageUtils.setHeaderValue(existingSoFulfillmentMessage, "TOTAL-NOMINATION-COUNT",
					"" + updatedTotalNominationCountForExisting);
			// TODO recheck over call, can simply set ttl or get to avoid
			// calculation
			redisConnectionPool.putInCache(nominationCacheKey,
					jsonObjectMapper.writeValueAsString(existingSoFulfillmentMessage), redisValidityForExistingMessage);
			String offerStartTimestamp = SOMessageUtils.getHeaderValue(existingSoFulfillmentMessage,
					"NOMINATION-START-TIME");
			String nominationStartTime = offerStartTimestamp;
			String initialvalidityTime = SOMessageUtils.getHeaderValue(existingSoFulfillmentMessage,
					"INITIAL-VALIDITY-TIME");
			if (StringUtils.isNumeric(nominationStartTime)
					&& StringUtils.isNumeric(initialvalidityTime)) {
				String offerEndTimestamp = ""
						+ (Long.parseLong(nominationStartTime) + Long.parseLong(initialvalidityTime));
				timeStampArray = offerStartTimestamp + ":" + offerEndTimestamp;
			}
		} else {
			timeStampArray = updateNominationKeyWithNewTime(soFulfillmentMessage, msisdn,
					updatedTotalNominationCountForExisting, existingSoFulfillmentMessage, initialValidityTime,
					initialValidityTimeForDB, nominationCacheKey);
		}
		return timeStampArray;
	}
	
	/**
	 * setTotalNominationCountForExisting
	 * 
	 * @param existingSoFulfillmentMessage
	 *            existingSoFulfillmentMessage
	 * @return nominationCountForExisting
	 */
	private int getTotalNominationCountForExisting(SOFulfillmentMessage existingSoFulfillmentMessage) {
		String totalNominationCountForExisting = SOMessageUtils.getHeaderValue(existingSoFulfillmentMessage,
				"TOTAL-NOMINATION-COUNT");
		if (StringUtils.isNumeric(totalNominationCountForExisting)) {
			return Integer.parseInt(totalNominationCountForExisting);
		}
		log.info("TOTAL-NOMINATION-COUNT not a type of numerals");
		return 0;
	}


	/**
	 * Redis validity of exisiting message is less than or equal to new
	 * nomination message. Updating TOTAL-NOMINATION-COUNT
	 * 
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @param msisdn
	 *            msisdn
	 * @param updatedTotalNominationCountForExisting
	 *            updatedTotalNominationCountForExisting
	 * @param existingSoFulfillmentMessage
	 *            existingSoFulfillmentMessage
	 * @param initialValidityTime
	 *            initialValidityTime
	 * @param initialValidityTimeForDB
	 *            initialValidityTimeForDB
	 * @param nominationCacheKey
	 *            nominationCacheKey
	 * @return timeStampArray
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	private String updateNominationKeyWithNewTime(SOFulfillmentMessage soFulfillmentMessage, String msisdn,
			int updatedTotalNominationCountForExisting, SOFulfillmentMessage existingSoFulfillmentMessage,
			int initialValidityTime, Long initialValidityTimeForDB, String nominationCacheKey)
			throws JsonProcessingException {
		log.info(
				"Redis validity of exisiting message is less than or equal to new nomination message. Updating TOTAL-NOMINATION-COUNT {} of new message",
				updatedTotalNominationCountForExisting);
		log.info("Generated nomination redis cache key for MSISDN {} is {}", msisdn, nominationCacheKey);
		SOMessageUtils.setHeaderValue(soFulfillmentMessage, "TOTAL-NOMINATION-COUNT",
				"" + updatedTotalNominationCountForExisting);
		SOMessageUtils.setHeaderValue(soFulfillmentMessage, "CURRENT-NOMINAION-COUNT",
				SOMessageUtils.getHeaderValue(existingSoFulfillmentMessage, "CURRENT-NOMINAION-COUNT"));
		String nominationStartTime = SOMessageUtils.getUnixTimestamp();
		SOMessageUtils.setHeaderValue(soFulfillmentMessage, "NOMINATION-START-TIME", nominationStartTime);
		SOMessageUtils.setHeaderValue(soFulfillmentMessage, "INITIAL-VALIDITY-TIME", "" + initialValidityTime);
		String offerStartTimestamp = nominationStartTime;
		String offerEndTimestamp = "" + (Long.parseLong(nominationStartTime) + initialValidityTimeForDB);
		log.info("Initial TTL for nomination cache key {} is {}", nominationCacheKey, initialValidityTime);
		redisConnectionPool.putInCache(nominationCacheKey, jsonObjectMapper.writeValueAsString(soFulfillmentMessage),
				initialValidityTime);
		return offerStartTimestamp + ":" + offerEndTimestamp;
	}	
    
	/**
	 * Removing unnecessary values OCS and CS and setting actionstatus to null
	 * 
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 */
	private void removeUnnecessaryHeaderValues(SOFulfillmentMessage soFulfillmentMessage) {
		SOMessageUtils.removeHeaderValue(soFulfillmentMessage, "PROVISIONED-OFFERID");
		SOMessageUtils.removeHeaderValue(soFulfillmentMessage, "PROVISIONED-VALUE");
		SOMessageUtils.removeHeaderValue(soFulfillmentMessage, "LAST-PROVISIONED-TARGET-SYSTEM");
		soFulfillmentMessage.setActionResponse(null);
	}
	
}
