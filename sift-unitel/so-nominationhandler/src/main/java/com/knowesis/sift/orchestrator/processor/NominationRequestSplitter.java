/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * 
 * Processes Fulfillment Message for NominationRequestSplitter
 * 
 * @author SO Development Team
 */
@Component("NominationRequestSplitter")
public class NominationRequestSplitter implements Processor {
    private final static Logger LOG = LoggerFactory.getLogger(NominationRequestSplitter.class);

    @Autowired
	ObjectMapper jsonObjectMapper;
	
	@Value("${so.actionType.nomination}")
	private String nomination;
	
	@Value("${so.triggerType.nominationRequest}")
	private String nominationRequest;
	
	@Value("${so.actionStatus.success}")
	private String success;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		String message = exchange.getIn().getBody(String.class);
		SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
		Map<String, String> offerPayLoad = SOMessageUtils.getOfferPayLoad(soMessage);
		String[] nomineeNumberList = offerPayLoad.get("Nominee_Numbers").split(",");
		List<SOMessage> messages = new ArrayList<>();
		List<String> messageRAHeader = new ArrayList<String>();
		if (nomineeNumberList.length > 0) {
			for (String nomineeNumber : nomineeNumberList) {
				SOMessage soMessageforRA = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
				SOMessageUtils.setHeaderValue(soMessageforRA, "NOMINEE-NUMBER", nomineeNumber);
                soMessageforRA.setActionType(nominationRequest);
                soMessageforRA.setTriggerType(nominationRequest);
                soMessageforRA.setActionStatus(success);
                messageRAHeader.add(jsonObjectMapper.writeValueAsString(soMessageforRA));
                soMessageforRA.setActionType(nomination);
                messages.add(soMessageforRA);
                LOG.debug("messages:{}",soMessageforRA);
			}
			 exchange.getIn().setHeader("MessageToPrg", messageRAHeader);
		}
		LOG.debug("nomineeNumberList:{} messages:{}",nomineeNumberList,messages);
		exchange.getIn().setBody(messages);
	}
}
