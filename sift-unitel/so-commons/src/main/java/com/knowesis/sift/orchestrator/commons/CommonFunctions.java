package com.knowesis.sift.orchestrator.commons;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

/**
 * common methods used for several handlers
 * 
 * @author SO Development Team
 */
@Component
public class CommonFunctions {


	private static long aDayInMilliseconds = 86400000;
	

	/**
	 * 
	 * Get the remining validity of Nomination message
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return remaining validity days
	 */
	public static int getRedisValidityForNomination(SOMessage soMessage) {
		FulfillmentProduct nominationFulfillmenProduct = SOMessageUtils
				.getNominationProduct(SOMessageUtils.getFulfillmentProducts((SOFulfillmentMessage) soMessage));
		Map<String, String> nominationDynamicParameterMap = SOMessageUtils
				.getFulfillmentProductDynamicParametersAsMap(nominationFulfillmenProduct);
		String nominationValidity = nominationDynamicParameterMap.get("NOMINATION_VALIDITY");
		long nominationStartTime = Long.parseLong(SOMessageUtils.getHeaderValue(soMessage, "NOMINATION-START-TIME"));

		long currentTimeStamp = Long.parseLong(SOMessageUtils.getUnixTimestamp());
		long pastTime = currentTimeStamp - nominationStartTime;	
		long nominationValidityTimestamp = aDayInMilliseconds * Integer.parseInt(nominationValidity);
		String redisValidity = "" + Math.round((float) (nominationValidityTimestamp - pastTime) / 1000);
		return Integer.parseInt(redisValidity);
	}

	/**
	 * 
	 * Fetch nomination message from redis reduce the CURRENT_NOMINATON COUNT
	 * header and save. Return updated nomination message to send register
	 * action
	 * 
	 * @param nominatorCacheKey
	 *            nominatorCacheKey
	 * @param provisionFulfillNotifyNominator
	 *            provisionFulfillNotifyNominator
	 * @param redisConnectionPool
	 *            redisConnectionPool
	 * @param jsonObjectMapper
	 *            jsonObjectMapper
	 * @param log
	 *            log
	 * @return updated soNominationMessage to send resister action
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	public static String reduceNominationCountInRedis(String nominatorCacheKey, String provisionFulfillNotifyNominator,
			RedisConnectionPool redisConnectionPool, ObjectMapper jsonObjectMapper, Logger log)
			throws JsonProcessingException {
		String nominatorRedisMessage = redisConnectionPool.getObjectFromCache(nominatorCacheKey);
		if (StringUtils.isNoneBlank(nominatorRedisMessage)) {
			SOFulfillmentMessage nominatorFulfillmentMessage = (SOFulfillmentMessage) RawMessageUtils
					.processRawMessage(nominatorRedisMessage, jsonObjectMapper);

			int nominationCount = Integer
					.parseInt(SOMessageUtils.getHeaderValue(nominatorFulfillmentMessage, "CURRENT-NOMINAION-COUNT"));
			log.info("CURRENT-NOMINAION-COUNT before reducing the count is {}", nominationCount);
			if (nominationCount > 0) {
				SOMessageUtils.setHeaderValue(nominatorFulfillmentMessage, "CURRENT-NOMINAION-COUNT",
						"" + --nominationCount);
				log.info("CURRENT-NOMINATION-COUNT after reducing the count is {}", nominationCount);

				nominatorFulfillmentMessage.setActionType(provisionFulfillNotifyNominator);
				nominatorRedisMessage = jsonObjectMapper.writeValueAsString(nominatorFulfillmentMessage);
				int updatedValidityTime = getRedisValidityForNomination(nominatorFulfillmentMessage);
				log.debug(" TTL of nomination entry with  key {} after reducing CURRET-NOMINATION-COUNT is {}",
						nominatorCacheKey, updatedValidityTime);
				redisConnectionPool.putInCache(nominatorCacheKey, nominatorRedisMessage, updatedValidityTime);
				return nominatorRedisMessage;
			}
		}
		return "nomination redis key not modified";
	}
}
