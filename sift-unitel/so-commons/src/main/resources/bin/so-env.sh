#! /bin/bash

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#title           :so-env.sh
#description     :This script sets env variable
#author          :SIFT SO Team
#version         :0.0.1   
#usage           :bash so-env.sh
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

export SO_HOME=$(dirname `pwd`)
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_60.jdk/Contents/Home
#export JAVA_HOME="$(/usr/libexec/java_home -v 1.8)"