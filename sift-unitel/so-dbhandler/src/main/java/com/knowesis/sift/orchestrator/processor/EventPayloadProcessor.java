/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SOEventPayload;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.SOEventPayloadObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * 
 * This processor prepares data for writing to EventPayload Table
 * 
 * @author SO Development Team
 */
@Component("EventPayloadProcessor")
public class EventPayloadProcessor implements Processor {
	@Autowired
	ObjectMapper jsonObjectMapper;
	@Value("${so.triggerType.offerNotify}")
	private static String notificationTriggerType;
	@Value("${so.triggerType.reminderNotify}")
	private static String reminderTriggerType;
	@Value("${so.triggerType.fulfill}")
	private static String fulfillTriggerType;

	@Value("${so.actionType.notify}")
	private static String notificationActionType;
	@Value("${so.actionType.reminderNotify}")
	private static String reminderActionType;
	@Value("${so.actionType.fulfill}")
	private static String fulfillActionType;

	@Override
	public void process(Exchange exchange) throws Exception {
		String rawSoMessage = exchange.getIn().getBody(String.class);
		JsonNode rawSoMessageObject = jsonObjectMapper.readTree(rawSoMessage);
		SOMessage soMessage = SOMessageUtils.createSoMessageFromRawMessage(jsonObjectMapper, rawSoMessageObject);
		List<SOEventPayload> soEventPayloads = SOEventPayloadObjectFactory.createSOEventPayload(soMessage,
				notificationTriggerType, reminderTriggerType, fulfillTriggerType, notificationActionType,
				reminderActionType, fulfillActionType);

		if (soEventPayloads != null) {
			exchange.getIn().setBody(soEventPayloads);
			exchange.getIn().setHeader("PAYLOADFOUND", true);
		} else {
			exchange.getIn().setHeader("PAYLOADFOUND", false);
		}
	}
}
