/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.knowesis.sift.orchestrator.domain.SOEventPayload;

/**
 * Writes and entry to SO database
 * 
 * @author SO Development Team
 */
@Component("SODBEventPayloadWrite")
public class SODBEventPayloadWrite implements Processor {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Value("${so.db.eventPayload.insert}")
	String sql;

	@Autowired
	JdbcTemplate jdbcTemplate;


	/**
	 * @param exchange
	 *            camel exchange
	 * 
	 * @throws Exception
	 *             exception
	 */
	@SuppressWarnings("unchecked")
    @Override
	public void process(Exchange exchange) throws Exception {
        List<SOEventPayload> sOEventPayloads = (List<SOEventPayload>) exchange.getIn().getBody();
		writeToDB(sOEventPayloads);
	}

	/**
	 * Inserts data into database
	 * 
	 * @param soEventPayloads
	 *            soEventPayload
	 */
	public void writeToDB(List<SOEventPayload> soEventPayloads) {
		for (SOEventPayload soEventPayload : soEventPayloads) {
			log.debug("executing {} with values {}", sql,
					new Object[] { soEventPayload.getFlowId(), soEventPayload.getPayloadFieldName(),
							soEventPayload.getPayloadFieldValue(), soEventPayload.getPayloadFieldDatatype(),
							soEventPayload.getTriggerType(), soEventPayload.getActionType(),
							soEventPayload.getCreatedOn() });

			jdbcTemplate.update(sql,
					new Object[] { soEventPayload.getFlowId(), soEventPayload.getPayloadFieldName(),
							soEventPayload.getPayloadFieldValue(), soEventPayload.getPayloadFieldDatatype(),
							soEventPayload.getTriggerType(), soEventPayload.getActionType(),
							soEventPayload.getCreatedOn() });
		}
	}

}
