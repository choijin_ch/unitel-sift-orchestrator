/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.knowesis.sift.orchestrator.domain.SOEventTrigger;

/**
 * Writes and entry to SO database
 * 
 * @author SO Development Team
 */
@Component("soDBWrite")
public class SODBWrite implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Value("${so.db.insert}")
    String sql;

    @Autowired
    JdbcTemplate jdbcTemplate;

    SOEventTrigger soEventTrigger;

    /**
     * @param exchange camel exchange
     * 
     * @throws Exception exception
     */
    @Override
    public void process(Exchange exchange) throws Exception {

        soEventTrigger = exchange.getIn().getBody(SOEventTrigger.class);
        writeToDB(soEventTrigger);

    }

    /**
     * Inserts data into database
     * 
     * @param soEventTrigger soEventTrigger
     * 
     * 
     */

    public void writeToDB(SOEventTrigger soEventTrigger) {

        log.debug(
                "executing {} with values {}",
                sql,
                new Object[] { soEventTrigger.getMsisdn(),
                        soEventTrigger.getProgramId(), soEventTrigger.getTriggerId(),soEventTrigger.getOfferId(),
                        soEventTrigger.getFlowId(), soEventTrigger.getMonitoredValue(),
                        soEventTrigger.getActualResponseValue(), soEventTrigger.getProvisioningOfferId(),
                        soEventTrigger.getProvisionedValue(), soEventTrigger.getProvisioningSystem(),
                        soEventTrigger.getTriggerTimestamp(), soEventTrigger.getNotificationMessage(),
                        soEventTrigger.getActionResponse(), soEventTrigger.getTriggerType(),
                        soEventTrigger.getActionType(), soEventTrigger.getChannel(),
                        soEventTrigger.getActionStatus(), soEventTrigger.getIsSimulated(),
                        soEventTrigger.getCreatedOn(), soEventTrigger.getIsControl(),
                        soEventTrigger.getOfferStartDate(), soEventTrigger.getOfferEndDate(),soEventTrigger.getSubscriptionId(),soEventTrigger.getContactDirection()});

        jdbcTemplate
                .update(sql,
                        new Object[] { soEventTrigger.getMsisdn(),soEventTrigger.getOfferId(),
                                soEventTrigger.getProgramId(), soEventTrigger.getTriggerId(),
                                soEventTrigger.getFlowId(), soEventTrigger.getMonitoredValue(),
                                soEventTrigger.getActualResponseValue(), soEventTrigger.getProvisioningOfferId(),
                                soEventTrigger.getProvisionedValue(), soEventTrigger.getProvisioningSystem(),
                                soEventTrigger.getTriggerTimestamp(), soEventTrigger.getNotificationMessage(),
                                soEventTrigger.getActionResponse(), soEventTrigger.getTriggerType(),
                                soEventTrigger.getActionType(), soEventTrigger.getChannel(),
                                soEventTrigger.getActionStatus(), soEventTrigger.getIsSimulated(),
                                soEventTrigger.getCreatedOn(), soEventTrigger.getIsControl(),
                                soEventTrigger.getOfferStartDate(), soEventTrigger.getOfferEndDate(),soEventTrigger.getSubscriptionId(),soEventTrigger.getContactDirection()});

    }
  
}
