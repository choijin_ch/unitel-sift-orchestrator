package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SOEventTrigger;

/**
 * Read from SO database
 * 
 * @author SO Development Team
 * 
 */
@Component("soDBRead")
public class SODBRead implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {

        HttpServletRequest request = exchange.getIn().getBody(HttpServletRequest.class);
        String args = request.getParameter("args");
        log.debug("args {}", args);
        InputStream query = (InputStream) exchange.getIn().getBody();
        String sql = convertStreamToString(query);
        log.debug("query {}", sql);
        List<SOEventTrigger> soEventTriggerList = new ArrayList<SOEventTrigger>();
        if (args != null && StringUtils.isNotBlank(sql) && sql != null) {
            getDataFromDb(soEventTriggerList, sql, args);
        }
        String reply = jsonObjectMapper.writeValueAsString(soEventTriggerList);
        exchange.getIn().setBody(reply);
    }

    /**
     * 
     * @param row is table row
     * @param keyword is the name of column
     * @return value in Double
     */

    public Double getDoubleValueOfKeyword(Map<String, Object> row, String keyword) {
        try {
            Double doubleValue = Double.parseDouble(String.valueOf(row.get(keyword)));
            return doubleValue;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Converts the inputstream to String
     * 
     * @param in inputstream to be converted
     * @return inputstream as String
     */
    public String convertStreamToString(InputStream in) {
        try {
            StringWriter writer = new StringWriter();
            IOUtils.copy(in, writer);
            String theString = writer.toString();
            return theString;
        } catch (IOException e) {
            log.debug("can't read input stream for DBRead", e.getMessage());
            log.trace("an error occured while read streamcache", e);
        }
        return null;
    }

    /**
     * 
     * 
     * @param row row
     * @param keyword keyword
     * @return String
     */

    public String getStringValueOfKeyword(Map<String, Object> row, String keyword) {
        try {
            String stringValue = String.valueOf(row.get(keyword));
            return stringValue;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 
     * @param soEventTriggerList soEventTriggerList
     * @param sql sql
     * @param args args
     */
    private void getDataFromDb(List<SOEventTrigger> soEventTriggerList, String sql, String args) {
        Object[] arguments = args.split(",");
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, arguments);
        if ((result != null) && (result.size() > 0)) {
            for (Map<String, Object> row : result) {
                SOEventTrigger soEventTrigger = new SOEventTrigger();
                soEventTrigger.setMsisdn(getStringValueOfKeyword(row, "MSISDN"));
                log.debug("msisdn {}", soEventTrigger.getMsisdn());
                soEventTrigger.setOfferId(getStringValueOfKeyword(row, "OFFER_ID"));
                soEventTrigger.setProgramId(getStringValueOfKeyword(row, "PROGRAM_ID"));
                soEventTrigger.setTriggerId(getStringValueOfKeyword(row, "TRIGGER_ID"));
                soEventTrigger.setFlowId(getStringValueOfKeyword(row, "FLOW_ID"));
                soEventTrigger.setMonitoredValue(getDoubleValueOfKeyword(row, "MONITORED_VALUE"));
                soEventTrigger.setActualResponseValue(getDoubleValueOfKeyword(row, "ACTUALRESPONSE_VALUE"));
                soEventTrigger.setProvisionedValue(getStringValueOfKeyword(row, "PROVISIONED_VALUE"));
                soEventTrigger.setProvisioningSystem(getStringValueOfKeyword(row, "PROVISIONING_SYSTEM"));
                soEventTrigger
                        .setTriggerTimestamp(getStringAsTimestamp(getStringValueOfKeyword(row, "TRIGGER_TIMESTAMP")));
                soEventTrigger.setNotificationMessage(getStringValueOfKeyword(row, "NOTIFICATION_MSG"));
                soEventTrigger.setActionResponse(getStringValueOfKeyword(row, "ACTION_RESPONSE"));
                soEventTrigger.setTriggerType(getStringValueOfKeyword(row, "TRIGGER_TYPE"));
                soEventTrigger.setActionType(getStringValueOfKeyword(row, "ACTION_TYPE"));
                soEventTrigger.setChannel(getStringValueOfKeyword(row, "CHANNEL"));
                soEventTrigger.setActionStatus(getStringValueOfKeyword(row, "ACTION_STATUS"));
                soEventTrigger.setIsSimulated(getStringValueOfKeyword(row, "IS_SIMULATED"));
                soEventTrigger.setCreatedOn(getStringAsTimestamp(getStringValueOfKeyword(row, "CREATED_ON")));
                soEventTrigger.setIsControl(getStringValueOfKeyword(row, "IS_CONTROL"));
                soEventTrigger.setUpdatedOn(getStringAsTimestamp(getStringValueOfKeyword(row, "LAST_UPDATED_ON")));
                soEventTrigger
                        .setOfferStartDate(getStringAsTimestamp(getStringValueOfKeyword(row, "OFFER_START_DATE")));
                soEventTrigger.setOfferEndDate(getStringAsTimestamp(getStringValueOfKeyword(row, "OFFER_END_DATE")));
                soEventTrigger.setSubscriptionId(getStringValueOfKeyword(row, "SUBSCRIPTION_ID"));
                soEventTrigger.setContactDirection(getStringValueOfKeyword(row, "CONTACT_DIRECTION"));
                soEventTriggerList.add(soEventTrigger);
            }
        }
    }

    /**
     * Converts String to Timestamp
     * 
     * @param timestampAsString timestampAsString
     * @return timeStamp
     */
    public Timestamp getStringAsTimestamp(String timestampAsString) {
        try {
            return Timestamp.valueOf(timestampAsString);
        } catch (Exception e) {
            return null;
        }
    }
}
