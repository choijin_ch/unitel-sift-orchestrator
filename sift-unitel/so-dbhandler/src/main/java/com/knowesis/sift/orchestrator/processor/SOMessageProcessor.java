/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SOEventTrigger;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.SOEventTriggerObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;



/**
 * 
 * This processor adds correlation to MetaInfo
 * @author SO Development Team
 */
@Component("SOMessageProcessor")
public class SOMessageProcessor implements Processor {
    
    private final Logger log = LoggerFactory.getLogger(getClass());
    
    @Autowired
    ObjectMapper jsonObjectMapper;
    
    @Value("${so.triggerType.optinResponse}")
    private String optinResponse;

    @Value("${so.triggerType.fulfill}")
    private String fulfill;
   
    @Value("${so.triggerType.nominationOptinResponse}")
    private String nominationOptinResponse;
    
    @Value("${so.fulfill.actionType.extendExpiryNotify}")
    private String extendExpiryNotify;
    
    /**
     * 
     * @param exchange camel exchange
     * 
     * @throws Exception Handle exception
     * 
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        String rawSoMessage = exchange.getIn().getBody(String.class);
        log.info("Raw Json Message received for DB entry: {}", rawSoMessage);
        JsonNode rawSoMessageObject = jsonObjectMapper.readTree(rawSoMessage);
        SOMessage soMessage = SOMessageUtils.createSoMessageFromRawMessage(jsonObjectMapper, rawSoMessageObject);
        SOEventTrigger soEventTrigger = SOEventTriggerObjectFactory.createSOEventTrigger(soMessage,optinResponse,fulfill, exchange, nominationOptinResponse, extendExpiryNotify);
        exchange.getIn().setBody(soEventTrigger);
    }
}
