/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Client for Redis Integration
 * 
 * @author SO Development Team
 */
public class RedisConnectionPool {

    private Logger log = LoggerFactory.getLogger(RedisConnectionPool.class);

    private JedisPool jedisPool;

    /**
     * Get Object from Cache
     * 
     * @param key Object keyjedisPooljedisPool
     * @return Object from cache
     */
    public String getObjectFromCache(String key) {

        JedisPool jedisPool = getJedisPool();
        Jedis jResource = null;
        String objectFromCache = null;
        try {
            jResource = jedisPool.getResource();
            objectFromCache = jResource.get(key);
            log.debug("Got object {} for Key {} ", objectFromCache, key);
        }catch(Exception e) {
            log.debug("Exception occured during redis read:", e);
        }finally {
            if(jResource != null)
                jedisPool.returnResource(jResource);
        }
        return objectFromCache;
    }

    /**
     * Delete Object from Cache
     * 
     * @param key object key
     */
    public void delObjectFromCache(String key) {

        JedisPool jedisPool = getJedisPool();
        Jedis jResource = null;
        try {
            jResource = jedisPool.getResource();
            jResource.del(key);
            log.debug("Key {} removed", key);
        }catch(Exception e) {
            log.debug("Exception occured during redis delete:", e);
        }finally {
            if(jResource != null)
                jedisPool.returnResource(jResource);
        }

    }

    /**
     * Save object in cache
     * 
     * @param key object key
     * @param obj object to store
     * @param messageExpiry TTL
     */
    public void putInCache(String key, String obj, int messageExpiry) {

        if (StringUtils.isBlank(key)) {
            log.debug("cannot cache with a blank/null key");
            return;
        }
        
        JedisPool jedisPool = getJedisPool();
        Jedis jResource = null;
        try {
            jResource = jedisPool.getResource();
            jResource.set(key, obj);
            jResource.expire(key, messageExpiry);
            log.debug("Redis key {}, write in Cache.", key);
        }catch(Exception e) {
            log.debug("Exception occured during redis write:", e);
        }finally {
            if(jResource != null)
                jedisPool.returnResource(jResource);
        }
    }

    
    /**
     * Getter for jedisPool
     * @return the jedisPool
     */
    public JedisPool getJedisPool() {
        return jedisPool;
    }

    /**
     * Setter for jedisPool
     * @param jedisPool the jedisPool to set
     */
    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }
}
