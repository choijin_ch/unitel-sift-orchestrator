/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.camel.Exchange;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.knowesis.sift.orchestrator.domain.SOEventTrigger;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.domain.SOReminderMessage;

/**
 * Create SOEventTrigger to enter into DB based on SOMessage
 * 
 * @author SO Development Team
 *
 */
public class SOEventTriggerObjectFactory {

	@Value("${so.triggerType.optinResponse}")
	 private String triggerTypeoptinResponse;

    /**
     * 
     * Creates SOEventTrigger object based on soMessage subclass type
     * 
     * @param soMessage soMessage
     * @param optinResponse optinResponse
     * @param fulfill fulfillment triggerType value
     * @param exchange exchange
     * @param nominationOptinResponse nominationOptinResponse
     * @param extendExpiryNotify extendExpiryNotify
     * @return SOEventTrigger to be inserted into SO database
     * @throws Exception exception
     */

    public static SOEventTrigger createSOEventTrigger(SOMessage soMessage, String optinResponse, String fulfill,
            Exchange exchange, String nominationOptinResponse, String extendExpiryNotify) throws Exception {
        if (soMessage instanceof SONotificationMessage) {
            return createSOEventTriggerFromSONotification(soMessage, optinResponse, exchange, extendExpiryNotify,
                    fulfill);
        } else if (soMessage instanceof SOReminderMessage) {
            return createSOEventTriggerFromReminderMessage(soMessage, exchange);
        } else if (soMessage instanceof SOFulfillmentMessage) {
            return createSOEventTriggerFromFulfillmentMessage(soMessage, nominationOptinResponse, fulfill, exchange);
        }
        return null;
    }

    /**
     * 
     * Creates SOEventTrigger from notification message
     * 
     * @param soMessage SONotificationMessage
     * @param optinResponse optinResponse triggerType value
     * @param exchange exchange
     * @param extendExpiryNotify extendExpiryNotify
     * @param fulfill fulfill
     * @return object to be saved in SO database
     * @throws Exception exception
     */

    private static SOEventTrigger createSOEventTriggerFromSONotification(SOMessage soMessage, String optinResponse,
            Exchange exchange, String extendExpiryNotify, String fulfill) throws Exception {
        SONotificationMessage soNotificationMessage = (SONotificationMessage) soMessage;
        SOEventTrigger soEventTrigger = new SOEventTrigger();
        soEventTrigger.setMsisdn(SOMessageUtils.getMSISDN(soNotificationMessage));
        soEventTrigger.setProgramId(SOMessageUtils.getProgramId(soNotificationMessage));
        soEventTrigger.setOfferId(SOMessageUtils.getOfferId(soNotificationMessage));
        soEventTrigger.setTriggerId(SOMessageUtils.getEventId(soNotificationMessage));
        soEventTrigger.setFlowId(SOMessageUtils.getFlowId(soNotificationMessage));
        soEventTrigger.setContactDirection(getContactDirectionForNotificationOptin(soNotificationMessage));
        soEventTrigger.setActionStatus(soNotificationMessage.getActionStatus());
        soEventTrigger.setActionType(soNotificationMessage.getActionType());
        soEventTrigger.setTriggerType(soNotificationMessage.getTriggerType());
        soEventTrigger.setCreatedOn(getUnixTimestamp());
        soEventTrigger.setChannel(SOMessageUtils.getChannelName(soNotificationMessage));
        if (soNotificationMessage.getTriggerType() != null
                && !soNotificationMessage.getTriggerType().equalsIgnoreCase(optinResponse)) {
            soEventTrigger.setNotificationMessage(SOMessageUtils.getTextToBeSent(soNotificationMessage));
        }
        Timestamp triggerTimeStamp = getUnixTimestamp();
        String notificationEventTimeStamp = SOMessageUtils.getEventTimestamp(soNotificationMessage);
        if (notificationEventTimeStamp != null) {
            triggerTimeStamp = convertToLocalTime(notificationEventTimeStamp);
        }
        soEventTrigger.setTriggerTimestamp(triggerTimeStamp);
        soEventTrigger.setIsSimulated(SOMessageUtils.getIsSimulated(soNotificationMessage));
        soEventTrigger.setIsControl(SOMessageUtils.getIsControl(soNotificationMessage));
        soEventTrigger.setActionResponse(soNotificationMessage.getActionResponse());
        soEventTrigger.setOfferStartDate(getTimestamp(SOMessageUtils.getOfferStartDate(soNotificationMessage)));
        soEventTrigger.setOfferEndDate(getTimestamp(SOMessageUtils.getOfferEndDate(soNotificationMessage)));
        soEventTrigger.setSubscriptionId(SOMessageUtils.getSubscriptionId(soNotificationMessage));
        setProvisioningSystemAndTargetForExtendExpiry(soEventTrigger, soMessage, fulfill, extendExpiryNotify);
        return soEventTrigger;
    }
    
    /**
     * Set ContactDirection for nomination and normal optin
     * 
     * @param soNotificationMessage notificationmessage
     * @return ContactDirection
     */
    private static String getContactDirectionForNotificationOptin(SONotificationMessage soNotificationMessage){
    	String contactDirection=soNotificationMessage.getMessage().getRequesterZone();
    	if(StringUtils.isNotBlank(SOMessageUtils.getOptinShortCode(soNotificationMessage))&&(soNotificationMessage.getTriggerType().equals("OFFERNOTIFY"))&&(soNotificationMessage.getActionType().equals("NOTIFY"))){
    		contactDirection=contactDirection+"_OPTIN_"+SOMessageUtils.getOptinShortCode(soNotificationMessage);
    	}
    	else if(StringUtils.isNotBlank(SOMessageUtils.getSmscShortCode(soNotificationMessage))&&soNotificationMessage.getTriggerType().equals("NOMINATION-NOTIFY")&&(soNotificationMessage.getActionType().equals("NOTIFY"))){
    		contactDirection=contactDirection+"_NOMINATION_OPTIN_"+SOMessageUtils.getSmscShortCode(soNotificationMessage);
    	}
    	return contactDirection;
    }
    
    
    
    
    /**
     * 
     * In case of EXTENDEXPIRY-NOTIFY set provisioningSystem and channel as the OCS Because this is
     * an exceptional case like provisioing inside notification
     * 
     * @param soEventTrigger soEventTrigger
     * @param soMessage soMessage
     * @param fulfill fulfill
     * @param extendExpiryNotify extendExpiryNotify
     */
    private static void setProvisioningSystemAndTargetForExtendExpiry(SOEventTrigger soEventTrigger,
            SOMessage soMessage, String fulfill, String extendExpiryNotify) {
        // For EXTENDEXPIRY_NOTIFY FULFILL only
        if (soMessage.getActionType().equals(fulfill)
                && SOMessageUtils.getActionType(soMessage).equals(extendExpiryNotify)) {
            String targetSystem = SOMessageUtils.getHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM");
            soEventTrigger.setProvisioningSystem(targetSystem);
            soEventTrigger.setChannel(targetSystem.split("_")[0]);
        }
    }

    /**
     * 
     * Creates SOEventTrigger from reminder message
     * 
     * @param soMessage SOReminderMessage
     * @param exchange exchange
     * @return object to be saved in SO database
     * @throws Exception exception
     */

    private static SOEventTrigger createSOEventTriggerFromReminderMessage(SOMessage soMessage, Exchange exchange)
            throws Exception {
        SOReminderMessage soReminderMessage = (SOReminderMessage) soMessage;
        SOEventTrigger soEventTrigger = new SOEventTrigger();
        soEventTrigger.setMsisdn(SOMessageUtils.getMSISDN(soReminderMessage));
        soEventTrigger.setProgramId(SOMessageUtils.getProgramId(soReminderMessage));
        soEventTrigger.setOfferId(SOMessageUtils.getOfferId(soReminderMessage));
        soEventTrigger.setTriggerId(SOMessageUtils.getEventId(soReminderMessage));
        soEventTrigger.setFlowId(SOMessageUtils.getFlowId(soReminderMessage));
        soEventTrigger.setContactDirection(soReminderMessage.getMessage().getRequesterZone());
        soEventTrigger.setIsSimulated(SOMessageUtils.getIsSimulated(soReminderMessage));
        soEventTrigger.setIsControl(SOMessageUtils.getIsControl(soReminderMessage));
        soEventTrigger.setActionStatus(soReminderMessage.getActionStatus());
        soEventTrigger.setActionType(soReminderMessage.getActionType());
        soEventTrigger.setTriggerType(soReminderMessage.getTriggerType());
        soEventTrigger.setCreatedOn(getUnixTimestamp());
        soEventTrigger.setNotificationMessage(SOMessageUtils.getTextToBeSent(soMessage));
        soEventTrigger.setChannel(SOMessageUtils.getChannelName(soReminderMessage));
        soEventTrigger.setTriggerTimestamp(convertToLocalTime(SOMessageUtils.getEventTimestamp(soMessage)));
        soEventTrigger.setSubscriptionId(SOMessageUtils.getSubscriptionId(soReminderMessage));
        soEventTrigger.setActionResponse(soReminderMessage.getActionResponse());
        return soEventTrigger;
    }

    /**
     * 
     * Creates SOEventTrigger from fulfillment message
     * 
     * @param soMessage SOFulfillmentMessage
     * @param nominationOptinResponse nomination optin response triggerType value
     * @param fulfill fulfillment triggerType value
     * @param exchange exchange
     * @return object to be saved in SO database
     * @throws Exception exception
     */

    private static SOEventTrigger createSOEventTriggerFromFulfillmentMessage(SOMessage soMessage,
            String nominationOptinResponse, String fulfill, Exchange exchange) throws Exception {
        SOFulfillmentMessage soFulfillmentMessage = (SOFulfillmentMessage) soMessage;
        SOEventTrigger soEventTrigger = new SOEventTrigger();
        Timestamp triggerTimeStamp = getUnixTimestamp();
        if (soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
                .getEventTimeStamp() != null) {
            triggerTimeStamp = convertToLocalTime(SOMessageUtils.getEventTimestamp(soFulfillmentMessage));
        }
        soEventTrigger.setMsisdn(SOMessageUtils.getMSISDN(soFulfillmentMessage));
        soEventTrigger.setProgramId(SOMessageUtils.getProgramId(soFulfillmentMessage));
        soEventTrigger.setOfferId(SOMessageUtils.getOfferId(soFulfillmentMessage));
        soEventTrigger.setTriggerId(SOMessageUtils.getEventId(soFulfillmentMessage));
        soEventTrigger.setFlowId(SOMessageUtils.getFlowId(soFulfillmentMessage));
        soEventTrigger.setIsSimulated(SOMessageUtils.getIsSimulated(soFulfillmentMessage));
        soEventTrigger.setIsControl(SOMessageUtils.getIsControl(soFulfillmentMessage));
        soEventTrigger.setActionStatus(soFulfillmentMessage.getActionStatus());
        soEventTrigger.setActionType(soFulfillmentMessage.getActionType());
        soEventTrigger.setTriggerType(soFulfillmentMessage.getTriggerType());
        soEventTrigger.setCreatedOn(getUnixTimestamp());
        soEventTrigger.setTriggerTimestamp(triggerTimeStamp);
        soEventTrigger.setSubscriptionId(SOMessageUtils.getSubscriptionId(soFulfillmentMessage));
        soEventTrigger.setContactDirection( soFulfillmentMessage.getMessage().getRequesterZone());
        setEventTriggerForFulfillment(exchange, soFulfillmentMessage, nominationOptinResponse, fulfill, soEventTrigger);
        String channel = SOMessageUtils.getHeaderValue(soFulfillmentMessage, "LAST-PROVISIONED-TARGET-SYSTEM");
        if(StringUtils.isNotBlank(channel)) channel = channel.split("_")[0];
        else {
            channel = SOMessageUtils.getChannelName(soFulfillmentMessage);
            if(null==channel){
            	 channel = "SIFT";
            }
        }
        soEventTrigger.setChannel(channel);
        soEventTrigger.setProvisioningSystem(
                SOMessageUtils.getHeaderValue(soFulfillmentMessage, "LAST-PROVISIONED-TARGET-SYSTEM"));
        soEventTrigger.setActionResponse(soFulfillmentMessage.getActionResponse());
        soEventTrigger.setOfferStartDate(getTimestamp(SOMessageUtils.getOfferStartDate(soFulfillmentMessage)));
        soEventTrigger.setOfferEndDate(getTimestamp(SOMessageUtils.getOfferEndDate(soFulfillmentMessage)));
        soEventTrigger.setProvisioningOfferId(SOMessageUtils.getHeaderValue(soMessage, "PROVISIONED-OFFERID"));
        soEventTrigger.setProvisionedValue(SOMessageUtils.getHeaderValue(soMessage, "PROVISIONED-VALUE"));
        return soEventTrigger;
    }

 
    /**
     * Creates SOEventTrigger from fulfillment message when there are products
     * 
     * @param exchange exchange
     * @param soFulfillmentMessage soFulfillmentMessage
     * @param nominationOptinResponse optinResponse triggerType value
     * @param fulfill fulfillment triggerType value
     * @param soEventTrigger to set event trigger
     * @throws Exception exception
     */
    private static void setEventTriggerForFulfillment(Exchange exchange, SOFulfillmentMessage soFulfillmentMessage,
            String nominationOptinResponse, String fulfill, SOEventTrigger soEventTrigger) throws Exception {

        if (soFulfillmentMessage.getTriggerType() != null
                && (soFulfillmentMessage.getTriggerType().equalsIgnoreCase(nominationOptinResponse)
                        || soFulfillmentMessage.getTriggerType().equalsIgnoreCase(fulfill))) {
            soEventTrigger.setNotificationMessage(null);

        } else {
            soEventTrigger.setNotificationMessage(SOMessageUtils.getTextToBeSent(soFulfillmentMessage));
        }

    }

    /**
     * 
     * To generate timestamp in UNIX format as the core requires it
     * 
     * @return timestamp String in UNIX format
     */
    private static Timestamp getUnixTimestamp() {
        Calendar cal = Calendar.getInstance();
        // Get the time in local timezone
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);
        int seconds = cal.get(Calendar.SECOND);
        // create time zone object with UTC and set the timezone
        // TimeZone tzone = TimeZone.getTimeZone("UTC");
        // cal.setTimeZone(tzone);
        // Set the current time as GMT Time
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minutes);
        cal.set(Calendar.SECOND, seconds);
        return new java.sql.Timestamp(cal.getTime().getTime());
    }

    /**
     * 
     * Convert to local time from GMT
     * 
     * @param timestamp Fulfillment timestamp
     * @return timestamp String in UNIX format
     */
    private static Timestamp convertToLocalTime(String timestamp) {
        long time = Long.parseLong(timestamp);

        Calendar calendarTiggerTime = Calendar.getInstance();
        calendarTiggerTime.setTimeInMillis(time);

        // Get the time in local timezone
        int hour = calendarTiggerTime.get(Calendar.HOUR_OF_DAY);
        int minutes = calendarTiggerTime.get(Calendar.MINUTE);
        int seconds = calendarTiggerTime.get(Calendar.SECOND);

        // create time zone object with UTC and set the timezone
        // TimeZone tzone = TimeZone.getTimeZone("UTC");
        // calendarTiggerTime.setTimeZone(tzone);

        // Set the current time as GMT Time
        calendarTiggerTime.set(Calendar.HOUR_OF_DAY, hour);
        calendarTiggerTime.set(Calendar.MINUTE, minutes);
        calendarTiggerTime.set(Calendar.SECOND, seconds);

        long epoch = calendarTiggerTime.getTimeInMillis();
        long epochLocalToUtcDelta = getLocalToUtcDelta();
        calendarTiggerTime.setTimeInMillis(epoch + epochLocalToUtcDelta);

        return new java.sql.Timestamp(calendarTiggerTime.getTime().getTime());
    }

    /**
     * 
     * To get Local to UTC Delta
     * 
     * @return timestamp String in UNIX format
     */
    public static long getLocalToUtcDelta() {
        Calendar local = Calendar.getInstance();
        local.clear();
        local.set(1970, Calendar.JANUARY, 1, 0, 0, 0);
        return local.getTimeInMillis();
    }

    /**
     * Convert the epoch time to TimeStamp
     * 
     * @param timestampInString timestamp as string
     * @return date as timestamp
     */
    public static Timestamp getTimestamp(String timestampInString) {
        if (StringUtils.isNotBlank(timestampInString) && timestampInString != null) {
            Date date = new Date(Long.parseLong(timestampInString));
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            String formatted = format.format(date);
            Timestamp timeStamp = Timestamp.valueOf(formatted);
            return timeStamp;
        } else {
            return null;
        }
    }

}
