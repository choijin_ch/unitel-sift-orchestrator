/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.knowesis.sift.orchestrator.core.domain.EventPayload;
import com.knowesis.sift.orchestrator.domain.SOEventPayload;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.domain.SOReminderMessage;

/**
 * Create SOEventPayload to enter into DB based on SOMessage
 * 
 * @author SO Development Team
 *
 */

public class SOEventPayloadObjectFactory {

    /**
     * 
     * Creates SOEventPayload object based on soMessage subclass type
     * 
     * @param soMessage soMessage
     * @param notificationTriggerType notificationTriggerType
     * @param reminderTriggerType reminderTriggerType
     * @param fulfillTriggerType fulfillTriggerType
     * @param notificationActionType notificationActionType
     * @param reminderActionType reminderActionType
     * @param fulfillActionType fulfillActionType
     * @return SOEventPayload to be inserted into SO database
     */
    public static List<SOEventPayload> createSOEventPayload(SOMessage soMessage, String notificationTriggerType,
            String reminderTriggerType, String fulfillTriggerType, String notificationActionType,
            String reminderActionType, String fulfillActionType) {
        if (soMessage instanceof SONotificationMessage) {
            return createSOEventPayloadFromSONotification(soMessage, notificationActionType, notificationTriggerType);
        } else if (soMessage instanceof SOReminderMessage) {
            return createSOEventPayloadFromReminderMessage(soMessage, reminderActionType, reminderTriggerType);
        } else if (soMessage instanceof SOFulfillmentMessage) {

            return createSOEventPayloadFromFulfillmentMessage(soMessage, fulfillActionType, fulfillTriggerType);
        }
        return null;
    }

    /**
     * 
     * Creates SOEventPayload from notification message
     * 
     * @param soMessage SONotificationMessage
     * 
     * @param actionType action type
     * @param triggerType trigger type
     * @return object to be saved in SO database
     */
    private static List<SOEventPayload> createSOEventPayloadFromSONotification(SOMessage soMessage, String actionType,
            String triggerType) {

        SONotificationMessage soNotificationMessage = (SONotificationMessage) soMessage;
        if (soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .getEventPayload() != null
                && (!soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                        .getEventPayload().isEmpty())) {

            List<SOEventPayload> soEventPayloads = new ArrayList<>();

            for (EventPayload eventPayload : soNotificationMessage.getMessage().getRequesterLocation().getLocations()
                    .get(0).getOffers().get(0).getEventPayload()) {
                SOEventPayload soEventPayload = new SOEventPayload();
                soEventPayload.setFlowId(soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0)
                        .getOffers().get(0).getFlowId());

                soEventPayload.setPayloadFieldName(eventPayload.getName());
                soEventPayload.setPayloadFieldValue(eventPayload.getValue());
                soEventPayload.setPayloadFieldDatatype(eventPayload.getDataType());
                soEventPayload.setTriggerType(triggerType);
                soEventPayload.setActionType(actionType);
                soEventPayload.setCreatedOn(getUnixTimestamp());
                soEventPayloads.add(soEventPayload);
            }

            return soEventPayloads;
        }
        return null;
    }

    /**
     * 
     * Creates SOEventPayload from reminder message
     * 
     * @param soMessage SOReminderMessage
     * 
     * @param actionType action type
     * @param triggerType trigger type
     * @return object to be saved in SO database
     */
    private static List<SOEventPayload> createSOEventPayloadFromReminderMessage(SOMessage soMessage, String actionType,
            String triggerType) {
        SOReminderMessage soReminderMessage = (SOReminderMessage) soMessage;
        if (soReminderMessage.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0)
                .getEventPayload() != null
                && (!soReminderMessage.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0)
                        .getEventPayload().isEmpty())) {
            List<SOEventPayload> soEventPayloads = new ArrayList<>();
            for (EventPayload eventPayload : soReminderMessage.getMessage().getRequesterLocation().getLocations().get(0)
                    .getReminders().get(0).getEventPayload()) {
                SOEventPayload soEventPayload = new SOEventPayload();
                soEventPayload.setFlowId(soReminderMessage.getMessage().getRequesterLocation().getLocations().get(0)
                        .getReminders().get(0).getFlowId());
                soEventPayload.setPayloadFieldName(eventPayload.getName());
                soEventPayload.setPayloadFieldValue(eventPayload.getValue());
                soEventPayload.setPayloadFieldDatatype(eventPayload.getDataType());
                soEventPayload.setTriggerType(triggerType);
                soEventPayload.setActionType(actionType);
                soEventPayload.setCreatedOn(getUnixTimestamp());
                soEventPayloads.add(soEventPayload);
            }
            return soEventPayloads;
        }
        return null;
    }

    /**
     * 
     * Creates SOEventPayload from fulfillment message
     * 
     * @param soMessage SOFulfillmentMessage
     * @param actionType action type
     * @param triggerType trigger type
     * @return object to be saved in SO database
     */
    private static List<SOEventPayload> createSOEventPayloadFromFulfillmentMessage(SOMessage soMessage,
            String actionType, String triggerType) {
        SOFulfillmentMessage soFulfillmentMessage = (SOFulfillmentMessage) soMessage;
        if (soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
                .getEventPayload() != null
                && (!soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments()
                        .get(0).getEventPayload().isEmpty())) {
            List<SOEventPayload> soEventPayloads = new ArrayList<>();
            for (EventPayload eventPayload : soFulfillmentMessage.getMessage().getRequesterLocation().getLocations()
                    .get(0).getFulfilments().get(0).getEventPayload()) {
                SOEventPayload soEventPayload = new SOEventPayload();
                soEventPayload.setFlowId(soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0)
                        .getFulfilments().get(0).getFlowId());
                soEventPayload.setPayloadFieldName(eventPayload.getName());
                soEventPayload.setPayloadFieldValue(eventPayload.getValue());
                soEventPayload.setPayloadFieldDatatype(eventPayload.getDataType());
                soEventPayload.setTriggerType(triggerType);
                soEventPayload.setActionType(actionType);
                soEventPayload.setCreatedOn(getUnixTimestamp());
                soEventPayloads.add(soEventPayload);
            }
            return soEventPayloads;
        }
        return null;
    }

    /**
     * 
     * To generate timestamp in UNIX format as the core requires it
     * 
     * @return timestamp String in UNIX format
     */
    private static Timestamp getUnixTimestamp() {
        Calendar cal = Calendar.getInstance();

        // Get the time in local timezone
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);
        int seconds = cal.get(Calendar.SECOND);
        // Set the current time as GMT Time
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minutes);
        cal.set(Calendar.SECOND, seconds);

        return new java.sql.Timestamp(cal.getTime().getTime());
    }

}
