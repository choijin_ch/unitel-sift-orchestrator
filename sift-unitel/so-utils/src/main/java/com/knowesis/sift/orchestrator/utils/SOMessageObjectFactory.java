/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.core.domain.CustomerIdentifier;
import com.knowesis.sift.orchestrator.core.domain.Offers;
import com.knowesis.sift.orchestrator.domain.Header;
import com.knowesis.sift.orchestrator.domain.MetaInfo;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;

/**
 * Object Factory for SOMessage
 * 
 * @author SO Development Team
 *
 */
public class SOMessageObjectFactory {

    /**
     * Create a default SONotificationMessage
     * 
     * @param soMessageTemplate sample template for so message
     * @param msisdn msisdn
     * @param soMessageAsJSON so message from redis
     * @param messageToSend sms text
     * @param channelName channelName
     * @param jsonObjectMapper json mapper
     * @return SONotificationMessage
     * @throws JsonProcessingException JsonProcessingException
     * @throws IOException IOException
     */
    public static SONotificationMessage createSONotificationMessage(String soMessageTemplate, String msisdn,
            String soMessageAsJSON, String messageToSend, String channelName, ObjectMapper jsonObjectMapper)
            throws JsonProcessingException, IOException {

        SONotificationMessage soTemplateNotificationMessage = (SONotificationMessage) RawMessageUtils
                .processRawMessage(soMessageTemplate, jsonObjectMapper);
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setMsisdn(msisdn);
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .getNotificationMessages().get(0).getMessages().get(0).setTextTobeSent(messageToSend);
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .getNotificationMessages().get(0).setChannelName(channelName);
        // when optin not found, soMessageAsJSON will be null and nothing to populate from
        // cached notification message, So return immediately
        if (soMessageAsJSON == null) {
            return soTemplateNotificationMessage;
        }
        SOMessage soMessage = RawMessageUtils.processRawMessage(soMessageAsJSON, jsonObjectMapper);

        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setOfferId(SOMessageUtils.getOfferId(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setProgramId(SOMessageUtils.getProgramId(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setEventId(SOMessageUtils.getEventId(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setFlowId(SOMessageUtils.getFlowId(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .getNotificationMessages().get(0).setShortCode(SOMessageUtils.getSmscShortCode(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setEventTimeStamp(SOMessageUtils.getEventTimestamp(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setOfferStartDate(SOMessageUtils.getOfferStartDate(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setOfferEndDate(SOMessageUtils.getOfferEndDate(soMessage));
        return soTemplateNotificationMessage;
    }

    /**
     * Create a default SONotificationMessage
     * 
     * @param soMessageTemplate sample template for so message
     * @param msisdn msisdn
     * @param soMessage soMessage
     * @param messageToSend sms text
     * @param channelName channelName
     * @param jsonObjectMapper json mapper
     * @return SONotificationMessage
     * @throws JsonProcessingException JsonProcessingException
     * @throws IOException IOException
     */
    public static SONotificationMessage createSONotificationMessage(String soMessageTemplate, String msisdn,
            SOMessage soMessage, String messageToSend, String channelName, ObjectMapper jsonObjectMapper)
            throws JsonProcessingException, IOException {

        SONotificationMessage soTemplateNotificationMessage = (SONotificationMessage) RawMessageUtils
                .processRawMessage(soMessageTemplate, jsonObjectMapper);
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setMsisdn(msisdn);
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .getNotificationMessages().get(0).getMessages().get(0).setTextTobeSent(messageToSend);
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .getNotificationMessages().get(0).setChannelName(channelName);
        // when optin not found, soMessageAsJSON will be null and nothing to populate from
        // cached notification message, So return immediately
        if (soMessage == null) {
            return soTemplateNotificationMessage;
        }

        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setOfferId(SOMessageUtils.getOfferId(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setProgramId(SOMessageUtils.getProgramId(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setEventId(SOMessageUtils.getEventId(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setFlowId(SOMessageUtils.getFlowId(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .getNotificationMessages().get(0).setShortCode(SOMessageUtils.getSmscShortCode(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setEventTimeStamp(SOMessageUtils.getEventTimestamp(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setOfferStartDate(SOMessageUtils.getOfferStartDate(soMessage));
        soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
                .setOfferEndDate(SOMessageUtils.getOfferEndDate(soMessage));
        return soTemplateNotificationMessage;
    }

	/**
	 * 
	 * Creates SONotificationMessage for redis storage in inbound flow
	 * 
	 * @param soMessageTemplate
	 *            soMessageTemplate
	 * @param coreResponseJson
	 *            coreResponseJson
	 * @param offerNode
	 *            offerNode
	 * @param jsonObjectMapper
	 *            jsonObjectMapper
	 * @param locationId
	 *            locationId
	 * @return sONotificationMessage
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */

	public static SONotificationMessage createSONotificationMessageForInbound(String soMessageTemplate,
			JsonNode coreResponseJson, JsonNode offerNode, ObjectMapper jsonObjectMapper, String locationId)
			throws JsonProcessingException {
		SONotificationMessage soTemplateNotificationMessage = (SONotificationMessage) RawMessageUtils
				.processRawMessage(soMessageTemplate, jsonObjectMapper);
		soTemplateNotificationMessage.getMessage().setStatus(coreResponseJson.get("STATUS").asText());
		soTemplateNotificationMessage.getMessage().setCustomerIdentifier(
				jsonObjectMapper.treeToValue(coreResponseJson.get("CUSTOMER_IDENTIFIER"), CustomerIdentifier.class));
		Offers offer = jsonObjectMapper.treeToValue(offerNode, Offers.class);
		List<Offers> offers = new ArrayList<Offers>();
		offers.add(offer);
		soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).setId(locationId);
		soTemplateNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).setOffers(offers);
		soTemplateNotificationMessage.getMessage().setRequestId(coreResponseJson.get("REQUEST_ID").asText());
		soTemplateNotificationMessage.getMessage()
				.setRequesterChannel(coreResponseJson.get("REQUESTER_CHANNEL").asText());
		soTemplateNotificationMessage.getMessage()
				.setRequesterApplication(coreResponseJson.get("REQUESTER_APPLICATION").asText());
		soTemplateNotificationMessage.getMessage().setRequesterZone(coreResponseJson.get("REQUESTER_ZONE").asText());
		// coreNotificationMessage.setRequesterLocation(locationId);
		@SuppressWarnings("unchecked")
		java.util.Map<String, Object> contextData = jsonObjectMapper.treeToValue(coreResponseJson.get("CONTEXT_DATA"),
				java.util.Map.class);
		soTemplateNotificationMessage.getMessage().setContextData(contextData);
		return soTemplateNotificationMessage;
	}

	/**
	 * Create default MetaInfo
	 * 
	 * @param channelName
	 *            channelName
	 * @return MetaInfo
	 */
	public static MetaInfo createDefaultMetaInfo(String channelName) {
		MetaInfo info = new MetaInfo();
		info.setChannelName(channelName);
		return info;
	}

	/**
	 * Create new Headers with an element from key/name parameters
	 * 
	 * @param key
	 *            the keyname
	 * @param value
	 *            the value
	 * @return Headers
	 */
	public static List<Header> createDefaultHeader(String key, String value) {
		List<Header> headers = new ArrayList<>();
		Header header = new Header();
		header.setKey(key);
		header.setValue(value);
		headers.add(header);
		return headers;
	}
}
