/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.core.domain.DynamicParameter;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.core.domain.MessageContainer;
import com.knowesis.sift.orchestrator.domain.Header;
import com.knowesis.sift.orchestrator.domain.MetaInfo;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.domain.SOReminderMessage;

/**
 * Common Utility methods for SOMessage
 * 
 * @author SO Development Team
 */
public class SOMessageUtils {

	private static Logger log = LoggerFactory.getLogger(SOMessageUtils.class);

	/**
	 * Get action from SOMessage
	 * 
	 * @param soMessage
	 *            SOMessage, can't be null
	 * @return ActionType
	 */
	public static String getActionType(SOMessage soMessage) {
		Map<String, String> offerPayLoad = getOfferPayLoad(soMessage);
		if (offerPayLoad != null) {
			return getOfferPayLoad(soMessage).get("Action_Type");
		}
		return null;
	}

	/**
	 * Fetch MSISDN from SOMessage
	 * 
	 * @param soMessage
	 *            SOMessage, can't be null
	 * @return MSISDN
	 */
	public static String getMSISDN(SOMessage soMessage) {
		// MSISDN can be moved up for performance
		if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getMsisdn();
		} else if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getMsisdn();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0).getMsisdn();
		}
		log.warn("returning null because unknown Message type encountered {}", soMessage);
		return null;
	}

	/**
	 * 
	 * Returns Event Id
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return eventId
	 */
	public static String getEventId(SOMessage soMessage) {
		if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getEventId();
		} else if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getEventId();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0).getEventId();
		}
		log.warn("returning null because unknown Message type encountered {}", soMessage);
		return null;
	}

	/**
	 * 
	 * Returns Flow Id
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return flowId
	 */
	public static String getFlowId(SOMessage soMessage) {
		if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getFlowId();
		} else if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getFlowId();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0).getFlowId();
		}
		log.warn("returning null because unknown Message type encountered {}", soMessage);
		return null;
	}

	/**
	 * 
	 * Returns Offer Id
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return offerId
	 */
	public static String getOfferId(SOMessage soMessage) {
		if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getOfferId();
		} else if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getOfferId();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0).getOfferId();
		}
		log.warn("returning null because unknown Message type encountered {}", soMessage);
		return null;
	}

	/**
	 * 
	 * Returns Program Id
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return programId
	 */
	public static String getProgramId(SOMessage soMessage) {
		if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getProgramId();
		} else if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getProgramId();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0)
					.getProgramId();
		}
		log.warn("returning null because unknown Message type encountered {}", soMessage);
		return null;
	}

	/**
	 * 
	 * Returns Event Time stamp
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return eventTimestamp
	 */
	public static String getEventTimestamp(SOMessage soMessage) {
		if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getEventTimeStamp();
		} else if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
					.getEventTimeStamp();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0)
					.getEventTimeStamp();
		}
		log.warn("returning null because unknown Message type encountered {}", soMessage);
		return null;
	}

	/**
	 * 
	 * Returns Channel Name
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return channelName
	 */
	public static String getChannelName(SOMessage soMessage) {
		if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return returnFulfillmentChannelName(message);
		} else if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
					.getNotificationMessages().get(0).getChannelName();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0)
					.getReminderMessages().get(0).getChannelName();
		}
		log.warn("returning null because unknown Message type encountered {}", soMessage);
		return null;
	}

	/**
	 * get channel name for fulfillmentMessages
	 * 
	 * @param message
	 *            fulfillmentMessages
	 * @return channel name or null
	 */
	private static String returnFulfillmentChannelName(SOFulfillmentMessage message) {
		List<MessageContainer> fulfillmentMessages = message.getMessage().getRequesterLocation().getLocations().get(0)
				.getFulfilments().get(0).getFulfillmentMessages();
		if (fulfillmentMessages != null && fulfillmentMessages.size() > 0) {
			return fulfillmentMessages.get(0).getChannelName();
		} else {
			log.info("FulfillmentMessage doesn't contain message array");
			return null;
		}
	}

	/**
	 * 
	 * Create SOMessage subclass based on the JSON message
	 * 
	 * @param jsonObjectMapper
	 *            com.fasterxml.jackson.databind.ObjectMapper instance
	 * @param message
	 *            json message received from cache
	 * @return subclass of SoMessage
	 */
	public static SOMessage createSoMessageFromRawMessage(ObjectMapper jsonObjectMapper, JsonNode message) {
		JsonNode messageNode = message.get("message");
		try {
			if (messageNode.findValue("NOTIFICATION_MESSAGES") != null) {
				return jsonObjectMapper.treeToValue(message, SONotificationMessage.class);
			} else if (messageNode.findValue("FULFILLMENT_MESSAGES") != null) {

				return jsonObjectMapper.treeToValue(message, SOFulfillmentMessage.class);
			} else if (messageNode.findValue("REMINDER_MESSAGES") != null) {

				return jsonObjectMapper.treeToValue(message, SOReminderMessage.class);
			}
		} catch (JsonProcessingException e) {
			log.warn("can't marshal {} due to {}", message.toString(), e.getMessage());
			log.error("an error occured", e);
		}
		return null;
	}

	/**
	 * 
	 * Extract optinShortCode from SOMessage
	 * 
	 * @param soMessage
	 *            SOMessage
	 * @return optinShortCode
	 */
	public static String getOptinShortCode(SOMessage soMessage) {
		if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage soNotificationMessage = (SONotificationMessage) soMessage;
			String optinShortCode = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0)
					.getOffers().get(0).getNotificationMessages().get(0).getOptInShortCode();
			return optinShortCode;
		} else if (soMessage instanceof SOFulfillmentMessage) {// TO DO check
																// this logic
			return getHeaderValue(soMessage, "optInShortCode");

		}

		return null;
	}

	/**
	 * 
	 * Extract optinKeyword from SOMessage
	 * 
	 * @param soMessage
	 *            SOMessage
	 * @return optinKeyword
	 */
	public static String getOptinKeyword(SOMessage soMessage) {
		if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage soNotificationMessage = (SONotificationMessage) soMessage;
			String optinKeyword = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0)
					.getOffers().get(0).getNotificationMessages().get(0).getOptInKeyWord();
			return optinKeyword;
		} else if (soMessage instanceof SOFulfillmentMessage) {
			return getHeaderValue(soMessage, "optInKeyWord");

		}

		return null;
	}

	/**
	 * 
	 * Extract optinChannel from SOMessage
	 * 
	 * @param soMessage
	 *            SOMessage
	 * @return optinChannel
	 */
	public static String getOptinChannel(SOMessage soMessage) {
		if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage soNotificationMessage = (SONotificationMessage) soMessage;
			String optinChannel = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0)
					.getOffers().get(0).getNotificationMessages().get(0).getOptInChannel();
			return optinChannel;
		} else if (soMessage instanceof SOFulfillmentMessage) {
			return getHeaderValue(soMessage, "optInChannel");

		}

		return null;
	}

	/**
	 * 
	 * Extract offerStartDate from SOMessage
	 * 
	 * @param soMessage
	 *            SOMessage
	 * @return offerStartDate in Unix timestamp
	 */
	public static String getOfferStartDate(SOMessage soMessage) {
		if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage soNotificationMessage = (SONotificationMessage) soMessage;
			String startDate = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0)
					.getOffers().get(0).getOfferStartDate();
			return startDate;
		} else if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getOfferStartDate();
		}
		return null;
	}

	/**
	 * 
	 * Extract offerEndDate from SOMessage
	 * 
	 * @param soMessage
	 *            SOMessage
	 * @return offerEndDate in Unix timestamp
	 */
	public static String getOfferEndDate(SOMessage soMessage) {
		if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage soNotificationMessage = (SONotificationMessage) soMessage;
			String endDate = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers()
					.get(0).getOfferEndDate();
			return endDate;
		} else if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getOfferEndDate();
		}
		return null;
	}

	/**
	 * Used to get value of a key from header
	 * 
	 * @param soMessage
	 *            any kind of SOMessage
	 * @param key
	 *            String key
	 * @return value of the key
	 */
	public static String getHeaderValue(SOMessage soMessage, String key) {
		List<Header> headers = soMessage.getMetaInfo().getHeaders();
		Header object = new Header();
		object.setKey(key);
		if (headers.contains(object)) {
			int index = headers.indexOf(object);
			return headers.get(index).getValue();
		}
		return null;
	}

	/**
	 * Used to set value of a key in header
	 * 
	 * @param soMessage
	 *            soMessage
	 * @param key
	 *            key
	 * @param value
	 *            value
	 */
	public static void setHeaderValue(SOMessage soMessage, String key, String value) {
		List<Header> headers = soMessage.getMetaInfo().getHeaders();
		Header object = new Header();
		object.setKey(key);
		if (headers.contains(object)) {
			int index = headers.indexOf(object);
			headers.get(index).setValue(value);
		} else {
			object.setValue(value);
			headers.add(object);
		}
	}

	/**
	 * 
	 * Removed a SOMessage header
	 * 
	 * @param soMessage
	 *            soMessage
	 * @param key
	 *            key
	 */
	public static void removeHeaderValue(SOMessage soMessage, String key) {
		List<Header> headers = soMessage.getMetaInfo().getHeaders();
		Header object = new Header();
		object.setKey(key);
		if (headers.contains(object)) {
			int index = headers.indexOf(object);
			headers.remove(index);
		}
	}

	/**
	 * 
	 * Checks whether the message needs to be cached for OPTIN by checking the
	 * optin keyword/optin shortcode/optin channel not null
	 * 
	 * @param soMessage
	 *            SONotificationMessage
	 * @return true/false
	 */
	public static boolean isSoMessageOptin(SOMessage soMessage) {
		String optinShortCode = SOMessageUtils.getOptinShortCode(soMessage);
		String optinKeyword = SOMessageUtils.getOptinKeyword(soMessage);
		String optinChannel = SOMessageUtils.getOptinChannel(soMessage);
		if (StringUtils.isNotBlank(optinShortCode) && StringUtils.isNotBlank(optinKeyword)
				&& StringUtils.isNotBlank(optinChannel)) {
			return true;
		}
		log.debug("Message does not contain optin information. {}", soMessage);
		return false;
	}

	/**
	 * 
	 * To generate timestamp in UNIX format as the core requires it
	 * 
	 * @return timestamp String in UNIX format
	 */
	public static String getUnixTimestamp() {
		Calendar cal = Calendar.getInstance();
		// Get the time in local timezone
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minutes = cal.get(Calendar.MINUTE);
		int seconds = cal.get(Calendar.SECOND);
		// create time zone object with UTC and set the timezone
		TimeZone tzone = TimeZone.getTimeZone("UTC");
		cal.setTimeZone(tzone);
		// Set the current time as GMT Time
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minutes);
		cal.set(Calendar.SECOND, seconds);
		long currentTimeInMillis = cal.getTimeInMillis();
		String eventTimeStamp = String.valueOf(currentTimeInMillis); // Current
																		// time
																		// in SO
		return eventTimeStamp;
	}

	/**
	 * 
	 * Create MetaInfo with default values
	 * 
	 * @param actionType
	 *            ActionType to be set
	 * @param serviceName
	 *            serviceName to be set
	 * @param stepName
	 *            stepName to be set
	 * @param defaultChannel
	 *            defaultChannel to be set
	 * @param actionTypeKeyName
	 *            actionTypeKeyName to be set
	 * @return metaInfo
	 */
	public static MetaInfo createDefaultMetaInfo(String actionType, String serviceName, String stepName,
			String defaultChannel, String actionTypeKeyName) {

		MetaInfo metaInfo = SOMessageObjectFactory.createDefaultMetaInfo(defaultChannel);
		metaInfo.setServiceName(serviceName);
		metaInfo.setStepName(stepName);
		List<Header> headers = SOMessageObjectFactory.createDefaultHeader(actionTypeKeyName, actionType);
		metaInfo.setHeaders(headers);
		return metaInfo;
	}

	/**
	 * 
	 * Returns the offer payload from SOMessage
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return offer payload Map
	 */
	public static Map<String, String> getOfferPayLoad(SOMessage soMessage) {
		if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
					.getOfferPayload();
		} else if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getOfferPayload();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0)
					.getOfferPayload();
		}
		return null;
	}

	/**
	 * 
	 * Gets the subscription ID from the SOMessage
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return IMSI in case of mobile and ContractID for other subscription type
	 */
	public static String getSubscriptionId(SOMessage soMessage) {
		Map<String, String> offerPayLoad = getOfferPayLoad(soMessage);
		if (offerPayLoad != null) {
			String subscriptionType = offerPayLoad.get("SubscriptionID");
			return subscriptionType;
		}
		return null;
	}

	/**
	 * 
	 * Get fulfillment product List
	 * 
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @return fulfillment products
	 */
	public static List<FulfillmentProduct> getFulfillmentProducts(SOFulfillmentMessage soFulfillmentMessage) {
		return soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
				.getFulfillmentProducts();
	}

	/**
	 * 
	 * Returns a Map<String, FulfillmentProduct> String- FulfillmentProduct
	 * Sequence
	 * 
	 * @param fulfillmentProducts
	 *            fulfillmentProducts
	 * @return fulfillmentProductSequenceMap
	 */
	public static Map<String, FulfillmentProduct> getFulfillmentProductBySequenceMap(
			List<FulfillmentProduct> fulfillmentProducts) {
		Map<String, FulfillmentProduct> fulfillmentProductSequenceMap = new HashMap<String, FulfillmentProduct>();
		for (FulfillmentProduct fulfillmentProduct : fulfillmentProducts) {
			if (StringUtils.startsWithAny(fulfillmentProduct.getTargetSystem(),
					new String[] { "OCS", "CS", "CBS", "UO" })) {
				String productSequence = getSequenceForProduct(fulfillmentProduct);
				fulfillmentProductSequenceMap.put(productSequence, fulfillmentProduct);
			}
		}
		return fulfillmentProductSequenceMap;
	}

	/**
	 * 
	 * Return Sequence of the fulfillment product
	 * 
	 * @param fulfillmentProduct
	 *            fulfillmentProduct
	 * @return SEQUENCE number
	 */
	private static String getSequenceForProduct(FulfillmentProduct fulfillmentProduct) {
		List<DynamicParameter> dynamicParameters = fulfillmentProduct.getDynamicParameters();
		if (dynamicParameters != null && dynamicParameters.size() > 0) {
			for (DynamicParameter dynamicParameter : dynamicParameters) {
				if (dynamicParameter.getKey().equalsIgnoreCase("SEQUENCE")) {
					String sequence = dynamicParameter.getResolvedValue();
					return sequence;
				}
			}
		}
		log.error("Dynamic Parameters doesn't contain a SEQUENCE. Please configure dynamic Parameter for product {}",
				fulfillmentProduct.getProductId());
		return null;
	}

	/**
	 * 
	 * Returns a FulfillmentProduct with NOMINATION details
	 * 
	 * @param fulfillmentProducts
	 *            fulfillmentProducts
	 * @return nominationFulfillmentProduct
	 */
	public static FulfillmentProduct getNominationProduct(List<FulfillmentProduct> fulfillmentProducts) {
		FulfillmentProduct nominationFulfillmentProduct = null;
		for (FulfillmentProduct fulfillmentProduct : fulfillmentProducts) {
			if (StringUtils.equals(fulfillmentProduct.getTargetSystem(), "NOMINATION")) {
				nominationFulfillmentProduct = fulfillmentProduct;
				break;
			}
		}
		return nominationFulfillmentProduct;
	}

	/**
	 * 
	 * Returns dynamicParameters as map with key and value
	 * 
	 * @param fulfillmentProduct
	 *            fulfillmentProduct
	 * @return fulfillmentProduct
	 */
	public static Map<String, String> getFulfillmentProductDynamicParametersAsMap(
			FulfillmentProduct fulfillmentProduct) {
		List<DynamicParameter> dynamicParameters = fulfillmentProduct.getDynamicParameters();
		Map<String, String> dynamicParametersMap = new HashMap<String, String>();
		if (dynamicParameters != null && dynamicParameters.size() > 0) {
			for (DynamicParameter dynamicParameter : dynamicParameters) {
				dynamicParametersMap.put(dynamicParameter.getKey(), dynamicParameter.getResolvedValue());
			}
		}
		return dynamicParametersMap;
	}

	/**
	 * 
	 * Only send SMS and fulfill products to a particular list of subscribers
	 * while testing (configured in property so.test.whiteListedMSISDNs as CSV)
	 * for enabling this so.test.enableWhiteListing property should be set to
	 * true
	 * 
	 * @param msisdn
	 *            mobile number of the subscriber
	 * @param enableWhiteListing
	 *            property from properties file and should set to true to enable
	 * @param whiteListedMSISDNList
	 *            list of mobile numbers to check with
	 * @return true/false to proceed with sending SMS
	 */
	public static boolean checkMsisdnWhiteListCondition(String msisdn, Boolean enableWhiteListing,
			List<String> whiteListedMSISDNList) {
		if (enableWhiteListing) {
			if (whiteListedMSISDNList != null && whiteListedMSISDNList.size() > 0) {
				if (!whiteListedMSISDNList.contains(msisdn)) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 
	 * Returns text to be sent
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return text to be sent
	 */
	public static String getTextToBeSent(SOMessage soMessage) {
		if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
					.getNotificationMessages().get(0).getMessages().get(0).getTextTobeSent();
		} else if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return returnFulfillmentTextToBeSent(message);
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0)
					.getReminderMessages().get(0).getMessages().get(0).getTextTobeSent();
		}
		return null;
	}

	/**
	 * get the text to be sent from fulfillment message
	 * 
	 * @param message
	 *            SOFulfillmentMessage
	 * @return text to be sent or null
	 */
	private static String returnFulfillmentTextToBeSent(SOFulfillmentMessage message) {
		List<MessageContainer> fulfillmentMessages = message.getMessage().getRequesterLocation().getLocations().get(0)
				.getFulfilments().get(0).getFulfillmentMessages();
		if (fulfillmentMessages != null && fulfillmentMessages.size() > 0) {
			return fulfillmentMessages.get(0).getMessages().get(0).getTextTobeSent();
		} else {
			log.info("FulfillmentMessage doesn't contain message array");
			return null;
		}
	}

	/**
	 * 
	 * Returns IS_CONTROL
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return IS_CONTROL
	 */
	public static String getIsControl(SOMessage soMessage) {
		if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getIsControl()
					.toString();
		} else if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getIsControl().toString();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0)
					.getIsControl().toString();
		}
		return null;
	}

	/**
	 * 
	 * Returns IS_SIMULATED
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return IS_SIMULATED
	 */
	public static String getIsSimulated(SOMessage soMessage) {
		if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getIsSimulated()
					.toString();
		} else if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getIsSimulated().toString();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0)
					.getIsSimulated().toString();
		}
		return null;
	}

	/**
	 * 
	 * Returns shortCode to be set in SMSC
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return text to be sent
	 */
	public static String getSmscShortCode(SOMessage soMessage) {
		if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
					.getNotificationMessages().get(0).getShortCode();
		} else if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			List<MessageContainer> fulfillmentMessages = message.getMessage().getRequesterLocation().getLocations()
					.get(0).getFulfilments().get(0).getFulfillmentMessages();
			if (fulfillmentMessages.isEmpty())
				return null;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
					.getFulfillmentMessages().get(0).getShortCode();
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return message.getMessage().getRequesterLocation().getLocations().get(0).getReminders().get(0)
					.getReminderMessages().get(0).getShortCode();
		}
		return null;
	}

}
