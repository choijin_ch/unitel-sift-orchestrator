/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.knowesis.sift.orchestrator.core.domain.DynamicParameter;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;

/**
 * Common Utility methods for RawJson Message to SOMessage conversion
 * 
 * @author SO Development Team
 */
public class ProvisionUtils {

    /**
     * 
     * Get Current product with the sequence Number (To be removed. Move to
     * SOMessageUtils)
     * 
     * @param soFulfillmentMessage
     *            soFulfillmentMessage
     * @param currentSequence
     *            currentSequence
     * @return currentSequence of fullfillment products map
     */
    public static FulfillmentProduct getCurrentProduct(SOFulfillmentMessage soFulfillmentMessage,
            String currentSequence) {
        List<FulfillmentProduct> fulfillmentProducts = SOMessageUtils.getFulfillmentProducts(soFulfillmentMessage);
        Map<String, FulfillmentProduct> fulfillmentProductSequenceMap = getFulfillmentProductSequenceMap(
                fulfillmentProducts);
        return fulfillmentProductSequenceMap.get(currentSequence);

    }

    /**
     * 
     * Returns a Map<String, FulfillmentProduct> String- FulfillmentProduct
     * Sequence
     * 
     * @param fulfillmentProducts
     *            fulfillmentProducts
     * @return fulfillmentProductSequenceMap 
     */
    private static Map<String, FulfillmentProduct> getFulfillmentProductSequenceMap(
            List<FulfillmentProduct> fulfillmentProducts) {
        Map<String, FulfillmentProduct> fulfillmentProductSequenceMap = new HashMap<String, FulfillmentProduct>();
        for (FulfillmentProduct fulfillmentProduct : fulfillmentProducts) {
            String productSequence = getSequenceForProduct(fulfillmentProduct);
            fulfillmentProductSequenceMap.put(productSequence, fulfillmentProduct);
        }
        return fulfillmentProductSequenceMap;
    }

    /**
     * 
     * Return Sequence of the fulfillment product
     * 
     * @param fulfillmentProduct
     *            fulfillmentProduct
     * @return SEQUENCE number
     */
    private static String getSequenceForProduct(FulfillmentProduct fulfillmentProduct) {
        List<DynamicParameter> dynamicParameters = fulfillmentProduct.getDynamicParameters();
        if (dynamicParameters != null && dynamicParameters.size() > 0) {
            for (DynamicParameter dynamicParameter : dynamicParameters) {
                if (dynamicParameter.getKey().equalsIgnoreCase("SEQUENCE")) {
                    String sequence = dynamicParameter.getResolvedValue();
                    return sequence;
                }
            }
        }

        return null;
    }

    /**
     * getting values from dynamic parameters
     * 
     * @param fulfillmentProduct
     *            fulfillmentProduct
     * @param key
     *            to be fetched from the parameter
     * @return value of the key
     */
    public static String getValueFromDynamicParameter(FulfillmentProduct fulfillmentProduct, String key) {
        List<DynamicParameter> dynamicParameters = fulfillmentProduct.getDynamicParameters();
        if (dynamicParameters != null && dynamicParameters.size() > 0) {
            for (DynamicParameter dynamicParameter : dynamicParameters) {
                if (dynamicParameter.getKey().equalsIgnoreCase(key)) {
                    String value = dynamicParameter.getResolvedValue();
                    return value;
                }
            }
        }
        return null;
    }
    
    
    /**
     * Get Dynamic parameter for a particular Key
     * 
     * @param fulfillmentProduct
     *            fulfillmentProduct
     * @param key
     *            to be fetched from the parameter
     * @return Dynamic Parameter
     */
    public static DynamicParameter getDynamicParameter(FulfillmentProduct fulfillmentProduct, String key) {
        List<DynamicParameter> dynamicParameters = fulfillmentProduct.getDynamicParameters();
        if (dynamicParameters != null && dynamicParameters.size() > 0) {
            for (DynamicParameter dynamicParameter : dynamicParameters) {
                if (dynamicParameter.getKey().equalsIgnoreCase(key)) {
                    return dynamicParameter;
                }
            }
        }
        return null;
    }

    /**
     * Set the status of current target api call
     * 
     * @param soMessage
     *            soMessage
     * @param currentSequence
     *            currentSequence
     * @param status
     *            will be set as per the scenario
     *            (PROCESSING,QUEUED,SUCCESS,FAILURE)
     */
    public static void setSequenceStatusHeaderKey(SOMessage soMessage, String currentSequence, String status) {
        String key = "SEQUENCE-" + currentSequence + "-STATUS";
        SOMessageUtils.setHeaderValue(soMessage, key, status);
    }
    
	/**
	 * To get rewardValidityDay for counter expire
	 * 
	 * @param fulfillmentProduct
	 *            fulfillment Product
	 * @return rewardValidityDay
	 */
    public static String getRewardValidityDayFromDynamicParameter(FulfillmentProduct fulfillmentProduct) {
        List<DynamicParameter> dynamicParameters = fulfillmentProduct.getDynamicParameters();
        if (dynamicParameters != null && dynamicParameters.size() > 0) {
            String rewardValidityDay=dynamicParameters.get(0).getRewardValidityDays().toString();
            return rewardValidityDay;
        }
        return null;
    }

	/**
	 * To get rewardValidityHour for counter expire
	 * 
	 * @param fulfillmentProduct
	 *            fulfillment Product
	 * @return rewardValidityHour
	 */
    public static String getRewardValidityHourFromDynamicParameter(FulfillmentProduct fulfillmentProduct) {
        List<DynamicParameter> dynamicParameters = fulfillmentProduct.getDynamicParameters();
        if (dynamicParameters != null && dynamicParameters.size() > 0) {
            String rewardValidityHour=dynamicParameters.get(0).getRewardValidityHours().toString();
            return rewardValidityHour;
        }
        return null;
    }
    
}
