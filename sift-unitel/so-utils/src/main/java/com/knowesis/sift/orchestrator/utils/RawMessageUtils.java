/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.domain.SOReminderMessage;

/**
 * Common Utility methods for RawJson Message to SOMessage conversion
 * 
 * @author SO Development Team
 */
public class RawMessageUtils {

    private static Logger log = LoggerFactory.getLogger(RawMessageUtils.class);

    /**
     * Process message in SO Format
     * 
     * @param message raw message(s)
     * @param jsonObjectMapper Object mappers
     * @return SOMessage, can be null
     */
    public static SOMessage processRawMessage(String message, ObjectMapper jsonObjectMapper) {
        // strategy pattern can be used if cyclomatic complexity is high
        SOMessage soMessage = null;
        try {
            JsonNode node = jsonObjectMapper.readTree(message);
            if (node.has("message")) {
                soMessage = getSOMessage(node, jsonObjectMapper);
            }
        } catch (IOException e) {
            log.warn("can't marshal {} due to {}", message, e.getMessage());
            log.error("an error occured", e);
        }
        return soMessage;
    }

    /**
     * Create SONotificationMessage or SOFulfillmentMessage or SOReminderMessage from SOMessage JSON
     * Payload
     * 
     * @param node JSON Node
     * @param jsonObjectMapper Object mapper
     * @return SOMessage, can be null
     */
    public static SOMessage getSOMessage(JsonNode node, ObjectMapper jsonObjectMapper) {
        SOMessage soMessage = null;
        try {
            JsonNode messageNode = node.get("message");
            if (messageNode.findValue("NOTIFICATION_MESSAGES") != null) {
                soMessage = jsonObjectMapper.treeToValue(node, SONotificationMessage.class);
            } else if (messageNode.findValue("FULFILLMENT_MESSAGES") != null) {
                soMessage = jsonObjectMapper.treeToValue(node, SOFulfillmentMessage.class);
            } else if (messageNode.findValue("REMINDER_MESSAGES") != null) {
                soMessage = jsonObjectMapper.treeToValue(node, SOReminderMessage.class);
            }
        } catch (JsonProcessingException e) {
            log.warn("can't marshal {} due to {}", node.toString(), e.getMessage());
            log.error("an error occured", e);
        }
        return soMessage;
    }
}
