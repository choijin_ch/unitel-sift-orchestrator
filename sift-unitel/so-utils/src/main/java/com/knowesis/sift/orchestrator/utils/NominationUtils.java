/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

import java.util.Calendar;
import java.util.Map;

import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;

/**
 * 
 * Nomination Utils
 * 
 * @author SO Development Team
 */
public class NominationUtils {

    /**
     * 
     * Get Nominator number
     * 
     * @param soMessage soFulfillmentMessage
     * @return nominatorNumber
     */
    public static String getNominatorNumber(SOMessage soMessage) {
        FulfillmentProduct nominationFulfillmenProduct = SOMessageUtils
                .getNominationProduct(SOMessageUtils.getFulfillmentProducts((SOFulfillmentMessage) soMessage));
        Map<String, String> nominationDynamicParameterMap = SOMessageUtils
                .getFulfillmentProductDynamicParametersAsMap(nominationFulfillmenProduct);
        String nominatorMsisdn = nominationDynamicParameterMap.get("NOMINATOR_NUMBER");
        return nominatorMsisdn;
    }

    /**
     * 
     * Get nomination short code
     * 
     * @param soMessage soFulfillmentMessage
     * @return nomination short code
     */
    public static String getNominationShortCode(SOMessage soMessage) {
        FulfillmentProduct nominationFulfillmenProduct = SOMessageUtils
                .getNominationProduct(SOMessageUtils.getFulfillmentProducts((SOFulfillmentMessage) soMessage));
        Map<String, String> nominationDynamicParameterMap = SOMessageUtils
                .getFulfillmentProductDynamicParametersAsMap(nominationFulfillmenProduct);
        String nominationShortCode = nominationDynamicParameterMap.get("NOMINATION_SHORT_CODE");
        return nominationShortCode;
    }
    
    /**
	 * Get milliseconds from midnight today
	 * 
	 * @return milliseconds
	 */
	public static Long getPastMillisecondForToday() {
		Calendar c = Calendar.getInstance();
		long now = c.getTimeInMillis();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		long passedLong = now - c.getTimeInMillis();
		String passedString = "" + passedLong;
		return Long.parseLong(passedString);// to set time as previous day End
											// Of Day
	}

}
