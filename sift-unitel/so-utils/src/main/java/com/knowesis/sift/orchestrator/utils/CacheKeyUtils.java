/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handy utility methods for generating cache keys
 * 
 * @author SO Development Team
 */
public class CacheKeyUtils {

	private static final Logger LOG = LoggerFactory.getLogger(CacheKeyUtils.class);

	/**
	 * Generate key for caching object with CamelSmppId
	 * 
	 * @param id
	 *            camel smppid
	 * @return cachekey
	 */
	public static String generateMTCacheKey(String id) {

		String key = null;
		if (StringUtils.isNotBlank(id)) {
			StringBuilder builder = new StringBuilder();
			builder.append("SMSMT-").append(id);
			key = builder.toString();
		}
		LOG.debug("Generated MT cache key {}", key);
		return key;
	}

	/**
	 * Generate key for optin cases
	 * 
	 * @param optinShortCode
	 *            the shortcode
	 * @param msisdn
	 *            msisdn
	 * @param optinKeyword
	 *            optinKeyword
	 * @return cachekey
	 */
	public static String generateOptinCacheKey(String optinShortCode, String msisdn, String optinKeyword) {

		String key = null;
		if (StringUtils.isNotBlank(msisdn) && StringUtils.isNotBlank(optinKeyword)) {
			StringBuilder builder = new StringBuilder();
			builder.append("OPTIN-").append(optinShortCode).append("-").append(msisdn).append("-")
					.append(optinKeyword.toUpperCase());
			key = builder.toString();
		}
		LOG.debug("Generated Optin cache key {}", key);
		return key;
	}

	

	/**
	 * Generate key for storing fulfillment messages temporarily while SSP in
	 * progress to avoid repeated optin's (Continuous YES by user in short span
	 * of time)
	 * 
	 * @param msisdn
	 *            msisdn
	 * @param offerId
	 *            offerId
	 * @return cachekey
	 */
	public static String generateOptinCacheKeyForProcessingFulfillment(String msisdn, String offerId) {

		String key = null;
		if (StringUtils.isNotBlank(msisdn) && StringUtils.isNotBlank(offerId)) {
			StringBuilder builder = new StringBuilder();
			builder.append("FULFILL-").append(msisdn).append("-").append(offerId);
			key = builder.toString();
		}
		LOG.debug("Generated Optin cache key {}", key);
		return key;
	}

	/**
	 * Generate key for storing Notification Message for storing NOtification
	 * Message for USSD Optin
	 * 
	 * @param msisdn
	 *            msisdn
	 * @param offerId
	 *            offerId
	 * @return cacheKey
	 */
	public static String generateOptinCacheKeyProcessingFulfillmentForUssdOptin(String msisdn, String offerId) {
		String key = null;
		if (StringUtils.isNotBlank(msisdn) && StringUtils.isNotBlank(offerId)) {
			StringBuilder builder = new StringBuilder();
			builder.append("FULFILL-USSD").append("-").append(msisdn).append("-").append(offerId);
			key = builder.toString();
		}
		LOG.debug("Generated USSD Optin cache key {}", key);
		return key;
	}

	/**
	 * Generate Redis-TTL for storing Notification Message
	 * 
	 * @param startDate
	 *            startDate in milliseconds
	 * @param endDate
	 *            endDate in milliseconds
	 * @return optinTTL
	 */
	public static int generateOptinTTL(String startDate, String endDate) {
		if (StringUtils.isNumeric(startDate) && StringUtils.isNumeric(endDate)) {
			Long startTime = Long.parseLong(startDate);
			Long endTime = Long.parseLong(endDate);
			Long ttl = (endTime - startTime) / 1000;
			LOG.debug("start time {} end time {} ttl {}", startTime, endTime, ttl);
			int optinTTL = ttl.intValue();
			LOG.debug("optin TTL without buffer {}", optinTTL);
			return optinTTL;
		}
		LOG.info("ttl is not generated as Startdate {} /EndDate{} is not numeric",startDate,endDate);
		return 0;
	}
	
	/**
	 * 
	 * Generate Nomination cache key
	 * 
	 * @param msisdn
	 *            msisdn
	 * @param nominationShortCode
	 *            nominationShortCode
	 * @return nomination cache key
	 */
	public static String generateNominationCacheKey(String msisdn, String nominationShortCode) {
		String key = null;
		if (StringUtils.isNotBlank(msisdn) && StringUtils.isNotBlank(nominationShortCode)) {
			StringBuilder builder = new StringBuilder();
			builder.append("NOMINATION-").append(nominationShortCode).append("-").append(msisdn);
			key = builder.toString();
		}
		LOG.debug("Generated Optin cache key {}", key);
		return key;
	}

	/**
	 * 
	 * Generate Inbound cache key
	 * 
	 * @param msisdn
	 *            msisdn
	 * @param flowId
	 *            flowId
	 * @return inbound cache key
	 */
	public static String generateInboundCacheKey(String msisdn, String flowId) {
		String key = null;
		if (StringUtils.isNotBlank(msisdn) && StringUtils.isNotBlank(flowId)) {
			StringBuilder builder = new StringBuilder();
			builder.append("INBOUND-").append(flowId).append("-").append(msisdn);
			key = builder.toString();
		} else {
			LOG.info("MSISDN, programId or offerId is blank in the incoming inbound request");
		}
		LOG.debug("Generated Inbound cache key {}", key);
		return key;
	}

	/**
	 * Generate key for Multi Level Optin cases
	 * 
	 * @param optinShortCode
	 *            the shortcode
	 * @param msisdn
	 *            msisdn
	 * @return cachekey
	 */
	public static String generateMultiLevelOptinCacheKey(String optinShortCode, String msisdn) {
		String key = null;
		if (StringUtils.isNotBlank(msisdn) && StringUtils.isNotBlank(optinShortCode)) {
			StringBuilder builder = new StringBuilder();
			builder.append("MULTILEVEL-").append(optinShortCode).append("-").append(msisdn);
			key = builder.toString();
		}
		LOG.debug("Generated Multi Level Optin cache key {}", key);
		return key;
	}

}
