package com.knowesis.sift.orchestrator.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by bat-oyu.b on 2/26/14.
 */
public class MQData {
    public final static int TYPE_STRING = 0;
    public final static int TYPE_INT = 1;
    public final static int TYPE_LONG = 2;
    public final static int TYPE_BYTEARRAY = 3;
    public final static int TYPE_DOUBLE = 4;

    private int _type;

    private byte[] _data;

    public MQData(String data) {
        _data = data.getBytes();
        _type = TYPE_STRING;
    }

    public MQData(byte[] data) {
        _data = data;
        _type = TYPE_BYTEARRAY;
    }

    public MQData(int data) {
        _data = ("" + data).getBytes();
        _type = TYPE_INT;
    }

    public MQData(long data) {
        _data = ("" + data).getBytes();
        _type = TYPE_LONG;
    }

    public MQData(double data) {
        _data = ("" + data).getBytes();
        _type = TYPE_DOUBLE;
    }

    public int get_length() {
        return _data.length + 8;
    }

    public void writeTo(ByteArrayOutputStream stream) throws IOException {
        stream.write(ByteUtil.intToByteArray(_type, false));
        stream.write(ByteUtil.intToByteArray(_data.length, false));
        stream.write(_data);
    }

    public void set_data(String data) {
        _data = data.getBytes();
        _type = TYPE_STRING;
    }

    public void set_data(byte[] data) {
        _data = data;
        _type = TYPE_BYTEARRAY;
    }

    public void set_data(int data) {
        _data = ("" + data).getBytes();
        _type = TYPE_INT;
    }

    public void set_data(long data) {
        _data = ("" + data).getBytes();
        _type = TYPE_LONG;
    }

    public void set_data(double data) {
        _data = ("" + data).getBytes();
        _type = TYPE_DOUBLE;
    }

    public Object get_data() {
        switch (_type) {
            case TYPE_STRING:
                return new String(_data);
            case TYPE_BYTEARRAY:
                return _data;
            case TYPE_DOUBLE:
                return Double.parseDouble(new String(_data));
            case TYPE_INT:
                return Integer.parseInt(new String(_data));
            case TYPE_LONG:
                return Long.parseLong(new String(_data));
        }
        return null;
    }

    @Override
    public String toString() {
        return "MQData{" +
                "_type=" + _type +
                ", _data=" + Arrays.toString(_data) +
                '}';
    }
}
