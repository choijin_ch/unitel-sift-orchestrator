/*
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.utils.RabbitMQMessage;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Properties;
import java.io.IOException;

//import org.springframework.beans.factory.annotation.Value;



/**
 * Processes Rabbit MQ requests
 * 
 * @author SO Development Team
 */
@Component("RabbitCUGProcessor")
public class RabbitCUGProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(RabbitCUGProcessor.class);


    @Autowired
    ObjectMapper jsonObjectMapper;

    @Resource(name="myProperties")
    private Properties config;


  /*
    @Value("${mo.optin.actionType}")
    private String actionTypeOptin;
    */


    /*
     * (non-Javadoc)
     * 
     * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        String messageType = (String) exchange.getIn().getHeader("rabbitmq.EXCHANGE_NAME");
        LOG.debug("Rabbit MQ Message Type: {}", messageType);
        processRabbitMQMessage(exchange);
    }

    /**
     * Process RabbitMQ Message request
     * 
     * @param exchange exchange
     * @throws Exception Exception
     * @throws IOException IOException
     * @throws JsonProcessingException JsonProcessingException
     * @throws JsonParseException JsonParseException
     * @throws JsonMappingException JsonMappingException
     */
    private void processRabbitMQMessage(Exchange exchange)
            throws Exception, IOException, JsonProcessingException, JsonParseException, JsonMappingException {


        byte[] rabbitMessage = (byte []) exchange.getIn().getBody();

        LOG.debug("Processing Rabbit MQ Message: {}", rabbitMessage);


        if(rabbitMessage !=null){

                RabbitMQMessage rmq = RabbitMQMessage.Parse(rabbitMessage,0);

                LOG.debug("Seq:" + rmq.get_seq());

                String jsonData = (String) rmq.get_data("JSON");
                LOG.debug("JSON Data:" + jsonData);
                String command = (String) rmq.get_data("CMD");
                LOG.debug("Command Data:" + command );

            exchange.getOut().setHeader("CMD",command);

            String siftQ = config.getProperty(command);

            LOG.debug("SiftQ from Property :" + siftQ );

            if((siftQ!=null) && (!(siftQ.equalsIgnoreCase("NA")))){
                siftQ = "activemq:queue:" + siftQ;

            } else {
                siftQ = "NA";
                LOG.warn("Deactivated - {} - Message Recieved from RabbitMQ - {}",command,jsonData);
            }

            LOG.debug("SiftQ for Header :" + siftQ );

            exchange.getOut().setHeader("SIFT-TARGET",siftQ);

            exchange.getOut().setBody(jsonData);

                rmq = null;
        }

    }


}
