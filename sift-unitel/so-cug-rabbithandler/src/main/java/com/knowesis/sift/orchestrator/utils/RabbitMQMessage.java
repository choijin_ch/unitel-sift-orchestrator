package com.knowesis.sift.orchestrator.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by bat-oyu.b on 2/19/14.
 */
public class RabbitMQMessage {
    public static Logger logger = LoggerFactory.getLogger(RabbitMQMessage.class);
//    public static final int TYPE_SMS = 1;
//    public static final int TYPE_OCS = 2;
//    public static final int TYPE_DIAMETER = 3;

    private int _type = 0;
    private int _length = 12;

    public Map<String, MQData> get_values() {
        return _values;
    }

    private Map<String, MQData> _values = new LinkedHashMap<String, MQData>();

    @Override
    public String toString() {
        return "RabbitMQMessage{" +
                "_type=" + _type +
                ", _length=" + _length +
                ", _values=" + _values +
                '}';
    }

    public String get_seq() {
        Object s = get_data("seq");
        return s == null ? null : s.toString();
    }

    public void set_seq(String seq) {
        set_data("seq", seq);
    }

    public byte[] get_bytes() throws Exception {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            stream.write(ByteUtil.intToByteArray(_length, false));
            stream.write(ByteUtil.intToByteArray(_type, false));
            stream.write(ByteUtil.intToByteArray(_values.size(), false));
            for(String name : _values.keySet()) {
                stream.write(ByteUtil.intToByteArray(name.length(), false));
                stream.write(name.getBytes());
                _values.get(name).writeTo(stream);
            }
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        }
        return stream.toByteArray();
    }

    public static RabbitMQMessage Parse(byte[] bytes, int start) throws Exception {
        //logger.debug(ByteUtil.byteArrayToHexString(bytes));
        RabbitMQMessage to = new RabbitMQMessage();
        if (bytes.length + start < 8) {
            throw new Exception("Not Enough Data for parsing for RabbitMQ message");
        }

        int length = ByteUtil.byteArrayToInt(bytes, start, false);
        logger.debug("length: " + (bytes.length + start - 4) + " : " + length);
        if (bytes.length + start - 4 < length) {
            throw new Exception("Not Enough Data Length for parsing for RabbitMQ message");
        }
        to.set_length(length);
        to.set_type(ByteUtil.byteArrayToInt(bytes, start + 4, false));
        length = ByteUtil.byteArrayToInt(bytes, start + 8, false);
        int offset = start + 12;
        while(length > 0) {
            int len = ByteUtil.byteArrayToInt(bytes, offset, false);
            String name = new String(bytes, offset + 4, len);
            int type = ByteUtil.byteArrayToInt(bytes, offset + 4 + len, false);
            int len1 = ByteUtil.byteArrayToInt(bytes, offset + 4 + len + 4, false);
            byte[] data = new byte[len1];
            System.arraycopy(bytes, offset + 4 + len + 4 + 4, data, 0, len1);
            switch (type) {
                case MQData.TYPE_STRING:
                    to.set_data(name, new String(data));
                    break;
                case MQData.TYPE_BYTEARRAY:
                    to.set_data(name, data);
                    break;
                case MQData.TYPE_DOUBLE:
                    to.set_data(name, Double.parseDouble(new String(data)));
                    break;
                case MQData.TYPE_INT:
                    to.set_data(name, Integer.parseInt(new String(data)));
                    break;
                case MQData.TYPE_LONG:
                    to.set_data(name, Long.parseLong(new String(data)));
                    break;
            }
            offset += 4 + len + 4 + 4 + len1;
            length--;
        }
        return to;
    }

    public void parse(byte[] bytes, int start) throws Exception {
        //logger.debug(ByteUtil.byteArrayToHexString(bytes));
        if (bytes.length + start < 8) {
            throw new Exception("Not Enough Data for parsing for RabbitMQ message");
        }

        int length = ByteUtil.byteArrayToInt(bytes, start, false);
        //logger.debug("length: " + (bytes.length + start - 4) + " : " + length);
        if (bytes.length + start - 4 < length) {
            throw new Exception("Not Enough Data Length for parsing for RabbitMQ message");
        }
        set_length(length);
        set_type(ByteUtil.byteArrayToInt(bytes, start + 4, false));
        length = ByteUtil.byteArrayToInt(bytes, start + 8, false);
        int offset = start + 12;
        while(length > 0) {
            int len = ByteUtil.byteArrayToInt(bytes, offset, false);
            String name = new String(bytes, offset + 4, len);
            int type = ByteUtil.byteArrayToInt(bytes, offset + 4 + len, false);
            int len1 = ByteUtil.byteArrayToInt(bytes, offset + 4 + len + 4, false);
            byte[] data = new byte[len1];
            System.arraycopy(bytes, offset + 4 + len + 4 + 4, data, 0, len1);
            switch (type) {
                case MQData.TYPE_STRING:
                    set_data(name, new String(data));
                    break;
                case MQData.TYPE_BYTEARRAY:
                    set_data(name, data);
                    break;
                case MQData.TYPE_DOUBLE:
                    set_data(name, Double.parseDouble(new String(data)));
                    break;
                case MQData.TYPE_INT:
                    set_data(name, Integer.parseInt(new String(data)));
                    break;
                case MQData.TYPE_LONG:
                    set_data(name, Long.parseLong(new String(data)));
                    break;
            }
            offset += 4 + len + 4 + 4 + len1;
            length--;
        }
    }

    public int get_type() {
        return _type;
    }

    public void set_type(int _type) {
        this._type = _type;
    }

    public int get_length() {
        return _length;
    }

    public void set_length(int _length) {
        this._length = _length;
    }

    public void set_data(String name, String value) {
        //if (value == null || name == null) return;
        MQData data;
        data = _values.get(name);
        if (data == null) {
            data = new MQData(value);
        } else {
            data.set_data(value);
        }
        _values.put(name, data);
        calc_length();
    }

    public void set_data(String name, int value) {
        MQData data;
        data = _values.get(name);
        if (data == null) {
            data = new MQData(value);
        } else {
            data.set_data(value);
        }
        _values.put(name, data);
        calc_length();
    }

    public void set_data(String name, long value) {
        MQData data;
        data = _values.get(name);
        if (data == null) {
            data = new MQData(value);
        } else {
            data.set_data(value);
        }
        _values.put(name, data);
        calc_length();
    }

    public void set_data(String name, double value) {
        MQData data;
        data = _values.get(name);
        if (data == null) {
            data = new MQData(value);
        } else {
            data.set_data(value);
        }
        _values.put(name, data);
        calc_length();
    }

    public void set_data(String name, byte[] value) {
        MQData data;
        data = _values.get(name);
        if (data == null) {
            data = new MQData(value);
        } else {
            data.set_data(value);
        }
        _values.put(name, data);
        calc_length();
    }

    private void calc_length() {
        _length = 8;
        for(String name : _values.keySet()) {
            _length += (_values.get(name).get_length() + 4 + name.length());
        }
    }

    public Object get_data(String name) {
        MQData data;
        data = _values.get(name);
        if (data == null) {
            return null;
        }
        return data.get_data();
    }
}
