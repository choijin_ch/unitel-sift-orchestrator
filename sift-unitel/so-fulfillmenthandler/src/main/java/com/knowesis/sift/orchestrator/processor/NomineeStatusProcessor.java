/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.commons.CommonFunctions;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.NominationUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * 
 * Processes Nominee status
 * 
 * @author SO Development Team
 */
@Component("NomineeStatusProcessor")
public class NomineeStatusProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${so.fulfill.actionType.provisionFulfillNotifyNominator}")
    private String provisionFulfillNotifyNominator;

    @Value("${so.notification.template}")
    private String soMessageTemplate;

    @Value("${fulfillment.notify.notPrepaidNumber.textToBeSend}")
    private String notPrepaidNumberTextToBeSend;

    @Value("${fulfillment.notify.inactiveNumber.textToBeSend}")
    private String inactiveNumberTextToBeSend;

    @Value("${fulfillment.notify.m2mNumber.textTobeSend}")
    private String m2mNumberTextToBeSend;

    @Value("${so.triggerType.nominationNotify}")
    private String nominationNotify;

    @Value("${so.actionType.notify}")
    private String notify;

    @Value("${so.actionStatus.failure}")
    private String failure;

    @Value("${so.triggerType.nominationStatusNotify}")
    private String nominationStatusNotify;

    @Autowired
    RedisConnectionPool redisConnectionPool;

    @Value("${fulfillment.m2m.products}")
    private String m2mProducts;
    
    @Value("${so.triggerType.fulfill}")
    private String fulfillActionType;
    
    List<String> m2mProductList;

    /**
     * Called when Bean is refreshed by Spring
     */
    @PostConstruct
    public void onPostConstruct() {
        m2mProductList = new ArrayList<>();
        String[] keywords = StringUtils.split(m2mProducts, ',');
        m2mProductList = Arrays.asList(keywords);
    }

  
    @Override
    public void process(Exchange exchange) throws Exception {
        String message = exchange.getIn().getBody(String.class);
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        String paymentType = SOMessageUtils.getHeaderValue(soMessage, "payment_type");
        String status = SOMessageUtils.getHeaderValue(soMessage, "status");
        String productName = SOMessageUtils.getHeaderValue(soMessage, "product_name").trim();
        String nominatorMsisdn = NominationUtils.getNominatorNumber(soMessage);
        String nominationShortCode = NominationUtils.getNominationShortCode(soMessage);
        String nominationCacheKey = CacheKeyUtils.generateNominationCacheKey(nominatorMsisdn, nominationShortCode);
        log.info("Checking whether the nominee is prepaid customer, non active users and M2M user");
        // Case3: If B number postpaid (Sorry, this number cant receive bonus . please send again
        // prepaid number) – payment type field will be referred.
        // 1-PRE 2-POST 3-HYB
        if (m2mProductList.contains(productName)) {
            log.info("Nominee is M2M number with product name {}. Sending nominator SMS and Nominator register action",
                    productName);
            // Case7: If B number is m2m number( Sorry the number cant receive bonus, send prepaid
            // number) – Product name will be referred(Clarification Pending)
            sendNotifcationMessageForInvalid(exchange, soMessage, nominatorMsisdn, m2mNumberTextToBeSend);
            String registerActionMessage = CommonFunctions.reduceNominationCountInRedis(nominationCacheKey,
                    provisionFulfillNotifyNominator, redisConnectionPool, jsonObjectMapper, log);
            exchange.getIn().setBody(registerActionMessage);
        } else if (!paymentType.equals("1")) {
            log.info(
                    "Nominee not a prepaid user with payment type: {}. Sending nominator SMS and Nominator register action",
                    paymentType);
            sendNotifcationMessageForInvalid(exchange, soMessage, nominatorMsisdn, notPrepaidNumberTextToBeSend);
            String registerActionMessage = CommonFunctions.reduceNominationCountInRedis(nominationCacheKey,
                    provisionFulfillNotifyNominator, redisConnectionPool, jsonObjectMapper, log);
            exchange.getIn().setBody(registerActionMessage);
        } else if (!status.equals("1") && !status.equals("3")) {
            log.info(
                    "Nominee not in ACTIVE or GRACE period with status {}. Sending nominator SMS and Nominator register action",
                    status);
            // Case6: If B number is neither in ACTIVE or GRACE status(Sorry the number is inactive
            // status, please send active prepaid number) – status field will be referred
            // 1 - active 3- grace
            sendNotifcationMessageForInvalid(exchange, soMessage, nominatorMsisdn, inactiveNumberTextToBeSend);
            String registerActionMessage = CommonFunctions.reduceNominationCountInRedis(nominationCacheKey,
                    provisionFulfillNotifyNominator, redisConnectionPool, jsonObjectMapper, log);
            exchange.getIn().setBody(registerActionMessage);
        } else {
            exchange.getIn().setHeader("isNomineeValid", true);
            log.info("Nominee is an active prepaid customer. Continue with further provisioning");
        }

    }

    /**
     * 
     * Prepares nomination message in case of invalid user
     * 
     * @param exchange exchange
     * @param soMessage soMessage
     * @param nominatorMsisdn nominatorMsisdn
     * @param textToBeSend textToBeSend
     * @throws JsonProcessingException JsonProcessingException
     * @throws IOException IOException
     */
    private void sendNotifcationMessageForInvalid(Exchange exchange, SOMessage soMessage, String nominatorMsisdn,
            String textToBeSend) throws JsonProcessingException, IOException {
        SONotificationMessage nominatorNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
                soMessageTemplate, nominatorMsisdn, soMessage, textToBeSend, "SMS", jsonObjectMapper);
        nominatorNotificationMessage.setTriggerType(nominationStatusNotify);
        nominatorNotificationMessage.setActionType(notify);
        nominatorNotificationMessage.setActionStatus(failure);
        exchange.getIn().setHeader("isNomineeValid", false);
        exchange.getIn().setHeader("nominatorNotificationMessage",
                jsonObjectMapper.writeValueAsString(nominatorNotificationMessage));
        
        soMessage.setActionType(fulfillActionType);
        exchange.getIn().setHeader("nomineeFulfillmentFailedRA", jsonObjectMapper.writeValueAsString(soMessage));
        exchange.getIn().setHeader("NomineeMSISDN", SOMessageUtils.getMSISDN(soMessage));
    }
}
