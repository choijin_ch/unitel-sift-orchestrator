/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.time.Instant;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.domain.SOReminderMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * 
 * Processes Fulfillment Responses
 * 
 * @author SO Development Team
 */
@Component("FulfillmentResponseProcessor")
public class FulfillmentResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${so.triggerType.fulfill}")
    private String fulfillTriggerType;

    @Value("${so.triggerType.fulfill}")
    private String fulfillActionType;

    @Value("${so.actionStatus.success}")
    private String success;

    @Value("${so.optin.status.key}")
    private String optinStatus;
    
    @Value("${so.actionStatus.failure}")
    private String failure;

    @Value("${so.notification.template}")
    private String soMessageTemplate;

    @Value("${fulfillment.notify.partial.textToBeSend}")
    private String partialFulfillmentTextToBeSend;

    @Value("${so.triggerType.fulfillNotify}")
    private String fulfillNotify;

    @Value("${so.actionType.notify}")
    private String notify;

    @Value("${so.fulfill.actionType.provisionFulfillNotifyNominator}")
    private String provisionFulfillNotifyNominator;

    @Value("${so.triggerType.nominatorFulfillNotify}")
    private String nominatorFulfillNotify;
    
    @Value("${so.triggerType.nominationStatusNotify}")
    private String nominationStatusNotify;
    
    @Value("${so.monitoring.ttl.additionalBufferValue}")
    private int ttlBuffer;
    
    @Value("${so.fulfill.failureText}")
    private String fulfillFailure;    
    
    @Value("${so.fulfill.nominationFailureText}")
    private String nominatorfulfillFailure;
    
    @Value("${so.fulfill.lowBalanceErrorReason}")
    private String lowBalanceErrorReason; 
    
    @Value("${so.fulfill.failureLowBalanceText}")
    private String fulfilFailureLowBalanceText; 

    @Autowired
    RedisConnectionPool redisConnectionPool;

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("Inside fulfillResponseProcessor.process() {}");
        String message = exchange.getIn().getBody(String.class);
        SOFulfillmentMessage soFulfillmentMessage = (SOFulfillmentMessage) RawMessageUtils.processRawMessage(message,
                jsonObjectMapper);
        String lastProvisioningStatus = SOMessageUtils.getHeaderValue(soFulfillmentMessage, "LAST-PROVISIONING-STATUS");
        String msisdn = SOMessageUtils.getMSISDN(soFulfillmentMessage);
        exchange.getIn().setHeader("msisdn", msisdn);
        log.info("LAST-PROVISIONING-STATUS for MSISDN {} is {}", msisdn, lastProvisioningStatus);
        soFulfillmentMessage.setTriggerType(fulfillTriggerType);
        soFulfillmentMessage.setActionType(fulfillActionType);
        soFulfillmentMessage.setActionResponse(
                SOMessageUtils.getHeaderValue(soFulfillmentMessage, "LAST-PROVISIONED-ACTION-RESPONSE"));
        if (lastProvisioningStatus.equalsIgnoreCase("SUCCESS")) {
            soFulfillmentMessage.setActionStatus(success);
            checkSequenceForNextProvisioning(exchange, soFulfillmentMessage);
        } else if (lastProvisioningStatus.equalsIgnoreCase("FAILURE")) {
            handleFulfillmentFailure(exchange, soFulfillmentMessage, msisdn, message);

        }
        exchange.getIn().setHeader("msisdn", SOMessageUtils.getMSISDN(soFulfillmentMessage));
        exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soFulfillmentMessage));
    }

	/**
	 * 
	 * Handles fulfillment failure
	 * 
	 * @param exchange
	 *            exchange
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @param msisdn
	 *            msisdn
	 * @param message
	 *            message
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 * @throws IOException
	 *             IOException
	 */
	private void handleFulfillmentFailure(Exchange exchange, SOFulfillmentMessage soFulfillmentMessage, String msisdn,
			String message) {
		soFulfillmentMessage.setActionStatus(failure);
		int currentSequenceCount = Integer
				.parseInt(SOMessageUtils.getHeaderValue(soFulfillmentMessage, "CURRENT-SEQUENCE-COUNT"));
		try {
			if (currentSequenceCount == 1) {
				prepareNominatorFulfillmentFailureMessage(exchange, soFulfillmentMessage, message);
				soFulfillmentMessage.setActionType(fulfillActionType);
				if (isSoFulfillmentMessageOptin(soFulfillmentMessage)) {
					resetRedisCacheForFulfillmentFailure(exchange, soFulfillmentMessage, msisdn);
				}
				exchange.getIn().setHeader("fulfillmentFailed", "true");
			} else {
				preparePartialFulfillmentNotificationMessage(exchange, msisdn, message,
						SOMessageUtils.getHeaderValue(soFulfillmentMessage, "fulfillActionType"));
				exchange.getIn().setHeader("partialFulfillment", "true");
			}
		} catch (Exception exception) {
			log.warn("Exception occured is {} during handle fulfillment failure", exception);
		}
	}

	/**
	 * ResetRedisCacheForFulfillmentFailure
	 * 
	 * @param exchange
	 *            exchange
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @param msisdn
	 *            msisdn
	 * @throws JsonParseException
	 *             JsonParseException
	 * @throws JsonMappingException
	 *             JsonMappingException
	 * @throws IOException
	 *             IOException
	 */
	private void resetRedisCacheForFulfillmentFailure(Exchange exchange, SOFulfillmentMessage soFulfillmentMessage,
			String msisdn) throws JsonParseException, JsonMappingException, IOException {
		log.debug("resetRedisCacheForFulfillmentFailure:");
		String offerId = SOMessageUtils.getOfferId(soFulfillmentMessage);
		String cacheKey = CacheKeyUtils.generateOptinCacheKeyForProcessingFulfillment(msisdn, offerId);
		String soNotificationMessageAsJson = redisConnectionPool.getObjectFromCache(cacheKey);
		redisConnectionPool.delObjectFromCache(cacheKey);
		SONotificationMessage soNotificationMessage = jsonObjectMapper.readValue(soNotificationMessageAsJson,
				SONotificationMessage.class);
		String optinShortCode = SOMessageUtils.getOptinShortCode(soNotificationMessage);
		String keyWordsFromCachekey = SOMessageUtils.getOptinKeyword(soNotificationMessage);
		String[] keyWord = keyWordsFromCachekey.split(",");
		log.debug("KeyWord length:{}", keyWord.length);
		for (int i = 0; i < keyWord.length; i++) {
			String optinCacheKey = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn, keyWord[i]);
			String contentToModify = redisConnectionPool.getObjectFromCache(optinCacheKey);
			log.debug("contentToModify:{}", contentToModify);
			if (contentToModify != null && contentToModify != "OFFER-TAKEN") {
				updateRedisAndHeaders(optinCacheKey, contentToModify);
			}
		}
		SOFulfillmentMessage newSOFulfillmentMessage = setErrorMessageForFirstOfferFailure(soFulfillmentMessage);
		exchange.getIn().setHeader("notifyFulfillmentFailure",
				jsonObjectMapper.writeValueAsString(newSOFulfillmentMessage));
		log.debug("notifyFulfillmentFailure:{}", jsonObjectMapper.writeValueAsString(newSOFulfillmentMessage));

	}
	
	/**
	 * updateRedisAndHeaders
	 * 
	 * @param optinCacheKey optinCacheKey
	 * @param contentToModify contentToModify
	 * @throws JsonParseException JsonParseException
	 * @throws JsonMappingException JsonMappingException
	 * @throws IOException IOException
	 */
	private void updateRedisAndHeaders(String optinCacheKey, String contentToModify)
			throws JsonParseException, JsonMappingException, IOException {
		SONotificationMessage soNotificationMessageOfCache = jsonObjectMapper.readValue(contentToModify,
				SONotificationMessage.class);
		SOMessageUtils.removeHeaderValue(soNotificationMessageOfCache, optinStatus);
		if (isMessageOptinOfferType(soNotificationMessageOfCache)
				&& SOMessageUtils.getHeaderValue(soNotificationMessageOfCache, "DOUBLECONFIRMATION") != null) {
			SOMessageUtils.removeHeaderValue(soNotificationMessageOfCache, "DOUBLECONFIRMATION");
			SOMessageUtils.removeHeaderValue(soNotificationMessageOfCache, "OptinKeyWord");
			SOMessageUtils.removeHeaderValue(soNotificationMessageOfCache, "optinStatus");
			removeRedisKeyOfMultiLevel(soNotificationMessageOfCache);
		}
		if (StringUtils.isNotBlank(SOMessageUtils.getHeaderValue(soNotificationMessageOfCache, "OPTIN-COUNT"))) {
			int optinCount = Integer
					.parseInt(SOMessageUtils.getHeaderValue(soNotificationMessageOfCache, "OPTIN-COUNT"));
			// SOMessageUtils.setHeaderValue(soFulfillmentMessage,
			// "LAST-PROVISIONING-STATUS", "SUCCESS");
			SOMessageUtils.setHeaderValue(soNotificationMessageOfCache, "OPTIN-COUNT", String.valueOf(optinCount - 1));
			// SOMessageUtils.setHeaderValue(soNotificationMessageOfCache,
			// optinStatus,
			// "OFFER-TAKEN-"+String.valueOf(optinCount-1));
		}
		Long currentTime = Instant.now().toEpochMilli();
		String endDate = soNotificationMessageOfCache.getMessage().getRequesterLocation().getLocations().get(0)
				.getOffers().get(0).getOfferEndDate();
		int optinTTL = CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) + ttlBuffer;
		String updatedRedisValue = jsonObjectMapper.writeValueAsString(soNotificationMessageOfCache);
		redisConnectionPool.putInCache(optinCacheKey, updatedRedisValue, optinTTL);
	}

	/**
	 * setErrorMessageForFirstOfferFailure
	 * 
	 * @param soFulfillmentMessage soFulfillmentMessage
	 * @return soFulfillmentMessage with message
	 * @throws JsonProcessingException JsonProcessingException
	 */
    private SOFulfillmentMessage setErrorMessageForFirstOfferFailure(SOFulfillmentMessage soFulfillmentMessage) throws JsonProcessingException {
    	String errorReason=null;
    	errorReason=SOMessageUtils.getHeaderValue(soFulfillmentMessage, "LAST-PROVISIONED-ACTION-RESPONSE").split(":")[1];
		if(StringUtils.isNotBlank(errorReason)&&errorReason.equalsIgnoreCase(lowBalanceErrorReason)){
			log.debug("low balance ");
			soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0).getFulfillmentMessages().get(0).getMessages().get(0).setTextTobeSent(fulfilFailureLowBalanceText);
		}else{
			log.debug("other than low balance");
			soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0).getFulfillmentMessages().get(0).getMessages().get(0).setTextTobeSent(fulfillFailure);
		}
		return soFulfillmentMessage;
	}

	/**
	 * 
	 * Prepares nominator failure message from dynamic parameters
	 * 
	 * @param exchange
	 *            exchange
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @param soMessageAsJSON
	 *            soMessageAsJSON
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 * @throws IOException
	 *             IOException
	 */
	private void prepareNominatorFulfillmentFailureMessage(Exchange exchange, SOFulfillmentMessage soFulfillmentMessage,
			String soMessageAsJSON) throws JsonProcessingException, IOException {
		String fulfillActionType = SOMessageUtils.getHeaderValue(soFulfillmentMessage, "fulfillActionType");
		if (StringUtils.isNotBlank(fulfillActionType) && fulfillActionType.equals(provisionFulfillNotifyNominator)) {
			FulfillmentProduct nominationProduct = SOMessageUtils
					.getNominationProduct(SOMessageUtils.getFulfillmentProducts(soFulfillmentMessage));
			Map<String, String> nominationDynamicProductsMap = SOMessageUtils
					.getFulfillmentProductDynamicParametersAsMap(nominationProduct);
			String nominatorMSISDN = nominationDynamicProductsMap.get("NOMINATOR_NUMBER");
			// String nominationShortCode =
			// nominationDynamicProductsMap.get("NOMINATION_SHORT_CODE");
			String failureMessage = nominationDynamicProductsMap.get("NOMINATOR_FAILUREMESSAGE");
			if (failureMessage == null) {
				failureMessage = nominatorfulfillFailure;
			}
			createNotificationMessageForNominatorFailure(exchange, nominatorMSISDN, failureMessage, soMessageAsJSON);
			// Register Action to reduce nomination count
			/*
			 * String nominatorRegisterActionMessage =CommonFunctions
			 * NominationUtils.reduceNominationCountInRedis(
			 * CacheKeyUtils.generateNominationCacheKey(nominatorMSISDN,
			 * nominationShortCode), provisionFulfillNotifyNominator,
			 * redisConnectionPool, jsonObjectMapper, log);
			 * exchange.getIn().setHeader("nominatorRegisterAction",
			 * nominatorRegisterActionMessage);
			 */
		}
	}

    /**
     * create notification message for notifying fulfilment failure of nominator
     * 
     * @param exchange exchange
     * @param nominatorMSISDN nominatorMSISDN
     * @param failureMessage failureMessage
     * @param soMessageAsJSON soMessageAsJSON
     * @throws JsonProcessingException JsonProcessingException
     * @throws IOException IOException
     */
	private void createNotificationMessageForNominatorFailure(Exchange exchange, String nominatorMSISDN,
			String failureMessage, String soMessageAsJSON) throws JsonProcessingException, IOException {
		if (StringUtils.isNotBlank(nominatorMSISDN) && StringUtils.isNotBlank(failureMessage)) {
			SONotificationMessage fulfillmentFailedNotificationMessage = SOMessageObjectFactory
					.createSONotificationMessage(soMessageTemplate, nominatorMSISDN, soMessageAsJSON, failureMessage,
							"SMS", jsonObjectMapper);
			fulfillmentFailedNotificationMessage.setTriggerType(nominationStatusNotify);
			fulfillmentFailedNotificationMessage.setActionType(notify);
			fulfillmentFailedNotificationMessage.setActionStatus(failure);
			exchange.getIn().setHeader("nomineeFulfillFailedNominatorNotify",
					jsonObjectMapper.writeValueAsString(fulfillmentFailedNotificationMessage));
		}
	}

	/**
	 * 
	 * Check the provisioning status and decide whether send for next
	 * fulfillment or redirect to other handlers
	 * 
	 * @param exchange
	 *            exchange
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	private void checkSequenceForNextProvisioning(Exchange exchange, SOFulfillmentMessage soFulfillmentMessage) {
		log.info("Checking for next product for fulfillment.");
		int currentSequenceCount = Integer
				.parseInt(SOMessageUtils.getHeaderValue(soFulfillmentMessage, "CURRENT-SEQUENCE-COUNT"));
		String getSubscriberAchieved = SOMessageUtils.getHeaderValue(soFulfillmentMessage, "product_name");
		if (getSubscriberAchieved != null) {
			exchange.getIn().setHeader("GetSubscriberPresent", "True");
		}
		exchange.getIn().setHeader("currentSequenceCount", currentSequenceCount);
		int totalSequenceCount = Integer
				.parseInt(SOMessageUtils.getHeaderValue(soFulfillmentMessage, "TOTAL-SEQUENCE-COUNT"));
		log.info("CURRENT-SEQUENCE-COUNT: {}, TOTAL-SEQUENCE-COUNT: {}", currentSequenceCount, totalSequenceCount);
		exchange.getIn().setHeader("fulfillActionType",
				SOMessageUtils.getHeaderValue(soFulfillmentMessage, "fulfillActionType"));
		if (currentSequenceCount < totalSequenceCount) {
			log.info("Found next fulfillment Product. Updating Message headers");
			String[] sequences = SOMessageUtils.getHeaderValue(soFulfillmentMessage, "COMMA-SEPARATED-SEQUENCES")
					.split(",");
			String currentSequence = sequences[currentSequenceCount];
			SOMessageUtils.setHeaderValue(soFulfillmentMessage, "CURRENT-SEQUENCE", currentSequence);
			SOMessageUtils.setHeaderValue(soFulfillmentMessage, "CURRENT-SEQUENCE-COUNT",
					"" + (++currentSequenceCount));
			log.info("Updated CURRENT-SEQUENCE: {} CURRENT-SEQUENCE-COUNT: {}", currentSequence, currentSequenceCount);
			exchange.getIn().setHeader("hasNextFulfillment", "true");
		} else {
			if (isSoFulfillmentMessageOptin(soFulfillmentMessage)) {
				swapOptinStatusHeaderToOfferTaken(exchange, soFulfillmentMessage);
			}
			// else
			// if(soFulfillmentMessage.getActionType().equals("NOTIFY-NOMINATOR")){
			// exchange.getIn().setHeader("notifyNominator", "true");
			// exchange.getIn().setHeader("targetSystem", "NOMINATION");
			// exchange.getIn().setHeader("hasNextFulfillment", "true");
			// exchange.getIn().setBody(soFulfillmentMessage);
			// return;
			// }
			exchange.getIn().setHeader("hasNextFulfillment", "false");

		}
	}
    
	/**
	 * swapOptinStatusHeaderToOfferTaken
	 * 
	 * @param exchange
	 *            exchange
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @throws JsonParseException
	 *             JsonParseException
	 * @throws JsonMappingException
	 *             JsonMappingException
	 * @throws IOException
	 *             IOException
	 */
	private void swapOptinStatusHeaderToOfferTaken(Exchange exchange, SOFulfillmentMessage soFulfillmentMessage) {
		String msisdn = SOMessageUtils.getMSISDN(soFulfillmentMessage);
		String offerId = SOMessageUtils.getOfferId(soFulfillmentMessage);
		String cacheKeyToProcess = CacheKeyUtils.generateOptinCacheKeyForProcessingFulfillment(msisdn, offerId);
		try {
			String soNotificationMessageAsJson = redisConnectionPool.getObjectFromCache(cacheKeyToProcess);
			redisConnectionPool.delObjectFromCache(cacheKeyToProcess);
			SONotificationMessage soNotificationMessage = jsonObjectMapper.readValue(soNotificationMessageAsJson,
					SONotificationMessage.class);
			String optinShortCode = SOMessageUtils.getOptinShortCode(soNotificationMessage);
			String keyWordsFromCachekey = SOMessageUtils.getOptinKeyword(soNotificationMessage);
			log.debug("KeyWord with , :{}", keyWordsFromCachekey);
			String[] keyWord = keyWordsFromCachekey.split(",");
			log.debug("KeyWord length:{}", keyWord.length);
			String optinCacheKeyFromRedis = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn, keyWord[0]);
			String redisValueForOfferTaken = redisConnectionPool.getObjectFromCache(optinCacheKeyFromRedis);
			SONotificationMessage soNotificationMessageOfOptinRedis = jsonObjectMapper
					.readValue(redisValueForOfferTaken, SONotificationMessage.class);
			int reccuringCountValue = 0;
			int optinCountValue = 0;
			if (StringUtils
					.isNotBlank(SOMessageUtils.getHeaderValue(soNotificationMessageOfOptinRedis, "RECURRING_COUNT"))) {
				reccuringCountValue = Integer
						.parseInt(SOMessageUtils.getHeaderValue(soNotificationMessageOfOptinRedis, "RECURRING_COUNT"));
				optinCountValue = Integer
						.parseInt(SOMessageUtils.getHeaderValue(soNotificationMessageOfOptinRedis, "OPTIN-COUNT"));
			}
			log.debug("reccuringCountValue:{} ,optinCountValue: {} ", reccuringCountValue, optinCountValue);
			redisUpdateForNormalOptin(soNotificationMessage, reccuringCountValue, optinCountValue, soNotificationMessageOfOptinRedis, msisdn, keyWord, optinShortCode);
			redisUpdateForReccuringOptin(soNotificationMessage, reccuringCountValue, optinCountValue, soNotificationMessageOfOptinRedis, msisdn, keyWord, optinShortCode);
			
		} catch (IOException exception) {
			log.debug("Exception occured {} during redis read, key not exist",exception);
		}
	}

	/**
	 * redis update for Reccurring optin
	 * 
	 * @param soNotificationMessage
	 *            soNotificationMessage
	 * @param reccuringCountValue
	 *            reccuringCountValue
	 * @param optinCountValue
	 *            optinCountValue
	 * @param soNotificationMessageOfOptinRedis
	 *            soNotificationMessageOfOptinRedis
	 * @param msisdn
	 *            msisdn
	 * @param keyWord
	 *            keyWord
	 * @param optinShortCode
	 *            optinShortCode
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	private void redisUpdateForReccuringOptin(SONotificationMessage soNotificationMessage, int reccuringCountValue,
			int optinCountValue, SONotificationMessage soNotificationMessageOfOptinRedis, String msisdn,
			String[] keyWord, String optinShortCode) throws JsonProcessingException {
		// offer-taken for reccurrent optin scenario
		if (reccuringCountValue == -1 || reccuringCountValue > optinCountValue) {
			for (int i = 0; i < keyWord.length; i++) {
				String optinCacheKey = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn, keyWord[i]);
				log.debug("in loop reccuringCountValue:{} ,optinCountValue: {} ", reccuringCountValue, optinCountValue);
				SOMessageUtils.setHeaderValue(soNotificationMessageOfOptinRedis, optinStatus,
						"OFFER-TAKEN-" + optinCountValue);
				Long currentTime = Instant.now().toEpochMilli();
				String endDate = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0)
						.getOffers().get(0).getOfferEndDate();
				int optinTTL = CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) + ttlBuffer;
				redisConnectionPool.putInCache(optinCacheKey, getJsonFromSOMessage(soNotificationMessageOfOptinRedis),
						optinTTL);
			}
		}
	}

	/**
	 * update redis for normal optin scenarios
	 * 
	 * @param soNotificationMessage
	 *            soNotificationMessage
	 * @param reccuringCountValue
	 *            reccuringCountValue
	 * @param optinCountValue
	 *            optinCountValue
	 * @param soNotificationMessageOfOptinRedis
	 *            soNotificationMessageOfOptinRedis
	 * @param msisdn
	 *            msisdn
	 * @param keyWord
	 *            keyWord
	 * @param optinShortCode
	 *            optinShortCode
	 */
	private void redisUpdateForNormalOptin(SONotificationMessage soNotificationMessage, int reccuringCountValue,
			int optinCountValue, SONotificationMessage soNotificationMessageOfOptinRedis, String msisdn,
			String[] keyWord, String optinShortCode) {
		// offer-taken for normal optin
		if ((optinCountValue == reccuringCountValue && optinCountValue > 0)
				|| (SOMessageUtils.getHeaderValue(soNotificationMessageOfOptinRedis, "RECURRING_COUNT") == null)) {
			resetRedisKeyWithSubscriberIdtoOffertaken(soNotificationMessageOfOptinRedis);
			for (int i = 0; i < keyWord.length; i++) {
				String optinCacheKey = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn, keyWord[i]);
				log.debug("optinCacheKey[i]:{}", optinCacheKey);
				// SOMessageUtils.setHeaderValue(soNotificationMessageOfOptinRedis,
				// optinStatus, "OFFER-TAKEN");
				String offerTaken = "OFFER-TAKEN";
				Long currentTime = Instant.now().toEpochMilli();
				String endDate = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0)
						.getOffers().get(0).getOfferEndDate();
				int optinTTL = CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) + ttlBuffer;
				redisConnectionPool.putInCache(optinCacheKey, offerTaken, optinTTL);
			}
		}
	}

	/**
     * 
     * Prepare notification message for partial fulfillment for cases other than
     * provisionFulfillNotifyNominator actiontypes
     * 
     * @param exchange exchange
     * @param msisdn msisdn
     * @param soMessageAsJSON soMessageAsJSON
     * @param fulfillActionType fulfillActionType
     * @throws JsonProcessingException exception
     * @throws IOException exception
     */
    private void preparePartialFulfillmentNotificationMessage(Exchange exchange, String msisdn, String soMessageAsJSON,
            String fulfillActionType) throws JsonProcessingException, IOException {
        if (StringUtils.isNotBlank(partialFulfillmentTextToBeSend)
                && !fulfillActionType.equals(provisionFulfillNotifyNominator)) {
            SONotificationMessage partialFulfillmentNotificationMessage = SOMessageObjectFactory
                    .createSONotificationMessage(soMessageTemplate, msisdn, soMessageAsJSON,
                            partialFulfillmentTextToBeSend, "SMS", jsonObjectMapper);
            partialFulfillmentNotificationMessage.setTriggerType(fulfillNotify);
            partialFulfillmentNotificationMessage.setActionType(notify);
            partialFulfillmentNotificationMessage.setActionStatus(success);
            exchange.getIn().setHeader("partialFulfillmentNotifyMessage",
                    jsonObjectMapper.writeValueAsString(partialFulfillmentNotificationMessage));
        }
    }
    
	/**
	 * 
	 * Checks whether the message needs to be cached for OPTIN by checking the
	 * optin keyword/optin shortcode/optin channel not null
	 * 
	 * @param soMessage
	 *            SOFulfullmentMessage
	 * @return true/false
	 */
	private boolean isSoFulfillmentMessageOptin(SOMessage soMessage) {
		SOFulfillmentMessage soFulfillmentMessage = (SOFulfillmentMessage) soMessage;
		if (!soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0).getFulfilments().get(0)
				.getFulfillmentMessages().isEmpty()) {
			String optinShortCode = soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0)
					.getFulfilments().get(0).getFulfillmentMessages().get(0).getOptInShortCode();
			String optinKeyword = soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0)
					.getFulfilments().get(0).getFulfillmentMessages().get(0).getOptInKeyWord();
			String optinChannel = soFulfillmentMessage.getMessage().getRequesterLocation().getLocations().get(0)
					.getFulfilments().get(0).getFulfillmentMessages().get(0).getOptInChannel();
			if (StringUtils.isNotBlank(optinShortCode) && StringUtils.isNotBlank(optinKeyword)
					&& StringUtils.isNotBlank(optinChannel)) {
				return true;
			}

		}
		log.debug("Message does not contain optin information. {}", soMessage);
		return false;
	}
    
    /**
     * check whether the notification message belongs to OptinOffer_Type
     * 
     * @param soMessage soMessage
     * @return true/false
     */
	private boolean isMessageOptinOfferType(SOMessage soMessage) {
		Map<String, String> offerPayload = SOMessageUtils.getOfferPayLoad(soMessage);
		String optinOfferTypeValue = offerPayload.get("OptinOffer_Type");
		if (optinOfferTypeValue != null) {
			return true;
		}
		return false;
	}
    
    
    /**
	 * remove second level redis key
	 * 
	 * @param soNotificationMessageOfCache notificationMessage
	 */
	private void removeRedisKeyOfMultiLevel(SONotificationMessage soNotificationMessageOfCache) {
		String msisdn = SOMessageUtils.getMSISDN(soNotificationMessageOfCache);
		String optinShortcode = SOMessageUtils.getOptinShortCode(soNotificationMessageOfCache);
		String cachekey = CacheKeyUtils.generateMultiLevelOptinCacheKey(optinShortcode, msisdn);
		redisConnectionPool.delObjectFromCache(cachekey);
	}
	
	/**
	 * reset second level redis key to offerTaken
	 * 
	 * @param soNotificationMessageOfOptinRedis
	 *            soNotificationMessageOfOptinRedis
	 */
	private void resetRedisKeyWithSubscriberIdtoOffertaken(SONotificationMessage soNotificationMessageOfOptinRedis) {
		if (isMessageOptinOfferType(soNotificationMessageOfOptinRedis)) {
			String msisdn = SOMessageUtils.getMSISDN(soNotificationMessageOfOptinRedis);
			String optinShortcode = SOMessageUtils.getOptinShortCode(soNotificationMessageOfOptinRedis);
			String cachekey = CacheKeyUtils.generateMultiLevelOptinCacheKey(optinShortcode, msisdn);
			String redisValue = redisConnectionPool.getObjectFromCache(cachekey);
			if (StringUtils.isNotBlank(redisValue)) {
				Long currentTime = Instant.now().toEpochMilli();
				String endDate = soNotificationMessageOfOptinRedis.getMessage().getRequesterLocation().getLocations()
						.get(0).getOffers().get(0).getOfferEndDate();
				int optinTTL = CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) + ttlBuffer;
				redisConnectionPool.putInCache(cachekey, "OFFER-TAKEN", optinTTL);
			}
		}
	}
	
	/**
	 * get json from message
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return Json message
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	private String getJsonFromSOMessage(SOMessage soMessage) throws JsonProcessingException {
		if (soMessage instanceof SOFulfillmentMessage) {
			SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
			return jsonObjectMapper.writeValueAsString(message);
		} else if (soMessage instanceof SONotificationMessage) {
			SONotificationMessage message = (SONotificationMessage) soMessage;
			return jsonObjectMapper.writeValueAsString(message);
		} else if (soMessage instanceof SOReminderMessage) {
			SOReminderMessage message = (SOReminderMessage) soMessage;
			return jsonObjectMapper.writeValueAsString(message);
		}
		log.warn("returning null because unknown Message type encountered {}", soMessage);
		return null;
	}

}
