/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * 
 * Processes Fulfillment Notification Message for Nominator
 * 
 * @author SO Development Team
 */
@Component("NominatorMessageProcessor")
public class NominatorMessageProcessor implements Processor {

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${so.notification.template}")
    private String soMessageTemplate;

    @Value("${so.triggerType.nominatorFulfillNotify}")
    private String nominatorFulfillNotify;

    @Value("${so.actionType.notify}")
    private String notify;

    @Value("${so.actionStatus.success}")
    private String success;

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void process(Exchange exchange) throws Exception {
        String message = exchange.getIn().getBody(String.class);
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        SOFulfillmentMessage soFulfillmentMessage = (SOFulfillmentMessage) soMessage;
        List<FulfillmentProduct> fulfillmentProducts = SOMessageUtils.getFulfillmentProducts(soFulfillmentMessage);
        FulfillmentProduct nominationFulfillmentProduct = SOMessageUtils.getNominationProduct(fulfillmentProducts);
        Map<String, String> dynamicParameterMap = SOMessageUtils
                .getFulfillmentProductDynamicParametersAsMap(nominationFulfillmentProduct);
        String msisdn = dynamicParameterMap.get("NOMINATOR_NUMBER");
        String messageTobeSent = dynamicParameterMap.get("NOMINATOR_SUCCESSMESSAGE");
        log.info("Nominee fulfillment success. Preparing SMS for nominator {} with content '{}' ", msisdn,
                messageTobeSent);
        SONotificationMessage fulfillmentSuccessNotificationMessage = SOMessageObjectFactory
                .createSONotificationMessage(soMessageTemplate, msisdn, soFulfillmentMessage,
                        messageTobeSent, "SMS", jsonObjectMapper);
        fulfillmentSuccessNotificationMessage.setTriggerType(nominatorFulfillNotify);
        fulfillmentSuccessNotificationMessage.setActionType(notify);
        fulfillmentSuccessNotificationMessage.setActionStatus(success);
        exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(fulfillmentSuccessNotificationMessage));
    }

}
