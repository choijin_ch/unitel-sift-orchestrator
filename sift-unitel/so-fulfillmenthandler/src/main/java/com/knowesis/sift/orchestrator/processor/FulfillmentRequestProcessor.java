/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.commons.CommonFunctions;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * 
 * Processes Fulfillment Requests
 * 
 * @author SO Development Team
 */
@Component("FulfillmentRequestProcessor")
public class FulfillmentRequestProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${so.fulfill.actionType.provisionFulfillNotifyNominator}")
    private String provisionFulfillNotifyNominator;

    @Value("${fulfillment.notify.nomineeNotUnitel.textToBeSend}")
    private String nomineeNotUnitel;

    @Value("${so.notification.template}")
    private String soMessageTemplate;

    @Value("${so.triggerType.nominationNotify}")
    private String nominationNotify;

    @Value("${so.actionType.notify}")
    private String notify;

    @Value("${so.actionStatus.success}")
    private String success;

    @Value("${so.actionStatus.failure}")   
    private String failure;

    @Autowired
    RedisConnectionPool redisConnectionPool;
    
    @Value("${so.triggerType.fulfill}")
    private String fulfill;
    
    @Value("${so.triggerType.fulfillNotify}")
    private String fulfillNotify;

    @Value("${so.triggerType.nominationStatusNotify}")
    private String nominationStatusNotify;

    @Value("${so.test.enableWhiteListing}")
    private Boolean enableWhiteListing;

    @Value("${so.test.whiteListedMSISDNs}")
    private String whiteListedMSISDNs;

    List<String> whiteListedMSISDNList;

    /**
     * Called when Bean is refreshed by Spring
     */
    @PostConstruct
    public void onPostConstruct() {
        whiteListedMSISDNList = new ArrayList<>();
        String[] keywords = StringUtils.split(whiteListedMSISDNs, ',');
        whiteListedMSISDNList = Arrays.asList(keywords);
    }

    @Override
	public void process(Exchange exchange) throws Exception {
		log.debug("Inside fulfillProcessor.process() {}");
		String message = exchange.getIn().getBody(String.class);
		SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
		SOMessageUtils.removeHeaderValue(soMessage, "PROVISIONED-OFFERID");
		SOMessageUtils.removeHeaderValue(soMessage, "PROVISIONED-VALUE");
		String msisdn = SOMessageUtils.getMSISDN(soMessage);
		boolean isWhiteListedMsisdnOrProduction = SOMessageUtils.checkMsisdnWhiteListCondition(msisdn,
				enableWhiteListing, whiteListedMSISDNList);
		log.info("Is white listed MSISDN or Production: {}", isWhiteListedMsisdnOrProduction);
		if (!isWhiteListedMsisdnOrProduction) {
			exchange.getIn().setHeader("msisdn", msisdn);
			exchange.getIn().setHeader("notWhiteListedMsisdnInTest", true);
			return;
		}
		String fulfillActionType = SOMessageUtils.getHeaderValue(soMessage, "fulfillActionType");
		if (StringUtils.isBlank(fulfillActionType)) {
			fulfillActionType = setActionTypeFromOfferPayload(exchange, soMessage);
			if(routeAccordingToActionType(exchange, soMessage, fulfillActionType))
				return;
			declareMessageHeadersForFulfillment(exchange, soMessage);
			initializeMessageHeadersForFulfillment(exchange, soMessage);
		} else {
			SOMessageUtils.setHeaderValue(soMessage, "fulfillActionType", fulfillActionType);
		}
		setTargetSystemHeader(exchange, fulfillActionType, soMessage);
		exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
	}

	/**
	 * route from fulfillmenthandler according to actionType
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 * @param fulfillActionType
	 *            fulfillActionType
	 * @return true/False
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 * @throws IOException
	 *             IOException
	 */
    //complexity is too high need to handle
	private boolean routeAccordingToActionType(Exchange exchange, SOMessage soMessage, String fulfillActionType)
			throws JsonProcessingException, IOException {
		// Nominee fulfillment related check
		if (fulfillActionType.equals(provisionFulfillNotifyNominator)
				&& SOMessageUtils.getSubscriptionId(soMessage).equals("0")) {
			prepareNominatorMessageAndReturn(exchange, soMessage);
			soMessage.setActionType(fulfill);
			exchange.getIn().setHeader("nomineeFulfillmentFailedRA", jsonObjectMapper.writeValueAsString(soMessage));
			return true;
		} else if (fulfillActionType.equalsIgnoreCase("FULFILLNOTIFY")
				|| fulfillActionType.equalsIgnoreCase("FULFILL-NOTIFY")) {
			soMessage.setActionStatus("success");
			soMessage.setTriggerType(fulfillNotify);
			soMessage.setActionType(notify);
			exchange.getIn().setHeader("targetSystem", "FULFILL-NOTIFY");
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
			return true;
		} else if (fulfillActionType.equalsIgnoreCase("NOMINATION_REQUEST")) {
			exchange.getIn().setHeader("NominationRequest", "NOMINATION_REQUEST");
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
			return true;
		} else if (fulfillActionType.equalsIgnoreCase("NOTIFY_NOMINATOR")) {
			exchange.getIn().setHeader("NotifyNominator", "True");
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
			return true;
		} else if (fulfillActionType.equalsIgnoreCase("NOMINATION")) {
			exchange.getIn().setHeader("targetSystem", "NOMINATION");
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
			return true;
		}
		return false;
	}

	/**
	 * 
	 * In Case of PROVISION-FULFILLNOTIFY-NOMINATOR if subscriber ID is 0 the
	 * send message to Nominator to send another message
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 * @throws IOException
	 *             IOException
	 */
    private void prepareNominatorMessageAndReturn(Exchange exchange, SOMessage soMessage)
            throws JsonProcessingException, IOException {
        SOFulfillmentMessage nomineeFulfillmentMessage = (SOFulfillmentMessage) soMessage;
        List<FulfillmentProduct> fulfillmentProducts = SOMessageUtils.getFulfillmentProducts(nomineeFulfillmentMessage);
        FulfillmentProduct nomineeFulfillmentProduct = SOMessageUtils.getNominationProduct(fulfillmentProducts);
        Map<String, String> dynamicParameterMap = SOMessageUtils
                .getFulfillmentProductDynamicParametersAsMap(nomineeFulfillmentProduct);
        String msisdn = dynamicParameterMap.get("NOMINATOR_NUMBER");
        log.info(
                "Nominee subscription id is 0. Sending nominator({}) register action and nominee({}) fulfillment failed register action",
                msisdn, SOMessageUtils.getMSISDN(soMessage));
        String nominationShortCode = dynamicParameterMap.get("NOMINATION_SHORT_CODE");
        String nominatorCacheKey = CacheKeyUtils.generateNominationCacheKey(msisdn, nominationShortCode);

        String nominatorRedisMessage = CommonFunctions.reduceNominationCountInRedis(nominatorCacheKey,
                provisionFulfillNotifyNominator, redisConnectionPool, jsonObjectMapper, log);

        SONotificationMessage soNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
                soMessageTemplate, msisdn, soMessage, nomineeNotUnitel, "SMS", jsonObjectMapper);

        soNotificationMessage.setTriggerType(nominationStatusNotify);
        soNotificationMessage.setActionType(notify);
        soNotificationMessage.setActionStatus(failure);
        exchange.getIn().setHeader("nominatorNotificationMessage",
                jsonObjectMapper.writeValueAsString(soNotificationMessage));

        exchange.getIn().setHeader("nominatorMsisdn", msisdn);
        exchange.getIn().setHeader("subsciberNotFound", true);
        exchange.getIn().setBody(nominatorRedisMessage);
    }

	/**
	 * 
	 * Setting fulfillActionType soMessage header from SOMessage offer payload
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 * @return fulfillActionType
	 */
    private String setActionTypeFromOfferPayload(Exchange exchange, SOMessage soMessage) {
        String fulfillActionType = SOMessageUtils.getOfferPayLoad(soMessage).get("Action_Type");
        log.debug("ActionType received from offerpayload: {}", fulfillActionType);
        if (StringUtils.isNotBlank(fulfillActionType))
            SOMessageUtils.setHeaderValue(soMessage, "fulfillActionType", fulfillActionType);
        else
            log.info("Action_Type OFFER_PAYLOAD is empty. Please change the configuration");
        return fulfillActionType;
    }

	/**
	 * 
	 * Setting CURRENT-SEQUENCE, TOTAL-SEQUENCE-COUNT, CURRENT-SEQUENCE-COUNT
	 * message headers for tracking the progress of fulfillments
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 */
    private void declareMessageHeadersForFulfillment(Exchange exchange, SOMessage soMessage) {
        if (soMessage instanceof SOFulfillmentMessage) {
            SOFulfillmentMessage soFulfillmentMessage = (SOFulfillmentMessage) soMessage;
            List<FulfillmentProduct> fulfillmentProducts = SOMessageUtils.getFulfillmentProducts(soFulfillmentMessage);
            if (fulfillmentProducts != null && fulfillmentProducts.size() > 0) {
                String commaSeparatedSequences = setStatusHeadersForFulfillment(exchange, soFulfillmentMessage);
                SOMessageUtils.setHeaderValue(soFulfillmentMessage, "COMMA-SEPARATED-SEQUENCES", // 6,8,10
                        commaSeparatedSequences);
                SOMessageUtils.setHeaderValue(soFulfillmentMessage, "CURRENT-SEQUENCE", "0");// 6,8,10
                SOMessageUtils.setHeaderValue(soFulfillmentMessage, "TOTAL-SEQUENCE-COUNT", // 3
                        "" + commaSeparatedSequences.split(",").length);
                SOMessageUtils.setHeaderValue(soFulfillmentMessage, "CURRENT-SEQUENCE-COUNT", "0");// 1,2,3
                SOMessageUtils.setHeaderValue(soFulfillmentMessage, "LAST-PROVISIONING-STATUS", "SUCCESS");
            }
        } else {
            log.info("Message is not Fulfilment Message. Not sending for provisioning");
        }
    }

	/**
	 * 
	 * 1. Define fulfillment flows 2. Check nomination status 3. Define SEQUENCE
	 * of exe 4. Control entry and exit conditions of multiple fulfillment
	 * 
	 * @param exchange
	 *            exchange
	 * @param soFulfillmentMessage
	 *            soFulfillmentMessage
	 * @return comma separated sorted sequences
	 */
    private String setStatusHeadersForFulfillment(Exchange exchange, SOFulfillmentMessage soFulfillmentMessage) {
        log.info("Processing fulfillment for MSISDN: {}", SOMessageUtils.getMSISDN(soFulfillmentMessage));
        List<FulfillmentProduct> fulfillmentProducts = SOMessageUtils.getFulfillmentProducts(soFulfillmentMessage);
        List<String> sequences = null;
        if (fulfillmentProducts != null && fulfillmentProducts.size() > 0) { // Product Level
            sequences = new ArrayList<String>(
                    SOMessageUtils.getFulfillmentProductBySequenceMap(fulfillmentProducts).keySet());
            Collections.sort(sequences, new Comparator<String>() {
                @Override
                public int compare(String first, String second) {
                    Integer firstNumber = Integer.parseInt(first);
                    Integer secondNumber = Integer.parseInt(second);
                    return firstNumber.compareTo(secondNumber);
                }
            });
            for (String sequence : sequences) {
                log.info(
                        "Setting the message header to track the provisining progress of fulfillMessage with Sequence: {}",
                        sequence);
                SOMessageUtils.setHeaderValue(soFulfillmentMessage, getSequenceStatusHeaderKey(sequence), "queued");
            }
            String commaSeparatedOrder = String.join(",", sequences);
            log.debug("comma separated sequence: {}", commaSeparatedOrder);
            return commaSeparatedOrder;
        }
        return null;

    }

	/**
	 * 
	 * GET SEQUENCE status header key
	 * 
	 * @param sequence
	 *            sequenceNumber
	 * @return Key for header
	 */
    private String getSequenceStatusHeaderKey(String sequence) {
        return "SEQUENCE-" + sequence + "-STATUS";
    }

	/**
	 * 
	 * Initialises provisioning related message headers for the first time
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 */
    private void initializeMessageHeadersForFulfillment(Exchange exchange, SOMessage soMessage) {
        log.info("Initializing CURRENT-SEQUENCE and CURRENT-SEQUENCE-COUNT for the first time.");
        int currentSequenceCount = Integer.parseInt(SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE-COUNT"));
        String[] sequences = SOMessageUtils.getHeaderValue(soMessage, "COMMA-SEPARATED-SEQUENCES").split(",");
        String currentSequence = sequences[currentSequenceCount];
        SOMessageUtils.setHeaderValue(soMessage, "CURRENT-SEQUENCE", currentSequence);
        SOMessageUtils.setHeaderValue(soMessage, "CURRENT-SEQUENCE-COUNT", "" + (++currentSequenceCount));
        SOMessageUtils.setHeaderValue(soMessage, "OCS-DESCRIPTION", getOcsDescription(soMessage));
        log.info("CURRENT-SEQUENCE: {} and CURRENT-SEQUENCE-COUNT: {}", currentSequence, currentSequenceCount);
    }

	/**
	 * 
	 * Creates description String that has to be send to OCS API's
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return OCS-DESCRIPTION String
	 */
    private String getOcsDescription(SOMessage soMessage) {
        String subscriptionId = SOMessageUtils.getSubscriptionId(soMessage);
        String unixTimeStamp = SOMessageUtils.getUnixTimestamp();
        String ocsDescription = "CMS_"+SOMessageUtils.getProgramId(soMessage)+"_" + subscriptionId.substring(subscriptionId.length() - 6)
                + unixTimeStamp.substring(unixTimeStamp.length() - 6);
        return ocsDescription;
    }

	/**
	 * 
	 * Setting the targetSystem in the message header and exchange header for
	 * redirection
	 * 
	 * @param exchange
	 *            exchange
	 * @param fulfillActionType
	 *            fulfillmentAction
	 * @param soMessage
	 *            soMessage
	 */
    private void setTargetSystemHeader(Exchange exchange, String fulfillActionType, SOMessage soMessage) {
        log.debug("fulfill action type: {}", fulfillActionType);
        if (fulfillActionType.equals("EXTENDEXPIRY-NOTIFY")) {
            log.info("Subscriber extend expiry date for MSISDN: {} with fulfillmentAction: {}",
                    SOMessageUtils.getMSISDN(soMessage), SOMessageUtils.getHeaderValue(soMessage, "fulfillActionType"));
            exchange.getIn().setHeader("targetSystem", SOMessageUtils.getHeaderValue(soMessage, "targetSystem"));
        } else if (fulfillActionType.equals("NOMINATION")) {
            exchange.getIn().setHeader("targetSystem", "NOMINATION");
        } else if (fulfillActionType.equals("FULFILL-NOTIFY")||fulfillActionType.equals("FULFILLNOTIFY")) {
            exchange.getIn().setHeader("targetSystem", "FULFILL-NOTIFY");
        } else { // PROVISION-FULFILLNOTIFY, PROVISION-FULFILLNOTIFY-NOMINATION,
                 // PROVISION-FULFILLNOTIFY-NOMINATOR
            setTargetSystemForFulfillment(exchange, soMessage);
        }

    }

	/**
	 * 
	 * Set targetSystem MessageHeader and exchangeHeader for redirection
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 */
    private void setTargetSystemForFulfillment(Exchange exchange, SOMessage soMessage) {
        String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
        List<FulfillmentProduct> fulfillmentProducts = SOMessageUtils
                .getFulfillmentProducts((SOFulfillmentMessage) soMessage);
        Map<String, FulfillmentProduct> fulfillmentProductSequenceMap = SOMessageUtils
                .getFulfillmentProductBySequenceMap(fulfillmentProducts);
        FulfillmentProduct fulfillmentProduct = fulfillmentProductSequenceMap.get(currentSequence);
        setTargetSystemExchangeHeader(exchange, fulfillmentProduct.getTargetSystem());
    }

	/**
	 * 
	 * Set targetSystem exchange header based on the fulfillmentProduct
	 * targetSystem
	 * 
	 * @param exchange
	 *            exchange
	 * @param targetSystem
	 *            targetSystem
	 */
    private void setTargetSystemExchangeHeader(Exchange exchange, String targetSystem) {
        if (targetSystem.startsWith("OCS")) {
            log.info("Setting target system as OCS");
            exchange.getIn().setHeader("targetSystem", "OCS");
        } else if (targetSystem.startsWith("CS")) {
            log.info("Setting target system as CS");
            exchange.getIn().setHeader("targetSystem", "CS");
        }else if (targetSystem.startsWith("CBS")) {
            log.info("Setting target system as CBS");
            exchange.getIn().setHeader("targetSystem", "CBS");
        }else if (targetSystem.startsWith("UO")) {
            log.info("Setting target system as UO");
            exchange.getIn().setHeader("targetSystem", "UO");
        } else {
            log.info("Target system is neither OCS, CBS, UO(MOVIE) nor CS. Setting target system as NOTSUPPORTED");
            exchange.getIn().setHeader("targetSystem", "NOTSUPPORTED");
        }
    }

}
