#! /bin/bash

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#title           :so-qpurge-handler.sh
#description     :This script manages so-qpurge-handler Process.
#author          :SIFT SO Team
#version         :0.0.1
#usage           :bash so-qpurge-handler.sh
#handler_version :0.0.1-release
#
#
# *** There could be multiple copies of this handler for each Q to purge in an automated way
# ** If manually purged, then the handler conf should be modifed with the name of the Q and start the handler
# ** Stop the handler after all the messages are purged. Remove the name of the Q from properties to avoid accidential
# ** purge of queue.
# ** ALWAYS set a unique name to the PNAME below to distinguis the process during start and stop
# ** ALWAYS change the Q Name in the properties file
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



source ./so-env.sh

: "${JAVA_HOME?JAVA_HOME not set}"
: "${SO_HOME?SO_HOME not set}"

# ***********************************************
export SCRIPT_HOME=$(dirname `pwd`)
export PURGE_HOME="$(dirname "$SCRIPT_HOME")"

# This is the unique process name to identify this purge handler. You may copy the purge handlers to purge multiple queues
# But each handler should have a unique PNAME to identify in the OS Process. For starting and stopping
PNAME="QPURGE"

echo "${handler-name}-${project.version}"
export API_CONF=$SCRIPT_HOME'/conf'
export LOG_HOME=$SCRIPT_HOME'/log'
export LOGBACK_XML=$API_CONF'/so-qpurge-handler-logback.xml'
export FLOW_LOC=$SCRIPT_HOME'/flow'
export CAMEL_ENCRYPTION_PASSWORD=secret

# OS specific support.
cygwin=false
case "`uname`" in
   CYGWIN*) cygwin=true ;;
esac

if $cygwin ; then
    echo 'cygwin shell detected'
    API_CONF=`cygpath -w "$API_CONF"`
    LOG_HOME=`cygpath -w "$LOG_HOME"`
    LOGBACK_XML=`cygpath -w "$LOGBACK_XML"`
    FLOW_LOC=`cygpath -w "$FLOW_LOC"`
fi

CP=$(echo $SO_HOME/lib/*.jar | tr ' ' ':')
export CLASSPATH=$API_CONF':'$CP':'$FLOW_LOC

ARGS='-DNAME='$PNAME' -Dlogback.configurationFile='$LOGBACK_XML' -DCONFIG_HOME='$API_CONF' -DLOG_HOME='$LOG_HOME' -jar '$FLOW_LOC'/${project.artifactId}-${project.version}.jar'
DAEMON=$JAVA_HOME/bin/java

case "$1" in
start)
    (
		pid=`pgrep -f $PNAME`
		if [ ! -z $pid ]; then 
			echo "process found with pid "$pid
			echo "use $0 stop"
		else 
			echo 'Starting...'
    		$DAEMON $ARGS > $LOG_HOME/${handler-name}_sysout.log 2>&1
			echo $!
		fi
	) & 
;;

status)
	pid=`pgrep -f $PNAME`
	if [ ! -z $pid ]; then 
		echo "process found with pid "$pid
	else 
		echo "process not found"
	fi
;;

stop)
	pid=`pgrep -f $PNAME`
	if [ ! -z $pid ]; then 
		echo "stopping ..."$pid
		pkill -f $PNAME
	else 
		echo "process not found"
	fi
;;

kill)
	pid=`pgrep -f $PNAME`
	if [ ! -z $pid ]; then 
		echo "killing ..."$pid
		pkill -9 -f $PNAME
	else 
		echo "process not found"
	fi
;;

log)
	tail -f $LOG_HOME/${handler-name}.log
;;

restart)
    $0 stop
    $0 start
;;

*)
    echo "Usage: $0 {status|start|stop}"
    exit 1
esac
unset CAMEL_ENCRYPTION_PASSWORD
