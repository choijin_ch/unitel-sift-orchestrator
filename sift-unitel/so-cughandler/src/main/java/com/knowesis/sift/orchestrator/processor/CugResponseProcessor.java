/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;

/**
 * Processes CUG response
 * 
 * @author SO Development Team
 */
@Component("CugResponseProcessor")
public class CugResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${so.actionType.notify}")
    private String notify;

    @Value("${so.cug.triggerType.cugmonitor}")
    private String cugMonitor;

    @Value("${so.actionStatus.success}")
    private String success;

    @Value("${so.actionStatus.failure}")
    private String failure;

    @Override
    public void process(Exchange exchange) throws Exception {
        String cugResponse = exchange.getIn().getBody(String.class);
        log.info("CUG add ticket response: {}", cugResponse);
        JsonNode responseNode = jsonObjectMapper.readTree(cugResponse);
        //String statusCode = responseNode.path("statusCode").asText();
        String statusMessage = responseNode.path("statusMessage").asText();
        String message = (String) exchange.getProperty("OringinalReqeustMessage");
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        soMessage.setTriggerType(cugMonitor);
        soMessage.setActionType(notify);

        if (statusMessage.equalsIgnoreCase("SUCCESS")) {
            exchange.getIn().setHeader("cugApiStatus", "SUCCESS");
            soMessage.setActionStatus(success);
        } else {
            exchange.getIn().setHeader("cugApiStatus", "FAILURE");
            soMessage.setActionStatus(failure);
        }
        exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
    }

}
