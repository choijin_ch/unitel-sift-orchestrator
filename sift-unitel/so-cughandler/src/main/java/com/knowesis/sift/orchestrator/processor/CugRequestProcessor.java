/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepared CUG reques
 *
 * @author SO Development Team
 */
@Component("CugRequestProcessor")
public class CugRequestProcessor implements Processor {

    private Logger log = LoggerFactory.getLogger(CugRequestProcessor.class);

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${cug.msisdn.countryCode.enable}")
    private String countryCode;

    private String addticket = "addticket";
    private String addbalance = "addbalance";
    private String deductbalance = "deductbalance";

    @Override
    public void process(Exchange exchange) throws Exception {
        String message = exchange.getIn().getBody(String.class);
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        String admin = SOMessageUtils.getMSISDN(soMessage);
        String targetSystemValue = null;

        SOFulfillmentMessage soFulfillmentMessage = (SOFulfillmentMessage) soMessage;
        String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
        FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct(soFulfillmentMessage,
                currentSequence);
        String targetSystem = fulfillmentProduct.getTargetSystem();
        log.info("Target System found for the CURRENT-SEQUENCE fulfillment product: {}", targetSystem);
        targetSystemValue = targetSystem.split("_")[1];

        log.debug("targetSystemValue: {}", targetSystemValue);
        String cugRequest = "";
        if ("AddTicket".equalsIgnoreCase(targetSystemValue)) {
            if (countryCode.equalsIgnoreCase("true")) {
                admin = admin.substring(3);
            }
            Map<String, String> offerPayload = SOMessageUtils.getOfferPayLoad(soMessage);
            String addLimit = offerPayload.get("Add_Limit");
            String groupType = offerPayload.get("GroupType");
            String templateId = offerPayload.get("TemplateID");
            long offerStartDate = Long.parseLong(SOMessageUtils.getOfferStartDate(soMessage));
            long offerEndDate = Long.parseLong(SOMessageUtils.getOfferEndDate(soMessage));
            long monitorDay = (offerEndDate - offerStartDate) / 86400000; //1000 * 60 * 60 * 24 - millisecond of a day
            Date date = new Date(Long.parseLong(SOMessageUtils.getOfferEndDate(soMessage)));
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            long expireDay = Long.parseLong(format.format(date));
            String maxMemberLimit = offerPayload.get("MaxMemberLimit");
            String deleteLimit = offerPayload.get("DeleteLimit");
            exchange.getIn().setHeader("urlSuffix", addticket);
            cugRequest = createCugAddTicketApiRequest(admin, addLimit, groupType, templateId, monitorDay, expireDay, maxMemberLimit, deleteLimit);
            log.info("CUG add ticket request: {}", cugRequest);
        }else if("AddBalance".equalsIgnoreCase(targetSystemValue)){
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            String amount = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "AMOUNT");
            String descr = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "DESCRIPTION");
            exchange.getIn().setHeader("urlSuffix", addbalance);
            cugRequest = createCugAddorDeductBalanceRequest( msisdn  , amount, "SIFT-" + descr);

        }else if("DeductBalance".equalsIgnoreCase(targetSystemValue)){
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            String amount = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "AMOUNT");
            String descr = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "DESCRIPTION");
            exchange.getIn().setHeader("urlSuffix", deductbalance);
            cugRequest = createCugAddorDeductBalanceRequest( msisdn  , amount, "SIFT-" + descr);
        }
        exchange.setProperty("OringinalReqeustMessage", message);
        exchange.getIn().setBody(cugRequest);
    }



    /**
     * Create Json request for CUG add ticket
     *
     * @param msisdn          As in MSISDN in the offer trigger
     * @param amount
     * @param descr
     * @return request JSON
     * @throws JsonProcessingException exception
     */
    private String createCugAddorDeductBalanceRequest(String msisdn, String amount, String descr) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();
        node.put("msisdn", msisdn);
        node.put("amount", amount);
        node.put("descr", descr);

        return jsonObjectMapper.writeValueAsString(node);
    }

    /**
     * Create Json request for CUG add ticket
     *
     * @param admin          As in MSISDN in the offer trigger
     * @param addLimit       Offer payload Add_Limit
     * @param groupType      Offer payload : GroupType
     * @param templateId     Offer payload : TemplateID
     * @param monitorDay     Days between offer start date and offer end date from trigger
     * @param expireDay      Offer end date as in offer trigger
     * @param deleteLimit
     * @param maxMemberLimit
     * @return request JSON
     * @throws JsonProcessingException exception
     */
    private String createCugAddTicketApiRequest(String admin, String addLimit, String groupType, String templateId,
                                                long monitorDay, long expireDay, String maxMemberLimit, String deleteLimit) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();
        node.put("admin", admin);
        node.put("addLimit", addLimit);
        node.put("groupType", groupType);
        node.put("templateId", templateId);
        node.put("monitorDay", monitorDay);
        node.put("expireDate", expireDay);
        node.put("deleteLimit", deleteLimit);
        node.put("maxMemberLimit", maxMemberLimit);
        return jsonObjectMapper.writeValueAsString(node);
    }
}
