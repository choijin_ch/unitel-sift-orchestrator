/**
 * 
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("MovieRequestProcessor")
public class MovieRequestProcessor implements Processor {

    private Logger log = LoggerFactory.getLogger(MovieRequestProcessor.class);

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
        if (soMessage != null) {
            String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
            FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                    currentSequence);
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            String subscriptionId = SOMessageUtils.getSubscriptionId(soMessage);
            // String purchaseGroupId =
            // ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct,
            // "purchaseGroupId");
            String category = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "category");
            String days = "30";
            if (StringUtils.isNotEmpty(ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "days"))) {
                days = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "days");
            }
            String autoCharge = "0";
            if (StringUtils.isNotEmpty(ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "autoCharge"))) {
                autoCharge = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "autoCharge");
            }
            String paymentType = SOMessageUtils.getOfferId(soMessage);

            // String
            // requestParameters="subscriberId="+subscriptionId+"&purchaseGroupId="+purchaseGroupId+"&paymentType="+paymentType;
            String requestParameters = "subscriberId=" + subscriptionId + "&category=" + category + "&days=" + days
                    + "&paymentType=" + paymentType + "&autoCharge=" + autoCharge;
            log.debug("parameters to pass:{} for msisdn:{}", requestParameters, msisdn);
            exchange.getIn().setHeader("getMovieRequest", requestParameters);
            exchange.getIn().setBody(null);
        }
    }

}
