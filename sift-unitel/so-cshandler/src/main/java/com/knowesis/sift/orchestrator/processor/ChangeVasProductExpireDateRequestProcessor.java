/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.CSUtils;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares status date update API request
 * 
 * @author SO Development Team
 */
@Component("ChangeVasProductExpireDateRequestProcessor")
public class ChangeVasProductExpireDateRequestProcessor implements Processor {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Value("${cs.change.userId}")
	private String userId;

	@Value("${cs.changevasapi.eventcd}")
	private String eventcd;

	@Value("${cs.changevasapi.chngdtcd}")
	private String chngdtcd;

	@Value("${cs.changevasapi.eventdtcd}")
	private String eventdtcd;

	@Value("${cs.changevasapi.prodDvCd}")
	private String prodDvCd;

	// String pymtRecpNo=null;//this field will be sent with request as null
	// always.
	@Autowired
	ObjectMapper jsonObjectMapper;

	@Override
	public void process(Exchange exchange) throws Exception {
		SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
		if (soMessage != null) {
			String msisdn = SOMessageUtils.getMSISDN(soMessage);
			exchange.getIn().setHeader("msisdn", msisdn);
			log.info("Subscriber status for msisdn {} is expired. Checking for statusChangeProductStatus parameter",
					msisdn);
			String prodCd = CSUtils.getproductCode(soMessage);//
			String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
			FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                    currentSequence);
			String rewardValidityHours = ProvisionUtils.getRewardValidityHourFromDynamicParameter(fulfillmentProduct);
			String rewardValidityDays = ProvisionUtils.getRewardValidityDayFromDynamicParameter(fulfillmentProduct);
			String svcEndDttm = calculateAndGetExpiryDate(rewardValidityHours, rewardValidityDays);// to get current time
			int svcCd = CSUtils.getProductDomain(soMessage); // setting
																// according to
																// product code
			String entrNo = SOMessageUtils.getSubscriptionId(soMessage);// to get from
																// offer
			exchange.getIn().setBody(createMessageBodyForTerminateVASProductMessage(prodCd, svcEndDttm, svcCd, entrNo, msisdn));
			String requestBody = createMessageBodyForTerminateVASProductMessage(prodCd, svcEndDttm, svcCd, entrNo, msisdn);
			log.info("JSON request object for status date update API call: {}", requestBody);
			exchange.getIn().setBody(requestBody);

		}

	}

	/**
	 * Creates JSON message to send to the API for changing product status
	 * 
	 * @param prodCd
	 *            product code
	 * @param svcEndDttm
	 *            service end datetime
	 * @param svcCd
	 *            service code
	 * @param entrNo
	 *            entrance number
	 * @param msisdn msisdn
	 * @return json node
	 * @throws JsonProcessingException
	 *             exception
	 */
	private String createMessageBodyForTerminateVASProductMessage(String prodCd, String svcEndDttm, int svcCd,
			String entrNo, String msisdn) throws JsonProcessingException {
		ObjectNode node = jsonObjectMapper.createObjectNode();
		ObjectNode operatorUpdate = jsonObjectMapper.createObjectNode();
		operatorUpdate.put("userId", userId);
		ObjectNode entranceUpdate = jsonObjectMapper.createObjectNode();
		ArrayNode products = jsonObjectMapper.createArrayNode();
		ObjectNode statusUpdateNode = jsonObjectMapper.createObjectNode();
		statusUpdateNode.put("prodCd", prodCd);
		statusUpdateNode.put("svcEndDttm", svcEndDttm);
		statusUpdateNode.put("svcCd", svcCd);
		statusUpdateNode.put("prodDvCd", prodDvCd);
		products.add(statusUpdateNode);
		entranceUpdate.set("products", products);
		CSUtils.setProductNo(entranceUpdate, svcCd, entrNo, msisdn);
		//entranceUpdate.put("entrNo", entrNo);
		node.set("operator", operatorUpdate);
		node.set("entrance", entranceUpdate);
		node.put("eventCd", eventcd);
		node.put("chngDtlCd", chngdtcd);
		node.put("eventDtlCd", eventdtcd);
		node.put("pymtRecpNo", "");
		return jsonObjectMapper.writeValueAsString(node);
	}

	/**
     * 
     * Calculate counter expire date
     * 
     * @param rewardValidityHours rewardValidityHours
     * @param rewardValidityDays rewardValidityDays
     * @return counterExpireDate counter expire date
     */
    private String calculateAndGetExpiryDate(String rewardValidityHours, String rewardValidityDays) {
        Date currentDate = new Date();
        Calendar calender = Calendar.getInstance();
        calender.setTime(currentDate);
        calender.add(Calendar.DATE, Integer.parseInt(rewardValidityDays));
        calender.add(Calendar.HOUR, Integer.parseInt(rewardValidityHours));
        Date expiryDate = calender.getTime();
        //DateFormat format = new SimpleDateFormat("yyyyMMdd");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        //String counterExpireDate = format.format(expiryDate) + "000000";
        String svcEndDttm = format.format(expiryDate);
        return svcEndDttm;
    }

}
