/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.CSUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("TerminateVASProductRequestAPI")
public class TerminateVASProductRequestAPI implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Value("${cs.terminate.userId}")
    private String userId;

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
        if (soMessage != null) {
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            exchange.getIn().setHeader("msisdn", msisdn);
            log.info("Subscriber status for msisdn {} is expired. Checking for statusChangeProductStatus parameter",
                    msisdn);
            // Map<String, String> offerPayLoad = SOMessageUtils.getOfferPayLoad(soMessage);
            String jsonUserId = userId;
            String productNo = CSUtils.getProductNumber(soMessage);// setting according to product
                                                                   // code
            int svcCd = CSUtils.getProductDomain(soMessage);// setting according to product code
            String productCode = CSUtils.getproductCode(soMessage);// need to get product Id from
                                                                   // message
            exchange.getIn()
                    .setBody(createMessageBodyForTerminateVASProductMessage(jsonUserId, productNo, svcCd, productCode, msisdn));
            String requestBody = createMessageBodyForTerminateVASProductMessage(jsonUserId, productNo, svcCd,
                    productCode, msisdn);
            log.info("JSON request object for status date update API call: {}", requestBody);
            exchange.getIn().setBody(requestBody);

        }

    }

    /**
     * Creates JSON message to send to the API for changing product status
     * 
     * @param jsonUserId userId
     * @param productNo productNo from json
     * @param svcCd service code
     * @param productCode product code
     * @param msisdn msisdn
     * @return json as string
     * @throws JsonProcessingException exception handling
     */
    private String createMessageBodyForTerminateVASProductMessage(String jsonUserId, String productNo, int svcCd,
            String productCode, String msisdn) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();
        ObjectNode operatorUpdate = jsonObjectMapper.createObjectNode();
        operatorUpdate.put("userId", jsonUserId);
        ObjectNode entranceUpdate = jsonObjectMapper.createObjectNode();
        CSUtils.setProductNo(entranceUpdate, svcCd, productNo, msisdn);
        // entranceUpdate.put("productNo", productNo);
        ArrayNode products = jsonObjectMapper.createArrayNode();
        ObjectNode statusUpdateNode = jsonObjectMapper.createObjectNode();
        statusUpdateNode.put("svcCd", svcCd);
        statusUpdateNode.put("prodCd", productCode);
        products.add(statusUpdateNode);
        entranceUpdate.set("products", products);
        node.set("operator", operatorUpdate);
        node.set("entrance", entranceUpdate);
        return jsonObjectMapper.writeValueAsString(node);
    }
}
