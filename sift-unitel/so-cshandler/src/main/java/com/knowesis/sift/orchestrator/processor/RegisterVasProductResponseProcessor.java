package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares status date update API request
 * 
 * @author SO Development Team
 */
@Component("RegisterVasProductResponseProcessor")
public class RegisterVasProductResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        String response = exchange.getIn().getBody(String.class);
        String message = (String) exchange.getProperty("OriginalRequestMessage");
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
        log.debug("Response from Set Status Date API: {}", response);
        ObjectNode responseJson = (ObjectNode) jsonObjectMapper.readTree(response);
        String responseCode = responseJson.get("resultCode").asText();
        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-ACTION-RESPONSE", responseCode);
        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM",
                ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage, currentSequence).getTargetSystem());
        if (responseCode.equals("SUCCESS")) {
            exchange.getIn().setHeader("registerVASProductSuccess", "true");
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "SUCCESS");
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONING-STATUS", "SUCCESS");
            exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
            // exchange.removeProperties("OriginalRequestMessage");
        } else {
            exchange.getIn().setHeader("registerVASProductSuccess", "false");
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "FAILURE");
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONING-STATUS", "FAILURE");
            exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
            // exchange.removeProperties("OriginalRequestMessage");
        }
        exchange.getIn().removeHeader("OriginalRequestMessage");
        exchange.getIn().removeHeader("targetAPI");
        exchange.removeProperty("OriginalRequestMessage");
    }

}
