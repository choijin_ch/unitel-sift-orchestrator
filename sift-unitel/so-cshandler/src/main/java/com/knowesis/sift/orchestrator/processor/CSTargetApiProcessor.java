/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Checks the targetAPI message soMessage header and decide which OCS API should
 * be called
 * 
 * @author SO Development Team
 */
@Component("CSTargetApiProcessor")
public class CSTargetApiProcessor implements Processor {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	ObjectMapper jsonObjectMapper;

	@Override
	public void process(Exchange exchange) throws Exception {
		String message = exchange.getIn().getBody(String.class);
		SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
		SOFulfillmentMessage soFulfillmentMessage = (SOFulfillmentMessage) soMessage;
		String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
		FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct(soFulfillmentMessage, currentSequence);
		String targetApi = fulfillmentProduct.getTargetSystem().split("_")[1];
		SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM", null);
		ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
		exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soFulfillmentMessage));
		exchange.getIn().setHeader("targetAPI", targetApi);
		exchange.getIn().setBody(soFulfillmentMessage);
	}

}
