package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.CSUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares status date update API request
 * 
 * @author SO Development Team
 */
@Component("RegisterVasProductRequestProcessor")
public class RegisterVasProductRequestProcessor implements Processor {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	ObjectMapper jsonObjectMapper;

	@Value("${cs.vas.register.userid}")
	private String userId;

	@Override
	public void process(Exchange exchange) throws Exception {
		SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
		if (soMessage != null) {
			String msisdn = SOMessageUtils.getMSISDN(soMessage);
			exchange.getIn().setHeader("msisdn", msisdn);
			log.info("Subscriber with msisdn {} is registering for VAS. Checking VAS products details", msisdn);
			String productCode =CSUtils.getproductCode(soMessage); // need to get product Id from message
			String productNumber =CSUtils.getProductNumber(soMessage);// need to get product code from message
			int serviceCode = CSUtils.getProductDomain(soMessage); //setting according to product code 
			String subscriptionId = SOMessageUtils.getSubscriptionId(soMessage);
			exchange.getIn().setHeader("subscriptionId", subscriptionId);
			String requestBody = createMessageBodyForExtendExpiryDateMessage(productCode, productNumber, serviceCode, msisdn);
			log.info("JSON request object for register vas product API call: {}", requestBody);
			exchange.getIn().setBody(requestBody);
		}

		else {
			log.error("Internal Error occured. Message null. Can not invoke API");
		}
	}

	/**
	 * 
	 * @param productCode
	 *            productCode
	 * @param productNumber
	 *            productNumber
	 * @param serviceCode
	 *            serviceCode
	 * @param msisdn msisdn
	 * @return node object to send to API
	 * @throws JsonProcessingException
	 *             JsonException
	 */
	private String createMessageBodyForExtendExpiryDateMessage(String productCode, String productNumber,
			int serviceCode, String msisdn) throws JsonProcessingException {

		ObjectNode node = jsonObjectMapper.createObjectNode();
		ObjectNode operatorNode = jsonObjectMapper.createObjectNode();
		operatorNode.put("userId", userId);
		node.set("operator", operatorNode);
		node.set("entrance", createEntranceNodeInfo(serviceCode, productCode, productNumber, msisdn));
		node.put("pymtRecpNo", "");
		return jsonObjectMapper.writeValueAsString(node);
	}
	
	/**
	 * 
	 * @param serviceCode serviceCode
	 * @param productCode productCode
	 * @param productNumber productNumber
	 * @param msisdn msisdn
	 * @return entanceNode
	 */
	private ObjectNode createEntranceNodeInfo(int serviceCode, String productCode, String productNumber, String msisdn){
		ObjectNode entranceNode = jsonObjectMapper.createObjectNode();
		ObjectNode productsNode = jsonObjectMapper.createObjectNode();
		ArrayNode products = jsonObjectMapper.createArrayNode();
		productsNode.put("svcCd", serviceCode);
		productsNode.put("prodCd", productCode);
		products.add(productsNode);
		CSUtils.setProductNo(entranceNode, serviceCode, productNumber, msisdn);
		//entranceNode.put("productNo", productNumber);
		entranceNode.set("products", products);
		return entranceNode;
	}
	
}
