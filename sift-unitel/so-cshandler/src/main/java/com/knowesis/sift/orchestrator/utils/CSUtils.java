/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;

/**
 * utility for cs handlers
 * 
 * @author SO Development Team
 */
public class CSUtils {

    private static final Logger LOG = LoggerFactory.getLogger(CSUtils.class);
    
	/**
	 * domainvalue mapping to svcCd
	 * 
	 * @param soMessage
	 *            SOMessage
	 * @return svcCd to be sent with API request
	 */
	public static Integer getProductDomain(SOMessage soMessage) {
		String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
		FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
				currentSequence);
		String productDomain = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "Product_Domain");
		Map<String, Integer> domainValueMap = new HashMap<>();
		domainValueMap.put("mobile", 2);
		domainValueMap.put("voip", 5);
		domainValueMap.put("iptv", 3);
		domainValueMap.put("broadband", 4);
		domainValueMap.put("cross", 1);
		if (domainValueMap.keySet().contains(productDomain.toLowerCase())) {
		    LOG.debug("inside loop",domainValueMap.get("mobile"));
			return domainValueMap.get(productDomain.toLowerCase());
		}
		return null;
	}

	/**
	 * domainvalue mapping to ProductNumber
	 * 
	 * @param soMessage
	 *            SOMessage
	 * @return ProductNumber
	 */
	public static String getProductNumber(SOMessage soMessage) {
		int productDomain = getProductDomain(soMessage);
		List<Integer> productDomainForMsisdnList=Arrays.asList(2,5);
		List<Integer> productDomainForSubscriptionIdList=Arrays.asList(1,3,4);
		if(productDomainForMsisdnList.contains(productDomain)){
			return SOMessageUtils.getMSISDN(soMessage).substring(3);
		}
		else if(productDomainForSubscriptionIdList.contains(productDomain)){
			return SOMessageUtils.getSubscriptionId(soMessage);
		}
		return "not available";// need to set while handling contract Id
	}

	/**
	 * product code mapping
	 * 
	 * @param soMessage
	 *            SOMessage
	 * @return productID as prodCd
	 */
	public static String getproductCode(SOMessage soMessage) {
		String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
        FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                currentSequence);
		return ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "ProductID");

	}
	
	/**
	 * set key as ProductNo or entrno 
	 * 
	 * @param entranceNode json nodeentranceNode
	 * @param serviceCode serviceCode
	 * @param productNumber productNumber
	 * @param msisdn msisdn
	 * @return json node
	 */
	public static ObjectNode setProductNo(ObjectNode entranceNode,int serviceCode,String productNumber, String msisdn){
		List<Integer> productDomainForMsisdnList=Arrays.asList(2,5);
		List<Integer> productDomainForSubscriptionIdList=Arrays.asList(1,3,4);
		if(msisdn.startsWith("976")){
			msisdn = msisdn.substring(3);
		}
		if(productDomainForMsisdnList.contains(serviceCode)){
			 return entranceNode.put("productNo", msisdn);
		}
		else if(productDomainForSubscriptionIdList.contains(serviceCode)){
			return entranceNode.put("entrNo", productNumber);
		}
		return null;
	}


}
