/**
 * 
 */
package com.knowesis.sift.orchestrator.processor;

import javax.servlet.http.HttpServletRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * 
 * @author SO Development Team
 */
@Component("MockMovieProcessor")
public class MockMovieProcessor implements Processor {

	private Logger log = LoggerFactory.getLogger(MockIptvProcessor.class);


	@Autowired
    ObjectMapper jsonObjectMapper;
	
	@Value("${mock.movie.success.response}")
    private String successResponse;
	
	@Value("${mock.movie.fail.response}")
    private String failResponse;
	
	
	@Value("${mock.movie.paymentType}")
    private String validPaymentType;
	
	@Value("${mock.movie.subId}")
    private String validSubId;
	
    /* (non-Javadoc)
     * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        HttpServletRequest request = exchange.getIn().getBody(HttpServletRequest.class);
        String responseAdId =  request.getParameter("paymentType");
        String responseSubId =  request.getParameter("subscriberId");
        log.debug("ad_id:{}----- subscriber_ids:{}",responseAdId,responseSubId);
        if(responseSubId.equalsIgnoreCase(validSubId) && responseAdId.equalsIgnoreCase(validPaymentType)){
        	exchange.getIn().setBody(successResponse);
        	return ;
        }
        else{
        	exchange.getIn().setBody(failResponse);
        	return ;
        	
        }
    }

}
