/**
 * 
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * 
 * 
 * @author SO Development Team
 */
@Component("MockCUGProcessor")
public class MockCUGProcessor implements Processor {

	private Logger log = LoggerFactory.getLogger(MockCUGProcessor.class);

	@Autowired
	ObjectMapper jsonObjectMapper;

	@Value("${mock.cug.success.response}")
	private String successResponse;

	@Value("${mock.cug.fail.response}")
	private String failResponse;

	@Value("${mock.cug.admin}")
	private String validAdmin;

	@Value("${mock.cug.templateId}")
	private String validTemplateId;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
	 */
	@Override
	public void process(Exchange exchange) throws Exception {
		String body = exchange.getIn().getBody(String.class);
		log.debug("extracted {}", body);
		ObjectNode responseJson = (ObjectNode) jsonObjectMapper.readTree(body);
		String responseAdmin = responseJson.get("admin").asText();
		String responseTemplateId = responseJson.get("templateId").asText();
		log.debug("responseAdmin:{}             responseTemplateId:{}", responseAdmin, responseTemplateId);
		log.debug("Property responseAdmin:{}             responseTemplateId:{}", validAdmin, validTemplateId);

		if (responseAdmin.equalsIgnoreCase(validAdmin) && responseTemplateId.equalsIgnoreCase(validTemplateId)) {
			exchange.getIn().setBody(successResponse);
			return;
		} else {
			exchange.getIn().setBody(failResponse);
			return;

		}
	}

}
