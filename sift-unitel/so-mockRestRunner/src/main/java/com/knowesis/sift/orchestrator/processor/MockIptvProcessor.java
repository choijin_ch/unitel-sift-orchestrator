/**
 * 
 */
package com.knowesis.sift.orchestrator.processor;

import javax.servlet.http.HttpServletRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * 
 * @author SO Development Team
 */
@Component("MockIptvProcessor")
public class MockIptvProcessor implements Processor {

	private Logger log = LoggerFactory.getLogger(MockIptvProcessor.class);

	@Autowired
    ObjectMapper jsonObjectMapper;
	
	@Value("${mock.iptv.success.response}")
    private String successResponse;
	
	@Value("${mock.iptv.fail.response}")
    private String failResponse;
	
	@Value("${mock.iptv.adid}")
    private String validAdId;
	
	@Value("${mock.iptv.subId}")
    private String validSubId;
	
    /* (non-Javadoc)
     * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
     */
    @Override
    public void process(Exchange exchange) throws Exception {
    	
		String body = exchange.getIn().getBody(String.class);
		String[] intialSplit=body.split("&");
		String[] addIdSplit=intialSplit[0].split("=");
		String[] subIdSplit=intialSplit[1].split("=");
        log.debug("ad_id:{}----- subscriber_ids:{}",addIdSplit[1],subIdSplit);
        if(addIdSplit[1].equalsIgnoreCase(validAdId) && subIdSplit[1].equalsIgnoreCase(validSubId)){
        	exchange.getIn().setBody(successResponse);
        	return ;
        }
        else{
        	exchange.getIn().setBody(failResponse);
        	return ;
        	
        }
    }

}
