/**
 * 
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 
 * This processor prepares data for writing to EventPayload Table
 * 
 * @author SO Development Team
 */
@Component("DummyBodyProcessor")
public class DummyBodyProcessor implements Processor {

    private Logger log = LoggerFactory.getLogger(DummyBodyProcessor.class);

    /* (non-Javadoc)
     * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class);
        log.debug("extracted {}", body);
        exchange.getIn().setBody(body);
    }

}
