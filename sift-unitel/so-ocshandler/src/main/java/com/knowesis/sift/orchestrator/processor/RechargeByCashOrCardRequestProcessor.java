/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;


import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("RechargeByCashOrCardRequestProcessor")
public class RechargeByCashOrCardRequestProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${ocs.recharge.rechargeChannel}")
    private String rechargeChannel;

    @Value("${ocs.recharge.rechargeType}")
    private String rechargeType;

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("RechargeByCashOrCardRequestProcessor start->");
        SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
        if (soMessage != null) {
            String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
            FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                    currentSequence);
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            exchange.getIn().setHeader("msisdn", msisdn);
            log.info("Trying to top up the subscriber by using virtual scratch cards for MSISDN {}", msisdn);
            String subscriptionId = SOMessageUtils.getSubscriptionId(soMessage);
//            String deviceType=ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct,"SUBSCRIPTION_TYPE");
//            List<String> definedDeviceTypesForMSISDN=Arrays.asList( new String[]{"MOBILE","VOIP","CROSS"});
//            List<String> definedDeviceTypesForSubscriptionId=Arrays.asList( new String[]{"IPTV","BROADBAND"});
            String userKey=SOMessageUtils.getMSISDN(soMessage);
//            	if (definedDeviceTypesForMSISDN.stream().anyMatch(deviceType::equalsIgnoreCase))  { //if device is MOBILE,VOIP or CROSSVOIP then userKey is MSISDN otherwise contractId
//            		userKey = SOMessageUtils.getMSISDN(soMessage);	
//                }
//            	else if(definedDeviceTypesForSubscriptionId.stream().anyMatch(deviceType::equalsIgnoreCase)){
//            		userKey =SOMessageUtils.getSubscriptionId(soMessage);
//            	}
            String voucherType = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "Voucher_Type");
            exchange.getIn().setHeader("subscriptionId", subscriptionId);
            String requestBody = createMessageBodyForExtendExpiryDateMessage(subscriptionId, userKey, voucherType);
            log.info("JSON request object for Recharge By Cash Or Card date update API call: {}", requestBody);
            exchange.getIn().setBody(requestBody);
            exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
        }
    }

    /**
     * 
     * @param subscriptionId subscriptionId
     * @param userKey is msisdn
     * @param voucherType voucherType
     * @return node object to send to API
     * @throws JsonProcessingException JsonException
     */
    private String createMessageBodyForExtendExpiryDateMessage(String subscriptionId, String userKey,
            String voucherType) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();

        ObjectNode rechargeNode = jsonObjectMapper.createObjectNode();
        rechargeNode.put("recharge_channel", Integer.parseInt(rechargeChannel));
        rechargeNode.put("recharge_type", Integer.parseInt(rechargeType));
        rechargeNode.put("subscription_key",  Long.parseLong(subscriptionId));
        rechargeNode.put("user_key", Long.parseLong(userKey));
        rechargeNode.put("voucher_type", Integer.parseInt(voucherType));
        node.set("recharge", rechargeNode);
        return jsonObjectMapper.writeValueAsString(node);
    }

}
