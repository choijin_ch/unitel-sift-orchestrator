/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("DeductPointCgResponseProcessor")
public class DeductPointCgResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        String response = exchange.getIn().getBody(String.class);
        log.debug("Response from Deduct Point API: {}", response);
        String message = (String) exchange.getProperty("OriginalRequestMessage");
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
        ObjectNode responseJson = (ObjectNode) jsonObjectMapper.readTree(response);
        String responseCode = responseJson.get("result_code").asText();
        String responseReason = responseJson.get("result_reason").asText();
        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-ACTION-RESPONSE", responseCode+":"+responseReason);
        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM",
                ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage, currentSequence).getTargetSystem());
        if (responseCode.equals("0")) {
            exchange.getIn().setHeader("deductPointSuccess", "true");
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "SUCCESS");
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONING-STATUS", "SUCCESS");
            exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
        } else {
            exchange.getIn().setHeader("deductPointSuccess", "false");
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "FAILURE");
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONING-STATUS", "FAILURE");
            exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
        }
        exchange.getIn().removeHeader("OriginalRequestMessage");
        exchange.getIn().removeHeader("targetAPI");
        exchange.removeProperty("OriginalRequestMessage");
    }

}
