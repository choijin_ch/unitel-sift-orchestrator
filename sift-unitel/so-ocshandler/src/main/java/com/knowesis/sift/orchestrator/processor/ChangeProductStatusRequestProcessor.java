/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("ChangeProductStatusRequestProcessor")
public class ChangeProductStatusRequestProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
        if (soMessage != null) {
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            exchange.getIn().setHeader("msisdn", msisdn);
            String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
            FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                    currentSequence);
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
            log.info(" Checking for statusChangeProductStatus parameter", msisdn);
            String productStatus = null;
            productStatus = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "STATUS");
            if (StringUtils.isNotBlank(productStatus)) {
                log.info(
                        "Status date extension parameter(productStatus) found for msisdn {} with value {}. Calling Change Product Status API",
                        msisdn, productStatus);
                String status = productStatus;
                String subscriptionId = SOMessageUtils.getSubscriptionId(soMessage);
                String productId = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "productId");
                exchange.getIn().setHeader("subscriptionId", subscriptionId);
                exchange.getIn()
                        .setBody(createMessageBodyForChangeProductStatusMessage(status, subscriptionId, productId));
                String requestBody = createMessageBodyForChangeProductStatusMessage(status, subscriptionId, productId);
                log.info("JSON request object for status date update API call: {}", requestBody);
                exchange.getIn().setBody(requestBody);

            }
            exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
        }

    }

    /**
     * Creates JSON message to send to the API for changing product status
     * 
     * @param status product status
     * @param subscriptionId mobile or other
     * @param productId productId
     * @return Json node node
     * @throws JsonProcessingException JsonException
     */

    private String createMessageBodyForChangeProductStatusMessage(String status, String subscriptionId,
            String productId) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();
        ArrayNode product = jsonObjectMapper.createArrayNode();
        ObjectNode statusUpdateNode = jsonObjectMapper.createObjectNode();
        statusUpdateNode.put("status", Integer.parseInt(status));
        statusUpdateNode.put("subscription_key", subscriptionId);
        statusUpdateNode.put("product_id", productId);
        product.add(statusUpdateNode);
        node.set("product", product);
        return jsonObjectMapper.writeValueAsString(node);

    }
}
