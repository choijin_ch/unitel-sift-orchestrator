/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("GetSubscriberRequestProcessor")
public class GetSubscriberRequestProcessor implements Processor {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
        String subscriptionId = SOMessageUtils.getSubscriptionId(soMessage);
        if (subscriptionId == null) {
            throw new Exception("nominee number not found, cannot invoke GET_SBSCRIBER_API");
        }
        log.info("Invoking Get Subscriber API for Nominee: {}", subscriptionId);
        exchange.getIn().setHeader("subscriptionId", subscriptionId);
        exchange.getIn().setBody(null);
        exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
    }
}
