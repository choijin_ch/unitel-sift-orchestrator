/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.core.domain.DynamicParameter;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares status date update API request
 * 
 * @author SO Development Team
 */
@Component("StatusDateUpdateRequestProcessor")
public class StatusDateUpdateRequestProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${so.fulfill.actionType.extendExpiryNotify}")
    private String extendExpiryNotify;

    @Value("${ocs.encodeExpireAndSubscription}")
    private Boolean encodeExpireAndSubscription;

    @Value("${ocs.enhancedflow}")
    private Boolean ocsEnhancedflow;

    @Override
    public void process(Exchange exchange) throws Exception {

        SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
        if (soMessage != null) {
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            String fulfillActionType = SOMessageUtils.getHeaderValue(soMessage, "fulfillActionType");
            if (fulfillActionType.equals(extendExpiryNotify)) {
                log.info("Subscriber status for msisdn {} is expired. Checking for status date extension parameter",
                        msisdn);
                createRequestForExtendExpiryNotify(exchange, soMessage, msisdn);
            } else {
                createRequestForProvisioning(exchange, soMessage, msisdn);
            }
            exchange.getIn().setHeader("msisdn", msisdn);
            exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
        } else {
            log.warn("no body found, this will likely cause exceptions");
        }
    }

    /**
     * 
     * Creates JSON request object for EXTENDEXPIRY_NOTIFY
     * 
     * @param exchange exchange
     * @param soMessage soMessage
     * @param msisdn msisdn
     * @throws JsonProcessingException JsonProcessingException
     * @throws ParseException ParseException, when dates go wrong
     */
    private void createRequestForExtendExpiryNotify(Exchange exchange, SOMessage soMessage, String msisdn)
            throws JsonProcessingException, ParseException {
        String validityDaysExtension = null;
        Map<String, String> offerPayLoad = SOMessageUtils.getOfferPayLoad(soMessage);
        validityDaysExtension = offerPayLoad.get("VALIDITYDAYS_EXTENSION");
        if (StringUtils.isNotBlank(validityDaysExtension)) {
            log.info(
                    "Status date extension parameter(VALIDITYDAYS_EXTENSION) found for msisdn {} with value {}. Calling Status date update API",
                    msisdn, validityDaysExtension);
            // Defaults to 1 for EXTENDEXPIRY_NOTIFY
            int status = 1; // active
            int expiryDate = Integer.parseInt(validityDaysExtension);
            // For DB entry.
            SOMessageUtils.setHeaderValue(soMessage, "PROVISIONED-OFFERID", "" + status);
            SOMessageUtils.setHeaderValue(soMessage, "PROVISIONED-VALUE", "" + expiryDate);
            String subscriptionId = SOMessageUtils.getSubscriptionId(soMessage);
            String subscriberStatus = exchange.getIn().getHeader("SubscriberStatus", String.class);
            String subscriberExpiry = exchange.getIn().getHeader("SubscriberExpiry", String.class);
            exchange.getIn().removeHeaders("CamelHttp*");
            exchange.getIn().setHeader("subscriptionId", subscriptionId);
            String requestBody = createMessageBodyForExtendExpiryDateMessage(status, expiryDate, subscriptionId,
                    subscriberStatus, subscriberExpiry, Boolean.TRUE);
            log.info("JSON request object for status date update API call: {}", requestBody);

            exchange.getIn().setBody(requestBody);
        } else {
            log.error("Not able to extend expiry date for msisdn {} as VALIDITYDAYS_EXTENSION is empty or null");
        }

    }

    /**
     * Creates JSON request object for Provisioning
     * 
     * @param exchange exchange
     * @param soMessage soMessage
     * @param msisdn msisdn
     * @throws JsonProcessingException JsonProcessingException
     * @throws ParseException ParseException ParseException, when dates go wrong
     */
    private void createRequestForProvisioning(Exchange exchange, SOMessage soMessage, String msisdn)
            throws JsonProcessingException, ParseException {
        String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
        FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                currentSequence);
        ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
        DynamicParameter stateIndexDynamicParameter = ProvisionUtils.getDynamicParameter(fulfillmentProduct,
                "StateIndex");
        String subscriberStatus = exchange.getIn().getHeader("SubscriberStatus", String.class);
        String subscriberExpiry = exchange.getIn().getHeader("SubscriberExpiry", String.class);
        String subscriptionId = SOMessageUtils.getSubscriptionId(soMessage);
        exchange.getIn().removeHeaders("CamelHttp*");
        exchange.getIn().setHeader("subscriptionId", subscriptionId);
        if (stateIndexDynamicParameter != null) {
            int status = Integer.parseInt(stateIndexDynamicParameter.getResolvedValue());
            int expiryDate = stateIndexDynamicParameter.getRewardValidityDays();
            // For DB entry.
            SOMessageUtils.setHeaderValue(soMessage, "PROVISIONED-OFFERID", "" + status);
            SOMessageUtils.setHeaderValue(soMessage, "PROVISIONED-VALUE", "" + expiryDate);
            String requestBody = createMessageBodyForExtendExpiryDateMessage(status, expiryDate, subscriptionId,
                    subscriberStatus, subscriberExpiry, Boolean.FALSE);
            log.info("JSON request object for status date update API call: {}", requestBody);
            exchange.getIn().setBody(requestBody);
        } else {
            log.error("No dynamic parameter with key StateIndex found. Not able to proceed fulfillment");
        }
    }

    /**
     * Creates JSON message to send to the API for extending expire date of
     * customer
     * 
     * @param status status
     * @param expiryDate expiryDate
     * @param subscriptionId subscriptionId
     * @param subscriberStatus subscriber status
     * @param subscriberExpiry subscriberExpiry
     * @param extendExpiry True in case of extended expire
     * @return node object to send to API
     * @throws JsonProcessingException JsonException
     * @throws ParseException ParseException ParseException, when dates go wrong
     */
    private String createMessageBodyForExtendExpiryDateMessage(int status, int expiryDate, String subscriptionId,
            String subscriberStatus, String subscriberExpiry, Boolean extendExpiry)
            throws JsonProcessingException, ParseException {

        ObjectNode node = jsonObjectMapper.createObjectNode();
        ArrayNode subscriptionState = jsonObjectMapper.createArrayNode();

        if (ocsEnhancedflow) {

            enhancedProvisioning(expiryDate, subscriptionId, subscriberStatus, subscriberExpiry, subscriptionState);
        } else {
            ObjectNode statusNode = createStatusNode(status, expiryDate, subscriptionId);
            subscriptionState.add(statusNode);
        }
        node.set("subscriptionState", subscriptionState);
        if (extendExpiry && ocsEnhancedflow) {
            ArrayNode subscription = createSubscriptionNode(1, subscriptionId);
            node.set("subscription", subscription);
        }
        return jsonObjectMapper.writeValueAsString(node);
    }

    /**
     * Following takes place iff <code>ocsEnhancedflow is true</code>
     * <p>
     * Call getSubscriber API and take out current state and its expiry date.
     * These are stored in arguments 1) subscriberStatus and 2) subscriberExpiry
     * <p>
     * If current status active then SIFT calculates as below
     * <li>Active : Current Expire date + Reward Validity days
     * <li>Grace : Updated Active Expire date + 14
     * <li>Expired : Updated Grace Expire Date + 60
     * 
     * <p>
     * If current status not active then SIFT calculates as below
     * <li>Active : Current date + Reward Validity days
     * <li>Grace : Updated Active Expire date + 14
     * <li>Expired : Updated Grace Expire Date + 60
     * 
     * <p>
     * Set Status API
     * <li>Set the Active expire dates as per the request from the core
     * <li>Update the grace and expired dates as above
     * 
     * @param expiryDate expiry date
     * @param subscriptionId subscription id
     * @param subscriberStatus subscriber status
     * @param subscriberExpiry expiry of subscriber
     * @param subscriptionState state of subscriber
     * @throws ParseException when date conversion fails
     */
    private void enhancedProvisioning(int expiryDate, String subscriptionId, String subscriberStatus,
            String subscriberExpiry, ArrayNode subscriptionState) throws ParseException {
        long expiry;
        long graceExpiry;
        long expiredExpiry;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar c = Calendar.getInstance();
        if ("unlimited".equalsIgnoreCase(subscriberExpiry) == true || "0".equalsIgnoreCase(subscriberExpiry) == true) {
            expiry = 0;
            graceExpiry = 0;
            expiredExpiry = 0;
        } else {
            if (StringUtils.equals(subscriberStatus, "1")) {
                Date date = sdf.parse(subscriberExpiry);
                c.setTime(date);
            } else {
                c.setTime(new Date());
            }
            c.add(Calendar.DATE, expiryDate);
            expiry = Long.parseLong(sdf.format(c.getTime()));

            c.add(Calendar.DATE, 14);
            graceExpiry = Long.parseLong(sdf.format(c.getTime()));

            c.add(Calendar.DATE, 60);
            expiredExpiry = Long.parseLong(sdf.format(c.getTime()));
        }

        ObjectNode activeStatusNode = createStatusNode(1, expiry, subscriptionId);
        ObjectNode graceStatusNode = createStatusNode(3, graceExpiry, subscriptionId);
        ObjectNode expiredStatusNode = createStatusNode(8, expiredExpiry, subscriptionId);
        subscriptionState.add(activeStatusNode);
        subscriptionState.add(graceStatusNode);
        subscriptionState.add(expiredStatusNode);
    }

    /**
     * Creates Subscription Node
     * 
     * @param i the state, should be 1
     * @param subscriptionId the subscription id
     * @return subscriptionNodeArray
     */
    private ArrayNode createSubscriptionNode(int i, String subscriptionId) {
        ArrayNode array = jsonObjectMapper.createArrayNode();

        ObjectNode status = jsonObjectMapper.createObjectNode();
        status.put("status", i);
        if (encodeExpireAndSubscription) {
            status.put("subscription_key", subscriptionId);
        } else {
            status.put("subscription_key", Long.parseLong(subscriptionId));
        }
        array.add(status);
        return array;
    }

    /**
     * Common utility method
     * 
     * @param status status
     * @param expiryDate expiry date of status
     * @param subscriptionId subscriber subscription id
     * @return json node
     */
    private ObjectNode createStatusNode(int status, long expiryDate, String subscriptionId) {
        ObjectNode statusUpdateNode = jsonObjectMapper.createObjectNode();

        statusUpdateNode.put("status", status);

        if (encodeExpireAndSubscription) {
            statusUpdateNode.put("expire_date", new Long(expiryDate).toString());
            statusUpdateNode.put("subscription_key", subscriptionId);
        } else {
            statusUpdateNode.put("expire_date", expiryDate);
            statusUpdateNode.put("subscription_key", Long.parseLong(subscriptionId));
        }
        return statusUpdateNode;
    }
}
