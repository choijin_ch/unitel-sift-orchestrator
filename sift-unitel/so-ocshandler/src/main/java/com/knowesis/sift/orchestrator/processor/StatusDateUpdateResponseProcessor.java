/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Processes OCS Add status date API response
 * 
 * @author SO Development Team
 */

@Component("StatusDateUpdateResponseProcessor")
public class StatusDateUpdateResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        log.info("StatusDateUpdateResponseProcessor Start.");
        String response = exchange.getIn().getBody(String.class);
        String message = (String) exchange.getProperty("OriginalRequestMessage");
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        log.info("Status Date Update API response for MSISDN: {} is {}", SOMessageUtils.getMSISDN(soMessage), response);
        String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
        ObjectNode responseJson = (ObjectNode) jsonObjectMapper.readTree(response);
        String responseCode = responseJson.get("result_code").asText();
        String responseReason = responseJson.get("result_reason").asText();
        exchange.getIn().setHeader("fulfillActionType", SOMessageUtils.getHeaderValue(soMessage, "fulfillActionType"));
        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-ACTION-RESPONSE", responseCode+":"+responseReason);
        log.debug("LAST-PROVISIONED-ACTION-RESPONSE {}",responseCode+":"+responseReason);
        if (StringUtils.isBlank(currentSequence)) {
            log.info("currentSequence is null. EXTENDEXPIRY-NOTIFY case");
            currentSequence = "EXTENDEXPIRY-NOTIFY";
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM", "OCS_Addstatusdates");
        } else {
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM", ProvisionUtils
                    .getCurrentProduct((SOFulfillmentMessage) soMessage, currentSequence).getTargetSystem());
        }
        // For EXTENDEXPIRY-NOTIFY
        exchange.getIn().setHeader("actionResponse", responseCode+":"+responseReason);
        if (responseCode.equals("0")) {
            exchange.getIn().setHeader("statusDateUpdateSuccess", "true");
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "SUCCESS");
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONING-STATUS", "SUCCESS");
        } else {
            exchange.getIn().setHeader("statusDateUpdateSuccess", "false");
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "FAILURE");
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONING-STATUS", "FAILURE");
        }
        exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
        exchange.getIn().removeHeader("OriginalRequestMessage");
        exchange.getIn().removeHeader("targetAPI");
        exchange.removeProperty("OriginalRequestMessage");
    }

}
