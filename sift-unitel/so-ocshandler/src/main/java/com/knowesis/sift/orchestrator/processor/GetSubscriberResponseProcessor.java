/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("GetSubscriberResponseProcessor")
public class GetSubscriberResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        String response = exchange.getIn().getBody(String.class);
        log.debug("Response message of GetSubscriber {}", response);
        String message = (String) exchange.getProperty("OriginalRequestMessage");
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
        ObjectNode responseJson = (ObjectNode) jsonObjectMapper.readTree(response);
        String responseCode = responseJson.get("result_code").asText();
        String responseReason = responseJson.get("result_reason").asText();
        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-ACTION-RESPONSE", responseCode+":"+responseReason);
        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM",
                ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage, currentSequence).getTargetSystem());
        if (responseCode.equals("0")) {
            exchange.getIn().setHeader("getsubscriberSuccess", "true");
            String paymentType = responseJson.get("1").get("subscription").get("payment_type").asText();
            String status = responseJson.get("1").get("subscription").get("status").asText();
            String productName = getProductName(responseJson);
            log.info("OCS_GetSubscriber API for MSISDN {} Called. payment_type: {}, status: {}, product_name: {}",
                    SOMessageUtils.getMSISDN(soMessage), paymentType, status, productName);
            SOMessageUtils.setHeaderValue(soMessage, "payment_type", paymentType);
            SOMessageUtils.setHeaderValue(soMessage, "status", status);
            SOMessageUtils.setHeaderValue(soMessage, "product_name", productName);
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "SUCCESS");
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONING-STATUS", "SUCCESS");
            exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
        } else {
            exchange.getIn().setHeader("getsubscriberSuccess", "false");
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "FAILURE");
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONING-STATUS", "FAILURE");
            exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
        }
        exchange.getIn().removeHeader("OriginalRequestMessage");
        exchange.getIn().removeHeader("targetAPI");
        exchange.removeProperty("OriginalRequestMessage");
    }

    /**
     * Getting product name with product_type 1
     * @param responseJson response recieved from getsubcriber call
     * @return productname
     */
    private String getProductName(ObjectNode responseJson) {
        String productname = null;
        JsonNode productArray = responseJson.get("1").get("product");
        if (productArray.isArray()) {
            for (JsonNode node : productArray) {
                if (node.get("product_type").asText().equalsIgnoreCase("1")) {
                    productname = node.get("product_name").asText();
                }
            }
        }
        return productname;
    }
}
