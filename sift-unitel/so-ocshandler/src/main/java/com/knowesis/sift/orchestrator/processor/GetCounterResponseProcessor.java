/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Process Get Counter API response
 * 
 * @author SO Development Team
 */
@Component("GetCounterResponseProcessor")
public class GetCounterResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        String response = exchange.getIn().getBody(String.class);
        String counterId = exchange.getIn().getHeader("counterId", String.class);
        exchange.getIn().removeHeaders("CamelHttp*");
        log.info("Response from Get Counter API: {}", response);
        ObjectNode responseJson = (ObjectNode) jsonObjectMapper.readTree(response);
        String responseCode = responseJson.get("result_code").asText();
        if (responseCode.equals("0")) {
            String expiryDate = getCounterExpiryDate(responseJson, counterId);
            log.info("greped counterExpiryDate({}) from Get Counter API for counter({})", expiryDate, counterId);
            exchange.getIn().setHeader("counterExpiryDate", expiryDate);
        }
    }

    /**
     * @param responseJson response received from get counter
     * @param counterId unused
     * @return expiry date of active counter
     */
    private String getCounterExpiryDate(ObjectNode responseJson, String counterId) {
        String counterExpire = null;
        JsonNode counters = responseJson.get("counter");
        if (counters != null && counters.isArray()) {
            for (JsonNode node : counters) {
                if (node.get("counter_id").asText().equalsIgnoreCase(counterId)) {
                    counterExpire = node.get("counter_expire").asText();
                }
            }
        }
        return counterExpire;
    }
}
