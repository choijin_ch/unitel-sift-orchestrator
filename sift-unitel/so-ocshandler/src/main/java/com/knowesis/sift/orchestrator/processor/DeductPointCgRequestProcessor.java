/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("DeductPointCgRequestProcessor")
public class DeductPointCgRequestProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${ocs.deductpoint.serviceid}")
    private String serviceId;
    @Value("${ocs.deductpoint.chargingtype}")
    private String chargingType;
    @Value("${ocs.deductpoint.contentcalltype}")
    private String contentCallType;
    @Value("${ocs.deductpoint.contentstype}")
    private String contentsType;
    @Value("${ocs.deductpoint.keeprest}")
    private String keepRest;

    @Override
    public void process(Exchange exchange) throws Exception {
        SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
        if (soMessage != null) {
            String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
            FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                    currentSequence);
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
            String subscriptionId = SOMessageUtils.getSubscriptionId(soMessage);
            exchange.getIn().setHeader("subscriptionId", subscriptionId);
            String fare = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "amount");
            String contentsDescription = SOMessageUtils.getHeaderValue(soMessage, "OCS-DESCRIPTION");
//            String deviceType=ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct,"SUBSCRIPTION_TYPE");
            String origId=SOMessageUtils.getMSISDN(soMessage);
//            if(deviceType.equalsIgnoreCase("MOBILE")){//if mobile then orgId is MSISDN otherwise contractId 
//            	origId = SOMessageUtils.getMSISDN(soMessage);
//            }
//            else{
//            	origId=SOMessageUtils.getSubscriptionId(soMessage);
//            }
            String requestBody = createMessageBodyForDeductMoney(soMessage, origId, fare, contentsDescription);
            log.info("JSON request object for Deduct Point API call: {}", requestBody);
            exchange.getIn().setBody(requestBody);
            exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
        } else {
            log.error("Internal Error occured. Message null. Can not invoke API");
        }

    }

    /**
     * Creates request body JSON message from SOMessage
     * 
     * @param soMessage soMessage
     * @param origId origId
     * @param fare fare
     * @param contentsDescription contentsDescription
     * @return JSON request String
     * @throws JsonProcessingException JSON Exception
     */
    private String createMessageBodyForDeductMoney(SOMessage soMessage, String origId, String fare,
            String contentsDescription) throws JsonProcessingException {
        ObjectNode requestNode = jsonObjectMapper.createObjectNode();
        requestNode.put("chargeTime", getCurrentTimeStamp());
        requestNode.put("serviceId", serviceId);
        requestNode.put("origId", origId);
        requestNode.set("contentsServiceInfo", createContentsServiceInfo(fare, contentsDescription));
        requestNode.put("keepRest", keepRest);
        return jsonObjectMapper.writeValueAsString(requestNode);
    }

    /**
     * Creates contactsServiceInfo inner JSON Object for request
     * 
     * @param fare fare
     * @param contentsDescription contentsDescription
     * @return contactsServiceInfo ObjectNode
     */
    private ObjectNode createContentsServiceInfo(String fare, String contentsDescription) {
        ObjectNode contentsServiceInfoNode = jsonObjectMapper.createObjectNode();
        contentsServiceInfoNode.put("chargingType", chargingType);
        contentsServiceInfoNode.put("contentCallType", contentCallType);
        contentsServiceInfoNode.put("fare", fare);
        contentsServiceInfoNode.put("contentsDescription", contentsDescription);
        contentsServiceInfoNode.put("contentsType", contentsType);
        return contentsServiceInfoNode;
    }

    /**
     * get timestamp in yyyyMMddHHmmss
     * 
     * @return current timestamp
     */
    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

}
