/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("CounterUpdateRequestProcessor")
public class CounterUpdateRequestProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${ocs.counter.desciption}")
    private String counterDescription;

    @Value("${ocs.counter.eventCd}")
    private String eventCd;

    @Value("${ocs.enhancedflow}")
    private Boolean ocsEnhancedflow;

    @Override
    public void process(Exchange exchange) throws Exception {
        SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
        if (soMessage != null) {
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            exchange.getIn().setHeader("msisdn", msisdn);
            String counterExpiryDateInOCS = exchange.getIn().getHeader("counterExpiryDate", String.class);
            //exchange.getIn().removeHeader("CamelHttp*");
            log.info("Recieved counter updation parameter for msisdn {}", msisdn);
            String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
            FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                    currentSequence);
            String targetSystemValue = fulfillmentProduct.getTargetSystem().split("_")[1];
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
            String counterId = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "CounterID");
            String counterValue = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "CounterValue");
            String amount = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "Amount");
            SOMessageUtils.setHeaderValue(soMessage, "PROVISIONED-OFFERID", "" + counterId);
            if (targetSystemValue.equalsIgnoreCase("SetCounter")) {
                SOMessageUtils.setHeaderValue(soMessage, "PROVISIONED-VALUE", "" + amount);
            }else {
                SOMessageUtils.setHeaderValue(soMessage, "PROVISIONED-VALUE", "" + counterValue);
            }
            String desciption = SOMessageUtils.getHeaderValue(soMessage, "OCS-DESCRIPTION");
            String rewardValidityDays = ProvisionUtils.getRewardValidityDayFromDynamicParameter(fulfillmentProduct);
            String rewardValidityHours=ProvisionUtils.getRewardValidityHourFromDynamicParameter(fulfillmentProduct);
            String expiryDate = calculateAndGetExpiryDate(rewardValidityHours,rewardValidityDays, counterExpiryDateInOCS);
            String rollOverMonthAgo = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct,
                    "Rollover_MonthAgo");
            String subscriptionId = SOMessageUtils.getSubscriptionId(soMessage);
            log.info("Subscription ID for MSISDN: {} is {}", msisdn, subscriptionId);
            exchange.getIn().setHeader("subscriptionId", subscriptionId);
            String requestBody = createMessageBodyForExtendExpiryDateMessage(subscriptionId, counterId, counterValue,
                    expiryDate, desciption, rollOverMonthAgo, targetSystemValue, amount);
            log.info("JSON request object for counter date update API call: {}", requestBody);
            exchange.getIn().setBody(requestBody);
            exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
        } else {
            log.error("Not able to extend counter date for msisdn as Counter expiry date is empty or null");
        }

    }

    /**
     * 
     * Calculate counter expire date
     * 
     * @param rewardValidityHours rewardValidityHours
     * @param rewardValidityDays rewardValidityDays
     * @param counterExpiryDateInOCS counterExpiryDateInOCS
     * @return counterExpireDate counter expire date
     */
    private String calculateAndGetExpiryDate(String rewardValidityHours, String rewardValidityDays, String counterExpiryDateInOCS) {
        Date currentDate = new Date();
        Calendar calender = Calendar.getInstance();
        calender.setTime(currentDate);
        calender.add(Calendar.DATE, Integer.parseInt(rewardValidityDays));
		if (Integer.parseInt(rewardValidityHours) > 0) {
			calender.add(Calendar.HOUR, Integer.parseInt(rewardValidityHours));
		} else {
			calender.set(Calendar.HOUR_OF_DAY, 0);
			calender.set(Calendar.MINUTE, 0);
			calender.set(Calendar.SECOND, 0);
		}
        Date expiryDate = calender.getTime();
        //DateFormat format = new SimpleDateFormat("yyyyMMdd");
        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        //String counterExpireDate = format.format(expiryDate) + "000000";
        String counterExpireDate = format.format(expiryDate);
        counterExpireDate = compareAndGetExpiryDate(counterExpireDate, counterExpiryDateInOCS);
        log.debug("counter expire date {},{},{}", counterExpireDate,rewardValidityHours,rewardValidityDays);
        return counterExpireDate;
    }
    
    
    
    /**
     * Return the oldest date from two arguments
     * 
     * @param counterExpireDate counterExpireDate
     * @param counterExpiryDateInOCS counterExpiryDateInOCS
     * @return oldest date from two arguments OR counterExpireDate in corner
     *         cases
     */
    // TODO: reduce cyclomatic complexity
    //For OCS_Setcounter the expiryDate should be as from Core(To be done phase 2)
    private String compareAndGetExpiryDate(String counterExpireDate, String counterExpiryDateInOCS) {

        try {
            if (StringUtils.isBlank(counterExpiryDateInOCS)) {
                return counterExpireDate;
            }
            if (Boolean.FALSE.equals(ocsEnhancedflow)) {
                return counterExpireDate;
            }
            if ("unlimited".equalsIgnoreCase(counterExpiryDateInOCS) == true
                    || "0".equalsIgnoreCase(counterExpiryDateInOCS) == true) {
                log.info("overridding sift expiry because {} is after {}", 0, counterExpireDate);
                return "0";
            }
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            Date siftDate = format.parse(counterExpireDate);
            Date ocsDate = format.parse(counterExpiryDateInOCS);
            if (ocsDate.after(siftDate)) {
                log.info("overridding sift expiry because {} is after {}", ocsDate, siftDate);
                return counterExpiryDateInOCS;
            }
        } catch (Exception e) {
            log.warn("an error ocurred while comparing OCS and sift date, returning SIFT date as default",
                    e.getMessage());
            log.debug("an error ocurred while comparing OCS and sift date, returning SIFT date as default", e);
        }
        return counterExpireDate;
    }

    /**
     * 
     * @param subscriptionId subscriptionId
     * @param counterId counterId
     * @param counterValue counterValue
     * @param expiryDate counter expiryDate
     * @param desciption counter desciption
     * @param rollOverMonthAgo counter rollOverMonthAgo
     * @param targetSystemValue counter targetSystemValue
     * @param amount amount
     * @return node object to send to API
     * @throws JsonProcessingException JsonException
     */

    private String createMessageBodyForExtendExpiryDateMessage(String subscriptionId, String counterId,
            String counterValue, String expiryDate, String desciption, String rollOverMonthAgo,
            String targetSystemValue,String amount) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();
        ArrayNode counter = jsonObjectMapper.createArrayNode();
        ObjectNode counterUpdateNode = jsonObjectMapper.createObjectNode();
        counterUpdateNode.put("subscription_key", subscriptionId);
        counterUpdateNode.put("counter_id", counterId);
        if (targetSystemValue.equalsIgnoreCase("AddCounter")) {
            counterUpdateNode.put("amount", Long.parseLong(counterValue));
        } else if (targetSystemValue.equalsIgnoreCase("DeductCounter")) {
            counterUpdateNode.put("amount", -Long.parseLong(counterValue));
        } else {
            counterUpdateNode.put("counter_value", Long.parseLong(amount));
        }
        counterUpdateNode.put("counter_expire", expiryDate);
        counterUpdateNode.put("description", desciption);
        if (StringUtils.isNotBlank(rollOverMonthAgo)) {
            counterUpdateNode.put("rollover_month_ago", Integer.parseInt(rollOverMonthAgo));
        }
        counter.add(counterUpdateNode);
        node.set("counter", counter);
        node.put("event_cd", eventCd);
        return jsonObjectMapper.writeValueAsString(node);
    }
}
