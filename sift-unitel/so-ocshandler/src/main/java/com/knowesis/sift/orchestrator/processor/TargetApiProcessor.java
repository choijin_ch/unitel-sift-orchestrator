/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Checks the targetAPI message soMessage header and decide which OCS API should be called
 * 
 * @author SO Development Team
 */

@Component("TargetApiProcessor")
public class TargetApiProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Value("${ocs.targetapi.mapping}")
    private String targetApiString;

    @Autowired
    ObjectMapper jsonObjectMapper;

    private Map<String, String> targetApiMap;

    /**
     * onPostConstruct to get values from property file
     */
    @PostConstruct
    private void onPostConstruct() {
        targetApiMap = new HashMap<>();
        String[] targetApiArray = targetApiString.split(",");
        for (String str : targetApiArray) {
            targetApiMap.put(str.split(":")[0], str.split(":")[1]);
        }
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        log.debug("TargetApiProcessor start->");
        String message = exchange.getIn().getBody(String.class);
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        String targetAPI = SOMessageUtils.getHeaderValue(soMessage, "targetAPI");
        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM", null);
        log.debug("target api recieved from prg handler is {}", targetAPI);
        if (StringUtils.isBlank(targetAPI)) {
            SOFulfillmentMessage soFulfillmentMessage = (SOFulfillmentMessage) soMessage;
            String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
            FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct(soFulfillmentMessage,
                    currentSequence);
            String targetSystem = fulfillmentProduct.getTargetSystem();
            log.info("Target System found for the CURRENT-SEQUENCE fulfillment product: {}", targetSystem);
            String targetSystemValue = targetSystem.split("_")[1];
            if (targetApiMap.keySet().contains(targetSystemValue)) {
                targetAPI = targetApiMap.get(targetSystemValue);
            }
            log.debug("Target api to be accessed -> {}", targetAPI);
        }
        exchange.getIn().setBody(soMessage);
        exchange.getIn().setHeader("targetAPI", targetAPI);
    }
}
