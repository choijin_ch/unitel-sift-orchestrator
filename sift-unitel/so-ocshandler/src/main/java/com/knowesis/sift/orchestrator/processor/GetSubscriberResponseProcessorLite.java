/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Processes Get Subscriber Response
 * 
 * @author SO Development Team
 */
@Component("GetSubscriberResponseProcessorLite")
public class GetSubscriberResponseProcessorLite implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        String response = exchange.getIn().getBody(String.class);
        log.debug("Response message of GetSubscriber {}", response);
        String message = (String) exchange.getProperty("OriginalRequestMessage");
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        String msisdn = SOMessageUtils.getMSISDN(soMessage);
        ObjectNode responseJson = (ObjectNode) jsonObjectMapper.readTree(response);
        String responseCode = responseJson.get("result_code").asText();
        exchange.getIn().removeHeaders("CamelHttp*");
        if (responseCode.equals("0")) {
            String status = responseJson.get("1").get("subscription").get("status").asText();
            String expiry = getExpiryOfStatus(responseJson, status);
            log.debug("subscriber {} has status {} and its expiry {}", new Object[] { msisdn, status, expiry });
            exchange.getIn().setHeader("SubscriberStatus", status);
            exchange.getIn().setHeader("SubscriberExpiry", expiry);
            return;
        }
        exchange.getIn().setHeader("SubscriberStatus", "-1"); // default
    }

    /**
     * Get the current expiry of the subscriber from subscriptionState array in
     * responseJson
     * 
     * @param responseJson getsubscriber response
     * @param status current status of subscriber
     * @return expiry of current status of subscriber
     */
    private String getExpiryOfStatus(ObjectNode responseJson, String status) {
        JsonNode subscriptionState = responseJson.get("1").get("subscriptionState");
        if (subscriptionState.isArray()) {
            for (JsonNode s : subscriptionState) {
                if (s.get("status").asText().equalsIgnoreCase(status)) {
                    return s.get("expire_date") != null ? s.get("expire_date").asText() : "";
                }
            }
        }
        return "";
    }
}
