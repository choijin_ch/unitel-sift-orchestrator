/*
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.commons.CommonFunctions;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.core.domain.Message;
import com.knowesis.sift.orchestrator.core.domain.MessageContainer;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.NominationUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageObjectFactory;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Processes MO requests
 * 
 * @author SO Development Team
 */
@Component("MOProcessor")
public class MOProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(MOProcessor.class);

    @Autowired
    RedisConnectionPool redisConnectionPool;

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("so.actionType.header.name")
    private String actionTypeKeyName;
	   			
    @Value("so.actionTypeOptinReply")			
    private String actionTypeOptinReply;
    
    @Value("${mo.notify.optin.invalidKeyword}")
    private String optinInvalidMessage;

    @Value("${mo.notify.optin.offer.alreadyProvisioned}")
    private String optinOfferTakenMessage;

    @Value("${mo.channel.name}")
    private String channelName;

    @Value("${mo.notify.optin.ssp.inprogress}")
    private String alreadyOptinSspInProgress;

    @Value("${mo.notify.optin.monitoring.expired}")
    private String notInMonitoringPeriodMessage;

    @Value("${so.optin.status.key}")
    private String optinStatus;

    @Value("${so.monitoring.ttl.additionalBufferValue}")
    private int ttlBuffer;

    @Value("${so.triggerType.optinNotify}")
    private String optinNotify;

    @Value("${so.actionType.notify}")
    private String notify;

    @Value("${so.triggerType.optinResponse}")
    private String optinResponse;

    @Value("${so.actionType.optin}")
    private String optin;

    @Value("${so.notification.template}")
    private String soMessageTemplate;

    @Value("${so.actionStatus.success}")
    private String success;

    @Value("${so.actionStatus.failure}")
    private String failure;

    @Value("${mo.optin.actionType}")
    private String actionTypeOptin;

    @Value("${mo.notify.nomination.invalidNumber}")
    private String nominaionInvalidNumber;

    @Value("${mo.notify.nomination.alreadyProvisioned}")
    private String nominationAlreadyProvisioned;

    @Value("${so.fulfill.actionType.nomination}")
    private String nomination;

    @Value("${so.triggerType.nominationStatusNotify}")
    private String nominationStatusNotify;

    @Value("${so.triggerType.nominationStatus}")
    private String nominationStatus;

    @Value("${so.triggerType.doubleConfirmationOptinResponse}")
    private String doubleConfirmationOptinResponse;
    
    @Value("${so.triggerType.doubleConfirmationOptinNotify}")
    private String doubleConfirmationOptinNotify;
    
    @Value("${mo.optin.countrycode}")
    private String countryCode;
    
    @Value("${so.multiOptin.notificationMessage}")
    private String multiOptinNotificationMessage;
    
    @Value("${mo.vod.keyword}")
    private String vodKeyWord;//VODOPTIN
    
    @Value("${mo.notify.reccuringCountReached}")
    private String reccuringCountReachedMessage;
    
	private long aDayInMilliseconds = 86400000;

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        String messageType = (String) exchange.getIn().getHeader("CamelSmppMessageType");
        LOG.debug("SMS MO Message Type: {}", messageType);
        processMobileOriginated(exchange);
    }

    /**
     * Process MO request
     * 
     * @param exchange exchange
     * @throws Exception Exception
     * @throws IOException IOException
     * @throws JsonProcessingException JsonProcessingException
     * @throws JsonParseException JsonParseException
     * @throws JsonMappingException JsonMappingException
     */
    private void processMobileOriginated(Exchange exchange)
            throws Exception, IOException, JsonProcessingException, JsonParseException, JsonMappingException {
        String msisdnWithoutCountryCode = exchange.getIn().getHeader("CamelSmppSourceAddr", String.class);
        LOG.info("MO received from the subscriber with MSISDN: {}", msisdnWithoutCountryCode);
        String msisdn = countryCode + msisdnWithoutCountryCode;
        LOG.info("MSISDN after adding country code: {}", msisdn);
        String userOptedKeyword = exchange.getIn().getBody(String.class);
        LOG.info("User Opted Keyword: {}", userOptedKeyword);
        String optinShortCode = exchange.getIn().getHeader("CamelSmppDestAddr", String.class);
        String camelSmppDestAddr= (String) exchange.getIn().getHeader("CamelSmppDestAddr");
        LOG.debug("CamelSmppDestAddr:{} camelSmppDestAddr (String):{}",optinShortCode,camelSmppDestAddr);
        String cacheKey = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn, userOptedKeyword);
        String soMessageAsJSON = null;
        //SOMessage redisSOMessage=null;
        if (StringUtils.isNotBlank(cacheKey)) {
            soMessageAsJSON = redisConnectionPool.getObjectFromCache(cacheKey);
//            if(soMessageAsJSON!="OFFER-TAKEN"){
//            redisSOMessage=jsonObjectMapper.readValue(soMessageAsJSON, SOMessage.class);
//            }
        }
        LOG.debug("SOMessage Fetched from Cache Key: {}, value: {}", cacheKey, soMessageAsJSON);
        if (soMessageAsJSON == null) {
            whenOptinNotFound(exchange, msisdn, soMessageAsJSON, optinShortCode, userOptedKeyword);
        } else if (soMessageAsJSON.equals("OFFER-TAKEN")) {
            whenOptinOfferTaken(exchange, msisdn, soMessageAsJSON, optinShortCode);
        }
//        } else if (isReccurentFulfills(redisSOMessage)) {
//            whenReccuringCountReached(exchange, msisdn, soMessageAsJSON, optinShortCode, userOptedKeyword);
//        }
            else {
            	whenOptinFound(exchange, soMessageAsJSON, msisdn, cacheKey, optinShortCode);
        }
    }
    
    
    /**
     * offer taken
     * 
     * @param redisSOMessage redisSOMessage
     * @return
     */
   /* private boolean isOfferTaken(SOMessage redisSOMessage) {
		String reccurentCount = SOMessageUtils.getHeaderValue(redisSOMessage, "RECURRING_COUNT");
		String optinStatusField=SOMessageUtils.getHeaderValue(redisSOMessage, optinStatus);
		if(StringUtils.isNotEmpty(optinStatusField)&&optinStatusField.equals("OFFER-TAKEN")){
			if(reccurentCount!="-1"||StringUtils.isEmpty(reccurentCount)){
				return true;
			}
		}
		
    	return false;
	}*/

	/**
     * 
     * Executes when reccuring count is reached
     * 
     * @param exchange exchange
     * @param msisdn msisdn
     * @param soMessageAsJSON soMessageAsJson
     * @param optinShortCode optinShortCode
     * @param userOptedKeyword userOptedKeyword
     * @throws Exception Exception to be thrown
     */
   /* private void whenReccuringCountReached(Exchange exchange, String msisdn, String soMessageAsJSON, String optinShortCode,
            String userOptedKeyword) throws Exception {
        String nominationCacheKey = CacheKeyUtils.generateNominationCacheKey(msisdn, optinShortCode);
        String nominationMessageAsJSON = redisConnectionPool.getObjectFromCache(nominationCacheKey);
        if (StringUtils.isNotBlank(nominationMessageAsJSON)) {
            processNominationFlow(exchange, nominationMessageAsJSON, optinShortCode, userOptedKeyword, msisdn);
        } else {
            String messageToSend = reccuringCountReachedMessage;
            LOG.info("No redis entry for msidn :{}: with keyword :{} ", msisdn, userOptedKeyword);
            if (StringUtils.isNotBlank(messageToSend)) {
                SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
                        soMessageTemplate, msisdn, soMessageAsJSON, messageToSend, channelName, jsonObjectMapper);
                optinNotificationMessage.setTriggerType(optinNotify);
                optinNotificationMessage.setActionType(notify);
                String messageSendToMtHandler = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
                exchange.getIn().setBody(messageSendToMtHandler);
            } else {
                exchange.getIn().setBody(null);
            }
            // TO DO add a new configuration file to add db entry
            exchange.getIn().setHeader("isInvalidKeyword", true);//needtocheck
            exchange.getIn().setHeader("isMOSuccess", false);
        }
    }*/

	/**
	 * 
	 * Executes when optin not found or invalid keyword
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @param soMessageAsJSON
	 *            soMessageAsJson
	 * @param optinShortCode
	 *            optinShortCode
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @throws Exception
	 *             Exception to be thrown
	 */
    private void whenOptinNotFound(Exchange exchange, String msisdn, String soMessageAsJSON, String optinShortCode,
            String userOptedKeyword) throws Exception {
        String nominationCacheKey = CacheKeyUtils.generateNominationCacheKey(msisdn, optinShortCode);
        String nominationMessageAsJSON = redisConnectionPool.getObjectFromCache(nominationCacheKey);
        String multiLevelOptinKey = CacheKeyUtils.generateMultiLevelOptinCacheKey(optinShortCode, msisdn);
        String multiLevelMessageAsJSON = redisConnectionPool.getObjectFromCache(multiLevelOptinKey);
        if (StringUtils.isNotBlank(nominationMessageAsJSON)) {
            processNominationFlow(exchange, nominationMessageAsJSON, optinShortCode, userOptedKeyword, msisdn);
        } else if(StringUtils.isNotBlank(multiLevelMessageAsJSON) && userOptedKeyword.length() >= 8){
        	processMultiLevelOptinScenario(exchange, multiLevelMessageAsJSON, optinShortCode, userOptedKeyword, msisdn, multiLevelOptinKey);
        	
        } else {
            String messageToSend = optinInvalidMessage;
            LOG.info("No redis entry for msidn :{}: with keyword :{} ", msisdn, userOptedKeyword);
            if (StringUtils.isNotBlank(messageToSend)) {
                SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
                        soMessageTemplate, msisdn, soMessageAsJSON, messageToSend, channelName, jsonObjectMapper);
                Long currentTime = Instant.now().toEpochMilli();optinNotificationMessage.setTriggerType(optinNotify);
                optinNotificationMessage.setActionType(notify);
                optinNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getNotificationMessages().get(0).setShortCode(optinShortCode);
                optinNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).getNotificationMessages().get(0).setOptInKeyWord(userOptedKeyword);
                optinNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0).setEventTimeStamp(String.valueOf(currentTime));
                String messageSendToMtHandler = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
                exchange.getIn().setBody(messageSendToMtHandler);
            } else {
                exchange.getIn().setBody(null);
            }
            // TO DO add a new configuration file to add db entry
            exchange.getIn().setHeader("isInvalidKeyword", true);
            exchange.getIn().setHeader("isMOSuccess", false);
        }
    }

	/**
	 * process multilevel optin key
	 * 
	 * @param exchange
	 *            exchange
	 * @param multiLevelMessageAsJSON
	 *            multiLevelMessageAsJSON
	 * @param optinShortCode
	 *            optinShortCode
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @param msisdn
	 *            msisdn
	 * @param multiLevelOptinKey
	 *            multiLevelOptinKey
	 */
	private void processMultiLevelOptinScenario(Exchange exchange, String multiLevelMessageAsJSON,
			String optinShortCode, String userOptedKeyword, String msisdn, String multiLevelOptinKey) {
		SOMessage soMessage = RawMessageUtils.processRawMessage(multiLevelMessageAsJSON, jsonObjectMapper);
		try {
			if (isMOInProcessing(soMessage)) {
				SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
						soMessageTemplate, msisdn, multiLevelMessageAsJSON, alreadyOptinSspInProgress, channelName,
						jsonObjectMapper);
				optinNotificationMessage.setTriggerType(optinNotify);
				optinNotificationMessage.setActionType(notify);
				optinNotificationMessage.setActionStatus(success);
				String finalMessageAsJson = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
				exchange.getIn().setHeader("messageSendToDbHandler",
						createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure));
				exchange.getIn().setHeader("isMOSuccess", false);
				exchange.getIn().setHeader("isInvalidKeyword", false);
				exchange.getIn().setBody(finalMessageAsJson);
			} else {
				checkValidMonitoringPeriod(exchange, msisdn, multiLevelMessageAsJSON, multiLevelOptinKey, soMessage,
						userOptedKeyword, optinShortCode);
			}
		} catch (Exception exception) {
			LOG.info("Exception occured during processing is: {} ", exception);
		}
	}  

	/**
     * 
     * Processes nomination work flow
     * 
     * @param exchange exchange
     * @param nominationMessageAsJSON nominationMessageAsJSON
     * @param optinShortCode optinShortCode
     * @param userOptedKeyword userOptedKeyword
     * @param msisdn msisdn
     * @throws IOException exception
     */
    // method too large for review. Recommended length is 50-60
    // method complexity too high to review, recommended value is 5
	private void processNominationFlow(Exchange exchange, String nominationMessageAsJSON, String optinShortCode,
			String userOptedKeyword, String msisdn) throws IOException {
		SOMessage soMessage = RawMessageUtils.processRawMessage(nominationMessageAsJSON, jsonObjectMapper);
		int totalNominationCount = Integer.parseInt(SOMessageUtils.getHeaderValue(soMessage, "TOTAL-NOMINATION-COUNT"));
		// Unhandled Numberformat exception
		int currentNominationCount = Integer
				.parseInt(SOMessageUtils.getHeaderValue(soMessage, "CURRENT-NOMINAION-COUNT"));
		// Unhandled Numberformat exception
		LOG.info("For MSISDN: {}, Total nomination count: {} and Current Nomination count: {}.", msisdn,
				totalNominationCount, currentNominationCount);
		exchange.getIn().setBody(null);
		String nominationAcionStatus = failure;
		if (userOptedKeyword.length() != 8) {
			LOG.info("User opted nominee number length is not 8. Sending nomination failed SMS to nominator {}",
					msisdn);
			SONotificationMessage nominationInvalidMessage = SOMessageObjectFactory.createSONotificationMessage(
					soMessageTemplate, msisdn, nominationMessageAsJSON, nominaionInvalidNumber, "SMS",
					jsonObjectMapper);
			nominationInvalidMessage.setTriggerType(nominationStatusNotify);
			nominationInvalidMessage.setActionType(notify);
			nominationInvalidMessage.setActionStatus(failure);
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(nominationInvalidMessage));
		} else if (currentNominationCount >= totalNominationCount) {
			LOG.info("Nomination Count is not available sending nomination failed SMS to nominator {}", msisdn);
			SONotificationMessage nominationProvisionedMessage = SOMessageObjectFactory.createSONotificationMessage(
					soMessageTemplate, msisdn, nominationMessageAsJSON, nominationAlreadyProvisioned, "SMS",
					jsonObjectMapper);
			nominationProvisionedMessage.setTriggerType(nominationStatusNotify);
			nominationProvisionedMessage.setActionType(notify);
			nominationProvisionedMessage.setActionStatus(failure);
			exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(nominationProvisionedMessage));
		}
		// Handle Case 5 above
		else if (currentNominationCount < totalNominationCount) {
			nominationAcionStatus = success;
			// Remove or use msisdn in logs or NDC/MDC
			LOG.info("Nomination count is available sending register actions to PRG handler");
			String userOptedKeywordWithCountryCode = countryCode + userOptedKeyword;
			SOMessageUtils.setHeaderValue(soMessage, "subscriberOptinTimestamp", SOMessageUtils.getUnixTimestamp());
			SOMessageUtils.setHeaderValue(soMessage, "NOMINEE-NUMBER", userOptedKeywordWithCountryCode);
			SOMessageUtils.setHeaderValue(soMessage, "NOMINATOR-NUMBER", msisdn);
			SOMessageUtils.setHeaderValue(soMessage, "NOMINATION-SHORTCODE", optinShortCode);
			SOMessageUtils.setHeaderValue(soMessage, "CURRENT-NOMINAION-COUNT", "" + (++currentNominationCount));
			// To handler nomination case at prg-handler
			soMessage.setActionType(nomination);
			String soMessageAsJson = jsonObjectMapper.writeValueAsString(soMessage);
			LOG.debug("-----> aDayInMilliseconds:{}",aDayInMilliseconds);
			int updatedValidityTime = CommonFunctions.getRedisValidityForNomination(soMessage);
			String nominatorCacheKey = CacheKeyUtils.generateNominationCacheKey(msisdn, optinShortCode);
			LOG.info("Updated TTL for nomination cache key {} after reducing CURRET-NOMINATION-COUNT is {}",
					nominatorCacheKey, updatedValidityTime);
			// negative values not handled
			redisConnectionPool.putInCache(nominatorCacheKey, soMessageAsJson, updatedValidityTime);
			exchange.getIn().setHeader("sendRegisterAction", true);
			exchange.getIn().setBody(soMessageAsJson);
		}
		soMessage.setActionResponse(userOptedKeyword);
		soMessage.setTriggerType(nominationStatus);
		soMessage.setActionType(nominationStatus);
		soMessage.setActionStatus(nominationAcionStatus);
		LOG.debug("soMessage where text tobe send is setting as null:{}", soMessage.toString());
		MessageContainer container = new MessageContainer();
		ArrayList<Message> arr = new ArrayList<Message>();
		Message msg = new Message();
		msg.setTextTobeSent(null);
		arr.add(msg);
		container.setMessages(arr);
		((SOFulfillmentMessage) soMessage).getMessage().getRequesterLocation().getLocations().get(0).getFulfilments()
				.get(0).getFulfillmentMessages().add(container);
		// ((SOFulfillmentMessage)
		// soMessage).getMessage().getRequesterLocation().getLocations().get(0).getFulfilments()
		// .get(0).getFulfillmentMessages().get(0).getMessages().get(0).setTextTobeSent(null);
		LOG.debug("soMessage where text tobe send is setting as null:{}", soMessage.toString());
		exchange.getIn().setHeader("nominationStatusDbMessage", jsonObjectMapper.writeValueAsString(soMessage));
		exchange.getIn().setHeader("isNomination", true);
		exchange.getIn().setHeader("isMOSuccess", false);
	}

	/**
	 * Executes when optin for already taken offer
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @param soMessageAsJSON
	 *            soMessageAsJSON
	 * @param optinShortCode
	 *            short code received as part of MO
	 * @throws Exception
	 *             Exception
	 */
	private void whenOptinOfferTaken(Exchange exchange, String msisdn, String soMessageAsJSON, String optinShortCode)
			throws Exception {
		String messageToSend = optinOfferTakenMessage;
		String userOptedKeyword = exchange.getIn().getBody(String.class);
		// remove such logs or use msisdn or NDC/MDC
		LOG.debug("Invalid Keyword: {}", userOptedKeyword);
		SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
				soMessageTemplate, msisdn, soMessageAsJSON, messageToSend, channelName, jsonObjectMapper);
		optinNotificationMessage.setMetaInfo(SOMessageObjectFactory.createDefaultMetaInfo(channelName));
		optinNotificationMessage.getMetaInfo()
				.setHeaders(SOMessageObjectFactory.createDefaultHeader(actionTypeKeyName, actionTypeOptinReply));
		String messageToMtHandler = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
		createOptinResponseMessageForDb(messageToMtHandler, userOptedKeyword, failure);
		exchange.getIn().setHeader("messageSendToDbHandler",
				createOptinResponseMessageForDb(messageToMtHandler, userOptedKeyword, failure));
		// Above is not recommended ! Please read javadoc
		if (StringUtils.isNotBlank(messageToSend)) {
			optinNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
					.getNotificationMessages().get(0).getMessages().get(0).setTextTobeSent(messageToSend);
			LOG.debug("messageToMtHandler: {}", messageToMtHandler);
			exchange.getIn().setBody(messageToMtHandler);
		} else {
			exchange.getIn().setBody(null);
		}
		exchange.getIn().setHeader("isInvalidKeyword", false);
		exchange.getIn().setHeader("isMOSuccess", false);
	}

	/**
	 * When optin message is not expired
	 * 
	 * @param exchange
	 *            camel exchange
	 * @param soMessageAsJSON
	 *            SOMessage in JSON
	 * @param msisdn
	 *            Mobile Number
	 * @param cacheKeyToRemove
	 *            key to remove
	 * @param optinShortCode
	 *            short code received as part of MO
	 * @throws Exception
	 *             if any
	 */
	private void whenOptinFound(Exchange exchange, String soMessageAsJSON, String msisdn, String cacheKeyToRemove,
			String optinShortCode) throws Exception {
		String userOptedKeyword = exchange.getIn().getBody(String.class);
		userOptedKeyword = userOptedKeyword != null ? userOptedKeyword.trim().toUpperCase() : null;
		SOMessage soMessage = RawMessageUtils.processRawMessage(soMessageAsJSON, jsonObjectMapper);
		LOG.info("Redis Entry found for msisdn: {} ,userResponse {}", msisdn, userOptedKeyword);
		if (validateUserResponse(soMessage, userOptedKeyword, optinShortCode, "SMS")) {
			onReceivingValidOptinInMO(exchange, msisdn, soMessageAsJSON, cacheKeyToRemove, soMessage, userOptedKeyword,
					optinShortCode);
		} else {
			onReceivingInValidOptinInMO(exchange, msisdn, soMessageAsJSON, userOptedKeyword, soMessage);
		}
	}

	/**
	 * 
	 * Checks whether the offer is being processed
	 * 
	 * @param exchange
	 *            Camel Exchange
	 * @param msisdn
	 *            Mobile
	 * @param soMessageAsJSON
	 *            soMessageAsJSON
	 * @param cacheKeyToRemove
	 *            key to remove
	 * @param soMessage
	 *            SONotificationMessage
	 * @param userOptedKeyword
	 *            SMS text that user send for OPT-IN
	 * @param optinShortCode
	 *            short code received as part of MO
	 * @throws IOException
	 *             ioException
	 * @throws JsonProcessingException
	 *             parse exception
	 * @throws Exception
	 *             Exception
	 */
    private void onReceivingValidOptinInMO(Exchange exchange, String msisdn, String soMessageAsJSON,
            String cacheKeyToRemove, SOMessage soMessage, String userOptedKeyword, String optinShortCode)
            throws JsonProcessingException, IOException, Exception {
        if (isMOInProcessing(soMessage)) {
            SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
                    soMessageTemplate, msisdn, soMessageAsJSON, alreadyOptinSspInProgress, channelName,
                    jsonObjectMapper);
            optinNotificationMessage.setTriggerType(optinNotify);
            optinNotificationMessage.setActionType(notify);
            optinNotificationMessage.setActionStatus(success);
            String finalMessageAsJson = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
            exchange.getIn().setHeader("messageSendToDbHandler",
                    createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure));
            exchange.getIn().setHeader("isMOSuccess", false);
            exchange.getIn().setHeader("isInvalidKeyword", false);
            exchange.getIn().setBody(finalMessageAsJson);
        } else {
            checkValidMonitoringPeriod(exchange, msisdn, soMessageAsJSON, cacheKeyToRemove, soMessage, userOptedKeyword,
                    optinShortCode);
        }
    }

	/**
	 * Checks whether the offer is being processed
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return true/false
	 */
    public boolean isMOInProcessing(SOMessage soMessage) {
        String optinStat = SOMessageUtils.getHeaderValue(soMessage, optinStatus);
        if (optinStat != null && optinStat.equals("PROCESSING")) {
            return true;
        } else {
            return false;
        }
    }
    
	/**
	 * Checks whether the offer is being processed
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return true/false
	 */
	public boolean isReccurentFulfills(SOMessage soMessage) {
		if (StringUtils.isNoneBlank(SOMessageUtils.getHeaderValue(soMessage, "OPTIN-COUNT"))) {
			int numberOfSuccessfulOptin = Integer.parseInt(SOMessageUtils.getHeaderValue(soMessage, "OPTIN-COUNT"));
			int reccurentValue = Integer.parseInt(SOMessageUtils.getHeaderValue(soMessage, "RECURRING_COUNT"));
			if (reccurentValue != -1 && numberOfSuccessfulOptin == reccurentValue) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	/**
	 * Checks whether the optin is in valid monitoring period
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @param soMessageAsJSON
	 *            soMessageAsJSON
	 * @param cacheKeyToRemove
	 *            cacheKeyToRemove
	 * @param soMessage
	 *            soMessage
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @param optinShortCode
	 *            optinShortCode
	 * @throws JsonProcessingException
	 *             Exception
	 * @throws IOException
	 *             Exception
	 * @throws Exception
	 *             Exception
	 */
    public void checkValidMonitoringPeriod(Exchange exchange, String msisdn, String soMessageAsJSON,
            String cacheKeyToRemove, SOMessage soMessage, String userOptedKeyword, String optinShortCode)
            throws JsonProcessingException, IOException, Exception {
        if (isMOInValidTime(soMessage)) {
            onReceivingValidMonitoringPeriodInMO(exchange, msisdn, cacheKeyToRemove, soMessage, userOptedKeyword,
                    optinShortCode);
        } else { // Not in Monitoring period
            SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
                    soMessageTemplate, msisdn, soMessageAsJSON, notInMonitoringPeriodMessage, channelName,
                    jsonObjectMapper);
            optinNotificationMessage.setTriggerType(optinNotify);
            optinNotificationMessage.setActionType(notify);
            optinNotificationMessage.setActionStatus(success);
            String finalMessageAsJson = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
            exchange.getIn().setHeader("messageSendToDbHandler",
                    createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure));
            exchange.getIn().setHeader("isMOSuccess", false);
            exchange.getIn().setHeader("isInvalidKeyword", false);
            exchange.getIn().setBody(finalMessageAsJson);
        }
    }

	/**
	 * Handles optin valid keyword case
	 * 
	 * @param exchange
	 *            Camel Exchange
	 * @param msisdn
	 *            Mobile
	 * @param cacheKeyToRemove
	 *            key to remove
	 * @param soMessage
	 *            SONotificationMessage
	 * @param userOptedKeyword
	 *            SMS text that user send for OPT-IN
	 * @param optinShortCode
	 *            short code received as part of MO
	 * @throws IOException
	 *             ioException
	 * @throws JsonProcessingException
	 *             parse exception
	 * @throws Exception
	 *             Exception
	 */
	private void onReceivingValidMonitoringPeriodInMO(Exchange exchange, String msisdn, String cacheKeyToRemove,
			SOMessage soMessage, String userOptedKeyword, String optinShortCode)
			throws IOException, JsonProcessingException, Exception {
		// for double confirmation ask customer to optin with subscriberId
		if (isMoOptinOfferType(msisdn, soMessage, userOptedKeyword)) {
			LOG.debug("MoOPtin For VOD");
			processForMoOptinOfferType(exchange, soMessage, cacheKeyToRemove, msisdn, optinShortCode, userOptedKeyword);
		} else {
			setTriggerTypeWithRespectToOptinLevel(soMessage);
			soMessage.setActionType(actionTypeOptin);
			soMessage.setActionStatus(success);
			soMessage.setActionResponse(userOptedKeyword);
			//set actionResponse as firstlevel optin key : second level key
			String firstLevelUserOptedKeyword = SOMessageUtils.getHeaderValue(soMessage, "userOptedKeyword");
			if (firstLevelUserOptedKeyword != null) {
				String updatedKeyWord = firstLevelUserOptedKeyword+"-"+userOptedKeyword;
				soMessage.setActionResponse(updatedKeyWord);
			}
			// To be sent to Core
			SOMessageUtils.setHeaderValue(soMessage, "subscriberOptinTimestamp", SOMessageUtils.getUnixTimestamp());
			SOMessageUtils.setHeaderValue(soMessage, "subscriberOptinChannel", "SMS");
			LOG.debug("SOMessage on receiving a valid keyword in MO: {}", soMessage);
			String messageToDB = jsonObjectMapper.writeValueAsString(soMessage);
			LOG.debug("Message to be send after valid MO: {}", messageToDB);
			soMessage.setActionType(optin);
			String messageToPRG = jsonObjectMapper.writeValueAsString(soMessage);
			exchange.getIn().setHeader("messageToPRG", messageToPRG);
			exchange.getIn().setHeader("isMOSuccess", true);
			exchange.getIn().setHeader("isInvalidKeyword", false);
			exchange.getIn().setBody(messageToDB);
			LOG.debug("Redis object after receiving a valid MO: {}", cacheKeyToRemove);
			swapFulfillmentOptinToProcessing(cacheKeyToRemove, msisdn, optinShortCode, userOptedKeyword);
			// Check this condition
			if (soMessage instanceof SOFulfillmentMessage) {
				exchange.getIn().setHeader("offerAlreadyProvisionedByCore", true);
			}
		}
	}

	/**
	 * Process For MoOptinOfferType
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 * @param cacheKeyToRemove
	 *            cacheKeyToRemove
	 * @param msisdn
	 *            msisdn
	 * @param optinShortCode
	 *            optinShortCode
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 */
	private void processForMoOptinOfferType(Exchange exchange, SOMessage soMessage, String cacheKeyToRemove,
			String msisdn, String optinShortCode, String userOptedKeyword) {
		try {
			soMessage.setTriggerType(optinResponse);
			soMessage.setActionType(actionTypeOptin);
			soMessage.setActionStatus(success);
			soMessage.setActionResponse(userOptedKeyword);
			SOMessageUtils.setHeaderValue(soMessage, "subscriberOptinTimestamp", SOMessageUtils.getUnixTimestamp());
			SOMessageUtils.setHeaderValue(soMessage, "subscriberOptinChannel", "SMS");
			LOG.debug("SOMessage on receiving a valid keyword in MO: {}", soMessage);
			String messageToDB = jsonObjectMapper.writeValueAsString(soMessage);
			LOG.debug("Message to be send after valid MO: {}", messageToDB);
			SONotificationMessage doubleConfirmationNotificationMessage = setMultiOptinParameters(exchange, msisdn,
					optinShortCode, soMessage, userOptedKeyword);
			exchange.getIn().setHeader("messageToMT",
					jsonObjectMapper.writeValueAsString(doubleConfirmationNotificationMessage));
			exchange.getIn().setHeader("isMOSuccess", true);
			exchange.getIn().setHeader("isInvalidKeyword", false);
			exchange.getIn().setBody(messageToDB);
			swapFulfillmentOptinToProcessing(cacheKeyToRemove, msisdn, optinShortCode, userOptedKeyword);
			// Check this condition
			if (soMessage instanceof SOFulfillmentMessage) {
				exchange.getIn().setHeader("offerAlreadyProvisionedByCore", true);
			}
		} catch (Exception exception) {
			LOG.debug("Exception occured is {}", exception);
		}
	}

	/**
	 * Set triggerType
	 * 
	 * @param soMessage
	 *            soMessage
	 */
	private void setTriggerTypeWithRespectToOptinLevel(SOMessage soMessage) {
		String checkHeader = SOMessageUtils.getHeaderValue(soMessage, "DOUBLECONFIRMATION");
		if (StringUtils.isNotBlank(checkHeader)) {
			soMessage.setTriggerType(doubleConfirmationOptinResponse);
		} else {
			soMessage.setTriggerType(optinResponse);
		}
	}

	/**
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @param optinShortCode
	 * @param soMessage
	 *            soMessage
	 * @param optinShortCode
	 *            optinShortCode
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @return soNotificationMessageForMultiOptin
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	private SONotificationMessage setMultiOptinParameters(Exchange exchange, String msisdn, String optinShortCode, SOMessage soMessage, String userOptedKeyword)
			throws JsonProcessingException {
		LOG.debug("set SOMessage For Double Confirmation");
		soMessage.setTriggerType(doubleConfirmationOptinNotify);
		soMessage.setActionType(notify);
		soMessage.setActionResponse(null);
		SONotificationMessage soNotificationMessageForMultiOptin = (SONotificationMessage) soMessage;
		String optinKeyWord = SOMessageUtils.getOptinKeyword(soNotificationMessageForMultiOptin);
		SOMessageUtils.setHeaderValue(soNotificationMessageForMultiOptin, "OptinKeyWord", optinKeyWord);
		soNotificationMessageForMultiOptin.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
				.getNotificationMessages().get(0).setOptInKeyWord(null);
		String messageTosendForDoubleConfirmation=getMessageForDoubleConfirmation(soMessage);
		soNotificationMessageForMultiOptin.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
				.getNotificationMessages().get(0).getMessages().get(0).setTextTobeSent(messageTosendForDoubleConfirmation);
		SOMessageUtils.setHeaderValue(soNotificationMessageForMultiOptin, "DOUBLECONFIRMATION", "true");
		SOMessageUtils.setHeaderValue(soNotificationMessageForMultiOptin, "userOptedKeyword", userOptedKeyword);
		Long currentTime = Instant.now().toEpochMilli();
		String endDate = soNotificationMessageForMultiOptin.getMessage().getRequesterLocation().getLocations().get(0).getOffers()
				.get(0).getOfferEndDate();
		int optinTTL = CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) + ttlBuffer;
		String processingKey = CacheKeyUtils.generateMultiLevelOptinCacheKey(optinShortCode, msisdn);
		String redisValueOfMultiOptinKeyWord = jsonObjectMapper.writeValueAsString(soNotificationMessageForMultiOptin);
		redisConnectionPool.putInCache(processingKey, redisValueOfMultiOptinKeyWord, optinTTL);//set redis key for multilevel optin
		return soNotificationMessageForMultiOptin;
	}

	/**
	 * set message for double confirmation
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return message getmessage from payload else from config
	 */
	private String getMessageForDoubleConfirmation(SOMessage soMessage) {
		Map<String, String> offerPayload = SOMessageUtils.getOfferPayLoad(soMessage);
		if (offerPayload.get("VODOPTINMESSAGE") != null) {
			return offerPayload.get("VODOPTINMESSAGE");
		} else
			return multiOptinNotificationMessage;
	}

	/**
	 * Handles invalid optin
	 * 
	 * @param exchange
	 *            exchange
	 * @param msisdn
	 *            msisdn
	 * @param soMessageAsJSON
	 *            soMessageAsJSON
	 * @param soMessage
	 *            fetch from cache
	 * @param userOptedKeyword
	 *            userOptedKeyword
	 * @throws Exception
	 *             Exception
	 */
	private void onReceivingInValidOptinInMO(Exchange exchange, String msisdn, String soMessageAsJSON,
			String userOptedKeyword, SOMessage soMessage) throws Exception {
		SONotificationMessage optinNotificationMessage = SOMessageObjectFactory.createSONotificationMessage(
				soMessageTemplate, msisdn, soMessageAsJSON, optinInvalidMessage, channelName, jsonObjectMapper);
		LOG.debug("Invalid Optin, Incorrect values");
		optinNotificationMessage.setTriggerType(optinNotify);
		optinNotificationMessage.setActionType(notify);
		optinNotificationMessage.setActionStatus(success);
		String finalMessageAsJson = jsonObjectMapper.writeValueAsString(optinNotificationMessage);
		exchange.getIn().setHeader("messageSendToDbHandler",
				createOptinResponseMessageForDb(finalMessageAsJson, userOptedKeyword, failure));
		exchange.getIn().setHeader("isMOSuccess", false);
		exchange.getIn().setHeader("isInvalidKeyword", false);
		exchange.getIn().setBody(finalMessageAsJson);
	}

	/**
	 * Swapping soMessage from optin to processing pool
	 * 
	 * @param optinCacheKey
	 *            redis key used to save optin message
	 * @param msisdn
	 *            mobile number
	 * @param optinShortCode
	 *            short code received as part of MO
	 * @param userOptedKeyword
	 *            keyword by user
	 * @throws Exception
	 *             Exception
	 */
	private void swapFulfillmentOptinToProcessing(String optinCacheKey, String msisdn, String optinShortCode,
			String userOptedKeyword) throws Exception {
		String objectToMoveToProcessing = redisConnectionPool.getObjectFromCache(optinCacheKey);
		SONotificationMessage soNotificationMessage = jsonObjectMapper.readValue(objectToMoveToProcessing,
				SONotificationMessage.class);
		SOMessageUtils.setHeaderValue(soNotificationMessage, optinStatus, "PROCESSING");
		String optinCount = SOMessageUtils.getHeaderValue(soNotificationMessage, "OPTIN-COUNT");
		if (StringUtils.isNotEmpty(optinCount)) {
			String updatedCount = Integer.toString(
					Integer.parseInt(SOMessageUtils.getHeaderValue(soNotificationMessage, "OPTIN-COUNT")) + 1);
			SOMessageUtils.setHeaderValue(soNotificationMessage, "OPTIN-COUNT", updatedCount);
		}
		// set optinkeyword by replacing subscriberid for multilevel
		if (StringUtils.isNotBlank(SOMessageUtils.getHeaderValue(soNotificationMessage, "OptinKeyWord"))
				&& !isMoOptinOfferType(msisdn, soNotificationMessage, userOptedKeyword)) {
			soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers().get(0)
					.getNotificationMessages().get(0)
					.setOptInKeyWord(SOMessageUtils.getHeaderValue(soNotificationMessage, "OptinKeyWord"));
		}
		// set userOptedKeyword header
		if (isMoOptinOfferType(msisdn, soNotificationMessage, userOptedKeyword)) {
			SOMessageUtils.setHeaderValue(soNotificationMessage, "userOptedKeyword", userOptedKeyword);
		}
		objectToMoveToProcessing = jsonObjectMapper.writeValueAsString(soNotificationMessage);

		String processingKey = CacheKeyUtils.generateOptinCacheKeyForProcessingFulfillment(msisdn,
		SOMessageUtils.getOfferId(soNotificationMessage));
		updateRedisToInProgress(msisdn, soNotificationMessage, userOptedKeyword, optinCacheKey, processingKey,
					optinShortCode, objectToMoveToProcessing);
	}

	/**
	 * 
	 * @param msisdn msisdn
	 * @param soNotificationMessage soNotificationMessage
	 * @param userOptedKeyword userOptedKeyword
	 * @param optinCacheKey optinCacheKey
	 * @param processingKey processingKey
	 * @param optinShortCode optinShortCode
	 * @param objectToMoveToProcessing objectToMoveToProcessing
	 */
	private void updateRedisToInProgress(String msisdn, SONotificationMessage soNotificationMessage,
			String userOptedKeyword, String optinCacheKey, String processingKey, String optinShortCode,
			String objectToMoveToProcessing) {
		if (StringUtils.isNotBlank(objectToMoveToProcessing)) {
			// TTL = offer end date - current time
			Long currentTime = Instant.now().toEpochMilli();
			String endDate = soNotificationMessage.getMessage().getRequesterLocation().getLocations().get(0).getOffers()
					.get(0).getOfferEndDate();
			int optinTTL = CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) + ttlBuffer;
			if (!isMoOptinOfferType(msisdn, soNotificationMessage, userOptedKeyword)) {
				redisConnectionPool.putInCache(processingKey, objectToMoveToProcessing, optinTTL);
				// print ttl in logs
				LOG.debug("Redis: Copied SOMessage from key {} to key {}.", optinCacheKey, processingKey);
			}
			String optinKeyword = SOMessageUtils.getOptinKeyword(soNotificationMessage);
			// update secondlevel key to processing if optin is second level
			if (StringUtils.isNotBlank(SOMessageUtils.getHeaderValue(soNotificationMessage, "DOUBLECONFIRMATION"))) {
				redisConnectionPool.putInCache(optinCacheKey, objectToMoveToProcessing, optinTTL);
				optinKeyword = SOMessageUtils.getHeaderValue(soNotificationMessage, "OptinKeyWord");
			}
			updateOptinRedisKey(optinKeyword, optinShortCode, msisdn, objectToMoveToProcessing, optinTTL,
					optinCacheKey);
		}
	}

	/**
	 * updateOptinRedisKey
	 * 
	 * @param optinKeyword optinKeyword
	 * @param optinShortCode ShortCode
	 * @param msisdn msisdn
	 * @param objectToMoveToProcessing objectToMoveToProcessing
	 * @param optinTTL optinTTL
	 * @param optinCacheKey optinCacheKey
	 */
	private void updateOptinRedisKey(String optinKeyword, String optinShortCode, String msisdn,
			String objectToMoveToProcessing, int optinTTL,String optinCacheKey) {
		String[] keywords = optinKeyword.split(",");
		LOG.debug("keyword to be shown: {}", keywords[0]);
		for (int i = 0; i < keywords.length; i++) {
			optinCacheKey = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn, keywords[i].toString());
			redisConnectionPool.putInCache(optinCacheKey, objectToMoveToProcessing, optinTTL);
		}
	}

	/**
     * check wether the optin keyword belongs to VOD
     * 
     * @param msisdn msisdn
     * @param soMessage soMessage
     * @param userOptedKeyword userOptedKeyword
     * @return true/false
     */
	private boolean isMoOptinOfferType(String msisdn, SOMessage soMessage, String userOptedKeyword) {
		Map<String, String> offerPayload = SOMessageUtils.getOfferPayLoad(soMessage);
		String optinOfferTypeValue = offerPayload.get("OptinOffer_Type");
		if (optinOfferTypeValue != null) {
			String[] optinOfferTypeValueArray = optinOfferTypeValue.split(",");
			Map<String, String> optinOfferMap = new HashMap<String, String>();
			for (String optinOfferTypeValueArrayValue : optinOfferTypeValueArray) {
				String[] optinOfferTypeMapValue = optinOfferTypeValueArrayValue.split(":");
				optinOfferMap.put(optinOfferTypeMapValue[1], optinOfferTypeMapValue[0]);
			}
			if (optinOfferMap.get(vodKeyWord) != null && optinOfferMap.get(vodKeyWord).equals(userOptedKeyword)) {
				return true;
			}
		}
		return false;
	}

    /**
     * 
     * Validate whether optin in success or not
     * 
     * @param soMessage message from cache
     * @param optinKeyword optinKeyword received from SMS
     * @param optinShortCode destination address of SMS
     * @param optinChannel SMS
     * @return true/false
     */
    private static boolean validateUserResponse(SOMessage soMessage, String optinKeyword, String optinShortCode,
            String optinChannel) {
        LOG.debug("SMS MO Cache Message: {} optinKeyword: {}, optinShortCode: {}, optinChannel: {}", soMessage,
                optinKeyword, optinShortCode, optinChannel);
        String optinKeywords = SOMessageUtils.getOptinKeyword(soMessage);
        String[] keywords = optinKeywords.split(",");
        for (int i = 0; i < keywords.length; i++) {
            if (SOMessageUtils.getOptinChannel(soMessage).equals(optinChannel)
                    && keywords[i].toString().equalsIgnoreCase(optinKeyword)
                    && SOMessageUtils.getOptinShortCode(soMessage).equals(optinShortCode)) {
                LOG.debug("MO OPTIN Success");
                return true;
            }
        }
        LOG.debug("MO OPTIN Failed");
        return false;
    }

	/**
	 * 
	 * Checks whether the current time is in between offer start and end dates
	 * 
	 * @param soMessage
	 *            soNotificationMessage
	 * @return true/false
	 */
	private static boolean isMOInValidTime(SOMessage soMessage) {
		long startTime = Long.parseLong(SOMessageUtils.getOfferStartDate(soMessage));
		long endTime = Long.parseLong(SOMessageUtils.getOfferEndDate(soMessage));
		Calendar calendarStart = Calendar.getInstance();
		calendarStart.setTimeInMillis(startTime);
		Calendar calendarEnd = Calendar.getInstance();
		calendarEnd.setTimeInMillis(endTime);
		Calendar calendarNow = Calendar.getInstance();
		// Get the time in local timezone
		int hour = calendarNow.get(Calendar.HOUR_OF_DAY);
		int minutes = calendarNow.get(Calendar.MINUTE);
		int seconds = calendarNow.get(Calendar.SECOND);
		// create time zone object with UTC and set the timezone
		TimeZone tzone = TimeZone.getTimeZone("UTC");
		calendarNow.setTimeZone(tzone);
		// Set the current time as GMT Time
		calendarNow.set(Calendar.HOUR_OF_DAY, hour);
		calendarNow.set(Calendar.MINUTE, minutes);
		calendarNow.set(Calendar.SECOND, seconds);
		long epochNow = calendarNow.getTimeInMillis();
		calendarNow.setTimeInMillis(epochNow);
		LOG.debug("Calender Start:{} Calender End: {} Calender Now: {}", calendarStart, calendarEnd, calendarNow);
		boolean result = calendarNow.after(calendarStart) && calendarNow.before(calendarEnd);
		LOG.info("isMOInValidTime: {}", result);
		return result;
	}

	/**
	 * 
	 * This message is to set triggerType, actionType,actionStatus and
	 * actionResponse for OPTINCASE for sending to DB
	 * 
	 * @param messageAsJson
	 *            message created for sending optin notify message
	 * @param userOptedKeyword
	 *            text sent by the user
	 * @param actionStatus
	 *            success/failure
	 * @return Somessage as json for sending to DB handler
	 * @throws JsonProcessingException
	 *             JsonProcessingException
	 */
	private String createOptinResponseMessageForDb(String messageAsJson, String userOptedKeyword, String actionStatus)
			throws JsonProcessingException {
		SOMessage soMessageForDB = RawMessageUtils.processRawMessage(messageAsJson, jsonObjectMapper);
		SOMessageUtils.setHeaderValue(soMessageForDB, "ACTION_STATUS", actionStatus);
		soMessageForDB.setActionStatus(actionStatus);
		SOMessageUtils.setHeaderValue(soMessageForDB, "TRIGGER_TYPE", optinResponse);
		soMessageForDB.setTriggerType(optinResponse);
		SOMessageUtils.setHeaderValue(soMessageForDB, "ACTION_TYPE", actionTypeOptin);
		soMessageForDB.setActionType(actionTypeOptin);
		SOMessageUtils.setHeaderValue(soMessageForDB, "ACTION_RESPONSE", userOptedKeyword);
		return jsonObjectMapper.writeValueAsString(soMessageForDB);
	}
}
