/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("IptvRequestProcessor")
public class IptvRequestProcessor implements Processor {

    private Logger log = LoggerFactory.getLogger(IptvRequestProcessor.class);
    
    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
    	String message = exchange.getIn().getBody(String.class);
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        String msisdn = SOMessageUtils.getMSISDN(soMessage);
        String subsciptionId = SOMessageUtils.getSubscriptionId(soMessage);
        String addId = SOMessageUtils.getTextToBeSent(soMessage);
        log.debug("SubscriptionId:{} & AddId:{}",subsciptionId,addId);
        exchange.getIn().setHeader("msisdn", msisdn);
        exchange.getIn().setHeader("subscriptionId", subsciptionId);
        exchange.getIn().setHeader("addId", addId);
        exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
        exchange.getIn().setBody("ad_id="+addId+"&subscriber_ids="+subsciptionId);
    }
    
}
