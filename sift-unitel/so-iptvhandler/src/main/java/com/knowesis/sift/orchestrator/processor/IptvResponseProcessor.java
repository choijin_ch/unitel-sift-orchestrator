/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("IptvResponseProcessor")
public class IptvResponseProcessor implements Processor {

    private Logger log = LoggerFactory.getLogger(IptvResponseProcessor.class);

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${sopg.actionType.offerFailed}")
    private String offerFailed;

    @Value("${so.triggerType.offerNotify}")
    private String triggerTypeOfferNotify;

    @Value("${so.fulfill.actionType.offerNotify}")
    private String actionTypeOfferNotify;

    @Override
    public void process(Exchange exchange) throws Exception {

        String response = exchange.getIn().getBody(String.class);
        log.debug("Response message of IPTV {}", response);
        String message = (String) exchange.getProperty("OriginalRequestMessage");
        exchange.removeProperty("OriginalRequestMessage");
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        ObjectNode responseJson = (ObjectNode) jsonObjectMapper.readTree(response);
        String responseCode = responseJson.get("state").asText();
        String responseReason = "Success";
        String actionResponse = "SUCCESS";
        if (responseJson.get("fault") != null) {
            responseReason = responseJson.get("fault").asText();
            String responseErrorValue = responseJson.get("fault").get("value").asText();
            String responseErrorMessage = responseJson.get("fault").get("message").asText();
            actionResponse = responseErrorValue + ":" + responseErrorMessage;
        }

        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-ACTION-RESPONSE",
                responseCode + ":" + responseReason);

        if (responseCode.equals("success") && responseJson.get("fault") == null) {
            soMessage.setActionType("actionTypeOfferNotify");
            soMessage.setActionStatus("SUCCESS");
            soMessage.setActionResponse(responseCode);
            soMessage.setTriggerType("triggerTypeOfferNotify");
            exchange.getIn().setHeader("IPTVstatus", "true");
            exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
        } else {
            exchange.getIn().setHeader("IPTVstatus", "false");
            soMessage.setActionType(offerFailed);
            soMessage.setActionStatus("FAIL");
            soMessage.setActionResponse(actionResponse);
            soMessage.setTriggerType("triggerTypeOfferNotify");
            exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
        }
    }

}
