/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.util.ArrayList;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Processes Messages from Sift Core
 * 
 * @author SO Development Team
 */
@Component("SMSCMTResponseProcessor")
public class SMSCMTResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper mapper;

    /* (non-Javadoc)
     * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
     */
    @Override
    public void process(Exchange exchange) throws Exception {

        String msg = exchange.getIn().getBody(String.class);
        ArrayList<?> arrMessageId = (ArrayList<?>) exchange.getIn().getHeader("CamelSmppId");
        if (arrMessageId != null) {

            log.info("Camel SMS MessageId: {}", arrMessageId);
            exchange.getIn().setHeader("MTCamelSmppIdArray", mapper.writeValueAsString(arrMessageId));
        }
        exchange.getIn().setBody(msg);
    }
}
