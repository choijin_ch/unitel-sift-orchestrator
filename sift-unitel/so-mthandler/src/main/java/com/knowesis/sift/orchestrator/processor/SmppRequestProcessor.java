/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.core.domain.MessageContainer;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.domain.SOReminderMessage;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepared SMPP request when a SO message comes to the queue
 * 
 * @author SO Development Team
 */
@Component("SmppRequestProcessor")
public class SmppRequestProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${mt.message.nullException}")
    private String nullExceptionMessage;

    @Value("${so.test.enableWhiteListing}")
    private Boolean enableWhiteListing;

    @Value("${so.test.whiteListedMSISDNs}")
    private String whiteListedMSISDNs;
    
    @Value("${so.actionStatus.failure}")
    private String actionStatusFailure;
    
    @Value("${so.actionStatus.success}")
    private String actionStatusSuccess;
    
    @Value("${mt.actionType.offerFailed}")
    private String offerFailed;

    List<String> whiteListedMSISDNList;

    /**
     * Called when Bean is refreshed by Spring
     */
    @PostConstruct
    public void onPostConstruct() {
        whiteListedMSISDNList = new ArrayList<>();
        String[] keywords = StringUtils.split(whiteListedMSISDNs, ',');
        whiteListedMSISDNList = Arrays.asList(keywords);
    }

    /**
     * Processes the So messages from queue and prepares SMPP requests
     * 
     * @param exchange camel exchange
     * @throws Exception exception
     */
    public void process(Exchange exchange) throws Exception {

        String message = exchange.getIn().getBody(String.class);
        exchange.getOut().setHeader("OriginalMTRequest", message);
        log.debug("raw message {}", message);
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        // LAST-PROVISIONED-TARGET-SYSTEM header is set from fulfillment systems to store in DB
        // Not needed in notification flow
         
        SOMessageUtils.removeHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM");
        SOMessageUtils.removeHeaderValue(soMessage, "PROVISIONED-OFFERID");
        SOMessageUtils.removeHeaderValue(soMessage, "PROVISIONED-VALUE");
        if (soMessage != null) {
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            boolean isWhiteListedMsisdnOrProduction = SOMessageUtils.checkMsisdnWhiteListCondition(msisdn, enableWhiteListing,
                    whiteListedMSISDNList);
            log.info("Is white listed MSISDN or Production: {}", isWhiteListedMsisdnOrProduction);
            if (isWhiteListedMsisdnOrProduction) {
            	processWhiteListedMsisdnToSentMessage(exchange, msisdn, soMessage);
            } else {
                exchange.getIn().setHeader("msisdn", msisdn);
                log.debug("can't process message {}", message);
            }
        }
        return;
    }

	/**
	 * process the whitelisted msisdn for sending message
	 * 
	 * @param exchange
	 *            camel exchange
	 * @param msisdn
	 *            msisdn
	 * @param soMessage
	 *            soMessage
	 * @throws Exception
	 *             Exception
	 */
	private void processWhiteListedMsisdnToSentMessage(Exchange exchange, String msisdn, SOMessage soMessage)
			throws Exception {
		String smsContent = getSMSContent(soMessage);
		String shortCode = SOMessageUtils.getSmscShortCode(soMessage);
		String actionType = SOMessageUtils.getActionType(soMessage);
		soMessage.setActionStatus(actionStatusFailure);
		soMessage.setActionType(offerFailed);
		if (StringUtils.isNotBlank(shortCode)) {
			exchange.getOut().setHeader("CamelSmppSourceAddr", shortCode);
		} else if (StringUtils.isNotBlank((String) exchange.getOut().getHeader("shortCode"))) {
			exchange.getOut().setHeader("CamelSmppSourceAddr", shortCode);
		} else {
			log.info("Short code is not available in either message or in header");
		}
		exchange.getOut().setHeader("CamelSmppSourceAddrNpi", 0);
		exchange.getOut().setHeader("CamelSmppSourceAddrTon", 0);
		exchange.getOut().setHeader("CamelSmppDestAddr", msisdn);
		exchange.getOut().setHeader("CamelSmppServiceType", null);
		exchange.getOut().setHeader("SmsContentFound", "false");
		exchange.getOut().setHeader("ActionType", actionType);
		exchange.getOut().setHeader("MessageProcessedForRA", jsonObjectMapper.writeValueAsString(soMessage));
		if (StringUtils.isNotEmpty(smsContent)) {
			soMessage.setActionStatus(actionStatusSuccess);
			exchange.getOut().setHeader("SmsContentFound", "true");
			soMessage.setActionType(actionType);
		}
		exchange.getOut().setBody(smsContent);
		log.debug("notify '{}' with SMS content '{}'", msisdn, smsContent);
		return;
	}

	/**
	 * Get SMS Text based on action
	 * 
	 * @param soMessage
	 *            SOMessage, can't be null
	 * @return text to be sent
	 * @throws Exception
	 *             exception
	 */
    private String getSMSContent(SOMessage soMessage) throws Exception {
        try {
            if (soMessage instanceof SOFulfillmentMessage) {
                SOFulfillmentMessage message = (SOFulfillmentMessage) soMessage;
                return getTextTobeSend(message.getMessage().getRequesterLocation().getLocations().get(0)
                        .getFulfilments().get(0).getFulfillmentMessages());
            } else if (soMessage instanceof SOReminderMessage) {
                SOReminderMessage message = (SOReminderMessage) soMessage;
                return getTextTobeSend(message.getMessage().getRequesterLocation().getLocations().get(0).getReminders()
                        .get(0).getReminderMessages());
            } else if (soMessage instanceof SONotificationMessage) {
                SONotificationMessage message = (SONotificationMessage) soMessage;
                return getTextTobeSend(message.getMessage().getRequesterLocation().getLocations().get(0).getOffers()
                        .get(0).getNotificationMessages());
            }
        } catch (Exception e) {
            throw new Exception(nullExceptionMessage);
        }
        return null;
    }

    /**
     * gets sms text to be sent customer from message
     * 
     * @param messages MessageContainer message
     * @return sms text
     */
    private String getTextTobeSend(List<MessageContainer> messages) {

        for (MessageContainer container : messages) {
            if ("sms".equalsIgnoreCase(container.getChannelName())) {
                return container.getMessages().get(0).getTextTobeSent();

            }
        }
        return "";
    }
    
}
