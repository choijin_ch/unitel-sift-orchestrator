/**
 * Copyright (c) 2016, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import java.io.IOException;
import java.time.Instant;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.component.RedisConnectionPool;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.domain.SONotificationMessage;
import com.knowesis.sift.orchestrator.domain.SOReminderMessage;
import com.knowesis.sift.orchestrator.utils.CacheKeyUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Processes SMPP response from the SMSC
 * 
 * @author SO Development Team
 */
@Component("SmppResponseProcessor")
public class SmppResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Autowired
    RedisConnectionPool redisConnectionPool;

    @Value("${so.monitoring.ttl.defaultValue}")
    private String redisCacheTimeToLive;

    private int redisCacheTTL;

    @Value("${mt.isCamelSmppIdInHexa}")
    private Boolean isCamelSmppIdInHexa = false;

    @Value("${mt.camelSmppIdLen}")
    private Integer camelSmppIdLen;

    @Value("${mt.padLeadingCharacter}")
    private Character padLeadingCharacter;

    @Value("${so.monitoring.ttl.additionalBufferValue}")
    private int ttlBuffer;

    // Properties added for Unitel
    @Value("${so.triggerType.offerNotify}")
    private String offerNotify;

    @Value("${so.triggerType.notify}")
    private String notifyTriggerType;

    @Value("${so.actionType.notify}")
    private String notify;

    @Value("${so.actionStatus.success}")
    private String success;

    @Value("${so.actionStatus.failure}")
    private String failure;

    @Value("${so.triggerType.reminderNotify}")
    private String reminderNotify;
    
    @Value("${so.fulfill.actionType.extendExpiryNotify}")
    private String extendExpiryNotify;

    
    /**
     * Called when Bean is refreshed by Spring
     */
    @PostConstruct
    public void onPostConstruct() {
        redisCacheTTL = Integer.parseInt(redisCacheTimeToLive);
        log.info("MT response is in Hexa ? {}", isCamelSmppIdInHexa);

    }

	/**
	 * Processes the Smpp Response
	 * 
	 * @param exchange
	 *            camel exchange
	 * @throws Exception
	 *             exception
	 */
    public void process(Exchange exchange) throws Exception {
        String originalSoMessageJson = exchange.getProperty("OriginalMTRequestProp", String.class);
        SOMessage soMessage = RawMessageUtils.processRawMessage(originalSoMessageJson, jsonObjectMapper);
        SOMessageUtils.removeHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM");
        log.info("Synchronous SMSC reponse has been received for msisdn: {}", SOMessageUtils.getMSISDN(soMessage));
        String messageIdArrayJson = (String) exchange.getIn().getHeader("MTCamelSmppIdArray");
        List<String> messageIdArray = jsonObjectMapper.readValue(messageIdArrayJson, new TypeReference<List<String>>() {
        });
        String value = null;
        setTriggerTypeAndActionTypeForCoreMessages(soMessage);
        if (messageIdArray != null && messageIdArray.size() > 0) {
        	
        	setSuccessfullDRHeaders(exchange, soMessage);
            soMessage.setActionStatus(success);
            soMessage = setHeaderValueForReccuringOptin(soMessage); 
            value = jsonObjectMapper.writeValueAsString(soMessage);
            setCacheKeyForDR(exchange, messageIdArray, value, soMessage);
            cacheForOptin(soMessage, value);
        } else {
            soMessage.setActionStatus(failure);
             if(SOMessageUtils.getHeaderValue(soMessage, "DOUBLECONFIRMATION") != null){
            	 handleSystemFailureForMultiLevelOptin(soMessage);
            	}
        }
        exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
    }

	/**
	 * setMessage headers for reccuring optin
	 * 
	 * @param soMessage
	 *            soMessage
	 * @return soMessage
	 */
	private SOMessage setHeaderValueForReccuringOptin(SOMessage soMessage) {
		//checks for reccuring optin
		if (SOMessageUtils.isSoMessageOptin(soMessage)
				&& SOMessageUtils.getOfferPayLoad(soMessage).containsKey("RECURRING_COUNT")) {
			SOMessageUtils.setHeaderValue(soMessage, "RECURRING_COUNT",
					SOMessageUtils.getOfferPayLoad(soMessage).get("RECURRING_COUNT"));
			SOMessageUtils.setHeaderValue(soMessage, "OPTIN-COUNT", "0");
		}
		return soMessage;
	}

	/**
	 * setSuccessfullDRHeaders for notifications
	 * 
	 * @param exchange
	 *            camel exchange
	 * @param soMessage
	 *            soMessage
	 */
	private void setSuccessfullDRHeaders(Exchange exchange, SOMessage soMessage) {
    	if (offerNotify.equalsIgnoreCase(soMessage.getTriggerType())) {
            exchange.getIn().setHeader("OFFERNOTIFY", true);
        }
        String actionType = SOMessageUtils.getActionType(soMessage);
        if(actionType != null && actionType.equals(extendExpiryNotify)) {
            exchange.getIn().setHeader("OFFERNOTIFY", true);
        }
	}

	/**
     * handleSystemFailureForMultiLevelOptin
     * 
     * @param soMessage soMessage
     */
    private void handleSystemFailureForMultiLevelOptin(SOMessage soMessage) {
    	try{
    	 String optinKeyWord=SOMessageUtils.getHeaderValue(soMessage, "OptinKeyWord");
    	 String msisdn=SOMessageUtils.getMSISDN(soMessage);
    	 String optinShortCode=SOMessageUtils.getOptinShortCode(soMessage);
    	 String[] keywords = optinKeyWord.split(",");
    	 String multiLevelKeyToBeRemoved = CacheKeyUtils.generateMultiLevelOptinCacheKey(optinShortCode, msisdn);
    	 redisConnectionPool.delObjectFromCache(multiLevelKeyToBeRemoved); //delete the key that is generated for multilevel optin 
    	 String optinCacheKey = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn, keywords[0].toString());
    	 String objectToRemoveFromProcessing = redisConnectionPool.getObjectFromCache(optinCacheKey);//message with in progress status
    	 SONotificationMessage soNotificationMessageForUpdatedRedis = jsonObjectMapper.readValue(objectToRemoveFromProcessing,SONotificationMessage.class);
    	 Long currentTime = Instant.now().toEpochMilli();
			 String endDate = soNotificationMessageForUpdatedRedis.getMessage().getRequesterLocation().getLocations().get(0)
					.getOffers().get(0).getOfferEndDate();
    	 int optinTTL = CacheKeyUtils.generateOptinTTL(currentTime.toString(), endDate) + ttlBuffer;
    	 SOMessageUtils.removeHeaderValue(soNotificationMessageForUpdatedRedis, "optinStatus");
    	 SOMessageUtils.removeHeaderValue(soNotificationMessageForUpdatedRedis, "DOUBLECONFIRMATION");
    	 SOMessageUtils.removeHeaderValue(soNotificationMessageForUpdatedRedis, "OptinKeyWord");
    	 for (int i = 0; i < keywords.length; i++) {
             optinCacheKey = CacheKeyUtils.generateOptinCacheKey(optinShortCode, msisdn, keywords[i].toString());
             redisConnectionPool.putInCache(optinCacheKey, jsonObjectMapper.writeValueAsString(soNotificationMessageForUpdatedRedis),  optinTTL);
         }
    	}
    	catch (Exception exception) {
    		
    		log.debug("Exception occured is:{}",exception);
    		
    	}
	}

	/**
     * 
     * Setting trigger type and action type for reminder messages and notification messages from
     * core. All other messages will come with preset trigger type and action type
     * 
     * @param soMessage soMessage
     */
    private void setTriggerTypeAndActionTypeForCoreMessages(SOMessage soMessage) {
        if (soMessage.getTriggerType().startsWith("CORE")) {
            if (soMessage instanceof SONotificationMessage) {
                soMessage.setTriggerType(offerNotify);
                soMessage.setActionType(notify);
            } else if (soMessage instanceof SOReminderMessage) {
                soMessage.setTriggerType(reminderNotify);
                soMessage.setActionType(notify);
            }
        }
    }

    /**
     * Setting Redis entry to track DR for this message
     * 
     * @param exchange camel Exchange
     * @param messageIdArray get from MTCamelSmppIdArray
     * @param value so message taken as string
     * @param soMessage so message object created from raw message
     * @throws JsonProcessingException Exception handling
     */
    private void setCacheKeyForDR(Exchange exchange, List<String> messageIdArray, String value, SOMessage soMessage)
            throws JsonProcessingException {
        String messageId = null;
        Iterator<String> iterator = messageIdArray.iterator();
        while (iterator.hasNext()) {
            messageId = iterator.next();
            messageId = getCamelSmppId(messageId);
            value = jsonObjectMapper.writeValueAsString(soMessage);
            String cacheKey = CacheKeyUtils.generateMTCacheKey(messageId);
            redisConnectionPool.putInCache(cacheKey, value, redisCacheTTL);
        }
    }

    /**
     * Set redis entry for optin if Optin keyword and optin short code is present
     * 
     * @param soMessage SOMessage
     * @param value JSON form of SOMessage
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonParseException 
     * 
     */
    private void cacheForOptin(SOMessage soMessage, String value) throws JsonParseException, JsonMappingException, IOException {
        if (SOMessageUtils.isSoMessageOptin(soMessage)) {
            String startDate = SOMessageUtils.getOfferStartDate(soMessage);
            String endDate = SOMessageUtils.getOfferEndDate(soMessage);
            int optinTTL = CacheKeyUtils.generateOptinTTL(startDate, endDate) + ttlBuffer;
            String msisdn = SOMessageUtils.getMSISDN(soMessage);          
            String optinKeyword = SOMessageUtils.getOptinKeyword(soMessage);
            log.info("optinKeyword" + optinKeyword);
            String[] keywords = optinKeyword.split(",");
            for (int i = 0; i < keywords.length; i++) {
                String cacheKey = CacheKeyUtils.generateOptinCacheKey(SOMessageUtils.getOptinShortCode(soMessage),
                        msisdn, keywords[i].toString());
                redisConnectionPool.putInCache(cacheKey, value, optinTTL);
            }
        }
    }

    /**
     * Converts Hexadecimal value to decimal. Fall back value is argument value
     * 
     * @param camelSmppIdInHex camelSmppid in decimal
     * @return smppId
     */
    public String getCamelSmppId(final String camelSmppIdInHex) {

        if (StringUtils.isBlank(camelSmppIdInHex)) {
            log.debug("can't convert null camelSmppId, returning null");
            return null;
        }
        String returnValue = new String(camelSmppIdInHex);
        try {
            if (isCamelSmppIdInHexa) {
                Long dec = Long.parseLong(camelSmppIdInHex, 16);
                returnValue = dec.toString();
                if (returnValue.length() < camelSmppIdLen) {
                    returnValue = StringUtils.leftPad(returnValue, camelSmppIdLen, padLeadingCharacter);
                }
            }
        } catch (Exception e) {
            log.debug("can't convert camelSmppId " + camelSmppIdInHex + " in hexa to decimal", e);
        }
        log.debug("returning decimal value of camelSmppId {} against input hexa value {}, isCamelSmppIdInHexa: {}",
                new Object[] { returnValue, camelSmppIdInHex, isCamelSmppIdInHexa });
        return returnValue;
    }

}
