/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.utils.AcquisitionUtils;
import com.knowesis.sift.orchestrator.utils.ComplexSearchUtils;

/**
 * 
 * Processes Reserve API core call
 * 
 * @author SO Development Team
 */
@Component("ReserveNumberApiProcessor")
public class ReserveNumberApiProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Value("${so.acquisition.numbermatch.availablepatterns}")
    private String numberMatchPatterns;

    @Value("${so.acquisition.template.match}")
    private String patternMathPatterns;

    @Value("${so.acquisition.query.prefix}")
    private String queryPrefix;

    @Value("${so.acquisition.query.suffix}")
    private String querySuffix;

    @Value("${so.acquisition.loadnonunitelnumberdetails.sql}")
    private String loadNonUnitelNumberSql;

    String[] numberMatchPatternsArray;
    String[] templateMatchPattersArray;

    /**
     * Called when Bean is refreshed by Spring
     */
    @PostConstruct
    public void onPostConstruct() {
        numberMatchPatternsArray = numberMatchPatterns.split(",");
        templateMatchPattersArray = patternMathPatterns.split(",");

    }

    @Override
    public void process(Exchange exchange) throws Exception {
        String requestBody = exchange.getIn().getBody(String.class);
        log.info("Reserve API call request received from core: {}", requestBody);
        JsonNode rootNode = jsonObjectMapper.readTree(requestBody);
        String nonUnitelMSISDN = rootNode.path("nonUnitelMSISDN").asText();
        loadNonUnitelDetails(exchange, nonUnitelMSISDN);
        String reserveFor = rootNode.path("reserveFor").asText();
        log.info("Aquisition request received for Non unitel number: {} and Reserved for: {}", nonUnitelMSISDN,
                reserveFor);
        JsonNode matchingList = rootNode.path("matchingList");
        Map<Integer, JsonNode> orderMatchingMap = getOrderMatchingMap(matchingList);
        log.debug("orderMatchingMap: {}", orderMatchingMap);
        String reservedMsisdn = matchAvailablePatterns(orderMatchingMap, exchange, nonUnitelMSISDN);
        String promocode = null;
        String status = "404";
        String description = "NOT-FOUND";
        if (StringUtils.isNotBlank(reservedMsisdn)) {
            status = "200";
            description = "SUCCESS";
            exchange.getIn().setHeader("isNumberReserved", true);
        } else {
            exchange.getIn().setHeader("isNumberReserved", false);
        }
        promocode = AcquisitionUtils.promocodeGenerator(2, 4);
        exchange.setProperty("status", status);
        exchange.setProperty("description", description);
        exchange.setProperty("reservedMSISDN", reservedMsisdn);
        exchange.setProperty("reserveFor", reserveFor);
        exchange.setProperty("nonUnitelMSISDN", nonUnitelMSISDN);
        exchange.setProperty("description", description);
        exchange.setProperty("promocode", promocode);
    }

    /**
     * 
     * Load Non unitel MSISDN details from database
     * 
     * @param exchange exchange
     * @param nonUnitelMSISDN nonUnitelMSISDN
     */
    private void loadNonUnitelDetails(Exchange exchange, String nonUnitelMSISDN) {
        List<Map<String, Object>> result = jdbcTemplate.queryForList(loadNonUnitelNumberSql,
                new Object[] { nonUnitelMSISDN });
        if (result != null && result.size() > 0) {
            log.info("Details found for non unitel number: {}.", nonUnitelMSISDN);
            Map<String, Object> nonUitelNumberDetails = result.get(0);
            exchange.setProperty("birthDay", formatTimeStamp((Timestamp) nonUitelNumberDetails.get("BIRTHDAY")));
            exchange.setProperty("familyId", nonUitelNumberDetails.get("FAMILYID"));
            exchange.setProperty("ssn", nonUitelNumberDetails.get("SSN"));
            exchange.setProperty("snapshotDate", nonUitelNumberDetails.get("SNAPSHOTDATE"));
        } else {
            log.info("Details not found for non unitel number: {}.", nonUnitelMSISDN);
            exchange.setProperty("birthDay", "");
            exchange.setProperty("familyId", "");
            exchange.setProperty("ssn", "");
            exchange.setProperty("snapshotDate", "");
        }
    }

    /**
     * 
     * Get Matching MSISDN based on given pattern
     * 
     * @param orderMatchingMap orderMatchingMap
     * @param exchange exchange
     * @param nonUnitelMsisdn nonUnitelMsisdn
     * @return msisdn
     */
    // TO DO nonunitel number isnot used for matching
    private String matchAvailablePatterns(Map<Integer, JsonNode> orderMatchingMap, Exchange exchange,
            String nonUnitelMsisdn) {
        String reservedMsisdn = null;
        for (Map.Entry<Integer, JsonNode> entry : orderMatchingMap.entrySet()) {
            String currentMatchingType = entry.getValue().path("type").asText();
            exchange.setProperty("type", currentMatchingType);
            if (currentMatchingType.equals("number-match")) {
                reservedMsisdn = findMsisdnUsingNumberMatch(exchange, nonUnitelMsisdn);
            } else if (currentMatchingType.equals("template-match")) {
                // reservedMsisdn =
                // findMsisdnUsingTemplateMatch(entry.getValue().path("pattern").asText(),
                // exchange);
            } else if (currentMatchingType.equals("birthday-match")) {
                // reservedMsisdn = findMsisdnUsingBirthdayMatch();
            } else if (currentMatchingType.equals("family-match")) {
                // reservedMsisdn = findMsisdnUsingFamilyMatch();
            } else {
                log.info("Current matching type {} is not supported.", currentMatchingType);
            }
            if (StringUtils.isNotBlank(reservedMsisdn)) {
                log.info("Reserved MSISDN: {}", reservedMsisdn);
                return reservedMsisdn;
            }
        }
        exchange.setProperty("type", "");
        return null;
    }

    /**
     * 
     * Returns a sorted map in matching order
     * 
     * @param matchingList matchingList from request
     * @return orderMatchingMap
     */
    private Map<Integer, JsonNode> getOrderMatchingMap(JsonNode matchingList) {
        Iterator<JsonNode> matchingListIterator = matchingList.elements();
        Map<Integer, JsonNode> orderMatchingMap = new TreeMap<Integer, JsonNode>();
        while (matchingListIterator.hasNext()) {
            JsonNode matching = matchingListIterator.next();
            orderMatchingMap.put(matching.path("order").asInt(), matching);
        }
        return orderMatchingMap;
    }

    /**
     * 
     * Find any numbers that matches any pattern from config
     * 
     * @param exchange exchange
     * @param nonUnitelMsisdn nonUnitelMsisdn
     * @return msisdn msisdn
     */
    private String findMsisdnUsingNumberMatch(Exchange exchange, String nonUnitelMsisdn) {
        String msisdn = null;
        msisdn = findMsisdnUsingNonUnitelNumberOverlaying(nonUnitelMsisdn);
        if (StringUtils.isNotBlank(msisdn)) {
            setPatternBasedOnTheNumber(exchange, msisdn, nonUnitelMsisdn);
        }
        return msisdn;
    }

    /**
     * 
     * Find out a pattern that matched the unitel number found
     * 
     * @param exchange exchange
     * @param msisdn msisdn
     * @param nonUnitelMsisdn nonUnitelMsisdn
     */
    private void setPatternBasedOnTheNumber(Exchange exchange, String msisdn, String nonUnitelMsisdn) {
        String pattern = null;
        for (int i = 0; i < numberMatchPatternsArray.length; i++) {
            pattern = ComplexSearchUtils.match(msisdn, nonUnitelMsisdn, numberMatchPatternsArray[i]);
            if (StringUtils.isNotBlank(pattern)) {
                exchange.setProperty("pattern", pattern);
                return;
            }
        }
        exchange.setProperty("pattern", pattern);

    }

    /**
     * 
     * Finds Matching Unitel MSISDN based on non unitel overlaying
     * 
     * @param nonUnitelMsisdn nonUnitelMsisdn
     * @return Available unitel number
     */
    private String findMsisdnUsingNonUnitelNumberOverlaying(String nonUnitelMsisdn) {
        for (int i = 1; i <= 4; i++) {
            String query = createNumberMatchingQuery(nonUnitelMsisdn.substring(i, nonUnitelMsisdn.length()));
            List<Map<String, Object>> result = jdbcTemplate.queryForList(query);
            if (result.size() > 0) {
                return String.valueOf(result.get(0).get("MSISDN"));
            }
        }
        return null;
    }

    /**
     * 
     * Create number matching query for nonUnitel number overlay pattern
     * 
     * @param substring substring
     * @return query
     */
    private String createNumberMatchingQuery(String substring) {
        StringBuilder builder = new StringBuilder();
        builder.append(queryPrefix);
        int k = 8;
        for (int j = substring.length(); j >= 1; j--) {
            builder.append("DIGIT").append(k--).append("=").append(substring.charAt(j - 1)).append(" AND ");
        }
        String finalQuery = builder.toString();
        finalQuery = finalQuery.substring(0, finalQuery.lastIndexOf(" AND "));
        finalQuery = finalQuery + " " + querySuffix;
        log.info("Query generated: {}", finalQuery);
        return finalQuery;
    }

    /**
     * 
     * Formats timestamp
     * 
     * @param timestamp timestamp
     * @return formatted time
     */
    private String formatTimeStamp(Timestamp timestamp) {
        return new SimpleDateFormat("yyyyMMdd").format(timestamp);
    }

}
