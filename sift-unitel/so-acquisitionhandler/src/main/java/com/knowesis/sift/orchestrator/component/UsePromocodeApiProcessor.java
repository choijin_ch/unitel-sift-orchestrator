/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * 
 * Processes Check promocode API call
 * 
 * @author SO Development Team
 */
@Component("UsePromocodeApiProcessor")
public class UsePromocodeApiProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Value("${so.externalsmsc.selectacqtxn.sql}")
    private String selectAcqTxnSql;

    @Value("${so.acquisition.updateacqtxn.sql}")
    private String updateAcqTxnQuery;

    @Value("${so.db.insert}")
    String sql;

    @Override
    public void process(Exchange exchange) throws Exception {
        String requestBody = exchange.getIn().getBody(String.class);
        log.info("Use Promocode API call request received from core: {}", requestBody);
        JsonNode rootNode = jsonObjectMapper.readTree(requestBody);
        String promocode = rootNode.path("promo_code").asText();
        String nonUnitelMSISDN = rootNode.path("non_uni_msisdn").asText();
        String familyId = rootNode.path("family_id").asText();
        String customerId = rootNode.path("customer_id").asText();
        String subscriptionKey = rootNode.path("subscription_key").asText();
        String revChurner = rootNode.path("rev_churner").asText();
        String unitelMsisdn = rootNode.path("uni_msisdn").asText();
        String registerDate = rootNode.path("register_date").asText();
        String entrNo = rootNode.path("entr_no").asText();
        String paymentType = rootNode.path("payment_type").asText();
        // String transactionId = rootNode.path("transaction_id").asText();

        updateAcqTxnTable("CORE_FEED_SENT", revChurner, subscriptionKey, customerId, familyId, nonUnitelMSISDN,
                promocode, unitelMsisdn);

        ObjectNode node = jsonObjectMapper.createObjectNode();
        node.put("statusMessage", "SUCCESS");
        node.put("statusCode", "000");
        List<String> registerActions = createRegisterActions(unitelMsisdn, nonUnitelMSISDN, promocode, revChurner,
                entrNo, familyId, paymentType, customerId, registerDate);
        exchange.getIn().setHeader("registerActions", registerActions);
        exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(node));

    }

    /**
     * 
     * Inserts ACQ_OPTIN DB entry
     * 
     * @param promocode promocode
     * @param nonUnitelMSISDN nonUnitelMSISDN
     * @param programId programId
     * @param offerId offerId
     * @param eventId eventId
     * @param flowId flowId
     * @param uniteMsisdn uniteMsisdn
     */
    private void insertAcqOptinDBEtry(String promocode, String nonUnitelMSISDN, String programId, String offerId,
            String eventId, String flowId, String uniteMsisdn) {
        insertEventTriggerEntry(nonUnitelMSISDN, offerId, programId, eventId, flowId, null/* "monitoredValue" */,
                null/* "actualResponseValue" */, null/* "provisioningOfferId" */, null/* "provisionedValue" */, "UO",
                getUnixTimestamp()/* triggerTimestamp */, null/* "notificationMessage" */, promocode + ":" + uniteMsisdn,
                "ACQ-RESPONSE", "NOTIFY", "UO" /* provisioningSystem or Channel */, "SUCCESS", "false", getUnixTimestamp(),
                "false", null/* offerStartDate */, null/* offerEndDate */, null/* "subscriptionId" */, "OUTBOUND");

    }

    /**
     * 
     * Created register actions for Use promocode
     * 
     * @param unitelMsisdn unitelMsisdn
     * @param nonUnitelMsisdn nonUnitelMsisdn
     * @param promocode promocode
     * @param revChurner revChurner
     * @param entrNo entrNo
     * @param familyId familyId
     * @param paymentType paymentType
     * @param customerId customerId
     * @param registerDate registerDate
     * @return register actions
     * @throws JsonProcessingException exceptions
     * @throws ParseException exception
     */
    private List<String> createRegisterActions(String unitelMsisdn, String nonUnitelMsisdn, String promocode,
            String revChurner, String entrNo, String familyId, String paymentType, String customerId,
            String registerDate) throws JsonProcessingException, ParseException {
        List<String> registerActions = new ArrayList<String>(2);

        List<Map<String, Object>> result = jdbcTemplate.queryForList(selectAcqTxnSql,
                new Object[] { nonUnitelMsisdn, promocode });
        String programId = null, offerId = null, eventId = null, flowId = null;

        if (result != null && result.size() > 0) {
            Map<String, Object> row = result.get(0);
            programId = (String) row.get("PROGRAM_ID");
            offerId = (String) row.get("OFFER_ID");
            eventId = (String) row.get("EVENT_ID");
            flowId = (String) row.get("FLOW_ID");
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date usePromocodeDate = sdf.parse(registerDate);

        insertAcqOptinDBEtry(promocode, nonUnitelMsisdn, programId, offerId, eventId, flowId, unitelMsisdn);

        ObjectNode unitelRA = jsonObjectMapper.createObjectNode();
        unitelRA.put("PACKAGEID", "");// OK
        unitelRA.put("MSISDN", unitelMsisdn); // OK
        unitelRA.put("EVENTID", eventId); // OK
        unitelRA.put("FLOW_ID", flowId); // OK
        unitelRA.put("SUBSCRIBER_ID", "");// OK
        unitelRA.put("RECORD_TYPE", "REGISTER_ACTION"); // OK
        unitelRA.put("PROGRAM_CODE", programId);// OK
        unitelRA.put("TIMESTAMP", ""+convertToLocalTime(usePromocodeDate.getTime()).getTime()); // OK
        unitelRA.put("OFFER_CODE", offerId);// OK
        unitelRA.put("OPTIN_SHORTCODE", ""); // OK
        unitelRA.put("CHANNEL_TYPE", "UO"); // OK
        unitelRA.put("ACTION_TYPE", "ACQUIRED_SUBSCRIBER"); // OK
        unitelRA.put("STATUS", ""); // OK
        unitelRA.put("PROMO_CODE", promocode); // OK
        unitelRA.put("CONTRACT_ID", entrNo); // OK
        unitelRA.put("REVOLVING_CHURNER", revChurner); // OK
        unitelRA.put("FAMILY_ID", familyId); // OK
        unitelRA.put("SUBSCRIPTION_KEY", ""); // OK
        unitelRA.put("SUBSCRIPTION_TYPE", paymentType); // OK
        unitelRA.put("CUSTOMER_ID", customerId); // OK
        String unitelRAJson = jsonObjectMapper.writeValueAsString(unitelRA);
        log.info("Acquired Subscriber RA: {}", unitelRA);
        registerActions.add(unitelRAJson);

        ObjectNode nonUnitelRA = jsonObjectMapper.createObjectNode();
        nonUnitelRA.put("PACKAGEID", ""); // OK
        nonUnitelRA.put("MSISDN", nonUnitelMsisdn); // OK
        nonUnitelRA.put("EVENTID", eventId);// OK
        nonUnitelRA.put("FLOW_ID", flowId);// OK
        nonUnitelRA.put("SUBSCRIBER_ID", ""); // OK
        nonUnitelRA.put("RECORD_TYPE", "REGISTER_ACTION"); // OK
        nonUnitelRA.put("PROGRAM_CODE", programId); // OK
        nonUnitelRA.put("TIMESTAMP", ""+convertToLocalTime(usePromocodeDate.getTime()).getTime()); // OK
        nonUnitelRA.put("OFFER_CODE", offerId); // OK
        nonUnitelRA.put("OPTIN_SHORTCODE", ""); // OK
        nonUnitelRA.put("CHANNEL_TYPE", "UO"); // OK
        nonUnitelRA.put("ACTION_TYPE", "ACQUISITION_RESPONSE"); // OK
        nonUnitelRA.put("STATUS", ""); // OK
        nonUnitelRA.put("PROMO_CODE", promocode); // OK
        nonUnitelRA.put("RESERVED_NUMBER", unitelMsisdn); // OK
        String nonUniteRAJson = jsonObjectMapper.writeValueAsString(nonUnitelRA);
        log.info("Acquisition Response RA: {}", nonUniteRAJson);
        registerActions.add(nonUniteRAJson);
        return registerActions;
    }

    /**
     * 
     * Update ACQ_TXN table
     * 
     * @param state state
     * @param revChurner revChurner
     * @param subscriptionKey subscriptionKey
     * @param customerId customerId
     * @param familyId familyId
     * @param nonUnitelMSISDN nonUnitelMSISDN
     * @param promocode promocode
     * @param unitelMsisdn unitelMsisdn
     */
    private void updateAcqTxnTable(String state, String revChurner, String subscriptionKey, String customerId,
            String familyId, String nonUnitelMSISDN, String promocode, String unitelMsisdn) {
        jdbcTemplate.update(updateAcqTxnQuery, new Object[] { state, revChurner, subscriptionKey, customerId, familyId,
                unitelMsisdn, nonUnitelMSISDN, promocode });

    }

    /**
     * 
     * Inserts Event trigger record
     * 
     * @param msisdn msisdn
     * @param offerId offerId
     * @param programId programId
     * @param triggerId triggerId
     * @param flowId flowId
     * @param monitoredValue monitoredValue
     * @param actualResponseValue actualResponseValue
     * @param provisioningOfferId provisioningOfferId
     * @param provisionedValue provisionedValue
     * @param provisioningSystem provisioningSystem
     * @param triggerTimestamp triggerTimestamp
     * @param notificationMessage notificationMessage
     * @param actionResponse actionResponse
     * @param triggerType triggerType
     * @param actionType actionType
     * @param channel channel
     * @param actionStatus actionStatus
     * @param isSimulated isSimulated
     * @param createdOn createdOn
     * @param isControl isControl
     * @param offerStartDate offerStartDate
     * @param offerEndDate offerEndDate
     * @param subscriptionId subscriptionId
     * @param contactDirection contactDirection
     */
    private void insertEventTriggerEntry(String msisdn, String offerId, String programId, String triggerId,
            String flowId, String monitoredValue, String actualResponseValue, String provisioningOfferId,
            String provisionedValue, String provisioningSystem, Timestamp triggerTimestamp, String notificationMessage,
            String actionResponse, String triggerType, String actionType, String channel, String actionStatus,
            String isSimulated, Timestamp createdOn, String isControl, Timestamp offerStartDate, Timestamp offerEndDate,
            String subscriptionId, String contactDirection) {
        jdbcTemplate.update(sql, new Object[] { msisdn, offerId, programId, triggerId, flowId, monitoredValue,
                actualResponseValue, provisioningOfferId, provisionedValue, provisioningSystem, triggerTimestamp,
                notificationMessage, actionResponse, triggerType, actionType, channel, actionStatus, isSimulated,
                createdOn, isControl, offerStartDate, offerEndDate, subscriptionId, contactDirection });
    }

    /**
     * 
     * To generate timestamp in UNIX format as the core requires it
     * 
     * @return timestamp String in UNIX format
     */
    private static Timestamp getUnixTimestamp() {
        Calendar cal = Calendar.getInstance();

        // Get the time in local timezone
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);
        int seconds = cal.get(Calendar.SECOND);
        // Set the current time as GMT Time
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minutes);
        cal.set(Calendar.SECOND, seconds);

        return new java.sql.Timestamp(cal.getTime().getTime());
    }
    
    
    /**
     * 
     * Convert to local time from GMT
     * 
     * @param time time in millisecond
     * @return timestamp String in UNIX format
     */
    private static Timestamp convertToLocalTime(long time) {
        //long time = Long.parseLong(timestamp);

        Calendar calendarTiggerTime = Calendar.getInstance();
        calendarTiggerTime.setTimeInMillis(time);

        // Get the time in local timezone
        int hour = calendarTiggerTime.get(Calendar.HOUR_OF_DAY);
        int minutes = calendarTiggerTime.get(Calendar.MINUTE);
        int seconds = calendarTiggerTime.get(Calendar.SECOND);

        // create time zone object with UTC and set the timezone
        TimeZone tzone = TimeZone.getTimeZone("UTC");
        calendarTiggerTime.setTimeZone(tzone);

        // Set the current time as GMT Time
        calendarTiggerTime.set(Calendar.HOUR_OF_DAY, hour);
        calendarTiggerTime.set(Calendar.MINUTE, minutes);
        calendarTiggerTime.set(Calendar.SECOND, seconds);

        long epoch = calendarTiggerTime.getTimeInMillis();
        long epochLocalToUtcDelta = getLocalToUtcDelta();
        calendarTiggerTime.setTimeInMillis(epoch + epochLocalToUtcDelta);

        return new java.sql.Timestamp(calendarTiggerTime.getTime().getTime());
    }
    
    /**
     * 
     * To get Local to UTC Delta
     * 
     * @return timestamp String in UNIX format
     */
    public static long getLocalToUtcDelta() {
        Calendar local = Calendar.getInstance();
        local.clear();
        local.set(1970, Calendar.JANUARY, 1, 0, 0, 0);
        return local.getTimeInMillis();
    }

}
