/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

import java.util.Stack;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.StringUtils;

/**
 * Provides simple pattern matching methods.
 * <p>
 * This class provides lightweight custom expression parsing (no Antlr or JavaCC
 * stuff)
 * 
 * @author SO Development Team
 */
public class ComplexSearchUtils {
    private static Logger log = LoggerFactory.getLogger(ComplexSearchUtils.class);

    /**
     * Check whether the supplied pattern will give <code>value</code> when
     * applied on <code>original</code>. This is essentially a reverse search.
     * 
     * @param result The result value that will be used to find pattern
     * @param original original value which when supplied to unknown algo gives
     *            <code>result</code>
     * @param pattern business pattern
     * @return pattern if matching, null if pattern is not matching
     */
    public static String match(String result, String original, String pattern) {

        String overlay = ComplexSearchUtils.overlay(result, pattern, new Character[] { 'a', 'b' });
        String regex = ComplexSearchUtils.preparePatterns(overlay);

        log.debug("matching {} with regex {}", result, regex);
        if (result.matches(regex)) {
            return pattern;
        }
        return null;
    }

    /**
     * Prepares regex patterns from business patterns. This method replaces 'x'
     * with \\d+ and * with .?
     * 
     * @param businessPattern business pattern
     * @return map with key as business pattern and value as its corresponding
     *         regex pattern
     */
    public static String preparePatterns(String businessPattern) {
        String result = businessPattern.replaceAll("x", "[0-9]{1}").replaceAll("\\*", ".{1}");
        log.debug("regex pattern for business pattern {} is {}", businessPattern, result);
        return result;
    }

    /**
     * Overlay <code>b</code> on <code>a</code> for all characters in
     * <code>c</code>
     * <p>
     * e.g. when overlaying <code>86121111</code> on <code>8*ab8*ab</code> for
     * <code>a,b</code>, the resulting string is <code>8*128*11</code>
     * 
     * @param a the unitel number
     * @param b the business pattern
     * @param c the variable symbol array
     * @return resulting string
     */
    public static String overlay(String a, String b, Character[] c) {

        char[] aStr = a.toCharArray();
        char[] symbols = createSymbols(aStr, b.toCharArray(), c);
        String expression = parseExpression(b.toCharArray(), symbols, c);

        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();
        Expression exp = parser.parseExpression(expression);
        String value = (String) exp.getValue(context);

        log.debug("overlayed result from a:{}, b:{} with c:{} is {}", new Object[] { a, b, c, value });
        return value;
    }

    /**
     * Parse expression by replacing variables with their values. Method also
     * formats expression so that it can be evaluated by Spring.
     * 
     * @param bStr character stream of operands and operators
     * @param symbols the symbols
     * @param variables list of variables in expression
     * @return parsed expression
     */
    private static String parseExpression(char[] bStr, char[] symbols, Character[] variables) {

        Stack<Character> stack = new Stack<>();
        boolean flagged = false;
        for (int i = 0; i < bStr.length; i++) {

            int index = ArrayUtils.indexOf(variables, bStr[i]);
            if (index >= 0) {
                bStr[i] = symbols[index];
            }
            if (bStr[i] == ')') {
                stack.push(bStr[i]);
                stack.push('+');
                flagged = false;
            } else if (flagged || bStr[i] == '(') {
                stack.push(bStr[i]);
                flagged = true;
            } else {
                stack.push('\'');
                stack.push(bStr[i]);
                stack.push('\'');
                stack.push('+');
            }
        }
        char[] abc = new char[stack.size()];

        for (int i = 0; i < stack.size(); i++) {
            abc[i] = stack.get(i);
        }
        String result = new String(abc);
        result = StringUtils.trimTrailingCharacter(result, '+');
        log.debug("for input pattern {}, expression array is {}", new String(bStr), result);
        return result;
    }

    /**
     * Create Symbol table
     * 
     * @param source the source array
     * @param stream character stream
     * @param var list of variables
     * @return symbols
     */
    private static char[] createSymbols(char[] source, char[] stream, Character[] var) {

        char[] symbols = new char[var.length];
        // create symbol map
        for (int i = 0; i < var.length; i++) {
            int index = ArrayUtils.indexOf(stream, var[i].charValue());
            if (index >= 0) {
                symbols[i] = source[index];
            }
        }
        return symbols;
    }
}
