/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.utils;

/**
 * utility for Acquisition handlers
 * 
 * @author SO Development Team
 */
public class AcquisitionUtils {

    /**
     * Generate promocode
     * 
     * @param alphabeticCount alphabetic count in promo code
     * @param numericCount numeric Count in promo code
     * @return promocode Alphanumeric code
     */
    public static String promocodeGenerator(int alphabeticCount, int numericCount) {
        String alphabeticString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String numericString = "1234567890";
        StringBuilder builder = new StringBuilder();

        while (alphabeticCount-- != 0) {
            int character = (int) (Math.random() * alphabeticString.length());
            builder.append(alphabeticString.charAt(character));
        }
        while (numericCount-- != 0) {
            int character = (int) (Math.random() * numericString.length());
            builder.append(numericString.charAt(character));
        }
        return builder.toString();
    }
}
