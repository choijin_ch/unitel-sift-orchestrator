/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Creates core response for ACQ API
 * 
 * @author SO Development Team
 *
 */
@Component("CoreResponseProcessor")
public class CoreResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Value("${so.acquisition.transaction.sql}")
    private String acqTxnQuery;

    @Value("${so.acquisition.updateAvailableNumber.sql}")
    private String updateAvailableNumberQuery;

    @Override
    public void process(Exchange exchange) throws Exception {
        String unitelMsisdn = (String) exchange.getProperty("reservedMSISDN");
        String nonUnitelMsisdn = (String) exchange.getProperty("nonUnitelMSISDN");
        String promocode = (String) exchange.getProperty("promocode");
        String status = (String) exchange.getProperty("status");
        String description = (String) exchange.getProperty("description");
        String pattern = (String) exchange.getProperty("pattern");
        String type = (String) exchange.getProperty("type");
        String snapshotDate = exchange.getProperty("snapshotDate").toString();
        String coreResponse = "";
        if (status.equals("200")) {
            insertAcquisitionTxnAndAvailableNumberTables(unitelMsisdn, nonUnitelMsisdn, promocode, pattern, status,
                    "NEW", description, null, null, null, null, null,
                    (Date) exchange.getProperty("reservationEndDate"));
            updateReservedForInAvailableUnitelNumuberTable(unitelMsisdn,
                    (Date) exchange.getProperty("reservationEndDate"));
            coreResponse = prepareCoreResponse(unitelMsisdn, nonUnitelMsisdn, promocode, status, description, pattern,
                    type, snapshotDate);

        } else {
            insertAcquisitionTxnAndAvailableNumberTables(null, nonUnitelMsisdn, promocode, "", status, "NEW",
                    description, null, null, null, null, null, null);
            coreResponse = prepareCoreResponse("", nonUnitelMsisdn, promocode, status, description, "", "",
                    snapshotDate);
        }
        exchange.getIn().setBody(coreResponse);
    }

    private void updateReservedForInAvailableUnitelNumuberTable(String unitelMsisdn, Date reservationEndDate) {
        jdbcTemplate.update(updateAvailableNumberQuery, new Object[] { reservationEndDate, unitelMsisdn });
    }

    /**
     * 
     * Prepares JSON response message
     * 
     * @param reservedMsisdn reservedMsisdn
     * @param nonUnitelMsisdn nonUnitelMsisdn
     * @param promocode promocode
     * @param status status
     * @param description description
     * @param pattern pattern
     * @param type type
     * @param snapshotDate snapshotDate
     * @return JSON String
     * @throws JsonProcessingException JsonProcessingException
     */
    private String prepareCoreResponse(String reservedMsisdn, String nonUnitelMsisdn, String promocode, String status,
            String description, String pattern, String type, String snapshotDate) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();

        node.put("nonUnitelMSISDN", nonUnitelMsisdn);
        node.put("unitelMSISDN", reservedMsisdn);
        node.put("promocode", promocode);
        node.put("type", type);
        node.put("pattern", pattern);
        node.put("snapshotDate", snapshotDate);
        node.put("statusCode", status);
        node.put("statusDescription", description);

        String response = jsonObjectMapper.writeValueAsString(node);
        log.info("Response to core: {}", response);
        return response;
    }

    /**
     * 
     * Insert Values into acquisition txn table
     * 
     * @param unitelMsisdn unitelMsisdn
     * @param nonUnitelMsisdn nonUnitelMsisdn
     * @param promocode promocode
     * @param pattern pattern
     * @param status status
     * @param state state
     * @param statusReason statusReason
     * @param revChurn revChurn
     * @param subscriptionKey subscriptionKey
     * @param contractId contractId
     * @param customerId customerId
     * @param familyId familyId
     * @param reservedFor reservedFor
     */
    private void insertAcquisitionTxnAndAvailableNumberTables(String unitelMsisdn, String nonUnitelMsisdn,
            String promocode, String pattern, String status, String state, String statusReason, String revChurn,
            String subscriptionKey, String contractId, String customerId, String familyId, Date reservedFor) {
        jdbcTemplate.update(acqTxnQuery, new Object[] { unitelMsisdn, nonUnitelMsisdn, promocode, pattern, status,
                state, statusReason, revChurn, subscriptionKey, contractId, customerId, familyId, reservedFor });
    }

}
