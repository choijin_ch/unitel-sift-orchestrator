/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Prepares reserve API response
 * 
 * @author SO Development Team
 *
 */
@Component("ReserveApiResponseProcessor")
public class ReserveApiResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        String responseJson = exchange.getIn().getBody(String.class);
        String unitelMsisdn = (String) exchange.getProperty("reservedMSISDN");
        log.info("Response from UO reserve API call for MSISDN {} is {}", unitelMsisdn, responseJson);
        JsonNode responseNode = jsonObjectMapper.readTree(responseJson);
        String statusCode = responseNode.path("statusCode").asText();
        String statusMessage = responseNode.path("statusMessage").asText();
        if (statusCode.equals("000")) {
            exchange.setProperty("status", "200");
            exchange.setProperty("description", statusMessage);
        } else {
            exchange.setProperty("status", "500");
            exchange.setProperty("description", statusMessage);
        }
    }
}
