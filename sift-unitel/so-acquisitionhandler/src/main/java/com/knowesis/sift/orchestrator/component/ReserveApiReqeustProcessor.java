/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Creates request for Reserve API call
 * @author SO Development Team
 *
 */
@Component("ReserveApiReqeustProcessor")
public class ReserveApiReqeustProcessor implements Processor {
    
    private final Logger log = LoggerFactory.getLogger(getClass());
    
    @Autowired
    ObjectMapper jsonObjectMapper;
    
    @Value("${so.acquisition.uo.reserveapi.reason}")
    private String reserveApiReason;

    @Value("${so.acquisition.uo.reserveapi.user}")
    private String reserveApiUser;

    @Override
    public void process(Exchange exchange) throws Exception {
        String unitelMsisdn = (String) exchange.getProperty("reservedMSISDN");
        String reserveFor = (String) exchange.getProperty("reserveFor");
        String nonUnitelNumber = (String) exchange.getProperty("nonUnitelMSISDN");
        String promocode = (String) exchange.getProperty("promocode");
        exchange.getIn().removeHeaders("*");
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, Integer.parseInt(reserveFor));
        //For end of the day reservation
        c.add(Calendar.MILLISECOND, getRemainingMillisecondForToday());
        date = c.getTime();
        exchange.setProperty("reservationEndDate", date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String reserveEndDate = dateFormat.format(date);
        log.info("Reserve End Date after adding {} day(s) for unitel number {} is {}", reserveFor, unitelMsisdn,
                reserveEndDate);
        String uoReserveApiRequest = createUoReserveApiRequest(unitelMsisdn, reserveEndDate, nonUnitelNumber, promocode);
        log.info("UO Reserve API request: {}", uoReserveApiRequest);
        exchange.getIn().setBody(uoReserveApiRequest);
    }
    
    
    /**
     * 
     * Create request object for UO reserve API call
     * 
     * @param unitelMsisdn unitelMsisdn
     * @param reserveEndDate reserveEndDate
     * @param nonUnitelNumber nonUnitelNumber
     * @param promocode promocode
     * @return UO request JSON
     * @throws JsonProcessingException exception thrown
     */
    private String createUoReserveApiRequest(String unitelMsisdn, String reserveEndDate, String nonUnitelNumber,
            String promocode) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();
        node.put("msisdn", unitelMsisdn);
        node.put("reserve_enddate", reserveEndDate);
        node.put("transaction_id", String.valueOf(UUID.randomUUID()));
        node.put("reason", reserveApiReason);
        node.put("user", reserveApiUser);
        node.put("non_uni_msisdn", nonUnitelNumber);
        node.put("promo_code", promocode);
        return jsonObjectMapper.writeValueAsString(node);
    }
    
    /**
     * 
     * Get past milliseconds
     * @return getPastMilliseconds
     */
    private static int getRemainingMillisecondForToday() {
        Calendar c = Calendar.getInstance();
        long now = c.getTimeInMillis();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long passedLong = now - c.getTimeInMillis();
        String passedString = "" + passedLong;
        //log.info("Millisecond from today's midnight: {}", passedLong);
        return 86400000 - Integer.parseInt(passedString);//to set time as previous day End Of Day
    }

}
