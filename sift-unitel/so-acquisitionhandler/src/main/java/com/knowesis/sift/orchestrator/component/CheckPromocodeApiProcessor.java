/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * 
 * Processes Check promocode API call
 * 
 * @author SO Development Team
 */
@Component("CheckPromocodeApiProcessor")
public class CheckPromocodeApiProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Value("${so.acquisition.checkpromocode.sql}")
    private String checkPromocodeSql;

    @Value("${so.notification.template}")
    private String soMessageTemplate;

    @Value("${so.db.insert}")
    String sql;

    @Override
    public void process(Exchange exchange) throws Exception {
        String requestBody = exchange.getIn().getBody(String.class);
        log.info("Check Promocode API call request received from core: {}", requestBody);
        JsonNode rootNode = jsonObjectMapper.readTree(requestBody);
        String promocode = rootNode.path("promo_code").asText();
        String nonUnitelMsisdn = rootNode.path("non_uni_msisdn").asText();
        String unitelMsisdn = "";//rootNode.path("uni_msisdn").asText();
        String checkDate = rootNode.path("check_date").asText();
        String statusCode = "";
        String statusMessage = "";
        String offerId = "";
        String programId = "";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(checkPromocodeSql,
                new Object[] { nonUnitelMsisdn, promocode });
        if (isRequestValid(promocode, nonUnitelMsisdn, checkDate)) {
            if (result != null && result.size() > 0) {
                Map<String, Object> row = result.get(0);
                offerId = (String) row.get("OFFER_ID");
                programId = (String) row.get("PROGRAM_ID");
                log.info("fetched programId and offer Id: {}, {}", offerId, programId);
                unitelMsisdn = (String) row.get("SELECTED_NEW_MSISDN");
                Map<String, String> codeAndMessage = validatePromocodeResult(result.get(0), checkDate);
                statusCode = codeAndMessage.get("statusCode");
                statusMessage = codeAndMessage.get("statusMessage");

                /**** Insert Records only if it is successful , invalid requests should not be logged **/
                insertAcqOptinDBEtry(promocode, nonUnitelMsisdn, checkDate, statusCode, statusMessage, result);
                insertAcqOptinNotifyDBEntry(promocode, nonUnitelMsisdn, checkDate, statusCode, statusMessage, result);


            } else {
                statusCode = "002";
                statusMessage = "Invalid Promo code";
            }
        } else {
            statusCode = "111";
            statusMessage = "Mandatory Parameter Missing";
        }

        exchange.getIn().setBody(createCheckPromocodeResponse(promocode, nonUnitelMsisdn, unitelMsisdn, checkDate,
                statusCode, statusMessage, programId, offerId));

    }

    /**
     * 
     * Inserts ACQ optin notify db entry
     * 
     * @param promocode promocode
     * @param nonUnitelMsisdn nonUnitelMsisdn
     * @param checkDate checkDate
     * @param statusCode statusCode
     * @param statusMessage statusMessage
     * @param result result
     */
    private void insertAcqOptinNotifyDBEntry(String promocode, String nonUnitelMsisdn, String checkDate,
            String statusCode, String statusMessage, List<Map<String, Object>> result) {
        String offerId = null, programId = null, triggerId = null, flowId = null;
        if (result != null && result.size() > 0) {
            Map<String, Object> row = result.get(0);
            offerId = (String) row.get("OFFER_ID");
            flowId = (String) row.get("FLOW_ID");
            triggerId = (String) row.get("EVENT_ID");
            programId = (String) row.get("PROGRAM_ID");
        }

        insertEventTriggerEntry(nonUnitelMsisdn, offerId, programId, triggerId, flowId, null/* "monitoredValue" */,
                null/* "actualResponseValue" */, null/* "provisioningOfferId" */, null/* "provisionedValue" */, null /*provisioningSystem*/,
                getUnixTimestamp()/* triggerTimestamp */, null/* "notificationMessage" */, statusMessage, "ACQ-OPTINNOTIFY",
                "NOTIFY", "UO", "SUCCESS", "false", getUnixTimestamp(), "false", null/* offerStartDate */,
                null/* offerEndDate */, null/* "subscriptionId" */, "OUTBOUND");

    }

    /**
     * 
     * Inserts ACQ optin db entry
     * 
     * @param promocode promocode
     * @param nonUnitelMsisdn nonUnitelMsisdn
     * @param checkDate checkDate
     * @param statusCode statusCode
     * @param statusMessage statusMessage
     * @param result result
     */
    private void insertAcqOptinDBEtry(String promocode, String nonUnitelMsisdn, String checkDate, String statusCode,
            String statusMessage, List<Map<String, Object>> result) {
        String offerId = null, programId = null, triggerId = null, flowId = null;
        if (result != null && result.size() > 0) {
            Map<String, Object> row = result.get(0);
            offerId = (String) row.get("OFFER_ID");
            flowId = (String) row.get("FLOW_ID");
            triggerId = (String) row.get("EVENT_ID");
            programId = (String) row.get("PROGRAM_ID");
        }

        insertEventTriggerEntry(nonUnitelMsisdn, offerId, programId, triggerId, flowId, null/* "monitoredValue" */,
                null/* "actualResponseValue" */, null/* "provisioningOfferId" */, null/* "provisionedValue" */, null /*provisioningSystem*/,
                getUnixTimestamp()/* triggerTimestamp */, null/* "notificationMessage" */, promocode, "ACQ-OPTIN",
                "OPTIN", "UO", "SUCCESS", "false", getUnixTimestamp(), "false", null/* offerStartDate */,
                null/* offerEndDate */, null/* "subscriptionId" */, "OUTBOUND");

    }

    /**
     * 
     * Inserts Event trigger record
     * 
     * @param msisdn msisdn
     * @param offerId offerId
     * @param programId programId
     * @param triggerId triggerId
     * @param flowId flowId
     * @param monitoredValue monitoredValue
     * @param actualResponseValue actualResponseValue
     * @param provisioningOfferId provisioningOfferId
     * @param provisionedValue provisionedValue
     * @param provisioningSystem provisioningSystem
     * @param triggerTimestamp triggerTimestamp
     * @param notificationMessage notificationMessage
     * @param actionResponse actionResponse
     * @param triggerType triggerType
     * @param actionType actionType
     * @param channel channel
     * @param actionStatus actionStatus
     * @param isSimulated isSimulated
     * @param createdOn createdOn
     * @param isControl isControl
     * @param offerStartDate offerStartDate
     * @param offerEndDate offerEndDate
     * @param subscriptionId subscriptionId
     * @param contactDirection contactDirection
     */
    private void insertEventTriggerEntry(String msisdn, String offerId, String programId, String triggerId,
            String flowId, String monitoredValue, String actualResponseValue, String provisioningOfferId,
            String provisionedValue, String provisioningSystem, Timestamp triggerTimestamp, String notificationMessage,
            String actionResponse, String triggerType, String actionType, String channel, String actionStatus,
            String isSimulated, Timestamp createdOn, String isControl, Timestamp offerStartDate, Timestamp offerEndDate,
            String subscriptionId, String contactDirection) {
        jdbcTemplate.update(sql, new Object[] { msisdn, offerId, programId, triggerId, flowId, monitoredValue,
                actualResponseValue, provisioningOfferId, provisionedValue, provisioningSystem, triggerTimestamp,
                notificationMessage, actionResponse, triggerType, actionType, channel, actionStatus, isSimulated,
                createdOn, isControl, offerStartDate, offerEndDate, subscriptionId, contactDirection });
    }

    /**
     * 
     * Validates the promocode result
     * 
     * @param row row
     * @param checkDate checkDate
     * @return result map
     * @throws ParseException exception
     */
    private Map<String, String> validatePromocodeResult(Map<String, Object> row, String checkDate)
            throws ParseException {
        String state = (String) row.get("STATE");
        Date reservedFor = (Date) row.get("RESERVEDFOR");
        Map<String, String> codeAndMessage = new HashMap<String, String>();
        //Status check
        if (state.equals("MSISDN_RESERVED_SUCCESS") || state.equals("CORE_FEED_SENT") || state.equals("NOTIFY")) {
            codeAndMessage.put("statusCode", "004");
            codeAndMessage.put("statusMessage", "Promo code used already");
            return codeAndMessage;
        } 
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date promocodeCheckDate = sdf.parse(checkDate);
        //Number not reserved.
        if(reservedFor != null) {
            if (!promocodeCheckDate.before(reservedFor)) {
                codeAndMessage.put("statusCode", "001");
                codeAndMessage.put("statusMessage", "Promo code Expired");
                return codeAndMessage;
            }
        } else {
            Date offerEndDate = (Date) row.get("OFFER_END_DATE");
            log.info("OFFER_END_DATE:{}", row);
            if(!promocodeCheckDate.before(offerEndDate)) {
                codeAndMessage.put("statusCode", "001");
                codeAndMessage.put("statusMessage", "Promo code Expired");
                return codeAndMessage;
            }
        }
        codeAndMessage.put("statusCode", "000");
        codeAndMessage.put("statusMessage", "Valid");
        return codeAndMessage;
    }

    /**
     * 
     * Validates the incoming request
     * 
     * @param promocode promocode
     * @param nonUnitelMsisdn nonUnitelMsisdn
     * @param checkDate checkDate
     * @return true/false
     */
    private boolean isRequestValid(String promocode, String nonUnitelMsisdn, String checkDate) {
        if (StringUtils.isNotBlank(promocode) && StringUtils.isNotBlank(nonUnitelMsisdn)
                && StringUtils.isNotBlank(checkDate))
            return true;
        return false;
    }

    /**
     * 
     * Creates JSON response for check promocode API
     * 
     * @param promocode promocode
     * @param nonUnitelMsisdn nonUnitelMsisdn
     * @param unitelMsisdn unitelMsisdn
     * @param checkDate checkDate
     * @param statusCode statusCode
     * @param statusMessage statusMessage
     * @param programId programId
     * @param offerId offerId
     * @return jsonResponseString
     * @throws JsonProcessingException JsonProcessingException
     */
    private String createCheckPromocodeResponse(String promocode, String nonUnitelMsisdn, String unitelMsisdn,
            String checkDate, String statusCode, String statusMessage, String programId, String offerId) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();
        node.put("promo_code", promocode);
        node.put("non_uni_msisdn", nonUnitelMsisdn);
        node.put("uni_msisdn", unitelMsisdn);
        node.put("check_date", checkDate);
        node.put("Program_ID", StringUtils.isNotBlank(programId) ? programId : "ProgramID");
        node.put("Offer_ID", StringUtils.isNotBlank(offerId) ? offerId : "OfferID");
        node.put("statusCode", statusCode);
        node.put("statusMessage", statusMessage);
        return jsonObjectMapper.writeValueAsString(node);
    }

    /**
     * 
     * To generate timestamp in UNIX format as the core requires it
     * 
     * @return timestamp String in UNIX format
     */
    private static Timestamp getUnixTimestamp() {
        Calendar cal = Calendar.getInstance();

        // Get the time in local timezone
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);
        int seconds = cal.get(Calendar.SECOND);
        // Set the current time as GMT Time
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minutes);
        cal.set(Calendar.SECOND, seconds);

        return new java.sql.Timestamp(cal.getTime().getTime());
    }
}
