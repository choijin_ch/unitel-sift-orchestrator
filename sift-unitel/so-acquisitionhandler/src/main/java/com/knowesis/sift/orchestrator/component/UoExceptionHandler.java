/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.component;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

/**
 * Handles when ConnectException in UO system
 * 
 * @author SO Development Team
 *
 */
@Component("UoExceptionHandler")
public class UoExceptionHandler implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        exchange.setProperty("status", "500");
        exchange.setProperty("description", "RESERVE-API-FAILURE");

    }

}
