/**
 * 
 */
package com.knowesis.sift.orchestrator.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author SO Development Team
 *
 */
public class ComplexSearchUtilTest {

    /**
     * tests search patterns
     */
    @Test
    public void searchPatterns1() {

        String nonUnitel = "87128712";
        String unitel = "89128912";
        String pattern = "8*ab8*ab";

        String actual = ComplexSearchUtils.match(unitel, nonUnitel, pattern);

        Assert.assertEquals(pattern, actual);
    }

    /**
     * tests search patterns
     */
    @Test
    public void searchPatterns2() {

        String nonUnitel = "97128712";
        String unitel = "89128912";
        String pattern = "8*xxaaab";

        String actual = ComplexSearchUtils.match(unitel, nonUnitel, pattern);

        Assert.assertNull(actual);
    }

    /**
     * tests search patterns
     */
    @Test
    public void searchPatterns3() {

        String nonUnitel = "87128712";
        String unitel = "89128912";
        String pattern = "8*xxabba";

        String actual = ComplexSearchUtils.match(unitel, nonUnitel, pattern);

        Assert.assertNull(actual);
    }

    /**
     * tests search patterns
     */
    @Test
    public void searchPatterns4() {

        String nonUnitel = "87128712";
        String unitel = "89121213";
        String pattern = "8*xxaba(b+1)";

        String actual = ComplexSearchUtils.match(unitel, nonUnitel, pattern);

        Assert.assertEquals(pattern, actual);
    }

    /**
     * tests search patterns
     */
    @Test
    public void searchPatterns5() {

        String nonUnitel = "99974646";
        String unitel = "89974646";
        String pattern = "8*xxabab";

        String actual = ComplexSearchUtils.match(unitel, nonUnitel, pattern);

        Assert.assertEquals(pattern, actual);
    }

    /**
     * tests search patterns
     */
    @Test
    public void searchPatterns6() {

        String nonUnitel = "99974646";
        String unitel = "88461727";
        String pattern = "8*xxab(a+1)b";

        String actual = ComplexSearchUtils.match(unitel, nonUnitel, pattern);

        Assert.assertEquals(pattern, actual);
    }

    /**
     * tests search patterns
     */
    // @Test
    public void searchPatterns7() {

        String nonUnitel = "99974646";
        String unitel = "88465767";
        String pattern = "8*ax(a+1)x(a+2)x";

        String actual = ComplexSearchUtils.match(unitel, nonUnitel, pattern);

        Assert.assertEquals(pattern, actual);
    }

    /**
     * tests search patterns
     */
    @Test
    public void searchPatterns8() {

        String nonUnitel = "99974646";
        String unitel = "88465666";
        String pattern = "8*ab(a+1)b(a+2)b";

        String actual = ComplexSearchUtils.match(unitel, nonUnitel, pattern);

        Assert.assertEquals(pattern, actual);
    }
    
    /**
     * tests search patterns
     */
    @Test
    public void searchPatterns9() {

        String nonUnitel = "98606699";
        String unitel = "88606699";
        String pattern = "8*xxaabb";

        String actual = ComplexSearchUtils.match(unitel, nonUnitel, pattern);

        Assert.assertEquals(pattern, actual);
    }
    
    /**
     * tests search patterns
     */
    @Test
    public void searchPatterns10() {

        String nonUnitel = "96333386";
        String unitel = "86333389";
        String pattern = "8*aaaa8*";

        String actual = ComplexSearchUtils.match(unitel, nonUnitel, pattern);

        Assert.assertEquals(pattern, actual);
    }

    /**
     * tests preparepattern
     */
    @Test
    public void preparePatterns1() {

        String actual = ComplexSearchUtils.preparePatterns("8xab8xa*");
        Assert.assertEquals("8[0-9]{1}ab8[0-9]{1}a.{1}", actual);
    }

    /**
     * Tests overlay
     */
    @Test
    public void overlay1() {

        Character[] c = { 'a', 'b' };
        String actual = ComplexSearchUtils.overlay("86121111", "8*ab8*ab", c);
        Assert.assertEquals("8*128*12", actual);
    }
}
