/**
 * Copyright (c) 2017, KNOWESIS PTE LTD. All rights reserved.
 * KNOWESIS PTE LTD. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.knowesis.sift.orchestrator.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * 
 * @author SO Development Team
 */
@Component("MonthlyDiscountRequestProcessor")
public class MonthlyDiscountRequestProcessor implements Processor {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	ObjectMapper jsonObjectMapper;

	@Value("${externalsmsc.operationId.value}")
	String operationId;

	String addDiscount = "adddiscount";
	String cancelDiscount = "canceldiscount";
	String freeseDiscount = "freesediscount";

	@Override
	public void process(Exchange exchange) throws Exception {
		SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
		String requestBody = "";
		if (soMessage != null) {
			String msisdn = SOMessageUtils.getMSISDN(soMessage);
			exchange.getIn().setHeader("msisdn", msisdn);
			String targetSystemValue = (String) exchange.getIn().getHeader("targetSystemValue");
			String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
			FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
					currentSequence);
			ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
			requestBody = routeAccordingToTargetSystemValue(exchange, soMessage, fulfillmentProduct, targetSystemValue);
			exchange.getIn().setBody(requestBody);
		}
		exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
	}

	/**
	 * route and set json body according to the recived fulfillment message
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 * @param fulfillmentProduct
	 *            fulfillmentProduct
	 * @param targetSystemValue
	 *            targetSystemValue
	 * @return requestbody prepared as json string
	 * @throws JsonProcessingException
	 *             Exception
	 */
	private String routeAccordingToTargetSystemValue(Exchange exchange, SOMessage soMessage,
			FulfillmentProduct fulfillmentProduct, String targetSystemValue) throws JsonProcessingException {
		String requestBody = "";
		if ("AddDiscount".equalsIgnoreCase(targetSystemValue)) {
			requestBody = createRequestForAddDiscount(exchange, soMessage, fulfillmentProduct);
		} else if ("CancelDiscount".equalsIgnoreCase(targetSystemValue)) {
			requestBody = createRequestForCancelDiscount(exchange, soMessage, fulfillmentProduct);
		} else if ("FreezeDiscount".equalsIgnoreCase(targetSystemValue)) {
			requestBody = createRequestForFreezeDiscount(exchange, soMessage, fulfillmentProduct);
		} else {
			log.info("Traget system API value is not authorised");
		}
		return requestBody;
	}

	/**
	 * create request to freeze discount
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 * @param fulfillmentProduct
	 *            fulfillmentProduct
	 * @return json string
	 * @throws JsonProcessingException
	 *             Exception
	 */
	private String createRequestForFreezeDiscount(Exchange exchange, SOMessage soMessage,
			FulfillmentProduct fulfillmentProduct) throws JsonProcessingException {
		String contractId = SOMessageUtils.getSubscriptionId(soMessage);
		String offerId = SOMessageUtils.getOfferId(soMessage);
		String chargeMonth = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "ChargeMonth");
		String discountType = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "DISCOUNTTYPE");
		exchange.getIn().setHeader("urlSuffix", freeseDiscount);
		return prepareFreezeDiscountRequestBody(contractId, discountType, offerId, chargeMonth);
	}

	/**
	 * create json requestbody
	 * 
	 * @param contractId
	 *            contractId
	 * @param discountType
	 *            discountType
	 * @param offerId
	 *            offerId
	 * @param chargeMonth
	 *            chargeMonth
	 * @return json string
	 * @throws JsonProcessingException
	 *             Exception
	 */
	private String prepareFreezeDiscountRequestBody(String contractId, String discountType, String offerId,
			String chargeMonth) throws JsonProcessingException {
		ObjectNode node = jsonObjectMapper.createObjectNode();
		node.put("contractId", contractId);
		node.put("discountType", discountType);
		node.put("charge_month", chargeMonth);
		node.put("offerID", offerId);
		return jsonObjectMapper.writeValueAsString(node);
	}

	/**
	 * create request for cancel discount
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 * @param fulfillmentProduct
	 *            fulfillmentProduct
	 * @return json body
	 * @throws JsonProcessingException
	 *             Exception
	 */
	private String createRequestForCancelDiscount(Exchange exchange, SOMessage soMessage,
			FulfillmentProduct fulfillmentProduct) throws JsonProcessingException {
		String contractId = SOMessageUtils.getSubscriptionId(soMessage);
		String offerId = SOMessageUtils.getOfferId(soMessage);
		String discountType = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "DISCOUNTTYPE");
		exchange.getIn().setHeader("urlSuffix", cancelDiscount);
		return prepareCancelDiscountRequestBody(contractId, offerId, discountType);
	}

	/**
	 * create json request body
	 * 
	 * @param contractId
	 *            contractId
	 * @param offerId
	 *            offerId
	 * @param discountType
	 *            discountType
	 * @return json as string
	 * @throws JsonProcessingException
	 *             Exception
	 */
	private String prepareCancelDiscountRequestBody(String contractId, String offerId, String discountType)
			throws JsonProcessingException {
		ObjectNode node = jsonObjectMapper.createObjectNode();
		node.put("contractId", contractId);
		node.put("discountType", discountType);
		node.put("offerID", offerId);
		return jsonObjectMapper.writeValueAsString(node);
	}

	/**
	 * createRequestForAddDiscount
	 * 
	 * @param exchange
	 *            exchange
	 * @param soMessage
	 *            soMessage
	 * @param fulfillmentProduct
	 *            fulfillmentProduct
	 * @return jsonbody
	 * @throws JsonProcessingException
	 *             Exception
	 */
	private String createRequestForAddDiscount(Exchange exchange, SOMessage soMessage,
			FulfillmentProduct fulfillmentProduct) throws JsonProcessingException {
		String contractId = SOMessageUtils.getSubscriptionId(soMessage);
		String offerId = SOMessageUtils.getOfferId(soMessage);
		// String marketCd = CBSUtils.getmarketCd(soMessage);
		String marketCd = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "MARKETCODE");
		String startMonth = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "STARTMONTH");
		String endMonth = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "ENDMONTH");
		String discountValue = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "DISCOUNTVALUE");
		// String discountType = CBSUtils.getDiscountType(soMessage);
		String discountType = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "DISCOUNTTYPE");
		exchange.getIn().setHeader("urlSuffix", addDiscount);
		return prepareAddDiscountRequestBody(contractId, offerId, marketCd, startMonth, endMonth, discountValue,
				discountType);

	}

	/**
	 * set json key and values
	 * 
	 * @param contractId
	 *            contractId
	 * @param offerId
	 *            offerId
	 * @param marketCd
	 *            marketCode
	 * @param startMonth
	 *            startMonth
	 * @param endMonth
	 *            endMonth
	 * @param discountValue
	 *            discountValue
	 * @param discountType
	 *            discountType
	 * @return json body
	 * @throws JsonProcessingException
	 *             Exception
	 */
	private String prepareAddDiscountRequestBody(String contractId, String offerId, String marketCd, String startMonth,
			String endMonth, String discountValue, String discountType) throws JsonProcessingException {
		ObjectNode node = jsonObjectMapper.createObjectNode();
		node.put("contractId", contractId);
		node.put("marketCd", marketCd);
		node.put("startMonth", startMonth);
		node.put("endMonth", endMonth);
		node.put("discountValue", discountValue);
		node.put("operatorId", operationId);
		node.put("offerId", offerId);
		node.put("discountType", discountType);
		return jsonObjectMapper.writeValueAsString(node);
	}

}
