package com.knowesis.sift.orchestrator.processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Calendar;

/**
 * Prepares API request
 *
 * @author SO Development Team
 */

@Component("UPointRequestProcessor")
public class UPointRequestProcessor implements Processor {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${externalsmsc.operationId.value}")
    String operationId;


    @Override
    public void process(Exchange exchange) throws Exception {
        SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
        String requestBody = "";
        if (soMessage != null) {
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            exchange.getIn().setHeader("msisdn", msisdn);
            String targetSystemValue = (String) exchange.getIn().getHeader("targetSystemValue");
            String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
            FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                    currentSequence);
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
            requestBody = routeAccordingToTargetSystemValue(exchange, soMessage, fulfillmentProduct, targetSystemValue);
            exchange.getIn().setBody(requestBody);
        }
        exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
    }


    /**
     * route and set json body according to the recived fulfillment message
     *
     * @param exchange           exchange
     * @param soMessage          soMessage
     * @param fulfillmentProduct fulfillmentProduct
     * @param targetSystemValue  targetSystemValue
     * @return requestbody prepared as json string
     * @throws JsonProcessingException Exception
     */
    private String routeAccordingToTargetSystemValue(Exchange exchange, SOMessage soMessage,
                                                     FulfillmentProduct fulfillmentProduct, String targetSystemValue) throws JsonProcessingException {
        String requestBody = "";
        if ("ChangeUPointBalance".equalsIgnoreCase(targetSystemValue)) {
            requestBody = createRequestForChangeBalance(exchange, soMessage, fulfillmentProduct);
        } else {
            log.info("Traget system API value is not authorised");
        }
        return requestBody;
    }


    /**
     * createRequestForDeductBalance
     *
     * @param exchange           exchange
     * @param soMessage          soMessage
     * @param fulfillmentProduct fulfillmentProduct
     * @return jsonbody
     * @throws JsonProcessingException Exception
     */
    private String createRequestForChangeBalance(Exchange exchange, SOMessage soMessage,
                                                 FulfillmentProduct fulfillmentProduct) throws JsonProcessingException {
        //String contractId = SOMessageUtils.getSubscriptionId(soMessage);
        String msisdn = SOMessageUtils.getMSISDN(soMessage);

        String contractId = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "CONTRACTID");
        double total = Double.parseDouble(ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "TOTAL"));
        double bonus = Double.parseDouble(ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "BONUS"));
        double cash = Double.parseDouble(ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "CASH"));
        double spend = Double.parseDouble(ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "SPEND"));
        String promotionId = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "CAMPAIGNID");
        String promotionName = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "CAMPAIGNNAME");
        String userType = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "USERTYPE");
        String sellProduct = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "SELLPRODUCT");
        String itemCode = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "OFFERID");
        String itemName = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "OFFERNAME");
        String description = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "DESCRIPTION");
        int type = Integer.parseInt(ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "TYPE"));
        String mainProduct = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "MAINPRODUCT");
        String company = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "COMPANY");


        //exchange.getIn().setHeader("urlSuffix", deductbalance);
        return prepareChangeBalanceRequestBody(contractId,
                msisdn,
                total,
                bonus,
                cash,
                spend,
                promotionId,
                promotionName,
                userType,
                sellProduct,
                itemCode,
                itemName,
                description,
                type,
                mainProduct,
                company);

    }


    /**
     * set json key and values
     *
     * @param contractId
     * @param msisdn
     * @param total         (bomus + cash + spend)
     * @param bonus         (will be added upoint amaount)
     * @param cash          (charged money)
     * @param spend         (charged upoint)
     * @param promotionId   (campaign id)
     * @param promotionName (campaign short name)
     * @param userType      (if personal = 1 corp = 2)
     * @param sellProduct   (selling product or counter id and value)
     * @param itemCode      (offer id)
     * @param itemName      (offer name)
     * @param description   (description)
     * @param type          (if deduct = 1 add = 2)
     * @param mainProduct   (main product id)
     * @param company       (Unitel, Univision)
     * @return Jsonbody
     * @throws JsonProcessingException Exception
     */

    private String prepareChangeBalanceRequestBody(String contractId,
                                                   String msisdn,
                                                   double total,
                                                   double bonus,
                                                   double cash,
                                                   double spend,
                                                   String promotionId,
                                                   String promotionName,
                                                   String userType,
                                                   String sellProduct,
                                                   String itemCode,
                                                   String itemName,
                                                   String description,
                                                   int type,
                                                   String mainProduct,
                                                   String company

    ) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();
        node.put("contractId", contractId);
        node.put("branch", operationId);
        node.put("phone", msisdn.substring(3));
        node.put("total", total);
        node.put("bonus", bonus);
        node.put("cash", cash);
        node.put("spend", spend);
        node.put("sendDate", Calendar.getInstance().getTime().getTime() + "");
        node.put("employee", operationId);
        node.put("promotionId", promotionId);
        node.put("promotionName", promotionName);
        node.put("userType", userType); //if personal =1 corp=2
        node.put("sellProduct", sellProduct);
        node.put("itemCode", itemCode);
        node.put("itemName", itemName);
        node.put("description", description);
        node.put("source", "SIFT");
        node.put("type", type);
        node.put("mainProduct", mainProduct);
        node.put("companyId", company);


        return jsonObjectMapper.writeValueAsString(node);
    }


}
