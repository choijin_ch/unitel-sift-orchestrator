package com.knowesis.sift.orchestrator.processor;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.RawMessageUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Prepares API request
 *
 * @author SO Development Team
 */

@Component("UPointResponseProcessor")
public class UPointResponseProcessor implements Processor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Override
    public void process(Exchange exchange) throws Exception {
        String response = exchange.getIn().getBody(String.class);
        String message = (String) exchange.getProperty("OriginalRequestMessage");
        log.info("OriginalRequestMessage: {}", message);
        SOMessage soMessage = RawMessageUtils.processRawMessage(message, jsonObjectMapper);
        String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
        log.info("Response from UPOINT API: {}", response);
        JsonNode responseJsonArray = jsonObjectMapper.readTree(response);
        JsonNode responseJson = null;
        if (responseJsonArray.isArray()) {
            responseJson = responseJsonArray.get(0);
        } else {
            responseJson = responseJsonArray;
        }
        String responseCode = responseJson.get("result").asText();
        String responseReason = responseJson.get("message").asText();

        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-ACTION-RESPONSE",
                responseCode + ":" + responseReason);
        SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONED-TARGET-SYSTEM",
                ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage, currentSequence).getTargetSystem());
        if (responseCode.equals("0")) {
            exchange.getIn().setHeader("upointSuccess", "true");
            String point_balance = responseJson.get("point_balance").asText();
            SOMessageUtils.setHeaderValue(soMessage, "point_balance", point_balance);
            SOMessageUtils.setHeaderValue(soMessage, "message", responseReason);
            SOMessageUtils.setHeaderValue(soMessage, "result", responseCode);
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "SUCCESS");
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONING-STATUS", "SUCCESS");
        } else {
            exchange.getIn().setHeader("upointSuccess", "false");
            SOMessageUtils.setHeaderValue(soMessage, "message", responseReason);
            SOMessageUtils.setHeaderValue(soMessage, "result", responseCode);
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "FAILURE");
            SOMessageUtils.setHeaderValue(soMessage, "LAST-PROVISIONING-STATUS", "FAILURE");
        }
        exchange.getIn().setBody(jsonObjectMapper.writeValueAsString(soMessage));
        exchange.getIn().removeHeader("OriginalRequestMessage");
        exchange.getIn().removeHeader("targetAPI");
        exchange.removeProperty("OriginalRequestMessage");
    }
}
