    package com.knowesis.sift.orchestrator.processor;
    import org.apache.camel.Exchange;
    import org.apache.camel.Processor;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.beans.factory.annotation.Value;
    import org.springframework.stereotype.Component;

    import com.fasterxml.jackson.core.JsonProcessingException;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import com.fasterxml.jackson.databind.node.ObjectNode;
    import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
    import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
    import com.knowesis.sift.orchestrator.domain.SOMessage;
    import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
    import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

    /**
     * Prepares API request
     * @author SO Development Team
     */

    @Component("UpsellRequestProcessor")
    public class UpsellRequestProcessor implements Processor{
        private final Logger log = LoggerFactory.getLogger(getClass());

        @Autowired
        ObjectMapper jsonObjectMapper;

        @Value("${externalsmsc.operationId.value}")
        String operationId;

        String upsellMobile = "upsellmobile";
        String cancelUpsellMobile = "cancelupsellmobile";
        String upsellUnivision = "upsellunivision";

        @Override
        public void process(Exchange exchange) throws Exception {
            SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
            String requestBody = "";
            if (soMessage != null) {
                String msisdn = SOMessageUtils.getMSISDN(soMessage);
                exchange.getIn().setHeader("msisdn", msisdn);
                String targetSystemValue = (String) exchange.getIn().getHeader("targetSystemValue");
                String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
                FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                        currentSequence);
                ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
                requestBody = routeAccordingToTargetSystemValue(exchange, soMessage, fulfillmentProduct, targetSystemValue);
                exchange.getIn().setBody(requestBody);
            }
            exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
        }



        /**
         * route and set json body according to the recived fulfillment message
         *
         * @param exchange
         *            exchange
         * @param soMessage
         *            soMessage
         * @param fulfillmentProduct
         *            fulfillmentProduct
         * @param targetSystemValue
         *            targetSystemValue
         * @return requestbody prepared as json string
         * @throws JsonProcessingException
         *             Exception
         */
        private String routeAccordingToTargetSystemValue(Exchange exchange, SOMessage soMessage,
                                                         FulfillmentProduct fulfillmentProduct, String targetSystemValue) throws JsonProcessingException {
            String requestBody = "";
            if ("UpsellMobile".equalsIgnoreCase(targetSystemValue)) {
                requestBody = createRequestForUpsellMobile(exchange, soMessage, fulfillmentProduct);
            } else if ("CancelUpsellMobile".equalsIgnoreCase(targetSystemValue)) {
                requestBody = createRequestForCancelUpsellMobile(exchange, soMessage, fulfillmentProduct);
            } else if ("UpsellUnivision".equalsIgnoreCase(targetSystemValue)) {
                requestBody = createRequestForUpsellUnivision(exchange, soMessage, fulfillmentProduct);
            }
            else {
                log.info("Traget system API value is not authorised");
            }
            return requestBody;
        }





        /**
         * createRequestForUpsellMobile
         *
         * @param exchange
         *            exchange
         * @param soMessage
         *            soMessage
         * @param fulfillmentProduct
         *            fulfillmentProduct
         * @return jsonbody
         * @throws JsonProcessingException
         *             Exception
         */
        private String createRequestForUpsellMobile(Exchange exchange, SOMessage soMessage,
                                                   FulfillmentProduct fulfillmentProduct) throws JsonProcessingException {
            //String contractId = SOMessageUtils.getSubscriptionId(soMessage);
            String contractId = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "SUBID");
            String beforeproductid = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "BEFOREPRODUCTID");
            String productid = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "PRODUCTID");
            exchange.getIn().setHeader("urlSuffix", upsellMobile);
            return prepareMobileUpsellRequestBody(contractId, beforeproductid, productid);

        }




        /**
         * createRequestForCancelUpsellMobile
         *
         * @param exchange
         *            exchange
         * @param soMessage
         *            soMessage
         * @param fulfillmentProduct
         *            fulfillmentProduct
         * @return jsonbody
         * @throws JsonProcessingException
         *             Exception
         */
        private String createRequestForCancelUpsellMobile(Exchange exchange, SOMessage soMessage,
                                                    FulfillmentProduct fulfillmentProduct) throws JsonProcessingException {
            //String contractId = SOMessageUtils.getSubscriptionId(soMessage);
            String contractId = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "SUBID");
            exchange.getIn().setHeader("urlSuffix", cancelUpsellMobile);
            return prepareMobileCancelUpsellRequest(contractId);

        }


        /**
         * createRequestForUpsellUnivision
         *
         * @param exchange
         *            exchange
         * @param soMessage
         *            soMessage
         * @param fulfillmentProduct
         *            fulfillmentProduct
         * @return jsonbody
         * @throws JsonProcessingException
         *             Exception
         */
        private String createRequestForUpsellUnivision(Exchange exchange, SOMessage soMessage,
                                                    FulfillmentProduct fulfillmentProduct) throws JsonProcessingException {
            //String contractId = SOMessageUtils.getSubscriptionId(soMessage);
            String contractId = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "SUBID");
            String productid = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "PRODUCTID");
            exchange.getIn().setHeader("urlSuffix", upsellUnivision);
            return prepareUnivisionUpsellRequestBody(contractId, productid);

        }



        /**
         * set json key and values
         *
         * @param contractId
         *            contractId
         * @param beforeProductId
         *            beforeProductId
         * @param productId
         *            productId
         * @return Jsonbody
         * @throws JsonProcessingException
         *             Exception
         */

        private String prepareMobileUpsellRequestBody(String contractId , String beforeProductId , String productId
                                                      ) throws JsonProcessingException {
            ObjectNode node = jsonObjectMapper.createObjectNode();
            node.put("contractId", contractId);
            node.put("beforeProductId", beforeProductId);
            node.put("productId", productId);
            node.put("operatorId", operationId);
            return jsonObjectMapper.writeValueAsString(node);
        }

        /**
         * set json key and values
         *
         * @param contractId
         *            contractId
         * @return Jsonbody
         * @throws JsonProcessingException
         *             Exception
         */
        private String prepareMobileCancelUpsellRequest(String contractId) throws JsonProcessingException {
            ObjectNode node = jsonObjectMapper.createObjectNode();
            node.put("contractId", contractId);
            node.put("operatorId", operationId);
            return jsonObjectMapper.writeValueAsString(node);
        }


        /**
         * set json key and values
         *
         * @param contractId
         *            contractId
         * @param productId
         *            productId
         * @return Jsonbody
         * @throws JsonProcessingException
         *             Exception
         */

        private String prepareUnivisionUpsellRequestBody(String contractId  , String productId
        ) throws JsonProcessingException {
            ObjectNode node = jsonObjectMapper.createObjectNode();
            node.put("contractId", contractId);
            node.put("productId", productId);
            node.put("operatorId", operationId);
            return jsonObjectMapper.writeValueAsString(node);
        }

    }
