package com.knowesis.sift.orchestrator.processor;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
import com.knowesis.sift.orchestrator.domain.SOMessage;
import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
import com.knowesis.sift.orchestrator.utils.SOMessageUtils;

/**
 * Prepares API request
 * @author SO Development Team
 */

@Component("FamilyPlanRequestProcessor")
public class FamilyPlanRequestProcessor implements Processor{
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Value("${externalsmsc.operationId.value}")
    String operationId;

    String addbalance = "addbalance";
    String deductbalance = "deductbalance";

    @Override
    public void process(Exchange exchange) throws Exception {
        SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
        String requestBody = "";
        if (soMessage != null) {
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            exchange.getIn().setHeader("msisdn", msisdn);
            String targetSystemValue = (String) exchange.getIn().getHeader("targetSystemValue");
            String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
            FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                    currentSequence);
            ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
            requestBody = routeAccordingToTargetSystemValue(exchange, soMessage, fulfillmentProduct, targetSystemValue);
            exchange.getIn().setBody(requestBody);
        }
        exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
    }



    /**
     * route and set json body according to the recived fulfillment message
     *
     * @param exchange
     *            exchange
     * @param soMessage
     *            soMessage
     * @param fulfillmentProduct
     *            fulfillmentProduct
     * @param targetSystemValue
     *            targetSystemValue
     * @return requestbody prepared as json string
     * @throws JsonProcessingException
     *             Exception
     */
    private String routeAccordingToTargetSystemValue(Exchange exchange, SOMessage soMessage,
                                                     FulfillmentProduct fulfillmentProduct, String targetSystemValue) throws JsonProcessingException {
        String requestBody = "";
        if ("AddFamilyPlanBalance".equalsIgnoreCase(targetSystemValue)) {
            requestBody = createRequestForAddBalance(exchange, soMessage, fulfillmentProduct);
        } else if ("DeductFamilyPlanBalance".equalsIgnoreCase(targetSystemValue)) {
            requestBody = createRequestForDeductBalance(exchange, soMessage, fulfillmentProduct);
        }
        else {
            log.info("Traget system API value is not authorised");
        }
        return requestBody;
    }





    /**
     * createRequestForAddBalance
     *
     * @param exchange
     *            exchange
     * @param soMessage
     *            soMessage
     * @param fulfillmentProduct
     *            fulfillmentProduct
     * @return jsonbody
     * @throws JsonProcessingException
     *             Exception
     */
    private String createRequestForAddBalance(Exchange exchange, SOMessage soMessage,
                                                FulfillmentProduct fulfillmentProduct) throws JsonProcessingException {
        //String contractId = SOMessageUtils.getSubscriptionId(soMessage);
        String msisdn = SOMessageUtils.getMSISDN(soMessage);
        String amount = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "AMOUNT");
        String descr = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "DESCRIPTION");
        exchange.getIn().setHeader("urlSuffix", addbalance);
        return prepareAddBalanceRequestBody(msisdn, amount,"SIFT-"+descr);

    }




    /**
     * createRequestForDeductBalance
     *
     * @param exchange
     *            exchange
     * @param soMessage
     *            soMessage
     * @param fulfillmentProduct
     *            fulfillmentProduct
     * @return jsonbody
     * @throws JsonProcessingException
     *             Exception
     */
    private String createRequestForDeductBalance(Exchange exchange, SOMessage soMessage,
                                                      FulfillmentProduct fulfillmentProduct) throws JsonProcessingException {
        //String contractId = SOMessageUtils.getSubscriptionId(soMessage);
        String msisdn = SOMessageUtils.getMSISDN(soMessage);
        String amount = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "AMOUNT");
        String descr = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "DESCRIPTION");
        exchange.getIn().setHeader("urlSuffix", deductbalance);
        return prepareDeductBalanceRequestBody(msisdn, amount,"SIFT-"+descr);

    }






    /**
     * set json key and values
     *
     * @param msisdn
     *            msisdn
     * @param amount
     *            amount
     * @param descr
     *            descr
     * @return Jsonbody
     * @throws JsonProcessingException
     *             Exception
     */

    private String prepareAddBalanceRequestBody(String msisdn , String amount , String descr
    ) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();
        node.put("msisdn", msisdn);
        node.put("amount", amount);
        node.put("descr", descr);
        return jsonObjectMapper.writeValueAsString(node);
    }

    /**
     * set json key and values
     *
     * @param msisdn
     *            msisdn
     * @param amount
     *            amount
     * @param descr
     *            descr
     * @return Jsonbody
     * @throws JsonProcessingException
     *             Exception
     */

    private String prepareDeductBalanceRequestBody(String msisdn , String amount , String descr
    ) throws JsonProcessingException {
        ObjectNode node = jsonObjectMapper.createObjectNode();
        node.put("msisdn", msisdn);
        node.put("amount", amount);
        node.put("descr", descr);
        return jsonObjectMapper.writeValueAsString(node);
    }




}
