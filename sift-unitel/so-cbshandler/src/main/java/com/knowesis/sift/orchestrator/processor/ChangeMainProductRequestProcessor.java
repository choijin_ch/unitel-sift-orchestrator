    package com.knowesis.sift.orchestrator.processor;

    import com.fasterxml.jackson.core.JsonProcessingException;
    import com.fasterxml.jackson.databind.ObjectMapper;
    import com.fasterxml.jackson.databind.node.ObjectNode;
    import com.knowesis.sift.orchestrator.core.domain.FulfillmentProduct;
    import com.knowesis.sift.orchestrator.domain.SOFulfillmentMessage;
    import com.knowesis.sift.orchestrator.domain.SOMessage;
    import com.knowesis.sift.orchestrator.utils.ProvisionUtils;
    import com.knowesis.sift.orchestrator.utils.SOMessageUtils;
    import org.apache.camel.Exchange;
    import org.apache.camel.Processor;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.beans.factory.annotation.Value;
    import org.springframework.stereotype.Component;

    /**
     * Prepares API request
     * @author SO Development Team
     */

    @Component("ChangeMainProductRequestProcessor")
    public class ChangeMainProductRequestProcessor implements Processor{
        private final Logger log = LoggerFactory.getLogger(getClass());

        @Autowired
        ObjectMapper jsonObjectMapper;

        @Value("${externalsmsc.operationId.value}")
        String operationId;


        String changeMainProduct = "changemainproduct";

        @Override
        public void process(Exchange exchange) throws Exception {
            SOMessage soMessage = exchange.getIn().getBody(SOMessage.class);
            String requestBody = "";
            if (soMessage != null) {
                String msisdn = SOMessageUtils.getMSISDN(soMessage);
                exchange.getIn().setHeader("msisdn", msisdn);
                String targetSystemValue = (String) exchange.getIn().getHeader("targetSystemValue");
                String currentSequence = SOMessageUtils.getHeaderValue(soMessage, "CURRENT-SEQUENCE");
                FulfillmentProduct fulfillmentProduct = ProvisionUtils.getCurrentProduct((SOFulfillmentMessage) soMessage,
                        currentSequence);
                ProvisionUtils.setSequenceStatusHeaderKey(soMessage, currentSequence, "PROCESSING");
                requestBody = routeAccordingToTargetSystemValue(exchange, soMessage, fulfillmentProduct, targetSystemValue);
                exchange.getIn().setBody(requestBody);
            }
            exchange.getIn().setHeader("OriginalRequestMessage", jsonObjectMapper.writeValueAsString(soMessage));
        }



        /**
         * route and set json body according to the recived fulfillment message
         *
         * @param exchange
         *            exchange
         * @param soMessage
         *            soMessage
         * @param fulfillmentProduct
         *            fulfillmentProduct
         * @param targetSystemValue
         *            targetSystemValue
         * @return requestbody prepared as json string
         * @throws JsonProcessingException
         *             Exception
         */
        private String routeAccordingToTargetSystemValue(Exchange exchange, SOMessage soMessage,
                                                         FulfillmentProduct fulfillmentProduct, String targetSystemValue) throws JsonProcessingException {
            String requestBody = "";
            if ("ChangeMainProduct".equalsIgnoreCase(targetSystemValue)) {
                requestBody = createRequestForChangeMainProduct(exchange, soMessage, fulfillmentProduct);
            }else {
                log.info("Traget system API value is not authorised");
            }
            return requestBody;
        }





        /**
         * createRequestForChangeMainProduct
         *
         * @param exchange
         *            exchange
         * @param soMessage
         *            soMessage
         * @param fulfillmentProduct
         *            fulfillmentProduct
         * @return jsonbody
         * @throws JsonProcessingException
         *             Exception
         */
        private String createRequestForChangeMainProduct(Exchange exchange, SOMessage soMessage,
                                                         FulfillmentProduct fulfillmentProduct) throws JsonProcessingException {
            String msisdn = SOMessageUtils.getMSISDN(soMessage);
            String productid = ProvisionUtils.getValueFromDynamicParameter(fulfillmentProduct, "PRODUCT_ID");
            exchange.getIn().setHeader("urlSuffix", changeMainProduct);
            return prepareChangeMainProductRequestBody(msisdn, productid);

        }







        /**
         * set json key and values
         *
         * @param msisdn
         *            msisdn
         * @param productId
         *            productId
         * @return Jsonbody
         * @throws JsonProcessingException
         *             Exception
         */

        private String prepareChangeMainProductRequestBody(String msisdn , String productId
                                                      ) throws JsonProcessingException {
            ObjectNode node = jsonObjectMapper.createObjectNode();
            node.put("msisdn", msisdn);
            node.put("productId", productId);
            node.put("operatorId", operationId);
            return jsonObjectMapper.writeValueAsString(node);
        }





    }
